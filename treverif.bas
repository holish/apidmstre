﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=6
@EndOfDesignText@
'Handler class
Sub Class_Globals
	Private clsid As String
End Sub

Public Sub Initialize
	clsid = "tre_verifikasi"
End Sub

Sub Handle(req As ServletRequest, resp As ServletResponse)
	Dim appid, hasil, method As String
	Dim response As String
	
	If req.Method <> "POST" Then
		resp.SendError(500, "method not supported.")
		Return
	End If
	
	'------------ CEK CLASS AKSES --------'
	appid = req.GetParameter("clsakses")
	
	method = req.GetParameter("method")
	'------------ JIKA AKSES TIDAK SAMA -----------'
	If appid <> clsid Then
		response = modGenerateReturn.return_json(modGenerateReturn.label_rejected, modGenerateReturn.kode_rejected_class, method)
		resp.Write(response)
		Return
	End If

	Select method
		Case "verif_staffuip"
			resp.Write(send_staffuip(req.GetParameter("idsurat_mohonbayar"), req.GetParameter("status"), req.GetParameter("tipebayar"), req.GetParameter("no_surat"), req.GetParameter("tgl_surat"), req.GetParameter("ket"), req.GetParameter("username"), req.GetParameter("path"), req.GetParameter("filename"), req.GetParameter("total"), req.GetParameter("kode_doc"), method))
		Case "verif_uip"
			resp.Write(send_uip(req.GetParameter("idsurat_mohonbayar"), req.GetParameter("status"), req.GetParameter("tipebayar"), req.GetParameter("no_surat"), req.GetParameter("tgl_surat"),req.GetParameter("username"),req.GetParameter("ket"),method))
		Case Else
			hasil =modGenerateReturn.return_json("TIDAK ADA METHOD", "404", method)
			resp.Write(hasil)
	End Select

End Sub

'no_surat gunakan delimeter #
'tgl surat gunakan delimeter #
'pathhfile gunakan delimeter #
'name file gunakan delimter #
'totalfile merupakan jumlah dari total file yang di upload
Private Sub send_staffuip(p_idsurat As String, p_status As String, p_tipe_bayar As String, p_no_surat As String, p_tglsurat As String, p_ket As String, p_username As String, p_path_file As String, p_filename As String, p_totalfile As String, p_kode_doc As String, method As String) As String
	Dim query, data As String
	p_totalfile = Regex.Split(",", p_tglsurat).Length
	query = $" select * from table(PKG_DMS_VERIFIKASI.fn_disposisi_uip_staff('${p_idsurat}','${p_status}', '${p_tipe_bayar}'
	'${p_no_surat}','${p_tglsurat}','${p_ket}','${p_username}', '${p_path_file}', '${p_filename}', ${p_totalfile}, 
	'${p_kode_doc}')) "$
	data  = exequery.multi_string(query, method)
	Return data
End Sub


'Function ini digunakan untuk semua jabatan di atas STAFF UIP
Private Sub send_uip(p_idsurat As String, p_status As String, p_tipe_bayar As String, p_no_surat As String, p_tglsurat As String, p_username As String,  p_ket As String, method As String) As String
	Dim query, data As String
	
	query = $" select * from table(PKG_DMS_VERIFIKASI.fn_disposisi_uip('${p_idsurat}','${p_status}',
	'${p_no_surat}','${p_tglsurat}','${p_username}','${p_ket}')) "$
	data  = exequery.multi_string(query, method)
	Return data
End Sub







