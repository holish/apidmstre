﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=5.82
@EndOfDesignText@
'Handler class
Sub Class_Globals
	Private clsid As String
	
End Sub

Public Sub Initialize
	clsid = "tre_management"
End Sub

Sub Handle(req As ServletRequest, resp As ServletResponse)
	Dim appid, hasil, method As String
	Dim response As String
	
	If req.Method <> "POST" Then
		resp.SendError(500, "method not supported.")
		Return
	End If
	
	'------------ CEK CLASS AKSES --------'
	appid = req.GetParameter("clsakses")
	method = req.GetParameter("method")
	
	'------------ JIKA AKSES TIDAK SAMA -----------'
	If appid <> clsid Then
		response = modGenerateReturn.return_json(modGenerateReturn.label_rejected, modGenerateReturn.kode_rejected_class, method)
		resp.Write(response)
		Return
	End If
	
	Select method
		Case "createrole"
			resp.Write( clsManagement.create_roles(req.GetParameter("nama"), req.GetParameter("ket"), req.GetParameter("menu"), method))
		Case "readrole"
			resp.Write( clsManagement.read_roles(req.GetParameter("cari"), method))
		Case "editrole"
			resp.Write( clsManagement.update_roles(req.GetParameter("id"),req.GetParameter("nama"), req.GetParameter("ket"), req.GetParameter("menu"), req.GetParameter("roleotoritas"), req.GetParameter("totalmenu"), method))
		Case "deactivate"
			resp.Write( clsManagement.deactivate_roles(req.GetParameter("id"),req.GetParameter("status"), method))
		Case "detailrole"
			resp.Write( clsManagement.detail_roles(req.GetParameter("id"), method))
			
		Case "listuser"
			resp.Write(clsManagement.list_user(req.GetParameter("cari"), method))
		Case "detailuser"
			resp.Write(clsManagement.detail_user(req.GetParameter("id"), method))
		Case "saveuser"
			resp.Write(clsManagement.save_user(req.GetParameter("username"),req.GetParameter("rolesid"),req.GetParameter("nip"),req.GetParameter("nama"),req.GetParameter("idunit"),req.GetParameter("idjabatan"),req.GetParameter("ispegawai"), method))
		Case "edituser"
			resp.Write(clsManagement.edit_user(req.GetParameter("iduser"),req.GetParameter("username"),req.GetParameter("rolesid"),req.GetParameter("nip"),req.GetParameter("nama"),req.GetParameter("idunit"),req.GetParameter("idjabatan"),req.GetParameter("ispegawai"), method))

		Case "listmenu"
			resp.Write(clsManagement.read_menu(req.GetParameter("id"), method))
		Case "savemenu"
			resp.Write(clsManagement.save_menu(req.GetParameter("nama"), req.GetParameter("url"),req.GetParameter("ket"),req.GetParameter("icon"),req.GetParameter("class"),req.GetParameter("parent"),req.GetParameter("islabel"),req.GetParameter("urutan"), method))

		Case Else
			hasil =modGenerateReturn.return_json("TIDAK ADA METHOD", "404", method)
			resp.Write(hasil)
	End Select
	
End Sub