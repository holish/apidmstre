﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=6
@EndOfDesignText@
'handler class
Sub Class_Globals
	Private clsid As String
	Private todb As clsDB
	Private todbdms As clsDBDms
End Sub

Public Sub Initialize
	clsid = "tre_notif"
	todb.Initialize
	todbdms.Initialize
	
End Sub


Sub Handle(req As ServletRequest, resp As ServletResponse)
	Dim appid, response, worknumb, hasil, username As String
	Dim strutl As StringUtils
	
	'------------ CEK CLASS AKSES --------'
	appid = req.GetParameter("clsakses")
'	method = req.GetParameter("method")
	
	'------------ JIKA AKSES TIDAK SAMA -----------'
	If appid <> clsid Then
		response = modGenerateReturn.return_json(modGenerateReturn.label_rejected, modGenerateReturn.kode_rejected_class, "Notif")
		resp.Write(response)
		Return
	End If
	
	username = req.GetParameter("username")
	worknumb = strutl.DecodeUrl(req.GetParameter("worknumb"), "UTF8")
	
	hasil = load_notif(username, worknumb)
	
	resp.Write(hasil)
	
End Sub


Private Sub load_notif(username As String, worknumb As String) As String
	Dim query, data As String
	Dim worknumbLst As List
	Dim totalData, totalcurr As Int
	Dim bagidec As Float
	Dim bagiint As Int
	worknumbLst.Initialize
	totalData = Regex.Split(",", worknumb).Length
	worknumbLst = Regex.Split(",", worknumb)
	totalcurr = 0
	If totalData > 100 Then
		bagidec = totalData / 50
		bagiint = bagidec
		
		If bagidec > bagiint Then bagiint = bagiint + 1
		For i = 0 To bagiint - 1
			worknumb = ""
			If totalcurr > (totalData - totalcurr) Then
				For x = totalcurr To totalData - 1 
					If x = totalcurr Then 
						worknumb = worknumbLst.Get(x)
					Else
						worknumb = worknumb & "," & worknumbLst.Get(x)
					End If
				Next
				query = $" select * from table(PKG_DMS_WORKFLOW.fn_save_notif_worknumber('${username}', '${worknumb}', ${Regex.Split(",", worknumb).Length})) "$
				data  = exequery.multi_string(query, "notif")
			Else
				For x = totalcurr To (totalcurr + 49) - 1
					If x = totalcurr Then
						worknumb = worknumbLst.Get(x)
					Else
						worknumb = worknumb & "," & worknumbLst.Get(x)
					End If
				Next
				query = $" select * from table(PKG_DMS_WORKFLOW.fn_save_notif_worknumber('${username}', '${worknumb}', ${Regex.Split(",", worknumb).Length})) "$
				data  = exequery.multi_string(query, "notif")
			End If
			totalcurr = totalcurr + 49
		Next
	End If
	
'	query = $" select * from table(PKG_DMS_WORKFLOW.fn_save_notif_worknumber('${username}', '${worknumb}', ${totalData})) "$
'	data  = exequery.multi_string(query, "notif")
	Return data
End Sub