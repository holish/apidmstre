﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=5.82
@EndOfDesignText@
'Handler class
Sub Class_Globals
	Private clsid As String
	Private todb As clsDB
	Private todbdms As clsDBDms
End Sub

Public Sub Initialize
	clsid = "tre_loginicon"
	todb.Initialize
	todbdms.Initialize
End Sub

Sub Handle(req As ServletRequest, resp As ServletResponse)
	Dim appid, response, method, hasil As String
	
	'------------ CEK CLASS AKSES --------'
	appid = req.GetParameter("clsakses")
	method = req.GetParameter("method")
	'------------ JIKA AKSES TIDAK SAMA -----------'
	If appid <> clsid Then
		response = modGenerateReturn.return_json(modGenerateReturn.label_rejected, modGenerateReturn.kode_rejected_class, method)
		resp.Write(response)
		Return
	End If
	Select method
		Case "checklogin"
			resp.Write(checklogin(req.GetParameter("t"), method))
		Case "getpwd"
			resp.Write(getpwd(req.GetParameter("username"), method))
		Case "loginvendor"
			resp.Write(loginvendor(req.GetParameter("u"), req.GetParameter("p"), method))
		Case Else
			hasil =modGenerateReturn.return_json("TIDAK ADA METHOD", "404", method)
			resp.Write(hasil)
	End Select
	
	
End Sub

Private Sub getpwd(username As String, method As String) As String
	Dim query,  data As String
	Dim mp As Map
	mp.Initialize
	
	query = $" select * from table(pkg_dms_tre.fn_getpwd('${username}'))"$
	data  = exequery.multi_string(query, method)
	
	Return data
End Sub

Private Sub checklogin(token As String, method As String) As String
	Dim query, ret, data As String
	Dim js As JSONParser
	Dim mp As Map
	mp.Initialize
	
	query = $" select * from table(check_token('${token}'))"$
	data  = exe_queryportal(query, method)
	js.Initialize(data)
	mp = js.NextObject
	
	If mp.Get("response") = "301" Then
		ret = getModul(mp.Get("data"), "-")
	Else
		ret = data
	End If
	
	Return ret
End Sub

Private Sub loginvendor(u As String, p As String, method As String) As String
	Dim query,  data, ret As String
	Dim js As JSONParser
	Dim mp As Map
	mp.Initialize
	
	query = $" select * from table(pkg_dms_tre.fn_login_vendor('${u}','${p}'))"$
	data  = exequery.multi_string(query, method)
	js.Initialize(data)
	mp = js.NextObject
	
	If mp.Get("response") = "301" Then
		ret = getModul(mp.Get("data"), "1")
	Else
		ret = data
	End If
	
	Return ret
End Sub

Private Sub getModul(data As List, isvendor As String) As String
	Dim ret, query, ispegawai, namapegawai, nippegawai, email, userid, pwd  As String
	Dim js As JSONParser
	Dim mp As Map
	mp = data.Get(0)
	If mp.Get("kode") = "RC01" Then
		If isvendor <> "1" Then
			nippegawai = mp.Get("f4")
			namapegawai = mp.Get("f2")
			modGenerateReturn.namaUser = namapegawai
			email = mp.Get("f3")
			userid = mp.Get("f1")
			ispegawai = mp.Get("f5")
			pwd = mp.Get("f6")
		Else
			userid = mp.Get("f1")
		End If
		query = $"select * from table(pkg_dms_tre.fn_getmenuakses('${userid}','${userid}','${namapegawai}','${nippegawai}','${ispegawai}', '${pwd}', '${isvendor}'))"$
		ret = exe_query(query, "GETMODUL")
	Else
		ret = modGenerateReturn.return_json(mp.Get("pesan"), "501", "GETMODUL")
	End If
	Return ret
End Sub

Private Sub exe_query(query As String, method As String) As String
	Dim hasil As String
	Dim mp As Map
	Dim lst As List
	Dim js As JSONGenerator
	
	lst.Initialize
	mp.Initialize
	
	mp = todb.SqlSelectMap(query, method)
	js.Initialize(mp)
	hasil = js.ToString
	
	Return hasil
End Sub

Private Sub exe_queryportal(query As String, method As String) As String
	Dim hasil As String
	Dim mp As Map
	Dim lst As List
	Dim js As JSONGenerator
	
	lst.Initialize
	mp.Initialize
	
	mp = todbdms.SqlSelectMap(query, method)
	js.Initialize(mp)
	hasil = js.ToString
	Return hasil
End Sub