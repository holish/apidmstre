﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=5.82
@EndOfDesignText@
'Handler class
Sub Class_Globals
	Private clsid As String
End Sub

Public Sub Initialize
	clsid = "tre_menumaster"
End Sub

Sub Handle(req As ServletRequest, resp As ServletResponse)
	Dim appid, hasil, method As String
	Dim response As String
	
	If req.Method <> "POST" Then
		resp.SendError(500, "method not supported.")
		Return
	End If
	
	'------------ CEK CLASS AKSES --------'
	appid = req.GetParameter("clsakses")
	
	method = req.GetParameter("method")
	'------------ JIKA AKSES TIDAK SAMA -----------'
	If appid <> clsid Then
		response = modGenerateReturn.return_json(modGenerateReturn.label_rejected, modGenerateReturn.kode_rejected_class, method)
		resp.Write(response)
		Return
	End If

	Select method
		Case "createbank"
			hasil = clsMaster.create_bank(req.GetParameter("namabank"), method)
		Case "readbank"
			hasil = clsMaster.read_bank(req.GetParameter("cari"), method)
		Case "updatebank"
			hasil = clsMaster.update_bank(req.GetParameter("id"), req.GetParameter("nama"), method)
		Case "deletebank"
			hasil = clsMaster.delete_bank(req.GetParameter("id"), method)
			
		Case "createvendor"
			hasil = clsMaster.create_vendor(req.GetParameter("namavendor"),req.GetParameter("namajabatan"),req.GetParameter("sebutanpejabatvendor"),req.GetParameter("namavendor1"),req.GetParameter("namavendor2"),req.GetParameter("namavendor3"),req.GetParameter("namavendor4"), method)
		Case "readvendor"
			hasil = clsMaster.read_vendor(req.GetParameter("cari"),method)
		Case "editvendor"
			hasil = clsMaster.update_vendor(req.GetParameter("kodevendor"), req.GetParameter("namavendor"),req.GetParameter("namajabatan"),req.GetParameter("sebutanpejabatvendor"),req.GetParameter("namavendor1"),req.GetParameter("namavendor2"),req.GetParameter("namavendor3"),req.GetParameter("namavendor4"), method)
		Case "deletevendor"
			hasil = clsMaster.delete_vendor(req.GetParameter("kodevendor"),method)
			
		Case "createtipebayar"
			hasil = clsMaster.create_tipebayar(req.GetParameter("namatipebayar"),  method)
		Case "readtipebayar"
			hasil = clsMaster.read_tipebayar(req.GetParameter("cari"), method)
		Case "updatetipebayar"
			hasil = clsMaster.update_tipebayar(req.GetParameter("id"), req.GetParameter("nama"),  method)
		Case "deletetipebayar"
			hasil = clsMaster.delete_tipebayar(req.GetParameter("id"), method)
			
		Case "createtermin"
			hasil = clsMaster.create_termin(req.GetParameter("namatermin"), req.GetParameter("kodetermin"), req.GetParameter("idbayar"), method)
		Case "readtermin"
			hasil = clsMaster.read_termin(req.GetParameter("cari"), method)
		Case "updatetermin"
			hasil = clsMaster.update_termin(req.GetParameter("id"), req.GetParameter("nama"),req.GetParameter("kode"), method)
		Case "deletetermin"
			hasil = clsMaster.delete_termin(req.GetParameter("id"), method)
			
		Case "createsumberdana"
			hasil = clsMaster.create_sumberdana(req.GetParameter("namatipebayar"),  method)
		Case "readsumberdana"
			hasil = clsMaster.read_sumberdana(req.GetParameter("cari"), method)
		Case "updatesumberdana"
			hasil = clsMaster.update_sumberdana(req.GetParameter("id"), req.GetParameter("nama"),  method)
		Case "deletesumberdana"
			hasil = clsMaster.delete_sumberdana(req.GetParameter("id"), method)
			
		Case "createjenisproyek"
			hasil = clsMaster.create_jenisproyek(req.GetParameter("nama"), req.GetParameter("kode"),  method)
		Case "readjenisproyek"
			hasil = clsMaster.read_jenisproyek(req.GetParameter("cari"), method)
		Case "updatejenisproyek"
			hasil = clsMaster.update_jenisproyek(req.GetParameter("id"), req.GetParameter("nama"),req.GetParameter("kode"),  method)
		Case "deletejenisproyek"
			hasil = clsMaster.delete_jenisproyek(req.GetParameter("id"), method)
			
		Case "createproyek"
			hasil = clsMaster.create_proyek(req.GetParameter("kode"), req.GetParameter("nama"), req.GetParameter("idunit"), req.GetParameter("idjenisproyek"),  method)
		Case "readproyek"
			hasil = clsMaster.read_proyek(req.GetParameter("cari"), method)
		Case "updateproyek"
			hasil = clsMaster.update_proyek(req.GetParameter("id"),req.GetParameter("kode"),req.GetParameter("nama"), req.GetParameter("idunit"), req.GetParameter("idjenisproyek"), method)
		Case "deleteproyek"
			hasil = clsMaster.delete_proyek(req.GetParameter("id"), method)
		
		Case "createunit"
			hasil = clsMaster.create_unit(req.GetParameter("nama"), req.GetParameter("singkatan"), req.GetParameter("as"), req.GetParameter("alamat"), req.GetParameter("ket"),  method)
		Case "readunit"
			hasil = clsMaster.read_unit(req.GetParameter("cari"), method)
		Case "updateunit"
			hasil = clsMaster.update_unit(req.GetParameter("id"), req.GetParameter("nama"), req.GetParameter("singkatan"), req.GetParameter("as"), req.GetParameter("alamat"), req.GetParameter("ket"),  method)
		Case "deleteunit"
			hasil = clsMaster.delete_unit(req.GetParameter("id"), method)
		
		Case "readjabatan"
			hasil = clsMaster.read_jabatan(req.GetParameter("cari"), method)
		
		Case Else
			hasil =modGenerateReturn.return_json("TIDAK ADA METHOD", "404", method)
	End Select
	
	resp.Write(hasil)
End Sub