﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=6
@EndOfDesignText@
'Handler class
Sub Class_Globals
	Private clsid As String
End Sub

Public Sub Initialize
	clsid = "tre_atasan"
End Sub

Sub Handle(req As ServletRequest, resp As ServletResponse)
	Dim appid, hasil, method As String
	Dim response As String
	
	If req.Method <> "POST" Then
		resp.SendError(500, "method not supported.")
		Return
	End If
	
	'------------ CEK CLASS AKSES --------'
	appid = req.GetParameter("clsakses")
	method = req.GetParameter("method")
	
	'------------ JIKA AKSES TIDAK SAMA -----------'
	If appid <> clsid Then
		response = modGenerateReturn.return_json(modGenerateReturn.label_rejected, modGenerateReturn.kode_rejected_class, method)
		resp.Write(response)
		Return
	End If

	Select method
		Case "get"
			resp.Write(getuip(req.GetParameter("username"), method))
		Case Else
			hasil =modGenerateReturn.return_json("TIDAK ADA METHOD", "404", method)
			resp.Write(hasil)
	End Select

End Sub


Private Sub getuip(username As String, method As String) As String
	Dim query, data As String
	Dim mp As Map
	mp.Initialize
	
	query = $" select a.id_jabatan from m_pegawai a
	inner join m_jabatan b on a.id_jabatan = b.id_jabatan 
	where upper(username_pegawai) = upper('${username}') "$
	mp = exequery.single_map(query, method)
	
	query = $" select * from table(PKG_DMS_USER_FLOW.fn_loaduseratas('${mp.Get("id_jabatan")}')) "$
	data  = exequery.multi_string(query, method)
	Return data
End Sub