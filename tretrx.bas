﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=6
@EndOfDesignText@
'handler class
Sub Class_Globals
	Private clsid As String
	Private todb As clsDB
	Private todbdms As clsDBDms
End Sub

Public Sub Initialize
	clsid = "tre_trx"
	todb.Initialize
	todbdms.Initialize
	
End Sub

Sub Handle(req As ServletRequest, resp As ServletResponse)
	Dim appid, response, method, hasil As String
	
	'------------ CEK CLASS AKSES --------'
	appid = req.GetParameter("clsakses")
	method = req.GetParameter("method")
	Log(req.GetParameter("key"))
	'------------ JIKA AKSES TIDAK SAMA -----------'
	If appid <> clsid Then
		response = modGenerateReturn.return_json(modGenerateReturn.label_rejected, modGenerateReturn.kode_rejected_class, method)
		resp.Write(response)
		Return
	End If
	Select method
		Case "mohonbayar"
			Dim nilaiinvoice As Int = req.GetParameter("nilaiinvoice")	
			Dim nilaivat As Int = req.GetParameter("nilaivat")
			Dim nilaiwithold As Int = req.GetParameter("nilaiwhitehold")
			resp.Write(mohonbayar(req.GetParameter("bank"), req.GetParameter("termin"), req.GetParameter("noinvoice"), req.GetParameter("vendor"), req.GetParameter("proyek"), req.GetParameter("bankgaransi"), req.GetParameter("perihalsurat"), req.GetParameter("nosuratkontrak"), req.GetParameter("nojanjikontrak"), req.GetParameter("nominalkontrak"), req.GetParameter("tglsurat"), req.GetParameter("norekening"), req.GetParameter("currency"), req.GetParameter("nokwitansi"), req.GetParameter("tglkwitansi"), req.GetParameter("nosertifikat"), req.GetParameter("tglexp"), req.GetParameter("nofaktur"), req.GetParameter("tglfaktur"), req.GetParameter("nobankgaransi"), req.GetParameter("tglbankgaransi"), req.GetParameter("swiftkode"),req.GetParameter("cabang"), req.GetParameter("pemilik"), req.GetParameter("terminke"), req.GetParameter("worknumber"), req.GetParameter("status"), req.GetParameter("tglinvoice"), nilaiinvoice, nilaivat, nilaiwithold, req.GetParameter("username") , method))
		Case "listbayar"
			resp.Write(list_pembayaran(req.GetParameter("nokontrak"), req.GetParameter("idvendor"), req.GetParameter("username"), method))
		Case "listverifikasi"
			resp.Write(list_verifikasi(req.GetParameter("username"), method))
		Case "docvendor"
			resp.Write(doc_vendor(req.GetParameter("id"), req.GetParameter("path"), req.GetParameter("name"), req.GetParameter("kode"), req.GetParameter("jml"), method))
		Case "detail_permohonan"
			resp.Write(detail_permohonan(req.GetParameter("id_permohonan"), method))
		Case "list_doc"
			resp.Write(list_doc(req.GetParameter("id_permohonan"), method))
		Case Else
			hasil =modGenerateReturn.return_json("TIDAK ADA METHOD", "404", method)
			resp.Write(hasil)
	End Select
	
	
End Sub

Private Sub mohonbayar(p_bank As String, p_termin As String, p_invoice As String, p_vendor As String, p_proyek As String,  p_bankgaransi As String, p_perihal_surat As String, p_nosurat_kontrak As String,  p_noperjanjian_kontrak As String, p_nominalkontrak As String, p_tglsurat As String, p_norekening As String,  p_currency As String, p_nokwitansi As String,  p_tglkwitansi As String, p_nosertifikat As String, p_tglexp As String, p_nofaktur As String, p_tglfaktur As String, p_nobank_garansi As String, p_tglbank_garansi As String, p_swiftkode_rekening As String, p_cabang As String, p_pemilik As String, p_termink_ke As String, p_worknumb As String, p_status As String, tglinvoice As String, nilaiinvoice As Int, nilaivat As Int, nilaiwhitehold As Int, username As String, method As String) As String
	Dim query, data As String
	Dim mp As Map
	mp.Initialize
	
	'Get status berdasarkan username
	
	
	query = $" select * from table(PKG_DMS_TRANSAKSI.fn_savemohonbayar('${p_bank}','${p_termin}', '${p_invoice}', '${p_vendor}',
	'${p_proyek}','${p_bankgaransi}','${p_perihal_surat}','${p_nosurat_kontrak}','${p_noperjanjian_kontrak}','${p_nominalkontrak}','${p_tglsurat}',
	'${p_norekening}','${p_currency}','${p_nokwitansi}','${p_tglkwitansi}','${p_nosertifikat}','${p_tglexp}',
	'${p_nofaktur}','${p_tglfaktur}',
	'${p_nobank_garansi}','${p_tglbank_garansi}','${p_swiftkode_rekening}','${p_cabang}','${p_pemilik}',
	'${p_termink_ke}','${p_worknumb}','${p_status}', '${tglinvoice}', '${nilaiinvoice}', '${nilaivat}', '${nilaiwhitehold}', '${username}')) "$
	data  = exequery.multi_string(query, method)
	Return data
End Sub

Private Sub list_pembayaran(no_surat As String, kode_vendor As String, username As String, method As String) As String
	Dim query, data As String
	Dim mp As Map
	mp.Initialize
	If username.Length= 0 Then username = "-"
	
	query = $" select * from table(PKG_DMS_TRANSAKSI.fn_listmohonbayar('${no_surat}', '${kode_vendor}', '${username}')) "$
	data  = exequery.multi_string(query, method)
	Return data
End Sub

Private Sub list_doc(id As String, method As String) As String
	Dim query, data As String
	Dim mp As Map
	mp.Initialize
	
	query = $" select * from v_load_path_file_vendor where f5 = '${id}' or f6 = '${id}' "$
	data = exequery.multi_string(query, method)
	
	Return data
End Sub

Private Sub list_verifikasi(p_username As String, method As String) As String
	Dim query, data As String
	Dim mp As Map
	mp.Initialize
	
	query = $" select * from table(PKG_DMS_TRANSAKSI.fn_load_verifikasi('${p_username}')) "$
	data = exequery.multi_string(query, method)
	
	Return data
End Sub

Private Sub load_data_verifikasi_uip
	
End Sub

'DI GUNAKAN UNTUK UPLOAD DOCUMENT
'id mohon bayar retrive setelah melakukan save mohon bayar
'path url, dilempar dari front end hasil upload document ke Filenet
'name doc yang ada pada filenet
Private Sub doc_vendor(idmohon_bayar As String, path_url As String, name_doc As String, kode_doc As String, jml_doc As Int, method As String) As String
	Dim query, data, temp As String
	Dim path(), name(), kode() As String
	Dim en As StringUtils
	Dim mp As Map
	mp.Initialize
	
	File.WriteList(File.DirApp, "path1.txt", path)
	File.WriteList(File.DirApp, "name2.txt", name)
	File.WriteList(File.DirApp, "kode3.txt", kode)
	
	path = Regex.Split(",", path_url)
	name = Regex.Split(",", name_doc)
	kode = Regex.Split(",", kode_doc)
	File.WriteList(File.DirApp, "path.txt", path)
	File.WriteList(File.DirApp, "name.txt", name)
	File.WriteList(File.DirApp, "kode.txt", kode)
	
	For i = 0 To jml_doc -1
		query = $" select * from table(PKG_DMS_TRANSAKSI.fn_document_vendor('${idmohon_bayar}','${path(i)}','${name(i)}','${kode(i)}',${1})) "$
		data  = exequery.multi_string(query, method)
		File.WriteString(File.DirApp, i &".txt", data)
		File.WriteString(File.DirApp, i &"q.txt", data)
	Next
	Return data
End Sub

Private Sub detail_permohonan(id As String, method As String) As String
	Dim query, data As String
	Dim mp As Map
	mp.Initialize
	
	query = $" select * from v_detail_trx_mohon_bayar where f1 = '${id}' or f37 = '${id}' "$
	data = exequery.multi_string(query, method)
	
	Return data
End Sub



