﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=5.82
@EndOfDesignText@
'Class module
Sub Class_Globals
	Private serverdb, pwd, user As String
	Dim std As String="oracle.jdbc.driver.OracleDriver"
	Private en As StringUtils
End Sub

Public Sub Initialize
	serverdb = Main.ipdb & "/" & Main.tns
	user = Main.userdb
	pwd = Main.pwddb
End Sub
 
Sub SqlSelectMap(Sql As String, method As String) As Map
	Dim koneksi As SQL
	Dim rs As ResultSet
	Dim i, j As Int
	Dim mp As Map
	Dim field, error As String
	Dim lRet As List
	mp.Initialize
	lRet.Initialize
	
	'Try Catch Connection
	Try
		If koneksi.IsInitialized = True Then
			koneksi.Close
		End If
		koneksi.Initialize2(std,"jdbc:oracle:thin:@//" & serverdb ,user,pwd)
	Catch
		mp = CreateMap("response": modGenerateReturn.kode_gagal_koneksi_db, "method" :method, "pesan" : en.EncodeUrl(LastException.Message, "UTF8"), "data": Array())
		Main.appendSqlLog(" ")
		Main.appendSqlLog(Sql)
		Main.appendSqlLog(LastException.Message)
		koneksi.Close
		Return mp
	End Try
	
	'Try Catch Query
	Try
		rs = koneksi.ExecQuery(Sql.Replace("'null'",Null ))
	Catch
		mp = CreateMap("response": modGenerateReturn.kode_gagal_execute_query, "method" :method, "pesan" : en.EncodeUrl(LastException.Message, "UTF8"), "data": Array())
		Main.appendSqlLog(" ")
		Main.appendSqlLog(Sql)
		Main.appendSqlLog(LastException.Message)
		koneksi.Close
		Return mp
	End Try
	
	'Try Catch Parse Data
	Try
		i=0
		j=0
		Do While rs.NextRow
			mp.Initialize
			j = j+1
			For i=0 To rs.ColumnCount-1
				Dim val As String
				val = rs.GetString2(i)
				If val <> Null Then
					val = en.EncodeUrl(val, "UTF8")
				Else
					val = "null"
				End If
				
				field = rs.GetColumnName(i).ToLowerCase
				If field = "GETDATE(ENTRYTIME)" Then
					field = "DATE"
				End If
				mp.Put(field, val)
			Next
			If method = "GETMODUL" Then mp.Put("nama",modGenerateReturn.namaUser)
			lRet.Add(mp)
			mp = Null
		Loop
		
		
		
	Catch
		mp = CreateMap("response": modGenerateReturn.kode_gagal_parsing, "method" :method,  "pesan" : en.EncodeUrl(LastException.Message, "UTF8"), "data": Array())
		Main.appendSqlLog(" ")
		Main.appendSqlLog(Sql)
		Main.appendSqlLog(LastException.Message)
		koneksi.Close
		Return mp
	End Try
	koneksi.Close
	mp = CreateMap("response":modGenerateReturn.kode_succes, "method" :method, "pesan":"", "data": lRet)
	Return mp
End Sub
 
Sub SqlSelectSingleMap(Sql As String, method As String) As Map
	Dim koneksi As SQL
	Dim rs As ResultSet
	Dim i As Int
	Dim mp As Map
	Dim field As String
	Dim lRet As List
	mp.Initialize
	lRet.Initialize
	
	'Try Catch Connection
	Try
		If koneksi.IsInitialized = True Then
			koneksi.Close
		End If
		koneksi.Initialize2(std,"jdbc:oracle:thin:@//" & serverdb ,user,pwd)
	Catch
		mp = CreateMap("response": modGenerateReturn.kode_gagal_koneksi_db, "method" :method, "pesan" : en.EncodeUrl(LastException.Message, "UTF8"), "data": Array(), "fn": "SqlSelectMapSinngle")
		Main.appendSqlLog(" ")
		Main.appendSqlLog(Sql)
		Main.appendSqlLog(LastException.Message)
		koneksi.Close
		Return mp
	End Try
	
	'Try Catch Query
	Try
		rs = koneksi.ExecQuery(Sql.Replace("'null'",Null ))
	Catch
		mp = CreateMap("response": modGenerateReturn.kode_gagal_execute_query, "method" :method, "pesan" : en.EncodeUrl(LastException.Message, "UTF8"), "data": Array(), "fn": "SqlSelectMapSinngle")
		Main.appendSqlLog(" ")
		Main.appendSqlLog(Sql)
		Main.appendSqlLog(LastException.Message)
		koneksi.Close
		Return mp
	End Try
	
	'Try Catch Parse Data
	Try
		i=0
		Do While rs.NextRow
			mp.Initialize
			For i=0 To rs.ColumnCount-1
				Dim val As String
				val = rs.GetString2(i)
				If val <> Null Then
					val = en.EncodeUrl(val, "UTF8")
				Else
					val = "null"
				End If
				
				field = rs.GetColumnName(i).ToLowerCase
				If field = "GETDATE(ENTRYTIME)" Then
					field = "DATE"
				End If
				mp.Put(field, val)
			Next
			mp.Put("response": modGenerateReturn.kode_succes)
		Loop
	Catch
		mp = CreateMap("response": modGenerateReturn.kode_gagal_parsing, "method" :method,  "pesan" : en.EncodeUrl(LastException.Message, "UTF8"), "data": Array(), "fn": "SqlSelectMapSinngle")
		Main.appendSqlLog(" ")
		Main.appendSqlLog(Sql)
		Main.appendSqlLog(LastException.Message)
		koneksi.Close
		Return mp
	End Try
	koneksi.Close
	Return mp
End Sub 

 
