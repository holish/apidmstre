﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=5.82
@EndOfDesignText@
'Handler class
Sub Class_Globals
	Private clsid As String
End Sub

Public Sub Initialize
	clsid = "tre_menuakses"
End Sub

Sub Handle(req As ServletRequest, resp As ServletResponse)
	Dim appid, query As String
	Dim response As String
	
	'------------ CEK CLASS AKSES --------'
	appid = req.GetParameter("clsakses")
	
	'------------ JIKA AKSES TIDAK SAMA -----------'
	If appid <> clsid Then
		response = modGenerateReturn.return_json(modGenerateReturn.label_rejected, modGenerateReturn.kode_rejected_class, "")
		resp.Write(response)
		Return
	End If
	
	query = ""
	
End Sub