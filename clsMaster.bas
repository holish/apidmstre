﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=5.82
@EndOfDesignText@

Sub Process_Globals
	Private koneksidb As clsDB
End Sub

Private Sub exe_query(query As String, method As String) As String
	Dim hasil As String
	Dim mp As Map
	Dim lst As List
	Dim js As JSONGenerator
	
	lst.Initialize
	mp.Initialize
	koneksidb.Initialize
	
	mp = koneksidb.SqlSelectMap(query, method)
	js.Initialize(mp)
	hasil = js.ToString
	Return hasil	
End Sub

#Region Master Bank

public Sub create_bank(namabank As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createbank('" & namabank & "'))", method)
End Sub

Public Sub read_bank(kriteria As String, method As String) As String
	If kriteria = "" Then kriteria = "-"
	Return exe_query("select f1 id_bank, f2 nama_bank from table(PKG_DMS_MASTER_TRE.fn_readbank('" & kriteria & "'))", method)
End Sub

Public Sub update_bank(id As String, nama As String, method As String) As String
	Return exe_query($" select * from table(PKG_DMS_MASTER_TRE.fn_editbank('${id}','${nama}')) "$, method)
End Sub

Public Sub delete_bank(id As String, method As String) As String
	Return exe_query($" select * from table(PKG_DMS_MASTER_TRE.fn_deletebank('${id}')) "$, method)
End Sub

#End Region

#Region Master Vendor

public Sub create_vendor(p1 As String,p2 As String,p3 As String,p4 As String,p5 As String,p6 As String, p7 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createvendor('" & p1 & "','" & p2 & "','" & p3 & "','" & p4 & "','" & p5 & "','" & p6 & "','" & p7 & "'))", method)
End Sub

Public Sub read_vendor(kriteria As String, method As String) As String
	If kriteria = "" Then kriteria = "-"
	Return exe_query($"select f1 kode_vendor, f2 nama_vendor, f3 sebutan_pejabat_vendor, f4 nama_vendor1, 
	f5 nama_vendor2, f6 nama_vendor3, f7 nama_vendor4, f8 nama_pejabat_vendor, f9 username, f10 password 
	from table(PKG_DMS_MASTER_TRE.fn_readvendor('${kriteria}')) "$, method)
End Sub

Public Sub update_vendor(kode As String, p1 As String,p2 As String,p3 As String,p4 As String,p5 As String,p6 As String, p7 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editvendor('" & kode & "','" & p1 & "','" & p2 & "','" & p3 & "','" & p4 & "','" & p5 & "','" & p6 & "','" & p7 & "'))", method)
End Sub

Public Sub delete_vendor(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletevendor('" & kode & "'))", method)
End Sub

#End Region

#Region Master Tipe Pembayaran

public Sub create_tipebayar(p1 As String,  method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createtipebayar('" & p1 & "'))", method)
End Sub

Public Sub read_tipebayar(kriteria As String, method As String) As String
	If kriteria = "" Then kriteria = "-"
	Return exe_query("select f1 id_tipe_bayar, f2 nama_tipe_bayar from table(PKG_DMS_MASTER_TRE.fn_readtipebayar('" & kriteria & "'))", method)
End Sub

Public Sub update_tipebayar(kode As String, p1 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_edittipebayar('" & kode & "','" & p1 & "'))", method)
End Sub

Public Sub delete_tipebayar(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletetipebayar('" & kode & "'))", method)
End Sub

#End Region

#Region Master TERMIN

public Sub create_termin(p1 As String, p2 As String, p3 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createtermin('" & p1 & "','" & p2 & "','" & p3 & "'))", method)
End Sub

Public Sub read_termin(kriteria As String, method As String) As String
	If kriteria = "" Then kriteria = "-"
	Return exe_query("select f1 id_termin,f2 nama_termin,f3 kode_termin,f4 id_tipe_bayar from table(PKG_DMS_MASTER_TRE.fn_readtermin('" & kriteria & "'))", method)
End Sub

Public Sub update_termin(kode As String, p1 As String,  p2 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_edittipebayar('" & kode & "','" & p1 & "','" & p2 & "'))", method)
End Sub

Public Sub delete_termin(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletetipebayar('" & kode & "'))", method)
End Sub

#End Region

#Region Master Sumber Dana

public Sub create_sumberdana(p1 As String,  method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createsumberdana('" & p1 & "'))", method)
End Sub

Public Sub read_sumberdana(kriteria As String, method As String) As String
	If kriteria = "" Then kriteria = "-"
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_readsumberdana('" & kriteria & "'))", method)
End Sub

Public Sub update_sumberdana(kode As String, p1 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editsumberdana('" & kode & "','" & p1 & "'))", method)
End Sub

Public Sub delete_sumberdana(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletesumberdana('" & kode & "'))", method)
End Sub

#End Region

#Region MASTER JENIS PROYEK

public Sub create_jenisproyek(p1 As String, p2 As String,  method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createjenisproyek('" & p1 & "','" & p2 & "'))", method)
End Sub

Public Sub read_jenisproyek(kriteria As String, method As String) As String
	If kriteria = "" Then kriteria = "-"
	Return exe_query("select f1 id_jenis_proyek, f2 nama_jenis_proyek, f3 kode_jenis_proyek from table(PKG_DMS_MASTER_TRE.fn_readjenisproyek('" & kriteria & "'))", method)
End Sub

Public Sub update_jenisproyek(kode As String, p1 As String, p2 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editjenisproyek('" & kode & "','" & p1 & "','" & p2 & "'))", method)
End Sub

Public Sub delete_jenisproyek(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletejenisproyek('" & kode & "'))", method)
End Sub

#End Region

#Region Master  Proyek

public Sub create_proyek(p1 As String, p2 As String, p3 As String,p4 As String,  method As String) As String
	Dim query	As String = "select * from table(PKG_DMS_MASTER_TRE.fn_createproyek('" & p1 & "','" & p2 & "', '" & p3 & "', '" & p4 & "'))"
	Return exe_query(query, method)
End Sub

Public Sub read_proyek(kriteria As String, method As String) As String
	If kriteria = "" Then kriteria = "-"
	Return exe_query("select f1 id_jenis_proyek,f2 nama_proyek,f3 kode_proyek,f4 id_unit,f5 id_proyek from table(PKG_DMS_MASTER_TRE.fn_readproyek('" & kriteria & "'))", method)
End Sub

Public Sub update_proyek(id As String, p1 As String, p2 As String,p3 As String,p4 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editproyek('" & id & "','" & p1 & "','" & p2 & "','" & p3 & "','" & p4 & "'))", method)
End Sub

Public Sub delete_proyek(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deleteproyek('" & kode & "'))", method)
End Sub

#End Region

#Region MASTER UNIT

public Sub create_unit(p1 As String, p2 As String, p3 As String, p4 As String, p5 As String,  method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createunit('" & p1 & "','" & p2 & "','" & p3 & "','" & p4 & "','" & p5 & "'))", method)
End Sub

Public Sub read_unit(kriteria As String, method As String) As String
	If kriteria = "" Then kriteria = "-"
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_readunit('" & kriteria & "'))", method)
End Sub

Public Sub update_unit(id As String, p1 As String, p2 As String,p3 As String,p4 As String,p5 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editunit('" & id & "','" & p1 & "','" & p2 & "','" & p3 & "','" & p4 & "','" & p5 & "'))", method)
End Sub

Public Sub delete_unit(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deleteunit('" & kode & "'))", method)
End Sub

#End Region

#Region MASTER JABATAN


public Sub create_jabatan(p1 As String, p2 As String, p3 As String, p4 As String, p5 As String,  method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createjabatan('" & p1 & "','" & p2 & "','" & p3 & "','" & p4 & "','" & p5 & "'))", method)
End Sub

Public Sub read_jabatan(kriteria As String, method As String) As String
	If kriteria = "" Then kriteria = "-"
	Return exe_query("select f1 id_jabatan,f2 nama_jabatan,f3 urut_flow from table(PKG_DMS_MASTER_TRE.fn_readjabatan('" & kriteria & "'))", method)
End Sub

Public Sub update_jabatan(id As String, p1 As String, p2 As String,p3 As String,p4 As String,p5 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editjabatan('" & id & "','" & p1 & "','" & p2 & "','" & p3 & "','" & p4 & "','" & p5 & "'))", method)
End Sub

Public Sub delete_jabatan(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletejabatan('" & kode & "'))", method)
End Sub


#End Region