﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=6
@EndOfDesignText@

Sub Process_Globals
	Private todb As clsDB
	Private todbdms As clsDBDms
End Sub


Public Sub single_string(query As String, method As String) As String
	todb.Initialize
	Dim hasil As String
	Dim mp As Map
	Dim lst As List
	Dim js As JSONGenerator
	
	lst.Initialize
	mp.Initialize
	
	mp = todb.SqlSelectSingleMap(query, method)
	js.Initialize(mp)
	hasil = js.ToString
	
	Return hasil
End Sub

Public Sub single_map(query As String, method As String) As Map
	todb.Initialize
	Dim mp As Map
	Dim lst As List
	
	lst.Initialize
	mp.Initialize
	
	mp = todb.SqlSelectSingleMap(query, method)

	Return mp
End Sub

Public Sub multi_string(query As String, method As String) As String
	todb.Initialize
	Dim hasil As String
	Dim mp As Map
	Dim lst As List
	Dim js As JSONGenerator
	
	lst.Initialize
	mp.Initialize
	
	mp = todb.SqlSelectMap(query, method)
	js.Initialize(mp)
	hasil = js.ToString
	
	Return hasil
End Sub

Public Sub multi_map(query As String, method As String) As Map
	todb.Initialize
	Dim mp As Map
	Dim lst As List
	
	lst.Initialize
	mp.Initialize
	
	mp = todb.SqlSelectMap(query, method)

	Return mp
End Sub