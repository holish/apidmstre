﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=5.82
@EndOfDesignText@

Sub Process_Globals
	Private koneksidb As clsDB
End Sub

Private Sub exe_query(query As String, method As String) As String
	Dim hasil As String
	Dim mp As Map
	Dim lst As List
	Dim js As JSONGenerator
	
	lst.Initialize
	mp.Initialize
	koneksidb.Initialize
	
	mp = koneksidb.SqlSelectMap(query, method)
	js.Initialize(mp)
	hasil = js.ToString
	Return hasil
End Sub

#Region Management Roles

Public Sub create_roles(p1 As String,  p2 As String, p3 As String, method As String) As String
	Dim data, query, p4, p5, templatemenu As String
	Dim jstut As StringUtils
	Dim arr, arrmenu, arrotoritas As List
	arr.Initialize
	arrmenu.Initialize
	arrotoritas.Initialize
	p3 = jstut.DecodeUrl(p3, "UTF8")
	arr = Regex.Split("#", p3)
	templatemenu = "1,2,3,4,5"
	p3 = ""
	p5 = arr.Size
	For i = 0 To arr.Size - 1
		arrmenu = Regex.Split("=", arr.Get(i))
		If p3.Length = 0 Then 
			p3 = p3 & arrmenu.Get(0)
		Else
			p3 = p3 & "#" & arrmenu.Get(0)
		End If
		
		For x = 1 To arrmenu.Size - 1
			templatemenu = "1,2,3,4,5"
			arrotoritas = Regex.Split(",", arrmenu.Get(x))
			If arrotoritas.Size = 1 Then
				Select arrotoritas.Get(0)
					Case "1"
						templatemenu = "1,0,0,0,0"
					Case "2"
						templatemenu = "0,1,0,0,0"
					Case "3"
						templatemenu = "0,0,1,0,0"
					Case "4"
						templatemenu = "0,0,0,1,0"
					Case "5"
						templatemenu = "0,0,0,0,1"
				End Select
			Else
				For y = 0 To arrotoritas.Size - 1
					If arrotoritas.Get(0) <> "1" Then templatemenu = templatemenu.Replace("1", "0")
					templatemenu = templatemenu.Replace(arrotoritas.Get(y), 1)
				Next
			End If
			
			If p4.Length = 0 Then
				p4 = templatemenu
			Else
				p4 = p4 & "#" & templatemenu
			End If
		Next
	Next
	query = "select * from table(PKG_DMS_MANAGEMENT_TRE.fn_createrole('" & p1 & "','" & p2 & "', '" & p3 & "', '" & p4 & "', " & p5 & "))"
	data = exe_query(query, method)
	Return data
End Sub

Public Sub read_roles(p1 As String, method As String) As String
	Dim data, query As String
	query = "select * from roles"
	data = exe_query(query, method)
	Return data
End Sub

Public Sub update_roles(p1 As String,  p2 As String, p3 As String, p4 As String,p5 As String, p6 As Int, method As String) As String
	Dim data, query As String
	query = "select * from table(PKG_DMS_MANAGEMENT_TRE.fn_editrole('" & p1 & "','" & p2 & "', '" & p3 & "', '" & p4 & "', '" & p5 & "', " & p6 & "))"
	data = exe_query(query, method)
	Return data
End Sub

Public Sub deactivate_roles(p1 As String, p2 As String, method As String) As String
	Dim data, query As String
	query = "select * from table(PKG_DMS_MANAGEMENT_TRE.fn_deactivate('" & p1 & "','" & p2 & "'))"
	data = exe_query(query, method)
	Return data
End Sub

Public Sub detail_roles(p1 As String, method As String) As String
	Dim data, query As String
	query = "select * from table(PKG_DMS_MANAGEMENT_TRE.fn_detailrole('" & p1 & "'))"
	data = exe_query(query, method)
	Return data
End Sub

#End Region

#Region "User"

Public Sub list_user(p1 As String, method As String) As String
	Dim data, query As String
	query = $" select id_pegawai f1, nama_pegawai f2, nip_pegawai f3, roles_nama f4,  nama_unit f5, 
	nama_jabatan f6 
	from v_user_pegawai "$
	data = exequery.multi_string(query, method)
	Return data
End Sub

Public Sub detail_user(p1 As String, method As String) As String
	Dim data, query As String
	query = $" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_detailuser('${p1}')) "$
	data = exequery.multi_string(query, method)
	Return data
End Sub

Public Sub save_user(p1 As String, p2 As String, p3 As String, p4 As String, p5 As String, p6 As String, p7 As String, method) As String
	Dim data, query As String
	query = $" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_saveuser('${p1}','${p2}','${p3}','${p4}','${p5}','${p6}','${p7}')) "$
	data = exequery.multi_string(query, method)
	Return data
End Sub

Public Sub edit_user(p1 As String, p2 As String, p3 As String, p4 As String, p5 As String, p6 As String, p7 As String, p8 As String, method) As String
	Dim data, query As String
	query = $" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_edituser('${p1}','${p2}','${p3}','${p4}','${p5}','${p6}','${p7}', '${p8}')) "$
	data = exequery.multi_string(query, method)
	Return data
End Sub

#End Region


#Region menu

Public Sub read_menu(p1 As String, method As String) As String
	
	Dim data, query As String
	query = $" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_menuapps('${p1}')) "$
	data = exequery.multi_string(query, method)
	Return data
	
End Sub

Public Sub save_menu(p1 As String,p2 As String,p3 As String,p4 As String,p5 As String,p6 As String,p7 As String, p8 As String, method As String) As String
	
	Dim data, query As String
	query = $" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_savemenu('${p1}','${p2}','${p3}','${p4}','${p5}','${p6}','${p7}',${p8})) "$
	data = exequery.multi_string(query, method)
	Return data
	
End Sub

#End Region

