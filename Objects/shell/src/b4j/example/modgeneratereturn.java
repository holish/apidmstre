
package b4j.example;

import java.io.IOException;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RDebug;
import anywheresoftware.b4a.pc.RemoteObject;
import anywheresoftware.b4a.pc.RDebug.IRemote;
import anywheresoftware.b4a.pc.Debug;
import anywheresoftware.b4a.pc.B4XTypes.B4XClass;
import anywheresoftware.b4a.pc.B4XTypes.DeviceClass;

public class modgeneratereturn implements IRemote{
	public static modgeneratereturn mostCurrent;
	public static RemoteObject ba;
    public static boolean processGlobalsRun;
    public static RemoteObject myClass;
    public static RemoteObject remoteMe;
	public modgeneratereturn() {
		mostCurrent = this;
	}
    public RemoteObject getRemoteMe() {
        return remoteMe;    
    }
    
public boolean isSingleton() {
		return true;
	}
    static {
		mostCurrent = new modgeneratereturn();
		remoteMe = RemoteObject.declareNull("b4j.example.modgeneratereturn");
        anywheresoftware.b4a.pc.RapidSub.moduleToObject.put(new B4XClass("modgeneratereturn"), "b4j.example.modgeneratereturn");
	}
    public static void main (String[] args) throws Exception {
		new RDebug(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), args[3]);
		RDebug.INSTANCE.waitForTask();

	}
    private static PCBA pcBA = new PCBA(null, modgeneratereturn.class);
	public static RemoteObject runMethod(boolean notUsed, String method, Object... args) throws Exception{
		return (RemoteObject) pcBA.raiseEvent(method.substring(1), args);
	}
    public static void runVoidMethod(String method, Object... args) throws Exception{
		runMethod(false, method, args);
	}
    public static RemoteObject getObject() {
		return myClass;
	 }
	public PCBA create(Object[] args) throws ClassNotFoundException{
		ba = (RemoteObject) args[1];
		pcBA = new PCBA(this, modgeneratereturn.class);
        main_subs_0.initializeProcessGlobals();
		return pcBA;
	}
public static RemoteObject __c = RemoteObject.declareNull("anywheresoftware.b4a.keywords.Common");
public static RemoteObject _label_rejected = RemoteObject.createImmutable("");
public static RemoteObject _kode_rejected_class = RemoteObject.createImmutable("");
public static RemoteObject _kode_succes = RemoteObject.createImmutable("");
public static RemoteObject _kode_gagal_koneksi_db = RemoteObject.createImmutable("");
public static RemoteObject _kode_gagal_execute_query = RemoteObject.createImmutable("");
public static RemoteObject _kode_gagal_parsing = RemoteObject.createImmutable("");
public static RemoteObject _namauser = RemoteObject.createImmutable("");
public static b4j.example.main _main = null;
public static b4j.example.exequery _exequery = null;
public static b4j.example.clsmanagement _clsmanagement = null;
public static b4j.example.clsmaster _clsmaster = null;
  public Object[] GetGlobals() {
		return new Object[] {"clsManagement",Debug.moduleToString(b4j.example.clsmanagement.class),"clsMaster",Debug.moduleToString(b4j.example.clsmaster.class),"exequery",Debug.moduleToString(b4j.example.exequery.class),"kode_gagal_execute_query",modgeneratereturn._kode_gagal_execute_query,"kode_gagal_koneksi_db",modgeneratereturn._kode_gagal_koneksi_db,"kode_gagal_parsing",modgeneratereturn._kode_gagal_parsing,"kode_rejected_class",modgeneratereturn._kode_rejected_class,"kode_succes",modgeneratereturn._kode_succes,"label_rejected",modgeneratereturn._label_rejected,"Main",Debug.moduleToString(b4j.example.main.class),"namaUser",modgeneratereturn._namauser};
}
}