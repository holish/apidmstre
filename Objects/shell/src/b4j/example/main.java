
package b4j.example;

import java.io.IOException;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RDebug;
import anywheresoftware.b4a.pc.RemoteObject;
import anywheresoftware.b4a.pc.RDebug.IRemote;
import anywheresoftware.b4a.pc.Debug;
import anywheresoftware.b4a.pc.B4XTypes.B4XClass;
import anywheresoftware.b4a.pc.B4XTypes.DeviceClass;

public class main implements IRemote{
	public static main mostCurrent;
	public static RemoteObject ba;
    public static boolean processGlobalsRun;
    public static RemoteObject myClass;
    public static RemoteObject remoteMe;
	public main() {
		mostCurrent = this;
	}
    public RemoteObject getRemoteMe() {
        return remoteMe;    
    }
    
public boolean isSingleton() {
		return true;
	}
    static {
		mostCurrent = new main();
		remoteMe = RemoteObject.declareNull("b4j.example.main");
        anywheresoftware.b4a.pc.RapidSub.moduleToObject.put(new B4XClass("main"), "b4j.example.main");
	}
    public static void main (String[] args) throws Exception {
		new RDebug(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), args[3]);
		RDebug.INSTANCE.waitForTask();

	}
    private static PCBA pcBA = new PCBA(null, main.class);
	public static RemoteObject runMethod(boolean notUsed, String method, Object... args) throws Exception{
		return (RemoteObject) pcBA.raiseEvent(method.substring(1), args);
	}
    public static void runVoidMethod(String method, Object... args) throws Exception{
		runMethod(false, method, args);
	}
    public static RemoteObject getObject() {
		return myClass;
	 }
	public PCBA create(Object[] args) throws ClassNotFoundException{
		ba = (RemoteObject) args[1];
		pcBA = new PCBA(this, main.class);
        main_subs_0.initializeProcessGlobals();
		return pcBA;
	}
public static RemoteObject __c = RemoteObject.declareNull("anywheresoftware.b4a.keywords.Common");
public static RemoteObject _srv = RemoteObject.declareNull("anywheresoftware.b4j.object.ServerWrapper");
public static RemoteObject _parammap = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
public static RemoteObject _urlsocketacmt = RemoteObject.createImmutable("");
public static RemoteObject _dirparentpath = RemoteObject.createImmutable("");
public static RemoteObject _ipftpdev = RemoteObject.createImmutable("");
public static RemoteObject _ipftpprod = RemoteObject.createImmutable("");
public static RemoteObject _ftpfolderdev = RemoteObject.createImmutable("");
public static RemoteObject _ftpfolderprod = RemoteObject.createImmutable("");
public static RemoteObject _tns = RemoteObject.createImmutable("");
public static RemoteObject _ipdb = RemoteObject.createImmutable("");
public static RemoteObject _userdb = RemoteObject.createImmutable("");
public static RemoteObject _pwddb = RemoteObject.createImmutable("");
public static RemoteObject _tnsdms = RemoteObject.createImmutable("");
public static RemoteObject _ipdbdms = RemoteObject.createImmutable("");
public static RemoteObject _userdbdms = RemoteObject.createImmutable("");
public static RemoteObject _pwddbdms = RemoteObject.createImmutable("");
public static RemoteObject _param = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
public static RemoteObject _paramdbdms = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
public static RemoteObject _json = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser");
public static RemoteObject _count = RemoteObject.createImmutable(0);
public static RemoteObject _nativeme = RemoteObject.declareNull("anywheresoftware.b4j.object.JavaObject");
public static RemoteObject _pwddms = RemoteObject.createImmutable("");
public static RemoteObject _userdms = RemoteObject.createImmutable("");
public static b4j.example.exequery _exequery = null;
public static b4j.example.clsmanagement _clsmanagement = null;
public static b4j.example.clsmaster _clsmaster = null;
public static b4j.example.modgeneratereturn _modgeneratereturn = null;
  public Object[] GetGlobals() {
		return new Object[] {"clsManagement",Debug.moduleToString(b4j.example.clsmanagement.class),"clsMaster",Debug.moduleToString(b4j.example.clsmaster.class),"count",main._count,"dirparentpath",main._dirparentpath,"exequery",Debug.moduleToString(b4j.example.exequery.class),"ftpfolderdev",main._ftpfolderdev,"ftpfolderprod",main._ftpfolderprod,"ipdb",main._ipdb,"ipdbdms",main._ipdbdms,"IpFtpDev",main._ipftpdev,"IpFtpProd",main._ipftpprod,"json",main._json,"modGenerateReturn",Debug.moduleToString(b4j.example.modgeneratereturn.class),"NativeMe",main._nativeme,"param",main._param,"paramdbDMS",main._paramdbdms,"paramMap",main._parammap,"pwddb",main._pwddb,"pwddbdms",main._pwddbdms,"pwdDMS",main._pwddms,"srv",main._srv,"tns",main._tns,"tnsdms",main._tnsdms,"urlSocketACMT",main._urlsocketacmt,"userdb",main._userdb,"userdbdms",main._userdbdms,"userDMS",main._userdms};
}
}