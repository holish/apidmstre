package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class modgeneratereturn_subs_0 {


public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Public label_rejected As String = \"Rejected Acces";
modgeneratereturn._label_rejected = BA.ObjectToString("Rejected Access");
 //BA.debugLineNum = 4;BA.debugLine="Public kode_rejected_class As String = \"501\"";
modgeneratereturn._kode_rejected_class = BA.ObjectToString("501");
 //BA.debugLineNum = 5;BA.debugLine="Public kode_succes As String = \"301\"";
modgeneratereturn._kode_succes = BA.ObjectToString("301");
 //BA.debugLineNum = 6;BA.debugLine="Public kode_gagal_koneksi_db As String = \"102\"";
modgeneratereturn._kode_gagal_koneksi_db = BA.ObjectToString("102");
 //BA.debugLineNum = 7;BA.debugLine="Public kode_gagal_execute_query As String = \"202\"";
modgeneratereturn._kode_gagal_execute_query = BA.ObjectToString("202");
 //BA.debugLineNum = 8;BA.debugLine="Public kode_gagal_parsing As String = \"402\"";
modgeneratereturn._kode_gagal_parsing = BA.ObjectToString("402");
 //BA.debugLineNum = 9;BA.debugLine="Public namaUser As String";
modgeneratereturn._namauser = RemoteObject.createImmutable("");
 //BA.debugLineNum = 10;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _return_json(RemoteObject _pesan,RemoteObject _kode,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("return_json (modgeneratereturn) ","modgeneratereturn",13,modgeneratereturn.ba,modgeneratereturn.mostCurrent,14);
if (RapidSub.canDelegate("return_json")) return b4j.example.modgeneratereturn.remoteMe.runUserSub(false, "modgeneratereturn","return_json", _pesan, _kode, _method);
RemoteObject _ret = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _strret = RemoteObject.createImmutable("");
RemoteObject _js = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");
Debug.locals.put("pesan", _pesan);
Debug.locals.put("kode", _kode);
Debug.locals.put("method", _method);
 BA.debugLineNum = 14;BA.debugLine="Public Sub return_json(pesan As String, kode As St";
Debug.ShouldStop(8192);
 BA.debugLineNum = 15;BA.debugLine="Dim ret As Map";
Debug.ShouldStop(16384);
_ret = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("ret", _ret);
 BA.debugLineNum = 16;BA.debugLine="Dim strRet As String";
Debug.ShouldStop(32768);
_strret = RemoteObject.createImmutable("");Debug.locals.put("strRet", _strret);
 BA.debugLineNum = 17;BA.debugLine="Dim js As JSONGenerator";
Debug.ShouldStop(65536);
_js = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");Debug.locals.put("js", _js);
 BA.debugLineNum = 19;BA.debugLine="ret = CreateMap(\"response\": kode, \"method\" :metho";
Debug.ShouldStop(262144);
_ret = modgeneratereturn.__c.runMethod(false, "createMap", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("response")),(_kode),RemoteObject.createImmutable(("method")),(_method),RemoteObject.createImmutable(("pesan")),(_pesan),RemoteObject.createImmutable(("data")),(RemoteObject.createNewArray("Object",new int[] {0},new Object[] {}))}));Debug.locals.put("ret", _ret);
 BA.debugLineNum = 20;BA.debugLine="js.Initialize(ret)";
Debug.ShouldStop(524288);
_js.runVoidMethod ("Initialize",(Object)(_ret));
 BA.debugLineNum = 21;BA.debugLine="strRet = js.ToString";
Debug.ShouldStop(1048576);
_strret = _js.runMethod(true,"ToString");Debug.locals.put("strRet", _strret);
 BA.debugLineNum = 22;BA.debugLine="Return strRet";
Debug.ShouldStop(2097152);
if (true) return _strret;
 BA.debugLineNum = 23;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}