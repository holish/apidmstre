package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class tremenu_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private clsid As String";
tremenu._clsid = RemoteObject.createImmutable("");__ref.setField("_clsid",tremenu._clsid);
 //BA.debugLineNum = 4;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _handle(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Handle (tremenu) ","tremenu",14,__ref.getField(false, "ba"),__ref,10);
if (RapidSub.canDelegate("handle")) return __ref.runUserSub(false, "tremenu","handle", __ref, _req, _resp);
RemoteObject _appid = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _response = RemoteObject.createImmutable("");
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 10;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
Debug.ShouldStop(512);
 BA.debugLineNum = 11;BA.debugLine="Dim appid, query As String";
Debug.ShouldStop(1024);
_appid = RemoteObject.createImmutable("");Debug.locals.put("appid", _appid);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
 BA.debugLineNum = 12;BA.debugLine="Dim response As String";
Debug.ShouldStop(2048);
_response = RemoteObject.createImmutable("");Debug.locals.put("response", _response);
 BA.debugLineNum = 15;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
Debug.ShouldStop(16384);
_appid = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("clsakses")));Debug.locals.put("appid", _appid);
 BA.debugLineNum = 18;BA.debugLine="If appid <> clsid Then";
Debug.ShouldStop(131072);
if (RemoteObject.solveBoolean("!",_appid,__ref.getField(true,"_clsid"))) { 
 BA.debugLineNum = 19;BA.debugLine="response = modGenerateReturn.return_json(modGene";
Debug.ShouldStop(262144);
_response = tremenu._modgeneratereturn.runMethod(true,"_return_json",(Object)(tremenu._modgeneratereturn._label_rejected),(Object)(tremenu._modgeneratereturn._kode_rejected_class),(Object)(RemoteObject.createImmutable("")));Debug.locals.put("response", _response);
 BA.debugLineNum = 20;BA.debugLine="resp.Write(response)";
Debug.ShouldStop(524288);
_resp.runVoidMethod ("Write",(Object)(_response));
 BA.debugLineNum = 21;BA.debugLine="Return";
Debug.ShouldStop(1048576);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 24;BA.debugLine="query = \"\"";
Debug.ShouldStop(8388608);
_query = BA.ObjectToString("");Debug.locals.put("query", _query);
 BA.debugLineNum = 26;BA.debugLine="End Sub";
Debug.ShouldStop(33554432);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (tremenu) ","tremenu",14,__ref.getField(false, "ba"),__ref,6);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "tremenu","initialize", __ref, _ba);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
 BA.debugLineNum = 6;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(32);
 BA.debugLineNum = 7;BA.debugLine="clsid = \"tre_menuakses\"";
Debug.ShouldStop(64);
__ref.setField ("_clsid",BA.ObjectToString("tre_menuakses"));
 BA.debugLineNum = 8;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}