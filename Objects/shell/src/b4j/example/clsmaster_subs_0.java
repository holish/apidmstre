package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class clsmaster_subs_0 {


public static RemoteObject  _create_bank(RemoteObject _namabank,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("create_bank (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,24);
if (RapidSub.canDelegate("create_bank")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","create_bank", _namabank, _method);
Debug.locals.put("namabank", _namabank);
Debug.locals.put("method", _method);
 BA.debugLineNum = 24;BA.debugLine="public Sub create_bank(namabank As String, method";
Debug.ShouldStop(8388608);
 BA.debugLineNum = 25;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(16777216);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_createbank('"),_namabank,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 26;BA.debugLine="End Sub";
Debug.ShouldStop(33554432);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _create_jabatan(RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _p5,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("create_jabatan (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,196);
if (RapidSub.canDelegate("create_jabatan")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","create_jabatan", _p1, _p2, _p3, _p4, _p5, _method);
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("p5", _p5);
Debug.locals.put("method", _method);
 BA.debugLineNum = 196;BA.debugLine="public Sub create_jabatan(p1 As String, p2 As Stri";
Debug.ShouldStop(8);
 BA.debugLineNum = 197;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(16);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_createjabatan('"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("','"),_p3,RemoteObject.createImmutable("','"),_p4,RemoteObject.createImmutable("','"),_p5,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 198;BA.debugLine="End Sub";
Debug.ShouldStop(32);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _create_jenisproyek(RemoteObject _p1,RemoteObject _p2,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("create_jenisproyek (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,131);
if (RapidSub.canDelegate("create_jenisproyek")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","create_jenisproyek", _p1, _p2, _method);
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("method", _method);
 BA.debugLineNum = 131;BA.debugLine="public Sub create_jenisproyek(p1 As String, p2 As";
Debug.ShouldStop(4);
 BA.debugLineNum = 132;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(8);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_createjenisproyek('"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 133;BA.debugLine="End Sub";
Debug.ShouldStop(16);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _create_proyek(RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("create_proyek (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,152);
if (RapidSub.canDelegate("create_proyek")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","create_proyek", _p1, _p2, _p3, _p4, _method);
RemoteObject _query = RemoteObject.createImmutable("");
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("method", _method);
 BA.debugLineNum = 152;BA.debugLine="public Sub create_proyek(p1 As String, p2 As Strin";
Debug.ShouldStop(8388608);
 BA.debugLineNum = 153;BA.debugLine="Dim query	As String = \"select * from table(PKG_DM";
Debug.ShouldStop(16777216);
_query = RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_createproyek('"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("', '"),_p3,RemoteObject.createImmutable("', '"),_p4,RemoteObject.createImmutable("'))"));Debug.locals.put("query", _query);Debug.locals.put("query", _query);
 BA.debugLineNum = 154;BA.debugLine="Return exe_query(query, method)";
Debug.ShouldStop(33554432);
if (true) return _exe_query(_query,_method);
 BA.debugLineNum = 155;BA.debugLine="End Sub";
Debug.ShouldStop(67108864);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _create_sumberdana(RemoteObject _p1,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("create_sumberdana (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,110);
if (RapidSub.canDelegate("create_sumberdana")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","create_sumberdana", _p1, _method);
Debug.locals.put("p1", _p1);
Debug.locals.put("method", _method);
 BA.debugLineNum = 110;BA.debugLine="public Sub create_sumberdana(p1 As String,  method";
Debug.ShouldStop(8192);
 BA.debugLineNum = 111;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(16384);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_createsumberdana('"),_p1,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 112;BA.debugLine="End Sub";
Debug.ShouldStop(32768);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _create_termin(RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("create_termin (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,89);
if (RapidSub.canDelegate("create_termin")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","create_termin", _p1, _p2, _p3, _method);
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("method", _method);
 BA.debugLineNum = 89;BA.debugLine="public Sub create_termin(p1 As String, p2 As Strin";
Debug.ShouldStop(16777216);
 BA.debugLineNum = 90;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(33554432);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_createtermin('"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("','"),_p3,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 91;BA.debugLine="End Sub";
Debug.ShouldStop(67108864);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _create_tipebayar(RemoteObject _p1,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("create_tipebayar (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,68);
if (RapidSub.canDelegate("create_tipebayar")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","create_tipebayar", _p1, _method);
Debug.locals.put("p1", _p1);
Debug.locals.put("method", _method);
 BA.debugLineNum = 68;BA.debugLine="public Sub create_tipebayar(p1 As String,  method";
Debug.ShouldStop(8);
 BA.debugLineNum = 69;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(16);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_createtipebayar('"),_p1,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 70;BA.debugLine="End Sub";
Debug.ShouldStop(32);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _create_unit(RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _p5,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("create_unit (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,174);
if (RapidSub.canDelegate("create_unit")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","create_unit", _p1, _p2, _p3, _p4, _p5, _method);
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("p5", _p5);
Debug.locals.put("method", _method);
 BA.debugLineNum = 174;BA.debugLine="public Sub create_unit(p1 As String, p2 As String,";
Debug.ShouldStop(8192);
 BA.debugLineNum = 175;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(16384);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_createunit('"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("','"),_p3,RemoteObject.createImmutable("','"),_p4,RemoteObject.createImmutable("','"),_p5,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 176;BA.debugLine="End Sub";
Debug.ShouldStop(32768);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _create_vendor(RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _p5,RemoteObject _p6,RemoteObject _p7,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("create_vendor (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,45);
if (RapidSub.canDelegate("create_vendor")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","create_vendor", _p1, _p2, _p3, _p4, _p5, _p6, _p7, _method);
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("p5", _p5);
Debug.locals.put("p6", _p6);
Debug.locals.put("p7", _p7);
Debug.locals.put("method", _method);
 BA.debugLineNum = 45;BA.debugLine="public Sub create_vendor(p1 As String,p2 As String";
Debug.ShouldStop(4096);
 BA.debugLineNum = 46;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(8192);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_createvendor('"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("','"),_p3,RemoteObject.createImmutable("','"),_p4,RemoteObject.createImmutable("','"),_p5,RemoteObject.createImmutable("','"),_p6,RemoteObject.createImmutable("','"),_p7,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 47;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _delete_bank(RemoteObject _id,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("delete_bank (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,37);
if (RapidSub.canDelegate("delete_bank")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","delete_bank", _id, _method);
Debug.locals.put("id", _id);
Debug.locals.put("method", _method);
 BA.debugLineNum = 37;BA.debugLine="Public Sub delete_bank(id As String, method As Str";
Debug.ShouldStop(16);
 BA.debugLineNum = 38;BA.debugLine="Return exe_query($\" select * from table(PKG_DMS_M";
Debug.ShouldStop(32);
if (true) return _exe_query((RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_MASTER_TRE.fn_deletebank('"),clsmaster.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_id))),RemoteObject.createImmutable("')) "))),_method);
 BA.debugLineNum = 39;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _delete_jabatan(RemoteObject _kode,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("delete_jabatan (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,209);
if (RapidSub.canDelegate("delete_jabatan")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","delete_jabatan", _kode, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("method", _method);
 BA.debugLineNum = 209;BA.debugLine="Public Sub delete_jabatan(kode As String, method A";
Debug.ShouldStop(65536);
 BA.debugLineNum = 210;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(131072);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_deletejabatan('"),_kode,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 211;BA.debugLine="End Sub";
Debug.ShouldStop(262144);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _delete_jenisproyek(RemoteObject _kode,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("delete_jenisproyek (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,144);
if (RapidSub.canDelegate("delete_jenisproyek")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","delete_jenisproyek", _kode, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("method", _method);
 BA.debugLineNum = 144;BA.debugLine="Public Sub delete_jenisproyek(kode As String, meth";
Debug.ShouldStop(32768);
 BA.debugLineNum = 145;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(65536);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_deletejenisproyek('"),_kode,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 146;BA.debugLine="End Sub";
Debug.ShouldStop(131072);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _delete_proyek(RemoteObject _kode,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("delete_proyek (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,166);
if (RapidSub.canDelegate("delete_proyek")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","delete_proyek", _kode, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("method", _method);
 BA.debugLineNum = 166;BA.debugLine="Public Sub delete_proyek(kode As String, method As";
Debug.ShouldStop(32);
 BA.debugLineNum = 167;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(64);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_deleteproyek('"),_kode,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 168;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _delete_sumberdana(RemoteObject _kode,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("delete_sumberdana (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,123);
if (RapidSub.canDelegate("delete_sumberdana")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","delete_sumberdana", _kode, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("method", _method);
 BA.debugLineNum = 123;BA.debugLine="Public Sub delete_sumberdana(kode As String, metho";
Debug.ShouldStop(67108864);
 BA.debugLineNum = 124;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(134217728);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_deletesumberdana('"),_kode,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 125;BA.debugLine="End Sub";
Debug.ShouldStop(268435456);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _delete_termin(RemoteObject _kode,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("delete_termin (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,102);
if (RapidSub.canDelegate("delete_termin")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","delete_termin", _kode, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("method", _method);
 BA.debugLineNum = 102;BA.debugLine="Public Sub delete_termin(kode As String, method As";
Debug.ShouldStop(32);
 BA.debugLineNum = 103;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(64);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_deletetipebayar('"),_kode,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 104;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _delete_tipebayar(RemoteObject _kode,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("delete_tipebayar (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,81);
if (RapidSub.canDelegate("delete_tipebayar")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","delete_tipebayar", _kode, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("method", _method);
 BA.debugLineNum = 81;BA.debugLine="Public Sub delete_tipebayar(kode As String, method";
Debug.ShouldStop(65536);
 BA.debugLineNum = 82;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(131072);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_deletetipebayar('"),_kode,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 83;BA.debugLine="End Sub";
Debug.ShouldStop(262144);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _delete_unit(RemoteObject _kode,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("delete_unit (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,187);
if (RapidSub.canDelegate("delete_unit")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","delete_unit", _kode, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("method", _method);
 BA.debugLineNum = 187;BA.debugLine="Public Sub delete_unit(kode As String, method As S";
Debug.ShouldStop(67108864);
 BA.debugLineNum = 188;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(134217728);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_deleteunit('"),_kode,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 189;BA.debugLine="End Sub";
Debug.ShouldStop(268435456);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _delete_vendor(RemoteObject _kode,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("delete_vendor (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,60);
if (RapidSub.canDelegate("delete_vendor")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","delete_vendor", _kode, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("method", _method);
 BA.debugLineNum = 60;BA.debugLine="Public Sub delete_vendor(kode As String, method As";
Debug.ShouldStop(134217728);
 BA.debugLineNum = 61;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(268435456);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_deletevendor('"),_kode,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 62;BA.debugLine="End Sub";
Debug.ShouldStop(536870912);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _exe_query(RemoteObject _query,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("exe_query (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,6);
if (RapidSub.canDelegate("exe_query")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","exe_query", _query, _method);
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _lst = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _js = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");
Debug.locals.put("query", _query);
Debug.locals.put("method", _method);
 BA.debugLineNum = 6;BA.debugLine="Private Sub exe_query(query As String, method As S";
Debug.ShouldStop(32);
 BA.debugLineNum = 7;BA.debugLine="Dim hasil As String";
Debug.ShouldStop(64);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 8;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(128);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 9;BA.debugLine="Dim lst As List";
Debug.ShouldStop(256);
_lst = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("lst", _lst);
 BA.debugLineNum = 10;BA.debugLine="Dim js As JSONGenerator";
Debug.ShouldStop(512);
_js = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");Debug.locals.put("js", _js);
 BA.debugLineNum = 12;BA.debugLine="lst.Initialize";
Debug.ShouldStop(2048);
_lst.runVoidMethod ("Initialize");
 BA.debugLineNum = 13;BA.debugLine="mp.Initialize";
Debug.ShouldStop(4096);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 14;BA.debugLine="koneksidb.Initialize";
Debug.ShouldStop(8192);
clsmaster._koneksidb.runClassMethod (b4j.example.clsdb.class, "_initialize",clsmaster.ba);
 BA.debugLineNum = 16;BA.debugLine="mp = koneksidb.SqlSelectMap(query, method)";
Debug.ShouldStop(32768);
_mp = clsmaster._koneksidb.runClassMethod (b4j.example.clsdb.class, "_sqlselectmap",(Object)(_query),(Object)(_method));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 17;BA.debugLine="js.Initialize(mp)";
Debug.ShouldStop(65536);
_js.runVoidMethod ("Initialize",(Object)(_mp));
 BA.debugLineNum = 18;BA.debugLine="hasil = js.ToString";
Debug.ShouldStop(131072);
_hasil = _js.runMethod(true,"ToString");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 19;BA.debugLine="Return hasil";
Debug.ShouldStop(262144);
if (true) return _hasil;
 BA.debugLineNum = 20;BA.debugLine="End Sub";
Debug.ShouldStop(524288);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private koneksidb As clsDB";
clsmaster._koneksidb = RemoteObject.createNew ("b4j.example.clsdb");
 //BA.debugLineNum = 4;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _read_bank(RemoteObject _kriteria,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("read_bank (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,28);
if (RapidSub.canDelegate("read_bank")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","read_bank", _kriteria, _method);
Debug.locals.put("kriteria", _kriteria);
Debug.locals.put("method", _method);
 BA.debugLineNum = 28;BA.debugLine="Public Sub read_bank(kriteria As String, method As";
Debug.ShouldStop(134217728);
 BA.debugLineNum = 29;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
Debug.ShouldStop(268435456);
if (RemoteObject.solveBoolean("=",_kriteria,BA.ObjectToString(""))) { 
_kriteria = BA.ObjectToString("-");Debug.locals.put("kriteria", _kriteria);};
 BA.debugLineNum = 30;BA.debugLine="Return exe_query(\"select f1 id_bank, f2 nama_bank";
Debug.ShouldStop(536870912);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select f1 id_bank, f2 nama_bank from table(PKG_DMS_MASTER_TRE.fn_readbank('"),_kriteria,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 31;BA.debugLine="End Sub";
Debug.ShouldStop(1073741824);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _read_jabatan(RemoteObject _kriteria,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("read_jabatan (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,200);
if (RapidSub.canDelegate("read_jabatan")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","read_jabatan", _kriteria, _method);
Debug.locals.put("kriteria", _kriteria);
Debug.locals.put("method", _method);
 BA.debugLineNum = 200;BA.debugLine="Public Sub read_jabatan(kriteria As String, method";
Debug.ShouldStop(128);
 BA.debugLineNum = 201;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
Debug.ShouldStop(256);
if (RemoteObject.solveBoolean("=",_kriteria,BA.ObjectToString(""))) { 
_kriteria = BA.ObjectToString("-");Debug.locals.put("kriteria", _kriteria);};
 BA.debugLineNum = 202;BA.debugLine="Return exe_query(\"select f1 id_jabatan,f2 nama_ja";
Debug.ShouldStop(512);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select f1 id_jabatan,f2 nama_jabatan,f3 urut_flow from table(PKG_DMS_MASTER_TRE.fn_readjabatan('"),_kriteria,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 203;BA.debugLine="End Sub";
Debug.ShouldStop(1024);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _read_jenisproyek(RemoteObject _kriteria,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("read_jenisproyek (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,135);
if (RapidSub.canDelegate("read_jenisproyek")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","read_jenisproyek", _kriteria, _method);
Debug.locals.put("kriteria", _kriteria);
Debug.locals.put("method", _method);
 BA.debugLineNum = 135;BA.debugLine="Public Sub read_jenisproyek(kriteria As String, me";
Debug.ShouldStop(64);
 BA.debugLineNum = 136;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
Debug.ShouldStop(128);
if (RemoteObject.solveBoolean("=",_kriteria,BA.ObjectToString(""))) { 
_kriteria = BA.ObjectToString("-");Debug.locals.put("kriteria", _kriteria);};
 BA.debugLineNum = 137;BA.debugLine="Return exe_query(\"select f1 id_jenis_proyek, f2 n";
Debug.ShouldStop(256);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select f1 id_jenis_proyek, f2 nama_jenis_proyek, f3 kode_jenis_proyek from table(PKG_DMS_MASTER_TRE.fn_readjenisproyek('"),_kriteria,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 138;BA.debugLine="End Sub";
Debug.ShouldStop(512);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _read_proyek(RemoteObject _kriteria,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("read_proyek (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,157);
if (RapidSub.canDelegate("read_proyek")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","read_proyek", _kriteria, _method);
Debug.locals.put("kriteria", _kriteria);
Debug.locals.put("method", _method);
 BA.debugLineNum = 157;BA.debugLine="Public Sub read_proyek(kriteria As String, method";
Debug.ShouldStop(268435456);
 BA.debugLineNum = 158;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
Debug.ShouldStop(536870912);
if (RemoteObject.solveBoolean("=",_kriteria,BA.ObjectToString(""))) { 
_kriteria = BA.ObjectToString("-");Debug.locals.put("kriteria", _kriteria);};
 BA.debugLineNum = 159;BA.debugLine="Return exe_query(\"select f1 id_jenis_proyek,f2 na";
Debug.ShouldStop(1073741824);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select f1 id_jenis_proyek,f2 nama_proyek,f3 kode_proyek,f4 id_unit,f5 id_proyek from table(PKG_DMS_MASTER_TRE.fn_readproyek('"),_kriteria,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 160;BA.debugLine="End Sub";
Debug.ShouldStop(-2147483648);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _read_sumberdana(RemoteObject _kriteria,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("read_sumberdana (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,114);
if (RapidSub.canDelegate("read_sumberdana")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","read_sumberdana", _kriteria, _method);
Debug.locals.put("kriteria", _kriteria);
Debug.locals.put("method", _method);
 BA.debugLineNum = 114;BA.debugLine="Public Sub read_sumberdana(kriteria As String, met";
Debug.ShouldStop(131072);
 BA.debugLineNum = 115;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
Debug.ShouldStop(262144);
if (RemoteObject.solveBoolean("=",_kriteria,BA.ObjectToString(""))) { 
_kriteria = BA.ObjectToString("-");Debug.locals.put("kriteria", _kriteria);};
 BA.debugLineNum = 116;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(524288);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_readsumberdana('"),_kriteria,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 117;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _read_termin(RemoteObject _kriteria,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("read_termin (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,93);
if (RapidSub.canDelegate("read_termin")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","read_termin", _kriteria, _method);
Debug.locals.put("kriteria", _kriteria);
Debug.locals.put("method", _method);
 BA.debugLineNum = 93;BA.debugLine="Public Sub read_termin(kriteria As String, method";
Debug.ShouldStop(268435456);
 BA.debugLineNum = 94;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
Debug.ShouldStop(536870912);
if (RemoteObject.solveBoolean("=",_kriteria,BA.ObjectToString(""))) { 
_kriteria = BA.ObjectToString("-");Debug.locals.put("kriteria", _kriteria);};
 BA.debugLineNum = 95;BA.debugLine="Return exe_query(\"select f1 id_termin,f2 nama_ter";
Debug.ShouldStop(1073741824);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select f1 id_termin,f2 nama_termin,f3 kode_termin,f4 id_tipe_bayar from table(PKG_DMS_MASTER_TRE.fn_readtermin('"),_kriteria,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 96;BA.debugLine="End Sub";
Debug.ShouldStop(-2147483648);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _read_tipebayar(RemoteObject _kriteria,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("read_tipebayar (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,72);
if (RapidSub.canDelegate("read_tipebayar")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","read_tipebayar", _kriteria, _method);
Debug.locals.put("kriteria", _kriteria);
Debug.locals.put("method", _method);
 BA.debugLineNum = 72;BA.debugLine="Public Sub read_tipebayar(kriteria As String, meth";
Debug.ShouldStop(128);
 BA.debugLineNum = 73;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
Debug.ShouldStop(256);
if (RemoteObject.solveBoolean("=",_kriteria,BA.ObjectToString(""))) { 
_kriteria = BA.ObjectToString("-");Debug.locals.put("kriteria", _kriteria);};
 BA.debugLineNum = 74;BA.debugLine="Return exe_query(\"select f1 id_tipe_bayar, f2 nam";
Debug.ShouldStop(512);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select f1 id_tipe_bayar, f2 nama_tipe_bayar from table(PKG_DMS_MASTER_TRE.fn_readtipebayar('"),_kriteria,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 75;BA.debugLine="End Sub";
Debug.ShouldStop(1024);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _read_unit(RemoteObject _kriteria,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("read_unit (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,178);
if (RapidSub.canDelegate("read_unit")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","read_unit", _kriteria, _method);
Debug.locals.put("kriteria", _kriteria);
Debug.locals.put("method", _method);
 BA.debugLineNum = 178;BA.debugLine="Public Sub read_unit(kriteria As String, method As";
Debug.ShouldStop(131072);
 BA.debugLineNum = 179;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
Debug.ShouldStop(262144);
if (RemoteObject.solveBoolean("=",_kriteria,BA.ObjectToString(""))) { 
_kriteria = BA.ObjectToString("-");Debug.locals.put("kriteria", _kriteria);};
 BA.debugLineNum = 180;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(524288);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_readunit('"),_kriteria,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 181;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _read_vendor(RemoteObject _kriteria,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("read_vendor (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,49);
if (RapidSub.canDelegate("read_vendor")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","read_vendor", _kriteria, _method);
Debug.locals.put("kriteria", _kriteria);
Debug.locals.put("method", _method);
 BA.debugLineNum = 49;BA.debugLine="Public Sub read_vendor(kriteria As String, method";
Debug.ShouldStop(65536);
 BA.debugLineNum = 50;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
Debug.ShouldStop(131072);
if (RemoteObject.solveBoolean("=",_kriteria,BA.ObjectToString(""))) { 
_kriteria = BA.ObjectToString("-");Debug.locals.put("kriteria", _kriteria);};
 BA.debugLineNum = 51;BA.debugLine="Return exe_query($\"select f1 kode_vendor, f2 nama";
Debug.ShouldStop(262144);
if (true) return _exe_query((RemoteObject.concat(RemoteObject.createImmutable("select f1 kode_vendor, f2 nama_vendor, f3 sebutan_pejabat_vendor, f4 nama_vendor1, \n"),RemoteObject.createImmutable("	f5 nama_vendor2, f6 nama_vendor3, f7 nama_vendor4, f8 nama_pejabat_vendor, f9 username, f10 password \n"),RemoteObject.createImmutable("	from table(PKG_DMS_MASTER_TRE.fn_readvendor('"),clsmaster.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_kriteria))),RemoteObject.createImmutable("')) "))),_method);
 BA.debugLineNum = 54;BA.debugLine="End Sub";
Debug.ShouldStop(2097152);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _update_bank(RemoteObject _id,RemoteObject _nama,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("update_bank (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,33);
if (RapidSub.canDelegate("update_bank")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","update_bank", _id, _nama, _method);
Debug.locals.put("id", _id);
Debug.locals.put("nama", _nama);
Debug.locals.put("method", _method);
 BA.debugLineNum = 33;BA.debugLine="Public Sub update_bank(id As String, nama As Strin";
Debug.ShouldStop(1);
 BA.debugLineNum = 34;BA.debugLine="Return exe_query($\" select * from table(PKG_DMS_M";
Debug.ShouldStop(2);
if (true) return _exe_query((RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_MASTER_TRE.fn_editbank('"),clsmaster.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_id))),RemoteObject.createImmutable("','"),clsmaster.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_nama))),RemoteObject.createImmutable("')) "))),_method);
 BA.debugLineNum = 35;BA.debugLine="End Sub";
Debug.ShouldStop(4);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _update_jabatan(RemoteObject _id,RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _p5,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("update_jabatan (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,205);
if (RapidSub.canDelegate("update_jabatan")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","update_jabatan", _id, _p1, _p2, _p3, _p4, _p5, _method);
Debug.locals.put("id", _id);
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("p5", _p5);
Debug.locals.put("method", _method);
 BA.debugLineNum = 205;BA.debugLine="Public Sub update_jabatan(id As String, p1 As Stri";
Debug.ShouldStop(4096);
 BA.debugLineNum = 206;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(8192);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_editjabatan('"),_id,RemoteObject.createImmutable("','"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("','"),_p3,RemoteObject.createImmutable("','"),_p4,RemoteObject.createImmutable("','"),_p5,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 207;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _update_jenisproyek(RemoteObject _kode,RemoteObject _p1,RemoteObject _p2,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("update_jenisproyek (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,140);
if (RapidSub.canDelegate("update_jenisproyek")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","update_jenisproyek", _kode, _p1, _p2, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("method", _method);
 BA.debugLineNum = 140;BA.debugLine="Public Sub update_jenisproyek(kode As String, p1 A";
Debug.ShouldStop(2048);
 BA.debugLineNum = 141;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(4096);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_editjenisproyek('"),_kode,RemoteObject.createImmutable("','"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 142;BA.debugLine="End Sub";
Debug.ShouldStop(8192);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _update_proyek(RemoteObject _id,RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("update_proyek (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,162);
if (RapidSub.canDelegate("update_proyek")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","update_proyek", _id, _p1, _p2, _p3, _p4, _method);
Debug.locals.put("id", _id);
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("method", _method);
 BA.debugLineNum = 162;BA.debugLine="Public Sub update_proyek(id As String, p1 As Strin";
Debug.ShouldStop(2);
 BA.debugLineNum = 163;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(4);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_editproyek('"),_id,RemoteObject.createImmutable("','"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("','"),_p3,RemoteObject.createImmutable("','"),_p4,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 164;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _update_sumberdana(RemoteObject _kode,RemoteObject _p1,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("update_sumberdana (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,119);
if (RapidSub.canDelegate("update_sumberdana")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","update_sumberdana", _kode, _p1, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("p1", _p1);
Debug.locals.put("method", _method);
 BA.debugLineNum = 119;BA.debugLine="Public Sub update_sumberdana(kode As String, p1 As";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 120;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(8388608);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_editsumberdana('"),_kode,RemoteObject.createImmutable("','"),_p1,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 121;BA.debugLine="End Sub";
Debug.ShouldStop(16777216);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _update_termin(RemoteObject _kode,RemoteObject _p1,RemoteObject _p2,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("update_termin (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,98);
if (RapidSub.canDelegate("update_termin")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","update_termin", _kode, _p1, _p2, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("method", _method);
 BA.debugLineNum = 98;BA.debugLine="Public Sub update_termin(kode As String, p1 As Str";
Debug.ShouldStop(2);
 BA.debugLineNum = 99;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(4);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_edittipebayar('"),_kode,RemoteObject.createImmutable("','"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 100;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _update_tipebayar(RemoteObject _kode,RemoteObject _p1,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("update_tipebayar (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,77);
if (RapidSub.canDelegate("update_tipebayar")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","update_tipebayar", _kode, _p1, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("p1", _p1);
Debug.locals.put("method", _method);
 BA.debugLineNum = 77;BA.debugLine="Public Sub update_tipebayar(kode As String, p1 As";
Debug.ShouldStop(4096);
 BA.debugLineNum = 78;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(8192);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_edittipebayar('"),_kode,RemoteObject.createImmutable("','"),_p1,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 79;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _update_unit(RemoteObject _id,RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _p5,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("update_unit (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,183);
if (RapidSub.canDelegate("update_unit")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","update_unit", _id, _p1, _p2, _p3, _p4, _p5, _method);
Debug.locals.put("id", _id);
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("p5", _p5);
Debug.locals.put("method", _method);
 BA.debugLineNum = 183;BA.debugLine="Public Sub update_unit(id As String, p1 As String,";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 184;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(8388608);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_editunit('"),_id,RemoteObject.createImmutable("','"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("','"),_p3,RemoteObject.createImmutable("','"),_p4,RemoteObject.createImmutable("','"),_p5,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 185;BA.debugLine="End Sub";
Debug.ShouldStop(16777216);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _update_vendor(RemoteObject _kode,RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _p5,RemoteObject _p6,RemoteObject _p7,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("update_vendor (clsmaster) ","clsmaster",9,clsmaster.ba,clsmaster.mostCurrent,56);
if (RapidSub.canDelegate("update_vendor")) return b4j.example.clsmaster.remoteMe.runUserSub(false, "clsmaster","update_vendor", _kode, _p1, _p2, _p3, _p4, _p5, _p6, _p7, _method);
Debug.locals.put("kode", _kode);
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("p5", _p5);
Debug.locals.put("p6", _p6);
Debug.locals.put("p7", _p7);
Debug.locals.put("method", _method);
 BA.debugLineNum = 56;BA.debugLine="Public Sub update_vendor(kode As String, p1 As Str";
Debug.ShouldStop(8388608);
 BA.debugLineNum = 57;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
Debug.ShouldStop(16777216);
if (true) return _exe_query(RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MASTER_TRE.fn_editvendor('"),_kode,RemoteObject.createImmutable("','"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("','"),_p3,RemoteObject.createImmutable("','"),_p4,RemoteObject.createImmutable("','"),_p5,RemoteObject.createImmutable("','"),_p6,RemoteObject.createImmutable("','"),_p7,RemoteObject.createImmutable("'))")),_method);
 BA.debugLineNum = 58;BA.debugLine="End Sub";
Debug.ShouldStop(33554432);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}