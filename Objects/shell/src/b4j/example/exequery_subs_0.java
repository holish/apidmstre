package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class exequery_subs_0 {


public static RemoteObject  _multi_map(RemoteObject _query,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("multi_map (exequery) ","exequery",2,exequery.ba,exequery.mostCurrent,55);
if (RapidSub.canDelegate("multi_map")) return b4j.example.exequery.remoteMe.runUserSub(false, "exequery","multi_map", _query, _method);
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _lst = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
Debug.locals.put("query", _query);
Debug.locals.put("method", _method);
 BA.debugLineNum = 55;BA.debugLine="Public Sub multi_map(query As String, method As St";
Debug.ShouldStop(4194304);
 BA.debugLineNum = 56;BA.debugLine="todb.Initialize";
Debug.ShouldStop(8388608);
exequery._todb.runClassMethod (b4j.example.clsdb.class, "_initialize",exequery.ba);
 BA.debugLineNum = 57;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(16777216);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 58;BA.debugLine="Dim lst As List";
Debug.ShouldStop(33554432);
_lst = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("lst", _lst);
 BA.debugLineNum = 60;BA.debugLine="lst.Initialize";
Debug.ShouldStop(134217728);
_lst.runVoidMethod ("Initialize");
 BA.debugLineNum = 61;BA.debugLine="mp.Initialize";
Debug.ShouldStop(268435456);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 63;BA.debugLine="mp = todb.SqlSelectMap(query, method)";
Debug.ShouldStop(1073741824);
_mp = exequery._todb.runClassMethod (b4j.example.clsdb.class, "_sqlselectmap",(Object)(_query),(Object)(_method));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 65;BA.debugLine="Return mp";
Debug.ShouldStop(1);
if (true) return _mp;
 BA.debugLineNum = 66;BA.debugLine="End Sub";
Debug.ShouldStop(2);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _multi_string(RemoteObject _query,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("multi_string (exequery) ","exequery",2,exequery.ba,exequery.mostCurrent,38);
if (RapidSub.canDelegate("multi_string")) return b4j.example.exequery.remoteMe.runUserSub(false, "exequery","multi_string", _query, _method);
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _lst = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _js = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");
Debug.locals.put("query", _query);
Debug.locals.put("method", _method);
 BA.debugLineNum = 38;BA.debugLine="Public Sub multi_string(query As String, method As";
Debug.ShouldStop(32);
 BA.debugLineNum = 39;BA.debugLine="todb.Initialize";
Debug.ShouldStop(64);
exequery._todb.runClassMethod (b4j.example.clsdb.class, "_initialize",exequery.ba);
 BA.debugLineNum = 40;BA.debugLine="Dim hasil As String";
Debug.ShouldStop(128);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 41;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(256);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 42;BA.debugLine="Dim lst As List";
Debug.ShouldStop(512);
_lst = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("lst", _lst);
 BA.debugLineNum = 43;BA.debugLine="Dim js As JSONGenerator";
Debug.ShouldStop(1024);
_js = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");Debug.locals.put("js", _js);
 BA.debugLineNum = 45;BA.debugLine="lst.Initialize";
Debug.ShouldStop(4096);
_lst.runVoidMethod ("Initialize");
 BA.debugLineNum = 46;BA.debugLine="mp.Initialize";
Debug.ShouldStop(8192);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 48;BA.debugLine="mp = todb.SqlSelectMap(query, method)";
Debug.ShouldStop(32768);
_mp = exequery._todb.runClassMethod (b4j.example.clsdb.class, "_sqlselectmap",(Object)(_query),(Object)(_method));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 49;BA.debugLine="js.Initialize(mp)";
Debug.ShouldStop(65536);
_js.runVoidMethod ("Initialize",(Object)(_mp));
 BA.debugLineNum = 50;BA.debugLine="hasil = js.ToString";
Debug.ShouldStop(131072);
_hasil = _js.runMethod(true,"ToString");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 52;BA.debugLine="Return hasil";
Debug.ShouldStop(524288);
if (true) return _hasil;
 BA.debugLineNum = 53;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private todb As clsDB";
exequery._todb = RemoteObject.createNew ("b4j.example.clsdb");
 //BA.debugLineNum = 4;BA.debugLine="Private todbdms As clsDBDms";
exequery._todbdms = RemoteObject.createNew ("b4j.example.clsdbdms");
 //BA.debugLineNum = 5;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _single_map(RemoteObject _query,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("single_map (exequery) ","exequery",2,exequery.ba,exequery.mostCurrent,25);
if (RapidSub.canDelegate("single_map")) return b4j.example.exequery.remoteMe.runUserSub(false, "exequery","single_map", _query, _method);
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _lst = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
Debug.locals.put("query", _query);
Debug.locals.put("method", _method);
 BA.debugLineNum = 25;BA.debugLine="Public Sub single_map(query As String, method As S";
Debug.ShouldStop(16777216);
 BA.debugLineNum = 26;BA.debugLine="todb.Initialize";
Debug.ShouldStop(33554432);
exequery._todb.runClassMethod (b4j.example.clsdb.class, "_initialize",exequery.ba);
 BA.debugLineNum = 27;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(67108864);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 28;BA.debugLine="Dim lst As List";
Debug.ShouldStop(134217728);
_lst = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("lst", _lst);
 BA.debugLineNum = 30;BA.debugLine="lst.Initialize";
Debug.ShouldStop(536870912);
_lst.runVoidMethod ("Initialize");
 BA.debugLineNum = 31;BA.debugLine="mp.Initialize";
Debug.ShouldStop(1073741824);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 33;BA.debugLine="mp = todb.SqlSelectSingleMap(query, method)";
Debug.ShouldStop(1);
_mp = exequery._todb.runClassMethod (b4j.example.clsdb.class, "_sqlselectsinglemap",(Object)(_query),(Object)(_method));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 35;BA.debugLine="Return mp";
Debug.ShouldStop(4);
if (true) return _mp;
 BA.debugLineNum = 36;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _single_string(RemoteObject _query,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("single_string (exequery) ","exequery",2,exequery.ba,exequery.mostCurrent,8);
if (RapidSub.canDelegate("single_string")) return b4j.example.exequery.remoteMe.runUserSub(false, "exequery","single_string", _query, _method);
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _lst = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _js = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");
Debug.locals.put("query", _query);
Debug.locals.put("method", _method);
 BA.debugLineNum = 8;BA.debugLine="Public Sub single_string(query As String, method A";
Debug.ShouldStop(128);
 BA.debugLineNum = 9;BA.debugLine="todb.Initialize";
Debug.ShouldStop(256);
exequery._todb.runClassMethod (b4j.example.clsdb.class, "_initialize",exequery.ba);
 BA.debugLineNum = 10;BA.debugLine="Dim hasil As String";
Debug.ShouldStop(512);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 11;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(1024);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 12;BA.debugLine="Dim lst As List";
Debug.ShouldStop(2048);
_lst = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("lst", _lst);
 BA.debugLineNum = 13;BA.debugLine="Dim js As JSONGenerator";
Debug.ShouldStop(4096);
_js = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");Debug.locals.put("js", _js);
 BA.debugLineNum = 15;BA.debugLine="lst.Initialize";
Debug.ShouldStop(16384);
_lst.runVoidMethod ("Initialize");
 BA.debugLineNum = 16;BA.debugLine="mp.Initialize";
Debug.ShouldStop(32768);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 18;BA.debugLine="mp = todb.SqlSelectSingleMap(query, method)";
Debug.ShouldStop(131072);
_mp = exequery._todb.runClassMethod (b4j.example.clsdb.class, "_sqlselectsinglemap",(Object)(_query),(Object)(_method));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 19;BA.debugLine="js.Initialize(mp)";
Debug.ShouldStop(262144);
_js.runVoidMethod ("Initialize",(Object)(_mp));
 BA.debugLineNum = 20;BA.debugLine="hasil = js.ToString";
Debug.ShouldStop(524288);
_hasil = _js.runMethod(true,"ToString");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 22;BA.debugLine="Return hasil";
Debug.ShouldStop(2097152);
if (true) return _hasil;
 BA.debugLineNum = 23;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}