package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class clsmanagement_subs_0 {


public static RemoteObject  _create_roles(RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("create_roles (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,24);
if (RapidSub.canDelegate("create_roles")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","create_roles", _p1, _p2, _p3, _method);
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _p4 = RemoteObject.createImmutable("");
RemoteObject _p5 = RemoteObject.createImmutable("");
RemoteObject _templatemenu = RemoteObject.createImmutable("");
RemoteObject _jstut = RemoteObject.declareNull("anywheresoftware.b4a.objects.StringUtils");
RemoteObject _arr = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _arrmenu = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _arrotoritas = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
int _i = 0;
int _x = 0;
int _y = 0;
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("method", _method);
 BA.debugLineNum = 24;BA.debugLine="Public Sub create_roles(p1 As String,  p2 As Strin";
Debug.ShouldStop(8388608);
 BA.debugLineNum = 25;BA.debugLine="Dim data, query, p4, p5, templatemenu As String";
Debug.ShouldStop(16777216);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_p4 = RemoteObject.createImmutable("");Debug.locals.put("p4", _p4);
_p5 = RemoteObject.createImmutable("");Debug.locals.put("p5", _p5);
_templatemenu = RemoteObject.createImmutable("");Debug.locals.put("templatemenu", _templatemenu);
 BA.debugLineNum = 26;BA.debugLine="Dim jstut As StringUtils";
Debug.ShouldStop(33554432);
_jstut = RemoteObject.createNew ("anywheresoftware.b4a.objects.StringUtils");Debug.locals.put("jstut", _jstut);
 BA.debugLineNum = 27;BA.debugLine="Dim arr, arrmenu, arrotoritas As List";
Debug.ShouldStop(67108864);
_arr = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("arr", _arr);
_arrmenu = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("arrmenu", _arrmenu);
_arrotoritas = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("arrotoritas", _arrotoritas);
 BA.debugLineNum = 28;BA.debugLine="arr.Initialize";
Debug.ShouldStop(134217728);
_arr.runVoidMethod ("Initialize");
 BA.debugLineNum = 29;BA.debugLine="arrmenu.Initialize";
Debug.ShouldStop(268435456);
_arrmenu.runVoidMethod ("Initialize");
 BA.debugLineNum = 30;BA.debugLine="arrotoritas.Initialize";
Debug.ShouldStop(536870912);
_arrotoritas.runVoidMethod ("Initialize");
 BA.debugLineNum = 31;BA.debugLine="p3 = jstut.DecodeUrl(p3, \"UTF8\")";
Debug.ShouldStop(1073741824);
_p3 = _jstut.runMethod(true,"DecodeUrl",(Object)(_p3),(Object)(RemoteObject.createImmutable("UTF8")));Debug.locals.put("p3", _p3);
 BA.debugLineNum = 32;BA.debugLine="arr = Regex.Split(\"#\", p3)";
Debug.ShouldStop(-2147483648);
_arr = clsmanagement.__c.runMethod(false, "ArrayToList", (Object)(clsmanagement.__c.getField(false,"Regex").runMethod(false,"Split",(Object)(BA.ObjectToString("#")),(Object)(_p3))));Debug.locals.put("arr", _arr);
 BA.debugLineNum = 33;BA.debugLine="templatemenu = \"1,2,3,4,5\"";
Debug.ShouldStop(1);
_templatemenu = BA.ObjectToString("1,2,3,4,5");Debug.locals.put("templatemenu", _templatemenu);
 BA.debugLineNum = 34;BA.debugLine="p3 = \"\"";
Debug.ShouldStop(2);
_p3 = BA.ObjectToString("");Debug.locals.put("p3", _p3);
 BA.debugLineNum = 35;BA.debugLine="p5 = arr.Size";
Debug.ShouldStop(4);
_p5 = BA.NumberToString(_arr.runMethod(true,"getSize"));Debug.locals.put("p5", _p5);
 BA.debugLineNum = 36;BA.debugLine="For i = 0 To arr.Size - 1";
Debug.ShouldStop(8);
{
final int step12 = 1;
final int limit12 = RemoteObject.solve(new RemoteObject[] {_arr.runMethod(true,"getSize"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = 0 ;
for (;(step12 > 0 && _i <= limit12) || (step12 < 0 && _i >= limit12) ;_i = ((int)(0 + _i + step12))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 37;BA.debugLine="arrmenu = Regex.Split(\"=\", arr.Get(i))";
Debug.ShouldStop(16);
_arrmenu = clsmanagement.__c.runMethod(false, "ArrayToList", (Object)(clsmanagement.__c.getField(false,"Regex").runMethod(false,"Split",(Object)(BA.ObjectToString("=")),(Object)(BA.ObjectToString(_arr.runMethod(false,"Get",(Object)(BA.numberCast(int.class, _i))))))));Debug.locals.put("arrmenu", _arrmenu);
 BA.debugLineNum = 38;BA.debugLine="If p3.Length = 0 Then";
Debug.ShouldStop(32);
if (RemoteObject.solveBoolean("=",_p3.runMethod(true,"length"),BA.numberCast(double.class, 0))) { 
 BA.debugLineNum = 39;BA.debugLine="p3 = p3 & arrmenu.Get(0)";
Debug.ShouldStop(64);
_p3 = RemoteObject.concat(_p3,_arrmenu.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 0))));Debug.locals.put("p3", _p3);
 }else {
 BA.debugLineNum = 41;BA.debugLine="p3 = p3 & \"#\" & arrmenu.Get(0)";
Debug.ShouldStop(256);
_p3 = RemoteObject.concat(_p3,RemoteObject.createImmutable("#"),_arrmenu.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 0))));Debug.locals.put("p3", _p3);
 };
 BA.debugLineNum = 44;BA.debugLine="For x = 1 To arrmenu.Size - 1";
Debug.ShouldStop(2048);
{
final int step19 = 1;
final int limit19 = RemoteObject.solve(new RemoteObject[] {_arrmenu.runMethod(true,"getSize"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_x = 1 ;
for (;(step19 > 0 && _x <= limit19) || (step19 < 0 && _x >= limit19) ;_x = ((int)(0 + _x + step19))  ) {
Debug.locals.put("x", _x);
 BA.debugLineNum = 45;BA.debugLine="templatemenu = \"1,2,3,4,5\"";
Debug.ShouldStop(4096);
_templatemenu = BA.ObjectToString("1,2,3,4,5");Debug.locals.put("templatemenu", _templatemenu);
 BA.debugLineNum = 46;BA.debugLine="arrotoritas = Regex.Split(\",\", arrmenu.Get(x))";
Debug.ShouldStop(8192);
_arrotoritas = clsmanagement.__c.runMethod(false, "ArrayToList", (Object)(clsmanagement.__c.getField(false,"Regex").runMethod(false,"Split",(Object)(BA.ObjectToString(",")),(Object)(BA.ObjectToString(_arrmenu.runMethod(false,"Get",(Object)(BA.numberCast(int.class, _x))))))));Debug.locals.put("arrotoritas", _arrotoritas);
 BA.debugLineNum = 47;BA.debugLine="If arrotoritas.Size = 1 Then";
Debug.ShouldStop(16384);
if (RemoteObject.solveBoolean("=",_arrotoritas.runMethod(true,"getSize"),BA.numberCast(double.class, 1))) { 
 BA.debugLineNum = 48;BA.debugLine="Select arrotoritas.Get(0)";
Debug.ShouldStop(32768);
switch (BA.switchObjectToInt(_arrotoritas.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 0))),RemoteObject.createImmutable(("1")),RemoteObject.createImmutable(("2")),RemoteObject.createImmutable(("3")),RemoteObject.createImmutable(("4")),RemoteObject.createImmutable(("5")))) {
case 0: {
 BA.debugLineNum = 50;BA.debugLine="templatemenu = \"1,0,0,0,0\"";
Debug.ShouldStop(131072);
_templatemenu = BA.ObjectToString("1,0,0,0,0");Debug.locals.put("templatemenu", _templatemenu);
 break; }
case 1: {
 BA.debugLineNum = 52;BA.debugLine="templatemenu = \"0,1,0,0,0\"";
Debug.ShouldStop(524288);
_templatemenu = BA.ObjectToString("0,1,0,0,0");Debug.locals.put("templatemenu", _templatemenu);
 break; }
case 2: {
 BA.debugLineNum = 54;BA.debugLine="templatemenu = \"0,0,1,0,0\"";
Debug.ShouldStop(2097152);
_templatemenu = BA.ObjectToString("0,0,1,0,0");Debug.locals.put("templatemenu", _templatemenu);
 break; }
case 3: {
 BA.debugLineNum = 56;BA.debugLine="templatemenu = \"0,0,0,1,0\"";
Debug.ShouldStop(8388608);
_templatemenu = BA.ObjectToString("0,0,0,1,0");Debug.locals.put("templatemenu", _templatemenu);
 break; }
case 4: {
 BA.debugLineNum = 58;BA.debugLine="templatemenu = \"0,0,0,0,1\"";
Debug.ShouldStop(33554432);
_templatemenu = BA.ObjectToString("0,0,0,0,1");Debug.locals.put("templatemenu", _templatemenu);
 break; }
}
;
 }else {
 BA.debugLineNum = 61;BA.debugLine="For y = 0 To arrotoritas.Size - 1";
Debug.ShouldStop(268435456);
{
final int step36 = 1;
final int limit36 = RemoteObject.solve(new RemoteObject[] {_arrotoritas.runMethod(true,"getSize"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_y = 0 ;
for (;(step36 > 0 && _y <= limit36) || (step36 < 0 && _y >= limit36) ;_y = ((int)(0 + _y + step36))  ) {
Debug.locals.put("y", _y);
 BA.debugLineNum = 62;BA.debugLine="If arrotoritas.Get(0) <> \"1\" Then templatemen";
Debug.ShouldStop(536870912);
if (RemoteObject.solveBoolean("!",_arrotoritas.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 0))),RemoteObject.createImmutable(("1")))) { 
_templatemenu = _templatemenu.runMethod(true,"replace",(Object)(BA.ObjectToString("1")),(Object)(RemoteObject.createImmutable("0")));Debug.locals.put("templatemenu", _templatemenu);};
 BA.debugLineNum = 63;BA.debugLine="templatemenu = templatemenu.Replace(arrotorit";
Debug.ShouldStop(1073741824);
_templatemenu = _templatemenu.runMethod(true,"replace",(Object)(BA.ObjectToString(_arrotoritas.runMethod(false,"Get",(Object)(BA.numberCast(int.class, _y))))),(Object)(BA.NumberToString(1)));Debug.locals.put("templatemenu", _templatemenu);
 }
}Debug.locals.put("y", _y);
;
 };
 BA.debugLineNum = 67;BA.debugLine="If p4.Length = 0 Then";
Debug.ShouldStop(4);
if (RemoteObject.solveBoolean("=",_p4.runMethod(true,"length"),BA.numberCast(double.class, 0))) { 
 BA.debugLineNum = 68;BA.debugLine="p4 = templatemenu";
Debug.ShouldStop(8);
_p4 = _templatemenu;Debug.locals.put("p4", _p4);
 }else {
 BA.debugLineNum = 70;BA.debugLine="p4 = p4 & \"#\" & templatemenu";
Debug.ShouldStop(32);
_p4 = RemoteObject.concat(_p4,RemoteObject.createImmutable("#"),_templatemenu);Debug.locals.put("p4", _p4);
 };
 }
}Debug.locals.put("x", _x);
;
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 74;BA.debugLine="query = \"select * from table(PKG_DMS_MANAGEMENT_T";
Debug.ShouldStop(512);
_query = RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MANAGEMENT_TRE.fn_createrole('"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("', '"),_p3,RemoteObject.createImmutable("', '"),_p4,RemoteObject.createImmutable("', "),_p5,RemoteObject.createImmutable("))"));Debug.locals.put("query", _query);
 BA.debugLineNum = 75;BA.debugLine="data = exe_query(query, method)";
Debug.ShouldStop(1024);
_data = _exe_query(_query,_method);Debug.locals.put("data", _data);
 BA.debugLineNum = 76;BA.debugLine="Return data";
Debug.ShouldStop(2048);
if (true) return _data;
 BA.debugLineNum = 77;BA.debugLine="End Sub";
Debug.ShouldStop(4096);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _deactivate_roles(RemoteObject _p1,RemoteObject _p2,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("deactivate_roles (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,93);
if (RapidSub.canDelegate("deactivate_roles")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","deactivate_roles", _p1, _p2, _method);
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("method", _method);
 BA.debugLineNum = 93;BA.debugLine="Public Sub deactivate_roles(p1 As String, p2 As St";
Debug.ShouldStop(268435456);
 BA.debugLineNum = 94;BA.debugLine="Dim data, query As String";
Debug.ShouldStop(536870912);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
 BA.debugLineNum = 95;BA.debugLine="query = \"select * from table(PKG_DMS_MANAGEMENT_T";
Debug.ShouldStop(1073741824);
_query = RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MANAGEMENT_TRE.fn_deactivate('"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("'))"));Debug.locals.put("query", _query);
 BA.debugLineNum = 96;BA.debugLine="data = exe_query(query, method)";
Debug.ShouldStop(-2147483648);
_data = _exe_query(_query,_method);Debug.locals.put("data", _data);
 BA.debugLineNum = 97;BA.debugLine="Return data";
Debug.ShouldStop(1);
if (true) return _data;
 BA.debugLineNum = 98;BA.debugLine="End Sub";
Debug.ShouldStop(2);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _detail_roles(RemoteObject _p1,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("detail_roles (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,100);
if (RapidSub.canDelegate("detail_roles")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","detail_roles", _p1, _method);
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
Debug.locals.put("p1", _p1);
Debug.locals.put("method", _method);
 BA.debugLineNum = 100;BA.debugLine="Public Sub detail_roles(p1 As String, method As St";
Debug.ShouldStop(8);
 BA.debugLineNum = 101;BA.debugLine="Dim data, query As String";
Debug.ShouldStop(16);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
 BA.debugLineNum = 102;BA.debugLine="query = \"select * from table(PKG_DMS_MANAGEMENT_T";
Debug.ShouldStop(32);
_query = RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MANAGEMENT_TRE.fn_detailrole('"),_p1,RemoteObject.createImmutable("'))"));Debug.locals.put("query", _query);
 BA.debugLineNum = 103;BA.debugLine="data = exe_query(query, method)";
Debug.ShouldStop(64);
_data = _exe_query(_query,_method);Debug.locals.put("data", _data);
 BA.debugLineNum = 104;BA.debugLine="Return data";
Debug.ShouldStop(128);
if (true) return _data;
 BA.debugLineNum = 105;BA.debugLine="End Sub";
Debug.ShouldStop(256);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _detail_user(RemoteObject _p1,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("detail_user (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,120);
if (RapidSub.canDelegate("detail_user")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","detail_user", _p1, _method);
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
Debug.locals.put("p1", _p1);
Debug.locals.put("method", _method);
 BA.debugLineNum = 120;BA.debugLine="Public Sub detail_user(p1 As String, method As Str";
Debug.ShouldStop(8388608);
 BA.debugLineNum = 121;BA.debugLine="Dim data, query As String";
Debug.ShouldStop(16777216);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
 BA.debugLineNum = 122;BA.debugLine="query = $\" select * from table(PKG_DMS_MANAGEMENT";
Debug.ShouldStop(33554432);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_detailuser('"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p1))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 123;BA.debugLine="data = exequery.multi_string(query, method)";
Debug.ShouldStop(67108864);
_data = clsmanagement._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 124;BA.debugLine="Return data";
Debug.ShouldStop(134217728);
if (true) return _data;
 BA.debugLineNum = 125;BA.debugLine="End Sub";
Debug.ShouldStop(268435456);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _edit_user(RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _p5,RemoteObject _p6,RemoteObject _p7,RemoteObject _p8,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("edit_user (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,134);
if (RapidSub.canDelegate("edit_user")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","edit_user", _p1, _p2, _p3, _p4, _p5, _p6, _p7, _p8, _method);
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("p5", _p5);
Debug.locals.put("p6", _p6);
Debug.locals.put("p7", _p7);
Debug.locals.put("p8", _p8);
Debug.locals.put("method", _method);
 BA.debugLineNum = 134;BA.debugLine="Public Sub edit_user(p1 As String, p2 As String, p";
Debug.ShouldStop(32);
 BA.debugLineNum = 135;BA.debugLine="Dim data, query As String";
Debug.ShouldStop(64);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
 BA.debugLineNum = 136;BA.debugLine="query = $\" select * from table(PKG_DMS_MANAGEMENT";
Debug.ShouldStop(128);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_edituser('"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p1))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p2))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p3))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p4))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p5))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p6))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p7))),RemoteObject.createImmutable("', '"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p8))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 137;BA.debugLine="data = exequery.multi_string(query, method)";
Debug.ShouldStop(256);
_data = clsmanagement._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 138;BA.debugLine="Return data";
Debug.ShouldStop(512);
if (true) return _data;
 BA.debugLineNum = 139;BA.debugLine="End Sub";
Debug.ShouldStop(1024);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _exe_query(RemoteObject _query,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("exe_query (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,6);
if (RapidSub.canDelegate("exe_query")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","exe_query", _query, _method);
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _lst = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _js = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");
Debug.locals.put("query", _query);
Debug.locals.put("method", _method);
 BA.debugLineNum = 6;BA.debugLine="Private Sub exe_query(query As String, method As S";
Debug.ShouldStop(32);
 BA.debugLineNum = 7;BA.debugLine="Dim hasil As String";
Debug.ShouldStop(64);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 8;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(128);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 9;BA.debugLine="Dim lst As List";
Debug.ShouldStop(256);
_lst = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("lst", _lst);
 BA.debugLineNum = 10;BA.debugLine="Dim js As JSONGenerator";
Debug.ShouldStop(512);
_js = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");Debug.locals.put("js", _js);
 BA.debugLineNum = 12;BA.debugLine="lst.Initialize";
Debug.ShouldStop(2048);
_lst.runVoidMethod ("Initialize");
 BA.debugLineNum = 13;BA.debugLine="mp.Initialize";
Debug.ShouldStop(4096);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 14;BA.debugLine="koneksidb.Initialize";
Debug.ShouldStop(8192);
clsmanagement._koneksidb.runClassMethod (b4j.example.clsdb.class, "_initialize",clsmanagement.ba);
 BA.debugLineNum = 16;BA.debugLine="mp = koneksidb.SqlSelectMap(query, method)";
Debug.ShouldStop(32768);
_mp = clsmanagement._koneksidb.runClassMethod (b4j.example.clsdb.class, "_sqlselectmap",(Object)(_query),(Object)(_method));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 17;BA.debugLine="js.Initialize(mp)";
Debug.ShouldStop(65536);
_js.runVoidMethod ("Initialize",(Object)(_mp));
 BA.debugLineNum = 18;BA.debugLine="hasil = js.ToString";
Debug.ShouldStop(131072);
_hasil = _js.runMethod(true,"ToString");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 19;BA.debugLine="Return hasil";
Debug.ShouldStop(262144);
if (true) return _hasil;
 BA.debugLineNum = 20;BA.debugLine="End Sub";
Debug.ShouldStop(524288);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _list_user(RemoteObject _p1,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("list_user (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,111);
if (RapidSub.canDelegate("list_user")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","list_user", _p1, _method);
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
Debug.locals.put("p1", _p1);
Debug.locals.put("method", _method);
 BA.debugLineNum = 111;BA.debugLine="Public Sub list_user(p1 As String, method As Strin";
Debug.ShouldStop(16384);
 BA.debugLineNum = 112;BA.debugLine="Dim data, query As String";
Debug.ShouldStop(32768);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
 BA.debugLineNum = 113;BA.debugLine="query = $\" select id_pegawai f1, nama_pegawai f2,";
Debug.ShouldStop(65536);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select id_pegawai f1, nama_pegawai f2, nip_pegawai f3, roles_nama f4,  nama_unit f5, \n"),RemoteObject.createImmutable("	nama_jabatan f6 \n"),RemoteObject.createImmutable("	from v_user_pegawai ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 116;BA.debugLine="data = exequery.multi_string(query, method)";
Debug.ShouldStop(524288);
_data = clsmanagement._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 117;BA.debugLine="Return data";
Debug.ShouldStop(1048576);
if (true) return _data;
 BA.debugLineNum = 118;BA.debugLine="End Sub";
Debug.ShouldStop(2097152);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private koneksidb As clsDB";
clsmanagement._koneksidb = RemoteObject.createNew ("b4j.example.clsdb");
 //BA.debugLineNum = 4;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _read_menu(RemoteObject _p1,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("read_menu (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,146);
if (RapidSub.canDelegate("read_menu")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","read_menu", _p1, _method);
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
Debug.locals.put("p1", _p1);
Debug.locals.put("method", _method);
 BA.debugLineNum = 146;BA.debugLine="Public Sub read_menu(p1 As String, method As Strin";
Debug.ShouldStop(131072);
 BA.debugLineNum = 148;BA.debugLine="Dim data, query As String";
Debug.ShouldStop(524288);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
 BA.debugLineNum = 149;BA.debugLine="query = $\" select * from table(PKG_DMS_MANAGEMENT";
Debug.ShouldStop(1048576);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_menuapps('"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p1))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 150;BA.debugLine="data = exequery.multi_string(query, method)";
Debug.ShouldStop(2097152);
_data = clsmanagement._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 151;BA.debugLine="Return data";
Debug.ShouldStop(4194304);
if (true) return _data;
 BA.debugLineNum = 153;BA.debugLine="End Sub";
Debug.ShouldStop(16777216);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _read_roles(RemoteObject _p1,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("read_roles (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,79);
if (RapidSub.canDelegate("read_roles")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","read_roles", _p1, _method);
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
Debug.locals.put("p1", _p1);
Debug.locals.put("method", _method);
 BA.debugLineNum = 79;BA.debugLine="Public Sub read_roles(p1 As String, method As Stri";
Debug.ShouldStop(16384);
 BA.debugLineNum = 80;BA.debugLine="Dim data, query As String";
Debug.ShouldStop(32768);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
 BA.debugLineNum = 81;BA.debugLine="query = \"select * from roles\"";
Debug.ShouldStop(65536);
_query = BA.ObjectToString("select * from roles");Debug.locals.put("query", _query);
 BA.debugLineNum = 82;BA.debugLine="data = exe_query(query, method)";
Debug.ShouldStop(131072);
_data = _exe_query(_query,_method);Debug.locals.put("data", _data);
 BA.debugLineNum = 83;BA.debugLine="Return data";
Debug.ShouldStop(262144);
if (true) return _data;
 BA.debugLineNum = 84;BA.debugLine="End Sub";
Debug.ShouldStop(524288);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _save_menu(RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _p5,RemoteObject _p6,RemoteObject _p7,RemoteObject _p8,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("save_menu (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,155);
if (RapidSub.canDelegate("save_menu")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","save_menu", _p1, _p2, _p3, _p4, _p5, _p6, _p7, _p8, _method);
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("p5", _p5);
Debug.locals.put("p6", _p6);
Debug.locals.put("p7", _p7);
Debug.locals.put("p8", _p8);
Debug.locals.put("method", _method);
 BA.debugLineNum = 155;BA.debugLine="Public Sub save_menu(p1 As String,p2 As String,p3";
Debug.ShouldStop(67108864);
 BA.debugLineNum = 157;BA.debugLine="Dim data, query As String";
Debug.ShouldStop(268435456);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
 BA.debugLineNum = 158;BA.debugLine="query = $\" select * from table(PKG_DMS_MANAGEMENT";
Debug.ShouldStop(536870912);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_savemenu('"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p1))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p2))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p3))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p4))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p5))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p6))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p7))),RemoteObject.createImmutable("',"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p8))),RemoteObject.createImmutable(")) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 159;BA.debugLine="data = exequery.multi_string(query, method)";
Debug.ShouldStop(1073741824);
_data = clsmanagement._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 160;BA.debugLine="Return data";
Debug.ShouldStop(-2147483648);
if (true) return _data;
 BA.debugLineNum = 162;BA.debugLine="End Sub";
Debug.ShouldStop(2);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _save_user(RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _p5,RemoteObject _p6,RemoteObject _p7,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("save_user (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,127);
if (RapidSub.canDelegate("save_user")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","save_user", _p1, _p2, _p3, _p4, _p5, _p6, _p7, _method);
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("p5", _p5);
Debug.locals.put("p6", _p6);
Debug.locals.put("p7", _p7);
Debug.locals.put("method", _method);
 BA.debugLineNum = 127;BA.debugLine="Public Sub save_user(p1 As String, p2 As String, p";
Debug.ShouldStop(1073741824);
 BA.debugLineNum = 128;BA.debugLine="Dim data, query As String";
Debug.ShouldStop(-2147483648);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
 BA.debugLineNum = 129;BA.debugLine="query = $\" select * from table(PKG_DMS_MANAGEMENT";
Debug.ShouldStop(1);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_saveuser('"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p1))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p2))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p3))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p4))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p5))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p6))),RemoteObject.createImmutable("','"),clsmanagement.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p7))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 130;BA.debugLine="data = exequery.multi_string(query, method)";
Debug.ShouldStop(2);
_data = clsmanagement._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 131;BA.debugLine="Return data";
Debug.ShouldStop(4);
if (true) return _data;
 BA.debugLineNum = 132;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _update_roles(RemoteObject _p1,RemoteObject _p2,RemoteObject _p3,RemoteObject _p4,RemoteObject _p5,RemoteObject _p6,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("update_roles (clsmanagement) ","clsmanagement",6,clsmanagement.ba,clsmanagement.mostCurrent,86);
if (RapidSub.canDelegate("update_roles")) return b4j.example.clsmanagement.remoteMe.runUserSub(false, "clsmanagement","update_roles", _p1, _p2, _p3, _p4, _p5, _p6, _method);
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
Debug.locals.put("p1", _p1);
Debug.locals.put("p2", _p2);
Debug.locals.put("p3", _p3);
Debug.locals.put("p4", _p4);
Debug.locals.put("p5", _p5);
Debug.locals.put("p6", _p6);
Debug.locals.put("method", _method);
 BA.debugLineNum = 86;BA.debugLine="Public Sub update_roles(p1 As String,  p2 As Strin";
Debug.ShouldStop(2097152);
 BA.debugLineNum = 87;BA.debugLine="Dim data, query As String";
Debug.ShouldStop(4194304);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
 BA.debugLineNum = 88;BA.debugLine="query = \"select * from table(PKG_DMS_MANAGEMENT_T";
Debug.ShouldStop(8388608);
_query = RemoteObject.concat(RemoteObject.createImmutable("select * from table(PKG_DMS_MANAGEMENT_TRE.fn_editrole('"),_p1,RemoteObject.createImmutable("','"),_p2,RemoteObject.createImmutable("', '"),_p3,RemoteObject.createImmutable("', '"),_p4,RemoteObject.createImmutable("', '"),_p5,RemoteObject.createImmutable("', "),_p6,RemoteObject.createImmutable("))"));Debug.locals.put("query", _query);
 BA.debugLineNum = 89;BA.debugLine="data = exe_query(query, method)";
Debug.ShouldStop(16777216);
_data = _exe_query(_query,_method);Debug.locals.put("data", _data);
 BA.debugLineNum = 90;BA.debugLine="Return data";
Debug.ShouldStop(33554432);
if (true) return _data;
 BA.debugLineNum = 91;BA.debugLine="End Sub";
Debug.ShouldStop(67108864);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}