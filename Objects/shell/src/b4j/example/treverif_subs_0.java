package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class treverif_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private clsid As String";
treverif._clsid = RemoteObject.createImmutable("");__ref.setField("_clsid",treverif._clsid);
 //BA.debugLineNum = 4;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _handle(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Handle (treverif) ","treverif",4,__ref.getField(false, "ba"),__ref,10);
if (RapidSub.canDelegate("handle")) return __ref.runUserSub(false, "treverif","handle", __ref, _req, _resp);
RemoteObject _appid = RemoteObject.createImmutable("");
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _method = RemoteObject.createImmutable("");
RemoteObject _response = RemoteObject.createImmutable("");
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 10;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
Debug.ShouldStop(512);
 BA.debugLineNum = 11;BA.debugLine="Dim appid, hasil, method As String";
Debug.ShouldStop(1024);
_appid = RemoteObject.createImmutable("");Debug.locals.put("appid", _appid);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
_method = RemoteObject.createImmutable("");Debug.locals.put("method", _method);
 BA.debugLineNum = 12;BA.debugLine="Dim response As String";
Debug.ShouldStop(2048);
_response = RemoteObject.createImmutable("");Debug.locals.put("response", _response);
 BA.debugLineNum = 14;BA.debugLine="If req.Method <> \"POST\" Then";
Debug.ShouldStop(8192);
if (RemoteObject.solveBoolean("!",_req.runMethod(true,"getMethod"),BA.ObjectToString("POST"))) { 
 BA.debugLineNum = 15;BA.debugLine="resp.SendError(500, \"method not supported.\")";
Debug.ShouldStop(16384);
_resp.runVoidMethodAndSync ("SendError",(Object)(BA.numberCast(int.class, 500)),(Object)(RemoteObject.createImmutable("method not supported.")));
 BA.debugLineNum = 16;BA.debugLine="Return";
Debug.ShouldStop(32768);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 20;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
Debug.ShouldStop(524288);
_appid = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("clsakses")));Debug.locals.put("appid", _appid);
 BA.debugLineNum = 22;BA.debugLine="method = req.GetParameter(\"method\")";
Debug.ShouldStop(2097152);
_method = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("method")));Debug.locals.put("method", _method);
 BA.debugLineNum = 24;BA.debugLine="If appid <> clsid Then";
Debug.ShouldStop(8388608);
if (RemoteObject.solveBoolean("!",_appid,__ref.getField(true,"_clsid"))) { 
 BA.debugLineNum = 25;BA.debugLine="response = modGenerateReturn.return_json(modGene";
Debug.ShouldStop(16777216);
_response = treverif._modgeneratereturn.runMethod(true,"_return_json",(Object)(treverif._modgeneratereturn._label_rejected),(Object)(treverif._modgeneratereturn._kode_rejected_class),(Object)(_method));Debug.locals.put("response", _response);
 BA.debugLineNum = 26;BA.debugLine="resp.Write(response)";
Debug.ShouldStop(33554432);
_resp.runVoidMethod ("Write",(Object)(_response));
 BA.debugLineNum = 27;BA.debugLine="Return";
Debug.ShouldStop(67108864);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 30;BA.debugLine="Select method";
Debug.ShouldStop(536870912);
switch (BA.switchObjectToInt(_method,BA.ObjectToString("verif_staffuip"),BA.ObjectToString("verif_uip"))) {
case 0: {
 BA.debugLineNum = 32;BA.debugLine="resp.Write(send_staffuip(req.GetParameter(\"idsu";
Debug.ShouldStop(-2147483648);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.treverif.class, "_send_staffuip",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idsurat_mohonbayar")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("status")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("tipebayar")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("no_surat")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("tgl_surat")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("ket")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("username")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("path")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("filename")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("total")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("kode_doc")))),(Object)(_method))));
 break; }
case 1: {
 BA.debugLineNum = 34;BA.debugLine="resp.Write(send_uip(req.GetParameter(\"idsurat_m";
Debug.ShouldStop(2);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.treverif.class, "_send_uip",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idsurat_mohonbayar")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("status")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("tipebayar")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("no_surat")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("tgl_surat")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("username")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("ket")))),(Object)(_method))));
 break; }
default: {
 BA.debugLineNum = 36;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
Debug.ShouldStop(8);
_hasil = treverif._modgeneratereturn.runMethod(true,"_return_json",(Object)(BA.ObjectToString("TIDAK ADA METHOD")),(Object)(BA.ObjectToString("404")),(Object)(_method));Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 37;BA.debugLine="resp.Write(hasil)";
Debug.ShouldStop(16);
_resp.runVoidMethod ("Write",(Object)(_hasil));
 break; }
}
;
 BA.debugLineNum = 40;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (treverif) ","treverif",4,__ref.getField(false, "ba"),__ref,6);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "treverif","initialize", __ref, _ba);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
 BA.debugLineNum = 6;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(32);
 BA.debugLineNum = 7;BA.debugLine="clsid = \"tre_verifikasi\"";
Debug.ShouldStop(64);
__ref.setField ("_clsid",BA.ObjectToString("tre_verifikasi"));
 BA.debugLineNum = 8;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _send_staffuip(RemoteObject __ref,RemoteObject _p_idsurat,RemoteObject _p_status,RemoteObject _p_tipe_bayar,RemoteObject _p_no_surat,RemoteObject _p_tglsurat,RemoteObject _p_ket,RemoteObject _p_username,RemoteObject _p_path_file,RemoteObject _p_filename,RemoteObject _p_totalfile,RemoteObject _p_kode_doc,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("send_staffuip (treverif) ","treverif",4,__ref.getField(false, "ba"),__ref,47);
if (RapidSub.canDelegate("send_staffuip")) return __ref.runUserSub(false, "treverif","send_staffuip", __ref, _p_idsurat, _p_status, _p_tipe_bayar, _p_no_surat, _p_tglsurat, _p_ket, _p_username, _p_path_file, _p_filename, _p_totalfile, _p_kode_doc, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
Debug.locals.put("p_idsurat", _p_idsurat);
Debug.locals.put("p_status", _p_status);
Debug.locals.put("p_tipe_bayar", _p_tipe_bayar);
Debug.locals.put("p_no_surat", _p_no_surat);
Debug.locals.put("p_tglsurat", _p_tglsurat);
Debug.locals.put("p_ket", _p_ket);
Debug.locals.put("p_username", _p_username);
Debug.locals.put("p_path_file", _p_path_file);
Debug.locals.put("p_filename", _p_filename);
Debug.locals.put("p_totalfile", _p_totalfile);
Debug.locals.put("p_kode_doc", _p_kode_doc);
Debug.locals.put("method", _method);
 BA.debugLineNum = 47;BA.debugLine="Private Sub send_staffuip(p_idsurat As String, p_s";
Debug.ShouldStop(16384);
 BA.debugLineNum = 48;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(32768);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 49;BA.debugLine="p_totalfile = Regex.Split(\",\", p_tglsurat).Length";
Debug.ShouldStop(65536);
_p_totalfile = BA.NumberToString(treverif.__c.getField(false,"Regex").runMethod(false,"Split",(Object)(BA.ObjectToString(",")),(Object)(_p_tglsurat)).getField(true,"length"));Debug.locals.put("p_totalfile", _p_totalfile);
 BA.debugLineNum = 50;BA.debugLine="query = $\" select * from table(PKG_DMS_VERIFIKASI";
Debug.ShouldStop(131072);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_VERIFIKASI.fn_disposisi_uip_staff('"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_idsurat))),RemoteObject.createImmutable("','"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_status))),RemoteObject.createImmutable("', '"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_tipe_bayar))),RemoteObject.createImmutable("'\n"),RemoteObject.createImmutable("	'"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_no_surat))),RemoteObject.createImmutable("','"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_tglsurat))),RemoteObject.createImmutable("','"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_ket))),RemoteObject.createImmutable("','"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_username))),RemoteObject.createImmutable("', '"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_path_file))),RemoteObject.createImmutable("', '"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_filename))),RemoteObject.createImmutable("', "),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_totalfile))),RemoteObject.createImmutable(", \n"),RemoteObject.createImmutable("	'"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_kode_doc))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 53;BA.debugLine="data  = exequery.multi_string(query, method)";
Debug.ShouldStop(1048576);
_data = treverif._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 54;BA.debugLine="Return data";
Debug.ShouldStop(2097152);
if (true) return _data;
 BA.debugLineNum = 55;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _send_uip(RemoteObject __ref,RemoteObject _p_idsurat,RemoteObject _p_status,RemoteObject _p_tipe_bayar,RemoteObject _p_no_surat,RemoteObject _p_tglsurat,RemoteObject _p_username,RemoteObject _p_ket,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("send_uip (treverif) ","treverif",4,__ref.getField(false, "ba"),__ref,59);
if (RapidSub.canDelegate("send_uip")) return __ref.runUserSub(false, "treverif","send_uip", __ref, _p_idsurat, _p_status, _p_tipe_bayar, _p_no_surat, _p_tglsurat, _p_username, _p_ket, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
Debug.locals.put("p_idsurat", _p_idsurat);
Debug.locals.put("p_status", _p_status);
Debug.locals.put("p_tipe_bayar", _p_tipe_bayar);
Debug.locals.put("p_no_surat", _p_no_surat);
Debug.locals.put("p_tglsurat", _p_tglsurat);
Debug.locals.put("p_username", _p_username);
Debug.locals.put("p_ket", _p_ket);
Debug.locals.put("method", _method);
 BA.debugLineNum = 59;BA.debugLine="Private Sub send_uip(p_idsurat As String, p_status";
Debug.ShouldStop(67108864);
 BA.debugLineNum = 60;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(134217728);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 62;BA.debugLine="query = $\" select * from table(PKG_DMS_VERIFIKASI";
Debug.ShouldStop(536870912);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_VERIFIKASI.fn_disposisi_uip('"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_idsurat))),RemoteObject.createImmutable("','"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_status))),RemoteObject.createImmutable("',\n"),RemoteObject.createImmutable("	'"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_no_surat))),RemoteObject.createImmutable("','"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_tglsurat))),RemoteObject.createImmutable("','"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_username))),RemoteObject.createImmutable("','"),treverif.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_ket))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 64;BA.debugLine="data  = exequery.multi_string(query, method)";
Debug.ShouldStop(-2147483648);
_data = treverif._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 65;BA.debugLine="Return data";
Debug.ShouldStop(1);
if (true) return _data;
 BA.debugLineNum = 66;BA.debugLine="End Sub";
Debug.ShouldStop(2);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}