
package b4j.example;

import java.io.IOException;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RDebug;
import anywheresoftware.b4a.pc.RemoteObject;
import anywheresoftware.b4a.pc.RDebug.IRemote;
import anywheresoftware.b4a.pc.Debug;
import anywheresoftware.b4a.pc.B4XTypes.B4XClass;
import anywheresoftware.b4a.pc.B4XTypes.DeviceClass;

public class exequery implements IRemote{
	public static exequery mostCurrent;
	public static RemoteObject ba;
    public static boolean processGlobalsRun;
    public static RemoteObject myClass;
    public static RemoteObject remoteMe;
	public exequery() {
		mostCurrent = this;
	}
    public RemoteObject getRemoteMe() {
        return remoteMe;    
    }
    
public boolean isSingleton() {
		return true;
	}
    static {
		mostCurrent = new exequery();
		remoteMe = RemoteObject.declareNull("b4j.example.exequery");
        anywheresoftware.b4a.pc.RapidSub.moduleToObject.put(new B4XClass("exequery"), "b4j.example.exequery");
	}
    public static void main (String[] args) throws Exception {
		new RDebug(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), args[3]);
		RDebug.INSTANCE.waitForTask();

	}
    private static PCBA pcBA = new PCBA(null, exequery.class);
	public static RemoteObject runMethod(boolean notUsed, String method, Object... args) throws Exception{
		return (RemoteObject) pcBA.raiseEvent(method.substring(1), args);
	}
    public static void runVoidMethod(String method, Object... args) throws Exception{
		runMethod(false, method, args);
	}
    public static RemoteObject getObject() {
		return myClass;
	 }
	public PCBA create(Object[] args) throws ClassNotFoundException{
		ba = (RemoteObject) args[1];
		pcBA = new PCBA(this, exequery.class);
        main_subs_0.initializeProcessGlobals();
		return pcBA;
	}
public static RemoteObject __c = RemoteObject.declareNull("anywheresoftware.b4a.keywords.Common");
public static RemoteObject _todb = RemoteObject.declareNull("b4j.example.clsdb");
public static RemoteObject _todbdms = RemoteObject.declareNull("b4j.example.clsdbdms");
public static b4j.example.main _main = null;
public static b4j.example.clsmanagement _clsmanagement = null;
public static b4j.example.clsmaster _clsmaster = null;
public static b4j.example.modgeneratereturn _modgeneratereturn = null;
  public Object[] GetGlobals() {
		return new Object[] {"clsManagement",Debug.moduleToString(b4j.example.clsmanagement.class),"clsMaster",Debug.moduleToString(b4j.example.clsmaster.class),"Main",Debug.moduleToString(b4j.example.main.class),"modGenerateReturn",Debug.moduleToString(b4j.example.modgeneratereturn.class),"todb",exequery._todb,"todbdms",exequery._todbdms};
}
}