package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class trenotiftas_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private clsid As String";
trenotiftas._clsid = RemoteObject.createImmutable("");__ref.setField("_clsid",trenotiftas._clsid);
 //BA.debugLineNum = 4;BA.debugLine="Private todb As clsDB";
trenotiftas._todb = RemoteObject.createNew ("b4j.example.clsdb");__ref.setField("_todb",trenotiftas._todb);
 //BA.debugLineNum = 5;BA.debugLine="Private todbdms As clsDBDms";
trenotiftas._todbdms = RemoteObject.createNew ("b4j.example.clsdbdms");__ref.setField("_todbdms",trenotiftas._todbdms);
 //BA.debugLineNum = 6;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _handle(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Handle (trenotiftas) ","trenotiftas",12,__ref.getField(false, "ba"),__ref,16);
if (RapidSub.canDelegate("handle")) return __ref.runUserSub(false, "trenotiftas","handle", __ref, _req, _resp);
RemoteObject _appid = RemoteObject.createImmutable("");
RemoteObject _response = RemoteObject.createImmutable("");
RemoteObject _worknumb = RemoteObject.createImmutable("");
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _username = RemoteObject.createImmutable("");
RemoteObject _strutl = RemoteObject.declareNull("anywheresoftware.b4a.objects.StringUtils");
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 16;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
Debug.ShouldStop(32768);
 BA.debugLineNum = 17;BA.debugLine="Dim appid, response, worknumb, hasil, username As";
Debug.ShouldStop(65536);
_appid = RemoteObject.createImmutable("");Debug.locals.put("appid", _appid);
_response = RemoteObject.createImmutable("");Debug.locals.put("response", _response);
_worknumb = RemoteObject.createImmutable("");Debug.locals.put("worknumb", _worknumb);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
_username = RemoteObject.createImmutable("");Debug.locals.put("username", _username);
 BA.debugLineNum = 18;BA.debugLine="Dim strutl As StringUtils";
Debug.ShouldStop(131072);
_strutl = RemoteObject.createNew ("anywheresoftware.b4a.objects.StringUtils");Debug.locals.put("strutl", _strutl);
 BA.debugLineNum = 21;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
Debug.ShouldStop(1048576);
_appid = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("clsakses")));Debug.locals.put("appid", _appid);
 BA.debugLineNum = 25;BA.debugLine="If appid <> clsid Then";
Debug.ShouldStop(16777216);
if (RemoteObject.solveBoolean("!",_appid,__ref.getField(true,"_clsid"))) { 
 BA.debugLineNum = 26;BA.debugLine="response = modGenerateReturn.return_json(modGene";
Debug.ShouldStop(33554432);
_response = trenotiftas._modgeneratereturn.runMethod(true,"_return_json",(Object)(trenotiftas._modgeneratereturn._label_rejected),(Object)(trenotiftas._modgeneratereturn._kode_rejected_class),(Object)(RemoteObject.createImmutable("Notif")));Debug.locals.put("response", _response);
 BA.debugLineNum = 27;BA.debugLine="resp.Write(response)";
Debug.ShouldStop(67108864);
_resp.runVoidMethod ("Write",(Object)(_response));
 BA.debugLineNum = 28;BA.debugLine="Return";
Debug.ShouldStop(134217728);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 31;BA.debugLine="username = req.GetParameter(\"username\")";
Debug.ShouldStop(1073741824);
_username = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("username")));Debug.locals.put("username", _username);
 BA.debugLineNum = 32;BA.debugLine="worknumb = strutl.DecodeUrl(req.GetParameter(\"wor";
Debug.ShouldStop(-2147483648);
_worknumb = _strutl.runMethod(true,"DecodeUrl",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("worknumb")))),(Object)(RemoteObject.createImmutable("UTF8")));Debug.locals.put("worknumb", _worknumb);
 BA.debugLineNum = 34;BA.debugLine="hasil = load_notif(username, worknumb)";
Debug.ShouldStop(2);
_hasil = __ref.runClassMethod (b4j.example.trenotiftas.class, "_load_notif",(Object)(_username),(Object)(_worknumb));Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 36;BA.debugLine="resp.Write(hasil)";
Debug.ShouldStop(8);
_resp.runVoidMethod ("Write",(Object)(_hasil));
 BA.debugLineNum = 38;BA.debugLine="End Sub";
Debug.ShouldStop(32);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (trenotiftas) ","trenotiftas",12,__ref.getField(false, "ba"),__ref,8);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "trenotiftas","initialize", __ref, _ba);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
 BA.debugLineNum = 8;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(128);
 BA.debugLineNum = 9;BA.debugLine="clsid = \"tre_notif\"";
Debug.ShouldStop(256);
__ref.setField ("_clsid",BA.ObjectToString("tre_notif"));
 BA.debugLineNum = 10;BA.debugLine="todb.Initialize";
Debug.ShouldStop(512);
__ref.getField(false,"_todb").runClassMethod (b4j.example.clsdb.class, "_initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 11;BA.debugLine="todbdms.Initialize";
Debug.ShouldStop(1024);
__ref.getField(false,"_todbdms").runClassMethod (b4j.example.clsdbdms.class, "_initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 13;BA.debugLine="End Sub";
Debug.ShouldStop(4096);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _load_notif(RemoteObject __ref,RemoteObject _username,RemoteObject _worknumb) throws Exception{
try {
		Debug.PushSubsStack("load_notif (trenotiftas) ","trenotiftas",12,__ref.getField(false, "ba"),__ref,41);
if (RapidSub.canDelegate("load_notif")) return __ref.runUserSub(false, "trenotiftas","load_notif", __ref, _username, _worknumb);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _worknumblst = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _totaldata = RemoteObject.createImmutable(0);
RemoteObject _totalcurr = RemoteObject.createImmutable(0);
RemoteObject _bagidec = RemoteObject.createImmutable(0f);
RemoteObject _bagiint = RemoteObject.createImmutable(0);
int _i = 0;
int _x = 0;
Debug.locals.put("username", _username);
Debug.locals.put("worknumb", _worknumb);
 BA.debugLineNum = 41;BA.debugLine="Private Sub load_notif(username As String, worknum";
Debug.ShouldStop(256);
 BA.debugLineNum = 42;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(512);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 43;BA.debugLine="Dim worknumbLst As List";
Debug.ShouldStop(1024);
_worknumblst = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("worknumbLst", _worknumblst);
 BA.debugLineNum = 44;BA.debugLine="Dim totalData, totalcurr As Int";
Debug.ShouldStop(2048);
_totaldata = RemoteObject.createImmutable(0);Debug.locals.put("totalData", _totaldata);
_totalcurr = RemoteObject.createImmutable(0);Debug.locals.put("totalcurr", _totalcurr);
 BA.debugLineNum = 45;BA.debugLine="Dim bagidec As Float";
Debug.ShouldStop(4096);
_bagidec = RemoteObject.createImmutable(0f);Debug.locals.put("bagidec", _bagidec);
 BA.debugLineNum = 46;BA.debugLine="Dim bagiint As Int";
Debug.ShouldStop(8192);
_bagiint = RemoteObject.createImmutable(0);Debug.locals.put("bagiint", _bagiint);
 BA.debugLineNum = 47;BA.debugLine="worknumbLst.Initialize";
Debug.ShouldStop(16384);
_worknumblst.runVoidMethod ("Initialize");
 BA.debugLineNum = 48;BA.debugLine="totalData = Regex.Split(\",\", worknumb).Length";
Debug.ShouldStop(32768);
_totaldata = trenotiftas.__c.getField(false,"Regex").runMethod(false,"Split",(Object)(BA.ObjectToString(",")),(Object)(_worknumb)).getField(true,"length");Debug.locals.put("totalData", _totaldata);
 BA.debugLineNum = 49;BA.debugLine="worknumbLst = Regex.Split(\",\", worknumb)";
Debug.ShouldStop(65536);
_worknumblst = trenotiftas.__c.runMethod(false, "ArrayToList", (Object)(trenotiftas.__c.getField(false,"Regex").runMethod(false,"Split",(Object)(BA.ObjectToString(",")),(Object)(_worknumb))));Debug.locals.put("worknumbLst", _worknumblst);
 BA.debugLineNum = 50;BA.debugLine="totalcurr = 0";
Debug.ShouldStop(131072);
_totalcurr = BA.numberCast(int.class, 0);Debug.locals.put("totalcurr", _totalcurr);
 BA.debugLineNum = 51;BA.debugLine="If totalData > 100 Then";
Debug.ShouldStop(262144);
if (RemoteObject.solveBoolean(">",_totaldata,BA.numberCast(double.class, 100))) { 
 BA.debugLineNum = 52;BA.debugLine="bagidec = totalData / 50";
Debug.ShouldStop(524288);
_bagidec = BA.numberCast(float.class, RemoteObject.solve(new RemoteObject[] {_totaldata,RemoteObject.createImmutable(50)}, "/",0, 0));Debug.locals.put("bagidec", _bagidec);
 BA.debugLineNum = 53;BA.debugLine="bagiint = bagidec";
Debug.ShouldStop(1048576);
_bagiint = BA.numberCast(int.class, _bagidec);Debug.locals.put("bagiint", _bagiint);
 BA.debugLineNum = 55;BA.debugLine="If bagidec > bagiint Then bagiint = bagiint + 1";
Debug.ShouldStop(4194304);
if (RemoteObject.solveBoolean(">",_bagidec,BA.numberCast(double.class, _bagiint))) { 
_bagiint = RemoteObject.solve(new RemoteObject[] {_bagiint,RemoteObject.createImmutable(1)}, "+",1, 1);Debug.locals.put("bagiint", _bagiint);};
 BA.debugLineNum = 56;BA.debugLine="For i = 0 To bagiint - 1";
Debug.ShouldStop(8388608);
{
final int step14 = 1;
final int limit14 = RemoteObject.solve(new RemoteObject[] {_bagiint,RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = 0 ;
for (;(step14 > 0 && _i <= limit14) || (step14 < 0 && _i >= limit14) ;_i = ((int)(0 + _i + step14))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 57;BA.debugLine="worknumb = \"\"";
Debug.ShouldStop(16777216);
_worknumb = BA.ObjectToString("");Debug.locals.put("worknumb", _worknumb);
 BA.debugLineNum = 58;BA.debugLine="If totalcurr > (totalData - totalcurr) Then";
Debug.ShouldStop(33554432);
if (RemoteObject.solveBoolean(">",_totalcurr,BA.numberCast(double.class, (RemoteObject.solve(new RemoteObject[] {_totaldata,_totalcurr}, "-",1, 1))))) { 
 BA.debugLineNum = 59;BA.debugLine="For x = totalcurr To totalData - 1";
Debug.ShouldStop(67108864);
{
final int step17 = 1;
final int limit17 = RemoteObject.solve(new RemoteObject[] {_totaldata,RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_x = _totalcurr.<Integer>get().intValue() ;
for (;(step17 > 0 && _x <= limit17) || (step17 < 0 && _x >= limit17) ;_x = ((int)(0 + _x + step17))  ) {
Debug.locals.put("x", _x);
 BA.debugLineNum = 60;BA.debugLine="If x = totalcurr Then";
Debug.ShouldStop(134217728);
if (RemoteObject.solveBoolean("=",RemoteObject.createImmutable(_x),BA.numberCast(double.class, _totalcurr))) { 
 BA.debugLineNum = 61;BA.debugLine="worknumb = worknumbLst.Get(x)";
Debug.ShouldStop(268435456);
_worknumb = BA.ObjectToString(_worknumblst.runMethod(false,"Get",(Object)(BA.numberCast(int.class, _x))));Debug.locals.put("worknumb", _worknumb);
 }else {
 BA.debugLineNum = 63;BA.debugLine="worknumb = worknumb & \",\" & worknumbLst.Get(";
Debug.ShouldStop(1073741824);
_worknumb = RemoteObject.concat(_worknumb,RemoteObject.createImmutable(","),_worknumblst.runMethod(false,"Get",(Object)(BA.numberCast(int.class, _x))));Debug.locals.put("worknumb", _worknumb);
 };
 }
}Debug.locals.put("x", _x);
;
 BA.debugLineNum = 66;BA.debugLine="query = $\" select * from table(PKG_DMS_WORKFLO";
Debug.ShouldStop(2);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_WORKFLOW.fn_save_notif_worknumber('"),trenotiftas.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_username))),RemoteObject.createImmutable("', '"),trenotiftas.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_worknumb))),RemoteObject.createImmutable("', "),trenotiftas.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((trenotiftas.__c.getField(false,"Regex").runMethod(false,"Split",(Object)(BA.ObjectToString(",")),(Object)(_worknumb)).getField(true,"length")))),RemoteObject.createImmutable(")) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 67;BA.debugLine="data  = exequery.multi_string(query, \"notif\")";
Debug.ShouldStop(4);
_data = trenotiftas._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(RemoteObject.createImmutable("notif")));Debug.locals.put("data", _data);
 }else {
 BA.debugLineNum = 69;BA.debugLine="For x = totalcurr To (totalcurr + 49) - 1";
Debug.ShouldStop(16);
{
final int step27 = 1;
final int limit27 = RemoteObject.solve(new RemoteObject[] {(RemoteObject.solve(new RemoteObject[] {_totalcurr,RemoteObject.createImmutable(49)}, "+",1, 1)),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_x = _totalcurr.<Integer>get().intValue() ;
for (;(step27 > 0 && _x <= limit27) || (step27 < 0 && _x >= limit27) ;_x = ((int)(0 + _x + step27))  ) {
Debug.locals.put("x", _x);
 BA.debugLineNum = 70;BA.debugLine="If x = totalcurr Then";
Debug.ShouldStop(32);
if (RemoteObject.solveBoolean("=",RemoteObject.createImmutable(_x),BA.numberCast(double.class, _totalcurr))) { 
 BA.debugLineNum = 71;BA.debugLine="worknumb = worknumbLst.Get(x)";
Debug.ShouldStop(64);
_worknumb = BA.ObjectToString(_worknumblst.runMethod(false,"Get",(Object)(BA.numberCast(int.class, _x))));Debug.locals.put("worknumb", _worknumb);
 }else {
 BA.debugLineNum = 73;BA.debugLine="worknumb = worknumb & \",\" & worknumbLst.Get(";
Debug.ShouldStop(256);
_worknumb = RemoteObject.concat(_worknumb,RemoteObject.createImmutable(","),_worknumblst.runMethod(false,"Get",(Object)(BA.numberCast(int.class, _x))));Debug.locals.put("worknumb", _worknumb);
 };
 }
}Debug.locals.put("x", _x);
;
 BA.debugLineNum = 76;BA.debugLine="query = $\" select * from table(PKG_DMS_WORKFLO";
Debug.ShouldStop(2048);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_WORKFLOW.fn_save_notif_worknumber('"),trenotiftas.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_username))),RemoteObject.createImmutable("', '"),trenotiftas.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_worknumb))),RemoteObject.createImmutable("', "),trenotiftas.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((trenotiftas.__c.getField(false,"Regex").runMethod(false,"Split",(Object)(BA.ObjectToString(",")),(Object)(_worknumb)).getField(true,"length")))),RemoteObject.createImmutable(")) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 77;BA.debugLine="data  = exequery.multi_string(query, \"notif\")";
Debug.ShouldStop(4096);
_data = trenotiftas._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(RemoteObject.createImmutable("notif")));Debug.locals.put("data", _data);
 };
 BA.debugLineNum = 79;BA.debugLine="totalcurr = totalcurr + 49";
Debug.ShouldStop(16384);
_totalcurr = RemoteObject.solve(new RemoteObject[] {_totalcurr,RemoteObject.createImmutable(49)}, "+",1, 1);Debug.locals.put("totalcurr", _totalcurr);
 }
}Debug.locals.put("i", _i);
;
 };
 BA.debugLineNum = 85;BA.debugLine="Return data";
Debug.ShouldStop(1048576);
if (true) return _data;
 BA.debugLineNum = 86;BA.debugLine="End Sub";
Debug.ShouldStop(2097152);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}