package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class main_subs_0 {


public static RemoteObject  _appendsqllog(RemoteObject _logdata) throws Exception{
try {
		Debug.PushSubsStack("appendSqlLog (main) ","main",0,main.ba,main.mostCurrent,106);
if (RapidSub.canDelegate("appendsqllog")) return b4j.example.main.remoteMe.runUserSub(false, "main","appendsqllog", _logdata);
RemoteObject _bin = RemoteObject.declareNull("b4j.example.jstringfunctions");
RemoteObject _tahun = RemoteObject.createImmutable("");
RemoteObject _bulan = RemoteObject.createImmutable("");
RemoteObject _tgl = RemoteObject.createImmutable("");
RemoteObject _kode = RemoteObject.createImmutable("");
RemoteObject _servername = RemoteObject.createImmutable("");
Debug.locals.put("logdata", _logdata);
 BA.debugLineNum = 106;BA.debugLine="Sub appendSqlLog(logdata As String)";
Debug.ShouldStop(512);
 BA.debugLineNum = 107;BA.debugLine="Dim bin As JStringFunctions";
Debug.ShouldStop(1024);
_bin = RemoteObject.createNew ("b4j.example.jstringfunctions");Debug.locals.put("bin", _bin);
 BA.debugLineNum = 109;BA.debugLine="bin.Initialize";
Debug.ShouldStop(4096);
_bin.runVoidMethod ("_initialize",main.ba);
 BA.debugLineNum = 111;BA.debugLine="Dim tahun As String = DateTime.GetYear(DateTime.N";
Debug.ShouldStop(16384);
_tahun = BA.NumberToString(main.__c.getField(false,"DateTime").runMethod(true,"GetYear",(Object)(main.__c.getField(false,"DateTime").runMethod(true,"getNow"))));Debug.locals.put("tahun", _tahun);Debug.locals.put("tahun", _tahun);
 BA.debugLineNum = 112;BA.debugLine="Dim bulan As String =   bin.Right(\"00\" & DateTime";
Debug.ShouldStop(32768);
_bulan = _bin.runMethod(true,"_right",(Object)(RemoteObject.concat(RemoteObject.createImmutable("00"),main.__c.getField(false,"DateTime").runMethod(true,"GetMonth",(Object)(main.__c.getField(false,"DateTime").runMethod(true,"getNow"))))),(Object)(BA.numberCast(long.class, 2)));Debug.locals.put("bulan", _bulan);Debug.locals.put("bulan", _bulan);
 BA.debugLineNum = 113;BA.debugLine="Dim tgl As String =  bin.Right(DateTime.GetDayOfM";
Debug.ShouldStop(65536);
_tgl = _bin.runMethod(true,"_right",(Object)(BA.NumberToString(main.__c.getField(false,"DateTime").runMethod(true,"GetDayOfMonth",(Object)(main.__c.getField(false,"DateTime").runMethod(true,"getNow"))))),(Object)(BA.numberCast(long.class, 2)));Debug.locals.put("tgl", _tgl);Debug.locals.put("tgl", _tgl);
 BA.debugLineNum = 115;BA.debugLine="Dim kode As String =  \"[\" & tahun & \"/\" & bulan &";
Debug.ShouldStop(262144);
_kode = RemoteObject.concat(RemoteObject.createImmutable("["),_tahun,RemoteObject.createImmutable("/"),_bulan,RemoteObject.createImmutable("/"),_tgl,RemoteObject.createImmutable(" "),main.__c.getField(false,"DateTime").runMethod(true,"Time",(Object)(main.__c.getField(false,"DateTime").runMethod(true,"getNow"))),RemoteObject.createImmutable("] "));Debug.locals.put("kode", _kode);Debug.locals.put("kode", _kode);
 BA.debugLineNum = 116;BA.debugLine="Dim servername As String";
Debug.ShouldStop(524288);
_servername = RemoteObject.createImmutable("");Debug.locals.put("servername", _servername);
 BA.debugLineNum = 118;BA.debugLine="servername  = \"SERVER_\"  & \"_\" & srv.Port & \"_\" &";
Debug.ShouldStop(2097152);
_servername = RemoteObject.concat(RemoteObject.createImmutable("SERVER_"),RemoteObject.createImmutable("_"),main._srv.runMethod(true,"getPort"),RemoteObject.createImmutable("_"),_tahun,_bulan,_tgl,RemoteObject.createImmutable(".txt"));Debug.locals.put("servername", _servername);
 BA.debugLineNum = 121;BA.debugLine="NativeMe.RunMethod(\"writeFile\",  Array As String(";
Debug.ShouldStop(16777216);
main._nativeme.runVoidMethod ("RunMethod",(Object)(BA.ObjectToString("writeFile")),(Object)((RemoteObject.createNewArray("String",new int[] {2},new Object[] {RemoteObject.concat(main.__c.getField(false,"File").runMethod(true,"getDirApp"),RemoteObject.createImmutable("/logsql/"),_servername),RemoteObject.concat(_kode,_logdata)}))));
 BA.debugLineNum = 122;BA.debugLine="End Sub";
Debug.ShouldStop(33554432);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _application_error(RemoteObject _error,RemoteObject _stacktrace) throws Exception{
try {
		Debug.PushSubsStack("Application_Error (main) ","main",0,main.ba,main.mostCurrent,143);
if (RapidSub.canDelegate("application_error")) return b4j.example.main.remoteMe.runUserSub(false, "main","application_error", _error, _stacktrace);
Debug.locals.put("Error", _error);
Debug.locals.put("StackTrace", _stacktrace);
 BA.debugLineNum = 143;BA.debugLine="Sub Application_Error (Error As Exception, StackTr";
Debug.ShouldStop(16384);
 BA.debugLineNum = 144;BA.debugLine="Return True";
Debug.ShouldStop(32768);
if (true) return main.__c.getField(true,"True");
 BA.debugLineNum = 145;BA.debugLine="End Sub";
Debug.ShouldStop(65536);
return RemoteObject.createImmutable(false);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _appstart(RemoteObject _args) throws Exception{
try {
		Debug.PushSubsStack("AppStart (main) ","main",0,main.ba,main.mostCurrent,25);
if (RapidSub.canDelegate("appstart")) return b4j.example.main.remoteMe.runUserSub(false, "main","appstart", _args);
RemoteObject _ss = RemoteObject.declareNull("anywheresoftware.b4a.objects.SocketWrapper.ServerSocketWrapper");
RemoteObject _str = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("Args", _args);
 BA.debugLineNum = 25;BA.debugLine="Sub AppStart (Args() As String)";
Debug.ShouldStop(16777216);
 BA.debugLineNum = 26;BA.debugLine="Dim ss As ServerSocket";
Debug.ShouldStop(33554432);
_ss = RemoteObject.createNew ("anywheresoftware.b4a.objects.SocketWrapper.ServerSocketWrapper");Debug.locals.put("ss", _ss);
 BA.debugLineNum = 27;BA.debugLine="NativeMe = Me";
Debug.ShouldStop(67108864);
main._nativeme.setObject(main.getObject());
 BA.debugLineNum = 28;BA.debugLine="param.Initialize";
Debug.ShouldStop(134217728);
main._param.runVoidMethod ("Initialize");
 BA.debugLineNum = 29;BA.debugLine="paramdbDMS.Initialize";
Debug.ShouldStop(268435456);
main._paramdbdms.runVoidMethod ("Initialize");
 BA.debugLineNum = 30;BA.debugLine="Log(\" \")";
Debug.ShouldStop(536870912);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 31;BA.debugLine="Log(\"********************************************";
Debug.ShouldStop(1073741824);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("****************************************************")));
 BA.debugLineNum = 32;BA.debugLine="Log(\"     Copyright (c) 2017\"  )";
Debug.ShouldStop(-2147483648);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("     Copyright (c) 2017")));
 BA.debugLineNum = 33;BA.debugLine="Log(\"     SBU TI - ICON+ \")";
Debug.ShouldStop(1);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("     SBU TI - ICON+ ")));
 BA.debugLineNum = 34;BA.debugLine="Log(\"     Webservice API DMS Treasury, Jetty Serv";
Debug.ShouldStop(2);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("     Webservice API DMS Treasury, Jetty Server 9.3")));
 BA.debugLineNum = 35;BA.debugLine="Log(\"     java 1.8 or higher \"  )";
Debug.ShouldStop(4);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("     java 1.8 or higher ")));
 BA.debugLineNum = 36;BA.debugLine="Log(\" \"  )";
Debug.ShouldStop(8);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 37;BA.debugLine="Log(\"********************************************";
Debug.ShouldStop(16);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("****************************************************")));
 BA.debugLineNum = 38;BA.debugLine="Args = Array As String(\"8821\")";
Debug.ShouldStop(32);
_args = RemoteObject.createNewArray("String",new int[] {1},new Object[] {RemoteObject.createImmutable("8821")});Debug.locals.put("Args", _args);
 BA.debugLineNum = 39;BA.debugLine="If Args.Length < 1 Then";
Debug.ShouldStop(64);
if (RemoteObject.solveBoolean("<",_args.getField(true,"length"),BA.numberCast(double.class, 1))) { 
 BA.debugLineNum = 40;BA.debugLine="Log(\" \")";
Debug.ShouldStop(128);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 41;BA.debugLine="Log(\"     Parameter Port Tidak ada     \")";
Debug.ShouldStop(256);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("     Parameter Port Tidak ada     ")));
 BA.debugLineNum = 42;BA.debugLine="Log(\" \")";
Debug.ShouldStop(512);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 43;BA.debugLine="ExitApplication";
Debug.ShouldStop(1024);
main.__c.runVoidMethod ("ExitApplication");
 };
 BA.debugLineNum = 46;BA.debugLine="Log(\" \")";
Debug.ShouldStop(8192);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 47;BA.debugLine="Log(\"     Checking file Config.json     \")";
Debug.ShouldStop(16384);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("     Checking file Config.json     ")));
 BA.debugLineNum = 48;BA.debugLine="Log(\" \")";
Debug.ShouldStop(32768);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 50;BA.debugLine="If File.Exists(File.DirApp, \"config.json\") Then";
Debug.ShouldStop(131072);
if (main.__c.getField(false,"File").runMethod(true,"Exists",(Object)(main.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(RemoteObject.createImmutable("config.json"))).<Boolean>get().booleanValue()) { 
 BA.debugLineNum = 51;BA.debugLine="Dim str As String";
Debug.ShouldStop(262144);
_str = RemoteObject.createImmutable("");Debug.locals.put("str", _str);
 BA.debugLineNum = 52;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(524288);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 53;BA.debugLine="str = File.ReadString(File.DirApp, \"config.json\"";
Debug.ShouldStop(1048576);
_str = main.__c.getField(false,"File").runMethod(true,"ReadString",(Object)(main.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(RemoteObject.createImmutable("config.json")));Debug.locals.put("str", _str);
 BA.debugLineNum = 54;BA.debugLine="json.Initialize(str)";
Debug.ShouldStop(2097152);
main._json.runVoidMethod ("Initialize",(Object)(_str));
 BA.debugLineNum = 55;BA.debugLine="mp = json.NextObject";
Debug.ShouldStop(4194304);
_mp = main._json.runMethod(false,"NextObject");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 56;BA.debugLine="param.AddAll(Array As String(mp.Get(\"ipdb\"), mp.";
Debug.ShouldStop(8388608);
main._param.runVoidMethod ("AddAll",(Object)(main.__c.runMethod(false, "ArrayToList", (Object)(RemoteObject.createNewArray("String",new int[] {4},new Object[] {BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("ipdb"))))),BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("tnsdb"))))),BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("userdb"))))),BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("pwddb")))))})))));
 BA.debugLineNum = 57;BA.debugLine="paramdbDMS.AddAll(Array As String(mp.Get(\"ipdbdm";
Debug.ShouldStop(16777216);
main._paramdbdms.runVoidMethod ("AddAll",(Object)(main.__c.runMethod(false, "ArrayToList", (Object)(RemoteObject.createNewArray("String",new int[] {4},new Object[] {BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("ipdbdms"))))),BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("tnsdbdms"))))),BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("userdbdms"))))),BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("pwddbdms")))))})))));
 }else {
 BA.debugLineNum = 59;BA.debugLine="Log(\"     Tidak ada File Config.json, Silahkan h";
Debug.ShouldStop(67108864);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("     Tidak ada File Config.json, Silahkan hubungi Team Terkait     ")));
 BA.debugLineNum = 60;BA.debugLine="Log(\"     \")";
Debug.ShouldStop(134217728);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("     ")));
 BA.debugLineNum = 61;BA.debugLine="ExitApplication";
Debug.ShouldStop(268435456);
main.__c.runVoidMethod ("ExitApplication");
 };
 BA.debugLineNum = 64;BA.debugLine="File.MakeDir(File.DirApp,\"temp\")";
Debug.ShouldStop(-2147483648);
main.__c.getField(false,"File").runVoidMethod ("MakeDir",(Object)(main.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(RemoteObject.createImmutable("temp")));
 BA.debugLineNum = 65;BA.debugLine="File.MakeDir(File.DirApp,\"log\")";
Debug.ShouldStop(1);
main.__c.getField(false,"File").runVoidMethod ("MakeDir",(Object)(main.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(RemoteObject.createImmutable("log")));
 BA.debugLineNum = 66;BA.debugLine="dirparentpath = File.Combine(File.DirApp,\"temp\")";
Debug.ShouldStop(2);
main._dirparentpath = main.__c.getField(false,"File").runMethod(true,"Combine",(Object)(main.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(RemoteObject.createImmutable("temp")));
 BA.debugLineNum = 68;BA.debugLine="Try";
Debug.ShouldStop(8);
try { BA.debugLineNum = 69;BA.debugLine="ipdb = param.Get(0)";
Debug.ShouldStop(16);
main._ipdb = BA.ObjectToString(main._param.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 0))));
 BA.debugLineNum = 70;BA.debugLine="tns = param.Get(1)";
Debug.ShouldStop(32);
main._tns = BA.ObjectToString(main._param.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 1))));
 BA.debugLineNum = 71;BA.debugLine="userdb = param.Get(2)";
Debug.ShouldStop(64);
main._userdb = BA.ObjectToString(main._param.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 2))));
 BA.debugLineNum = 72;BA.debugLine="pwddb = param.Get(3)";
Debug.ShouldStop(128);
main._pwddb = BA.ObjectToString(main._param.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 3))));
 BA.debugLineNum = 74;BA.debugLine="ipdbdms = paramdbDMS.Get(0)";
Debug.ShouldStop(512);
main._ipdbdms = BA.ObjectToString(main._paramdbdms.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 0))));
 BA.debugLineNum = 75;BA.debugLine="tnsdms = paramdbDMS.Get(1)";
Debug.ShouldStop(1024);
main._tnsdms = BA.ObjectToString(main._paramdbdms.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 1))));
 BA.debugLineNum = 76;BA.debugLine="userdbdms = paramdbDMS.Get(2)";
Debug.ShouldStop(2048);
main._userdbdms = BA.ObjectToString(main._paramdbdms.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 2))));
 BA.debugLineNum = 77;BA.debugLine="pwddbdms = paramdbDMS.Get(3)";
Debug.ShouldStop(4096);
main._pwddbdms = BA.ObjectToString(main._paramdbdms.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 3))));
 BA.debugLineNum = 79;BA.debugLine="srv.Port = Args(0)";
Debug.ShouldStop(16384);
main._srv.runMethod(true,"setPort",BA.numberCast(int.class, _args.getArrayElement(true,BA.numberCast(int.class, 0))));
 BA.debugLineNum = 80;BA.debugLine="srv.Initialize(\"serverInitialize\")";
Debug.ShouldStop(32768);
main._srv.runVoidMethod ("Initialize",main.ba,(Object)(RemoteObject.createImmutable("serverInitialize")));
 BA.debugLineNum = 81;BA.debugLine="srv.AddHandler(\"/login\", \"treLogin\", False)";
Debug.ShouldStop(65536);
main._srv.runVoidMethod ("AddHandler",(Object)(BA.ObjectToString("/login")),(Object)(BA.ObjectToString("treLogin")),(Object)(main.__c.getField(true,"False")));
 BA.debugLineNum = 82;BA.debugLine="srv.AddHandler(\"/notif\", \"trenotiftas\", False)";
Debug.ShouldStop(131072);
main._srv.runVoidMethod ("AddHandler",(Object)(BA.ObjectToString("/notif")),(Object)(BA.ObjectToString("trenotiftas")),(Object)(main.__c.getField(true,"False")));
 BA.debugLineNum = 83;BA.debugLine="srv.AddHandler(\"/getmenu\", \"tremenu\", False)";
Debug.ShouldStop(262144);
main._srv.runVoidMethod ("AddHandler",(Object)(BA.ObjectToString("/getmenu")),(Object)(BA.ObjectToString("tremenu")),(Object)(main.__c.getField(true,"False")));
 BA.debugLineNum = 85;BA.debugLine="srv.AddHandler(\"/master\", \"tremaster\", False)";
Debug.ShouldStop(1048576);
main._srv.runVoidMethod ("AddHandler",(Object)(BA.ObjectToString("/master")),(Object)(BA.ObjectToString("tremaster")),(Object)(main.__c.getField(true,"False")));
 BA.debugLineNum = 86;BA.debugLine="srv.AddHandler(\"/management\", \"tremanagement\", F";
Debug.ShouldStop(2097152);
main._srv.runVoidMethod ("AddHandler",(Object)(BA.ObjectToString("/management")),(Object)(BA.ObjectToString("tremanagement")),(Object)(main.__c.getField(true,"False")));
 BA.debugLineNum = 87;BA.debugLine="srv.AddHandler(\"/setting\", \"tredatasetting\", Fal";
Debug.ShouldStop(4194304);
main._srv.runVoidMethod ("AddHandler",(Object)(BA.ObjectToString("/setting")),(Object)(BA.ObjectToString("tredatasetting")),(Object)(main.__c.getField(true,"False")));
 BA.debugLineNum = 88;BA.debugLine="srv.AddHandler(\"/trx\", \"tretrx\", False)";
Debug.ShouldStop(8388608);
main._srv.runVoidMethod ("AddHandler",(Object)(BA.ObjectToString("/trx")),(Object)(BA.ObjectToString("tretrx")),(Object)(main.__c.getField(true,"False")));
 BA.debugLineNum = 89;BA.debugLine="srv.AddHandler(\"/verif\", \"treverif\", False)";
Debug.ShouldStop(16777216);
main._srv.runVoidMethod ("AddHandler",(Object)(BA.ObjectToString("/verif")),(Object)(BA.ObjectToString("treverif")),(Object)(main.__c.getField(true,"False")));
 BA.debugLineNum = 90;BA.debugLine="srv.AddHandler(\"/up\", \"treuseratas\", False)";
Debug.ShouldStop(33554432);
main._srv.runVoidMethod ("AddHandler",(Object)(BA.ObjectToString("/up")),(Object)(BA.ObjectToString("treuseratas")),(Object)(main.__c.getField(true,"False")));
 BA.debugLineNum = 92;BA.debugLine="srv.Start";
Debug.ShouldStop(134217728);
main._srv.runVoidMethod ("Start");
 BA.debugLineNum = 94;BA.debugLine="Log(\"Server Started --> \" & ss.GetMyIP & \":\" & s";
Debug.ShouldStop(536870912);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.concat(RemoteObject.createImmutable("Server Started --> "),_ss.runMethod(true,"GetMyIP"),RemoteObject.createImmutable(":"),main._srv.runMethod(true,"getPort"))));
 BA.debugLineNum = 95;BA.debugLine="Log(\" \")";
Debug.ShouldStop(1073741824);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 96;BA.debugLine="StartMessageLoop";
Debug.ShouldStop(-2147483648);
main.__c.runVoidMethod ("StartMessageLoop",main.ba);
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e64) {
			BA.rdebugUtils.runVoidMethod("setLastException",main.ba, e64.toString()); BA.debugLineNum = 98;BA.debugLine="appendSqlLog(LastException.Message)";
Debug.ShouldStop(2);
_appendsqllog(main.__c.runMethod(false,"LastException",main.ba).runMethod(true,"getMessage"));
 BA.debugLineNum = 99;BA.debugLine="Log(\"-------======= Gagal Menjalankan API DMS Tr";
Debug.ShouldStop(4);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("-------======= Gagal Menjalankan API DMS Treasury =======-------")));
 BA.debugLineNum = 100;BA.debugLine="Log(\"-------======= Periksa Kembali Paramter ===";
Debug.ShouldStop(8);
main.__c.runVoidMethod ("Log",(Object)(RemoteObject.createImmutable("-------======= Periksa Kembali Paramter =======-------")));
 };
 BA.debugLineNum = 103;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}

private static boolean processGlobalsRun;
public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        main_subs_0._process_globals();
exequery_subs_0._process_globals();
clsmanagement_subs_0._process_globals();
clsmaster_subs_0._process_globals();
modgeneratereturn_subs_0._process_globals();
main.myClass = BA.getDeviceClass ("b4j.example.main");
tretrx.myClass = BA.getDeviceClass ("b4j.example.tretrx");
exequery.myClass = BA.getDeviceClass ("b4j.example.exequery");
clsdb.myClass = BA.getDeviceClass ("b4j.example.clsdb");
treverif.myClass = BA.getDeviceClass ("b4j.example.treverif");
tredatasetting.myClass = BA.getDeviceClass ("b4j.example.tredatasetting");
clsmanagement.myClass = BA.getDeviceClass ("b4j.example.clsmanagement");
tremanagement.myClass = BA.getDeviceClass ("b4j.example.tremanagement");
trelogin.myClass = BA.getDeviceClass ("b4j.example.trelogin");
clsmaster.myClass = BA.getDeviceClass ("b4j.example.clsmaster");
tremaster.myClass = BA.getDeviceClass ("b4j.example.tremaster");
clsdbdms.myClass = BA.getDeviceClass ("b4j.example.clsdbdms");
trenotiftas.myClass = BA.getDeviceClass ("b4j.example.trenotiftas");
modgeneratereturn.myClass = BA.getDeviceClass ("b4j.example.modgeneratereturn");
tremenu.myClass = BA.getDeviceClass ("b4j.example.tremenu");
treuseratas.myClass = BA.getDeviceClass ("b4j.example.treuseratas");
		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static RemoteObject  _process_globals() throws Exception{
 //BA.debugLineNum = 8;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Private srv As Server";
main._srv = RemoteObject.createNew ("anywheresoftware.b4j.object.ServerWrapper");
 //BA.debugLineNum = 10;BA.debugLine="Public paramMap As Map";
main._parammap = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");
 //BA.debugLineNum = 11;BA.debugLine="Public urlSocketACMT As String";
main._urlsocketacmt = RemoteObject.createImmutable("");
 //BA.debugLineNum = 12;BA.debugLine="Public dirparentpath As String";
main._dirparentpath = RemoteObject.createImmutable("");
 //BA.debugLineNum = 13;BA.debugLine="Public IpFtpDev As String";
main._ipftpdev = RemoteObject.createImmutable("");
 //BA.debugLineNum = 14;BA.debugLine="Public IpFtpProd As String";
main._ipftpprod = RemoteObject.createImmutable("");
 //BA.debugLineNum = 15;BA.debugLine="Public ftpfolderdev, ftpfolderprod As String";
main._ftpfolderdev = RemoteObject.createImmutable("");
main._ftpfolderprod = RemoteObject.createImmutable("");
 //BA.debugLineNum = 16;BA.debugLine="Public tns, ipdb, userdb, pwddb As String";
main._tns = RemoteObject.createImmutable("");
main._ipdb = RemoteObject.createImmutable("");
main._userdb = RemoteObject.createImmutable("");
main._pwddb = RemoteObject.createImmutable("");
 //BA.debugLineNum = 17;BA.debugLine="Public tnsdms, ipdbdms, userdbdms, pwddbdms As St";
main._tnsdms = RemoteObject.createImmutable("");
main._ipdbdms = RemoteObject.createImmutable("");
main._userdbdms = RemoteObject.createImmutable("");
main._pwddbdms = RemoteObject.createImmutable("");
 //BA.debugLineNum = 18;BA.debugLine="Private param, paramdbDMS As List";
main._param = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");
main._paramdbdms = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");
 //BA.debugLineNum = 19;BA.debugLine="Private json As JSONParser";
main._json = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser");
 //BA.debugLineNum = 20;BA.debugLine="Public count As Int";
main._count = RemoteObject.createImmutable(0);
 //BA.debugLineNum = 21;BA.debugLine="Public NativeMe As JavaObject";
main._nativeme = RemoteObject.createNew ("anywheresoftware.b4j.object.JavaObject");
 //BA.debugLineNum = 22;BA.debugLine="Public ipdbdms, pwdDMS, userDMS, tnsdms As String";
main._ipdbdms = RemoteObject.createImmutable("");
main._pwddms = RemoteObject.createImmutable("");
main._userdms = RemoteObject.createImmutable("");
main._tnsdms = RemoteObject.createImmutable("");
 //BA.debugLineNum = 23;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
}