package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class tremanagement_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private clsid As String";
tremanagement._clsid = RemoteObject.createImmutable("");__ref.setField("_clsid",tremanagement._clsid);
 //BA.debugLineNum = 5;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _handle(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Handle (tremanagement) ","tremanagement",7,__ref.getField(false, "ba"),__ref,11);
if (RapidSub.canDelegate("handle")) return __ref.runUserSub(false, "tremanagement","handle", __ref, _req, _resp);
RemoteObject _appid = RemoteObject.createImmutable("");
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _method = RemoteObject.createImmutable("");
RemoteObject _response = RemoteObject.createImmutable("");
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 11;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
Debug.ShouldStop(1024);
 BA.debugLineNum = 12;BA.debugLine="Dim appid, hasil, method As String";
Debug.ShouldStop(2048);
_appid = RemoteObject.createImmutable("");Debug.locals.put("appid", _appid);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
_method = RemoteObject.createImmutable("");Debug.locals.put("method", _method);
 BA.debugLineNum = 13;BA.debugLine="Dim response As String";
Debug.ShouldStop(4096);
_response = RemoteObject.createImmutable("");Debug.locals.put("response", _response);
 BA.debugLineNum = 15;BA.debugLine="If req.Method <> \"POST\" Then";
Debug.ShouldStop(16384);
if (RemoteObject.solveBoolean("!",_req.runMethod(true,"getMethod"),BA.ObjectToString("POST"))) { 
 BA.debugLineNum = 16;BA.debugLine="resp.SendError(500, \"method not supported.\")";
Debug.ShouldStop(32768);
_resp.runVoidMethodAndSync ("SendError",(Object)(BA.numberCast(int.class, 500)),(Object)(RemoteObject.createImmutable("method not supported.")));
 BA.debugLineNum = 17;BA.debugLine="Return";
Debug.ShouldStop(65536);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 21;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
Debug.ShouldStop(1048576);
_appid = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("clsakses")));Debug.locals.put("appid", _appid);
 BA.debugLineNum = 22;BA.debugLine="method = req.GetParameter(\"method\")";
Debug.ShouldStop(2097152);
_method = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("method")));Debug.locals.put("method", _method);
 BA.debugLineNum = 25;BA.debugLine="If appid <> clsid Then";
Debug.ShouldStop(16777216);
if (RemoteObject.solveBoolean("!",_appid,__ref.getField(true,"_clsid"))) { 
 BA.debugLineNum = 26;BA.debugLine="response = modGenerateReturn.return_json(modGene";
Debug.ShouldStop(33554432);
_response = tremanagement._modgeneratereturn.runMethod(true,"_return_json",(Object)(tremanagement._modgeneratereturn._label_rejected),(Object)(tremanagement._modgeneratereturn._kode_rejected_class),(Object)(_method));Debug.locals.put("response", _response);
 BA.debugLineNum = 27;BA.debugLine="resp.Write(response)";
Debug.ShouldStop(67108864);
_resp.runVoidMethod ("Write",(Object)(_response));
 BA.debugLineNum = 28;BA.debugLine="Return";
Debug.ShouldStop(134217728);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 31;BA.debugLine="Select method";
Debug.ShouldStop(1073741824);
switch (BA.switchObjectToInt(_method,BA.ObjectToString("createrole"),BA.ObjectToString("readrole"),BA.ObjectToString("editrole"),BA.ObjectToString("deactivate"),BA.ObjectToString("detailrole"),BA.ObjectToString("listuser"),BA.ObjectToString("detailuser"),BA.ObjectToString("saveuser"),BA.ObjectToString("edituser"),BA.ObjectToString("listmenu"),BA.ObjectToString("savemenu"))) {
case 0: {
 BA.debugLineNum = 33;BA.debugLine="resp.Write( clsManagement.create_roles(req.GetP";
Debug.ShouldStop(1);
_resp.runVoidMethod ("Write",(Object)(tremanagement._clsmanagement.runMethod(true,"_create_roles",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("ket")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("menu")))),(Object)(_method))));
 break; }
case 1: {
 BA.debugLineNum = 35;BA.debugLine="resp.Write( clsManagement.read_roles(req.GetPar";
Debug.ShouldStop(4);
_resp.runVoidMethod ("Write",(Object)(tremanagement._clsmanagement.runMethod(true,"_read_roles",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cari")))),(Object)(_method))));
 break; }
case 2: {
 BA.debugLineNum = 37;BA.debugLine="resp.Write( clsManagement.update_roles(req.GetP";
Debug.ShouldStop(16);
_resp.runVoidMethod ("Write",(Object)(tremanagement._clsmanagement.runMethod(true,"_update_roles",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("ket")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("menu")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("roleotoritas")))),(Object)(BA.numberCast(int.class, _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("totalmenu"))))),(Object)(_method))));
 break; }
case 3: {
 BA.debugLineNum = 39;BA.debugLine="resp.Write( clsManagement.deactivate_roles(req.";
Debug.ShouldStop(64);
_resp.runVoidMethod ("Write",(Object)(tremanagement._clsmanagement.runMethod(true,"_deactivate_roles",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("status")))),(Object)(_method))));
 break; }
case 4: {
 BA.debugLineNum = 41;BA.debugLine="resp.Write( clsManagement.detail_roles(req.GetP";
Debug.ShouldStop(256);
_resp.runVoidMethod ("Write",(Object)(tremanagement._clsmanagement.runMethod(true,"_detail_roles",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_method))));
 break; }
case 5: {
 BA.debugLineNum = 44;BA.debugLine="resp.Write(clsManagement.list_user(req.GetParam";
Debug.ShouldStop(2048);
_resp.runVoidMethod ("Write",(Object)(tremanagement._clsmanagement.runMethod(true,"_list_user",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cari")))),(Object)(_method))));
 break; }
case 6: {
 BA.debugLineNum = 46;BA.debugLine="resp.Write(clsManagement.detail_user(req.GetPar";
Debug.ShouldStop(8192);
_resp.runVoidMethod ("Write",(Object)(tremanagement._clsmanagement.runMethod(true,"_detail_user",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_method))));
 break; }
case 7: {
 BA.debugLineNum = 48;BA.debugLine="resp.Write(clsManagement.save_user(req.GetParam";
Debug.ShouldStop(32768);
_resp.runVoidMethod ("Write",(Object)(tremanagement._clsmanagement.runMethod(true,"_save_user",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("username")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("rolesid")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nip")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idunit")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idjabatan")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("ispegawai")))),(Object)(_method))));
 break; }
case 8: {
 BA.debugLineNum = 50;BA.debugLine="resp.Write(clsManagement.edit_user(req.GetParam";
Debug.ShouldStop(131072);
_resp.runVoidMethod ("Write",(Object)(tremanagement._clsmanagement.runMethod(true,"_edit_user",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("iduser")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("username")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("rolesid")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nip")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idunit")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idjabatan")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("ispegawai")))),(Object)(_method))));
 break; }
case 9: {
 BA.debugLineNum = 53;BA.debugLine="resp.Write(clsManagement.read_menu(req.GetParam";
Debug.ShouldStop(1048576);
_resp.runVoidMethod ("Write",(Object)(tremanagement._clsmanagement.runMethod(true,"_read_menu",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_method))));
 break; }
case 10: {
 BA.debugLineNum = 55;BA.debugLine="resp.Write(clsManagement.save_menu(req.GetParam";
Debug.ShouldStop(4194304);
_resp.runVoidMethod ("Write",(Object)(tremanagement._clsmanagement.runMethod(true,"_save_menu",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("url")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("ket")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("icon")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("class")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("parent")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("islabel")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("urutan")))),(Object)(_method))));
 break; }
default: {
 BA.debugLineNum = 58;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
Debug.ShouldStop(33554432);
_hasil = tremanagement._modgeneratereturn.runMethod(true,"_return_json",(Object)(BA.ObjectToString("TIDAK ADA METHOD")),(Object)(BA.ObjectToString("404")),(Object)(_method));Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 59;BA.debugLine="resp.Write(hasil)";
Debug.ShouldStop(67108864);
_resp.runVoidMethod ("Write",(Object)(_hasil));
 break; }
}
;
 BA.debugLineNum = 62;BA.debugLine="End Sub";
Debug.ShouldStop(536870912);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (tremanagement) ","tremanagement",7,__ref.getField(false, "ba"),__ref,7);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "tremanagement","initialize", __ref, _ba);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
 BA.debugLineNum = 7;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(64);
 BA.debugLineNum = 8;BA.debugLine="clsid = \"tre_management\"";
Debug.ShouldStop(128);
__ref.setField ("_clsid",BA.ObjectToString("tre_management"));
 BA.debugLineNum = 9;BA.debugLine="End Sub";
Debug.ShouldStop(256);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}