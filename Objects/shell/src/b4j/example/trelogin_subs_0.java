package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class trelogin_subs_0 {


public static RemoteObject  _checklogin(RemoteObject __ref,RemoteObject _token,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("checklogin (trelogin) ","trelogin",8,__ref.getField(false, "ba"),__ref,52);
if (RapidSub.canDelegate("checklogin")) return __ref.runUserSub(false, "trelogin","checklogin", __ref, _token, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _ret = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _js = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("token", _token);
Debug.locals.put("method", _method);
 BA.debugLineNum = 52;BA.debugLine="Private Sub checklogin(token As String, method As";
Debug.ShouldStop(524288);
 BA.debugLineNum = 53;BA.debugLine="Dim query, ret, data As String";
Debug.ShouldStop(1048576);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_ret = RemoteObject.createImmutable("");Debug.locals.put("ret", _ret);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 54;BA.debugLine="Dim js As JSONParser";
Debug.ShouldStop(2097152);
_js = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser");Debug.locals.put("js", _js);
 BA.debugLineNum = 55;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(4194304);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 56;BA.debugLine="mp.Initialize";
Debug.ShouldStop(8388608);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 58;BA.debugLine="query = $\" select * from table(check_token('${tok";
Debug.ShouldStop(33554432);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(check_token('"),trelogin.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_token))),RemoteObject.createImmutable("'))")));Debug.locals.put("query", _query);
 BA.debugLineNum = 59;BA.debugLine="data  = exe_queryportal(query, method)";
Debug.ShouldStop(67108864);
_data = __ref.runClassMethod (b4j.example.trelogin.class, "_exe_queryportal",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 60;BA.debugLine="js.Initialize(data)";
Debug.ShouldStop(134217728);
_js.runVoidMethod ("Initialize",(Object)(_data));
 BA.debugLineNum = 61;BA.debugLine="mp = js.NextObject";
Debug.ShouldStop(268435456);
_mp = _js.runMethod(false,"NextObject");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 63;BA.debugLine="If mp.Get(\"response\") = \"301\" Then";
Debug.ShouldStop(1073741824);
if (RemoteObject.solveBoolean("=",_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("response")))),RemoteObject.createImmutable(("301")))) { 
 BA.debugLineNum = 64;BA.debugLine="ret = getModul(mp.Get(\"data\"), \"-\")";
Debug.ShouldStop(-2147483648);
_ret = __ref.runClassMethod (b4j.example.trelogin.class, "_getmodul",RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.collections.List"), _mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("data"))))),(Object)(RemoteObject.createImmutable("-")));Debug.locals.put("ret", _ret);
 }else {
 BA.debugLineNum = 66;BA.debugLine="ret = data";
Debug.ShouldStop(2);
_ret = _data;Debug.locals.put("ret", _ret);
 };
 BA.debugLineNum = 69;BA.debugLine="Return ret";
Debug.ShouldStop(16);
if (true) return _ret;
 BA.debugLineNum = 70;BA.debugLine="End Sub";
Debug.ShouldStop(32);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private clsid As String";
trelogin._clsid = RemoteObject.createImmutable("");__ref.setField("_clsid",trelogin._clsid);
 //BA.debugLineNum = 4;BA.debugLine="Private todb As clsDB";
trelogin._todb = RemoteObject.createNew ("b4j.example.clsdb");__ref.setField("_todb",trelogin._todb);
 //BA.debugLineNum = 5;BA.debugLine="Private todbdms As clsDBDms";
trelogin._todbdms = RemoteObject.createNew ("b4j.example.clsdbdms");__ref.setField("_todbdms",trelogin._todbdms);
 //BA.debugLineNum = 6;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _exe_query(RemoteObject __ref,RemoteObject _query,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("exe_query (trelogin) ","trelogin",8,__ref.getField(false, "ba"),__ref,117);
if (RapidSub.canDelegate("exe_query")) return __ref.runUserSub(false, "trelogin","exe_query", __ref, _query, _method);
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _lst = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _js = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");
Debug.locals.put("query", _query);
Debug.locals.put("method", _method);
 BA.debugLineNum = 117;BA.debugLine="Private Sub exe_query(query As String, method As S";
Debug.ShouldStop(1048576);
 BA.debugLineNum = 118;BA.debugLine="Dim hasil As String";
Debug.ShouldStop(2097152);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 119;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(4194304);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 120;BA.debugLine="Dim lst As List";
Debug.ShouldStop(8388608);
_lst = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("lst", _lst);
 BA.debugLineNum = 121;BA.debugLine="Dim js As JSONGenerator";
Debug.ShouldStop(16777216);
_js = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");Debug.locals.put("js", _js);
 BA.debugLineNum = 123;BA.debugLine="lst.Initialize";
Debug.ShouldStop(67108864);
_lst.runVoidMethod ("Initialize");
 BA.debugLineNum = 124;BA.debugLine="mp.Initialize";
Debug.ShouldStop(134217728);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 126;BA.debugLine="mp = todb.SqlSelectMap(query, method)";
Debug.ShouldStop(536870912);
_mp = __ref.getField(false,"_todb").runClassMethod (b4j.example.clsdb.class, "_sqlselectmap",(Object)(_query),(Object)(_method));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 127;BA.debugLine="js.Initialize(mp)";
Debug.ShouldStop(1073741824);
_js.runVoidMethod ("Initialize",(Object)(_mp));
 BA.debugLineNum = 128;BA.debugLine="hasil = js.ToString";
Debug.ShouldStop(-2147483648);
_hasil = _js.runMethod(true,"ToString");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 130;BA.debugLine="Return hasil";
Debug.ShouldStop(2);
if (true) return _hasil;
 BA.debugLineNum = 131;BA.debugLine="End Sub";
Debug.ShouldStop(4);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _exe_queryportal(RemoteObject __ref,RemoteObject _query,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("exe_queryportal (trelogin) ","trelogin",8,__ref.getField(false, "ba"),__ref,133);
if (RapidSub.canDelegate("exe_queryportal")) return __ref.runUserSub(false, "trelogin","exe_queryportal", __ref, _query, _method);
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _lst = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _js = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");
Debug.locals.put("query", _query);
Debug.locals.put("method", _method);
 BA.debugLineNum = 133;BA.debugLine="Private Sub exe_queryportal(query As String, metho";
Debug.ShouldStop(16);
 BA.debugLineNum = 134;BA.debugLine="Dim hasil As String";
Debug.ShouldStop(32);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 135;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(64);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 136;BA.debugLine="Dim lst As List";
Debug.ShouldStop(128);
_lst = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("lst", _lst);
 BA.debugLineNum = 137;BA.debugLine="Dim js As JSONGenerator";
Debug.ShouldStop(256);
_js = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");Debug.locals.put("js", _js);
 BA.debugLineNum = 139;BA.debugLine="lst.Initialize";
Debug.ShouldStop(1024);
_lst.runVoidMethod ("Initialize");
 BA.debugLineNum = 140;BA.debugLine="mp.Initialize";
Debug.ShouldStop(2048);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 142;BA.debugLine="mp = todbdms.SqlSelectMap(query, method)";
Debug.ShouldStop(8192);
_mp = __ref.getField(false,"_todbdms").runClassMethod (b4j.example.clsdbdms.class, "_sqlselectmap",(Object)(_query),(Object)(_method));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 143;BA.debugLine="js.Initialize(mp)";
Debug.ShouldStop(16384);
_js.runVoidMethod ("Initialize",(Object)(_mp));
 BA.debugLineNum = 144;BA.debugLine="hasil = js.ToString";
Debug.ShouldStop(32768);
_hasil = _js.runMethod(true,"ToString");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 145;BA.debugLine="Return hasil";
Debug.ShouldStop(65536);
if (true) return _hasil;
 BA.debugLineNum = 146;BA.debugLine="End Sub";
Debug.ShouldStop(131072);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getmodul(RemoteObject __ref,RemoteObject _data,RemoteObject _isvendor) throws Exception{
try {
		Debug.PushSubsStack("getModul (trelogin) ","trelogin",8,__ref.getField(false, "ba"),__ref,92);
if (RapidSub.canDelegate("getmodul")) return __ref.runUserSub(false, "trelogin","getmodul", __ref, _data, _isvendor);
RemoteObject _ret = RemoteObject.createImmutable("");
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _ispegawai = RemoteObject.createImmutable("");
RemoteObject _namapegawai = RemoteObject.createImmutable("");
RemoteObject _nippegawai = RemoteObject.createImmutable("");
RemoteObject _email = RemoteObject.createImmutable("");
RemoteObject _userid = RemoteObject.createImmutable("");
RemoteObject _pwd = RemoteObject.createImmutable("");
RemoteObject _js = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("data", _data);
Debug.locals.put("isvendor", _isvendor);
 BA.debugLineNum = 92;BA.debugLine="Private Sub getModul(data As List, isvendor As Str";
Debug.ShouldStop(134217728);
 BA.debugLineNum = 93;BA.debugLine="Dim ret, query, ispegawai, namapegawai, nippegawa";
Debug.ShouldStop(268435456);
_ret = RemoteObject.createImmutable("");Debug.locals.put("ret", _ret);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_ispegawai = RemoteObject.createImmutable("");Debug.locals.put("ispegawai", _ispegawai);
_namapegawai = RemoteObject.createImmutable("");Debug.locals.put("namapegawai", _namapegawai);
_nippegawai = RemoteObject.createImmutable("");Debug.locals.put("nippegawai", _nippegawai);
_email = RemoteObject.createImmutable("");Debug.locals.put("email", _email);
_userid = RemoteObject.createImmutable("");Debug.locals.put("userid", _userid);
_pwd = RemoteObject.createImmutable("");Debug.locals.put("pwd", _pwd);
 BA.debugLineNum = 94;BA.debugLine="Dim js As JSONParser";
Debug.ShouldStop(536870912);
_js = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser");Debug.locals.put("js", _js);
 BA.debugLineNum = 95;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(1073741824);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 96;BA.debugLine="mp = data.Get(0)";
Debug.ShouldStop(-2147483648);
_mp.setObject(_data.runMethod(false,"Get",(Object)(BA.numberCast(int.class, 0))));
 BA.debugLineNum = 97;BA.debugLine="If mp.Get(\"kode\") = \"RC01\" Then";
Debug.ShouldStop(1);
if (RemoteObject.solveBoolean("=",_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("kode")))),RemoteObject.createImmutable(("RC01")))) { 
 BA.debugLineNum = 98;BA.debugLine="If isvendor <> \"1\" Then";
Debug.ShouldStop(2);
if (RemoteObject.solveBoolean("!",_isvendor,BA.ObjectToString("1"))) { 
 BA.debugLineNum = 99;BA.debugLine="nippegawai = mp.Get(\"f4\")";
Debug.ShouldStop(4);
_nippegawai = BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("f4")))));Debug.locals.put("nippegawai", _nippegawai);
 BA.debugLineNum = 100;BA.debugLine="namapegawai = mp.Get(\"f2\")";
Debug.ShouldStop(8);
_namapegawai = BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("f2")))));Debug.locals.put("namapegawai", _namapegawai);
 BA.debugLineNum = 101;BA.debugLine="modGenerateReturn.namaUser = namapegawai";
Debug.ShouldStop(16);
trelogin._modgeneratereturn._namauser = _namapegawai;
 BA.debugLineNum = 102;BA.debugLine="email = mp.Get(\"f3\")";
Debug.ShouldStop(32);
_email = BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("f3")))));Debug.locals.put("email", _email);
 BA.debugLineNum = 103;BA.debugLine="userid = mp.Get(\"f1\")";
Debug.ShouldStop(64);
_userid = BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("f1")))));Debug.locals.put("userid", _userid);
 BA.debugLineNum = 104;BA.debugLine="ispegawai = mp.Get(\"f5\")";
Debug.ShouldStop(128);
_ispegawai = BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("f5")))));Debug.locals.put("ispegawai", _ispegawai);
 BA.debugLineNum = 105;BA.debugLine="pwd = mp.Get(\"f6\")";
Debug.ShouldStop(256);
_pwd = BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("f6")))));Debug.locals.put("pwd", _pwd);
 }else {
 BA.debugLineNum = 107;BA.debugLine="userid = mp.Get(\"f1\")";
Debug.ShouldStop(1024);
_userid = BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("f1")))));Debug.locals.put("userid", _userid);
 };
 BA.debugLineNum = 109;BA.debugLine="query = $\"select * from table(pkg_dms_tre.fn_get";
Debug.ShouldStop(4096);
_query = (RemoteObject.concat(RemoteObject.createImmutable("select * from table(pkg_dms_tre.fn_getmenuakses('"),trelogin.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_userid))),RemoteObject.createImmutable("','"),trelogin.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_userid))),RemoteObject.createImmutable("','"),trelogin.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_namapegawai))),RemoteObject.createImmutable("','"),trelogin.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_nippegawai))),RemoteObject.createImmutable("','"),trelogin.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_ispegawai))),RemoteObject.createImmutable("', '"),trelogin.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_pwd))),RemoteObject.createImmutable("', '"),trelogin.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_isvendor))),RemoteObject.createImmutable("'))")));Debug.locals.put("query", _query);
 BA.debugLineNum = 110;BA.debugLine="ret = exe_query(query, \"GETMODUL\")";
Debug.ShouldStop(8192);
_ret = __ref.runClassMethod (b4j.example.trelogin.class, "_exe_query",(Object)(_query),(Object)(RemoteObject.createImmutable("GETMODUL")));Debug.locals.put("ret", _ret);
 }else {
 BA.debugLineNum = 112;BA.debugLine="ret = modGenerateReturn.return_json(mp.Get(\"pesa";
Debug.ShouldStop(32768);
_ret = trelogin._modgeneratereturn.runMethod(true,"_return_json",(Object)(BA.ObjectToString(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("pesan")))))),(Object)(BA.ObjectToString("501")),(Object)(RemoteObject.createImmutable("GETMODUL")));Debug.locals.put("ret", _ret);
 };
 BA.debugLineNum = 114;BA.debugLine="Return ret";
Debug.ShouldStop(131072);
if (true) return _ret;
 BA.debugLineNum = 115;BA.debugLine="End Sub";
Debug.ShouldStop(262144);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getpwd(RemoteObject __ref,RemoteObject _username,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("getpwd (trelogin) ","trelogin",8,__ref.getField(false, "ba"),__ref,41);
if (RapidSub.canDelegate("getpwd")) return __ref.runUserSub(false, "trelogin","getpwd", __ref, _username, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("username", _username);
Debug.locals.put("method", _method);
 BA.debugLineNum = 41;BA.debugLine="Private Sub getpwd(username As String, method As S";
Debug.ShouldStop(256);
 BA.debugLineNum = 42;BA.debugLine="Dim query,  data As String";
Debug.ShouldStop(512);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 43;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(1024);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 44;BA.debugLine="mp.Initialize";
Debug.ShouldStop(2048);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 46;BA.debugLine="query = $\" select * from table(pkg_dms_tre.fn_get";
Debug.ShouldStop(8192);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(pkg_dms_tre.fn_getpwd('"),trelogin.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_username))),RemoteObject.createImmutable("'))")));Debug.locals.put("query", _query);
 BA.debugLineNum = 47;BA.debugLine="data  = exequery.multi_string(query, method)";
Debug.ShouldStop(16384);
_data = trelogin._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 49;BA.debugLine="Return data";
Debug.ShouldStop(65536);
if (true) return _data;
 BA.debugLineNum = 50;BA.debugLine="End Sub";
Debug.ShouldStop(131072);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _handle(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Handle (trelogin) ","trelogin",8,__ref.getField(false, "ba"),__ref,14);
if (RapidSub.canDelegate("handle")) return __ref.runUserSub(false, "trelogin","handle", __ref, _req, _resp);
RemoteObject _appid = RemoteObject.createImmutable("");
RemoteObject _response = RemoteObject.createImmutable("");
RemoteObject _method = RemoteObject.createImmutable("");
RemoteObject _hasil = RemoteObject.createImmutable("");
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 14;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
Debug.ShouldStop(8192);
 BA.debugLineNum = 15;BA.debugLine="Dim appid, response, method, hasil As String";
Debug.ShouldStop(16384);
_appid = RemoteObject.createImmutable("");Debug.locals.put("appid", _appid);
_response = RemoteObject.createImmutable("");Debug.locals.put("response", _response);
_method = RemoteObject.createImmutable("");Debug.locals.put("method", _method);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 18;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
Debug.ShouldStop(131072);
_appid = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("clsakses")));Debug.locals.put("appid", _appid);
 BA.debugLineNum = 19;BA.debugLine="method = req.GetParameter(\"method\")";
Debug.ShouldStop(262144);
_method = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("method")));Debug.locals.put("method", _method);
 BA.debugLineNum = 21;BA.debugLine="If appid <> clsid Then";
Debug.ShouldStop(1048576);
if (RemoteObject.solveBoolean("!",_appid,__ref.getField(true,"_clsid"))) { 
 BA.debugLineNum = 22;BA.debugLine="response = modGenerateReturn.return_json(modGene";
Debug.ShouldStop(2097152);
_response = trelogin._modgeneratereturn.runMethod(true,"_return_json",(Object)(trelogin._modgeneratereturn._label_rejected),(Object)(trelogin._modgeneratereturn._kode_rejected_class),(Object)(_method));Debug.locals.put("response", _response);
 BA.debugLineNum = 23;BA.debugLine="resp.Write(response)";
Debug.ShouldStop(4194304);
_resp.runVoidMethod ("Write",(Object)(_response));
 BA.debugLineNum = 24;BA.debugLine="Return";
Debug.ShouldStop(8388608);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 26;BA.debugLine="Select method";
Debug.ShouldStop(33554432);
switch (BA.switchObjectToInt(_method,BA.ObjectToString("checklogin"),BA.ObjectToString("getpwd"),BA.ObjectToString("loginvendor"))) {
case 0: {
 BA.debugLineNum = 28;BA.debugLine="resp.Write(checklogin(req.GetParameter(\"t\"), me";
Debug.ShouldStop(134217728);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.trelogin.class, "_checklogin",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("t")))),(Object)(_method))));
 break; }
case 1: {
 BA.debugLineNum = 30;BA.debugLine="resp.Write(getpwd(req.GetParameter(\"username\"),";
Debug.ShouldStop(536870912);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.trelogin.class, "_getpwd",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("username")))),(Object)(_method))));
 break; }
case 2: {
 BA.debugLineNum = 32;BA.debugLine="resp.Write(loginvendor(req.GetParameter(\"u\"), r";
Debug.ShouldStop(-2147483648);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.trelogin.class, "_loginvendor",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("u")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("p")))),(Object)(_method))));
 break; }
default: {
 BA.debugLineNum = 34;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
Debug.ShouldStop(2);
_hasil = trelogin._modgeneratereturn.runMethod(true,"_return_json",(Object)(BA.ObjectToString("TIDAK ADA METHOD")),(Object)(BA.ObjectToString("404")),(Object)(_method));Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 35;BA.debugLine="resp.Write(hasil)";
Debug.ShouldStop(4);
_resp.runVoidMethod ("Write",(Object)(_hasil));
 break; }
}
;
 BA.debugLineNum = 39;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (trelogin) ","trelogin",8,__ref.getField(false, "ba"),__ref,8);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "trelogin","initialize", __ref, _ba);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
 BA.debugLineNum = 8;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(128);
 BA.debugLineNum = 9;BA.debugLine="clsid = \"tre_loginicon\"";
Debug.ShouldStop(256);
__ref.setField ("_clsid",BA.ObjectToString("tre_loginicon"));
 BA.debugLineNum = 10;BA.debugLine="todb.Initialize";
Debug.ShouldStop(512);
__ref.getField(false,"_todb").runClassMethod (b4j.example.clsdb.class, "_initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 11;BA.debugLine="todbdms.Initialize";
Debug.ShouldStop(1024);
__ref.getField(false,"_todbdms").runClassMethod (b4j.example.clsdbdms.class, "_initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 12;BA.debugLine="End Sub";
Debug.ShouldStop(2048);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _loginvendor(RemoteObject __ref,RemoteObject _u,RemoteObject _p,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("loginvendor (trelogin) ","trelogin",8,__ref.getField(false, "ba"),__ref,72);
if (RapidSub.canDelegate("loginvendor")) return __ref.runUserSub(false, "trelogin","loginvendor", __ref, _u, _p, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _ret = RemoteObject.createImmutable("");
RemoteObject _js = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("u", _u);
Debug.locals.put("p", _p);
Debug.locals.put("method", _method);
 BA.debugLineNum = 72;BA.debugLine="Private Sub loginvendor(u As String, p As String,";
Debug.ShouldStop(128);
 BA.debugLineNum = 73;BA.debugLine="Dim query,  data, ret As String";
Debug.ShouldStop(256);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_ret = RemoteObject.createImmutable("");Debug.locals.put("ret", _ret);
 BA.debugLineNum = 74;BA.debugLine="Dim js As JSONParser";
Debug.ShouldStop(512);
_js = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser");Debug.locals.put("js", _js);
 BA.debugLineNum = 75;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(1024);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 76;BA.debugLine="mp.Initialize";
Debug.ShouldStop(2048);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 78;BA.debugLine="query = $\" select * from table(pkg_dms_tre.fn_log";
Debug.ShouldStop(8192);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(pkg_dms_tre.fn_login_vendor('"),trelogin.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_u))),RemoteObject.createImmutable("','"),trelogin.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p))),RemoteObject.createImmutable("'))")));Debug.locals.put("query", _query);
 BA.debugLineNum = 79;BA.debugLine="data  = exequery.multi_string(query, method)";
Debug.ShouldStop(16384);
_data = trelogin._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 80;BA.debugLine="js.Initialize(data)";
Debug.ShouldStop(32768);
_js.runVoidMethod ("Initialize",(Object)(_data));
 BA.debugLineNum = 81;BA.debugLine="mp = js.NextObject";
Debug.ShouldStop(65536);
_mp = _js.runMethod(false,"NextObject");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 83;BA.debugLine="If mp.Get(\"response\") = \"301\" Then";
Debug.ShouldStop(262144);
if (RemoteObject.solveBoolean("=",_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("response")))),RemoteObject.createImmutable(("301")))) { 
 BA.debugLineNum = 84;BA.debugLine="ret = getModul(mp.Get(\"data\"), \"1\")";
Debug.ShouldStop(524288);
_ret = __ref.runClassMethod (b4j.example.trelogin.class, "_getmodul",RemoteObject.declareNull("anywheresoftware.b4a.AbsObjectWrapper").runMethod(false, "ConvertToWrapper", RemoteObject.createNew("anywheresoftware.b4a.objects.collections.List"), _mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("data"))))),(Object)(RemoteObject.createImmutable("1")));Debug.locals.put("ret", _ret);
 }else {
 BA.debugLineNum = 86;BA.debugLine="ret = data";
Debug.ShouldStop(2097152);
_ret = _data;Debug.locals.put("ret", _ret);
 };
 BA.debugLineNum = 89;BA.debugLine="Return ret";
Debug.ShouldStop(16777216);
if (true) return _ret;
 BA.debugLineNum = 90;BA.debugLine="End Sub";
Debug.ShouldStop(33554432);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}