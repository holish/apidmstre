package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class tretrx_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private clsid As String";
tretrx._clsid = RemoteObject.createImmutable("");__ref.setField("_clsid",tretrx._clsid);
 //BA.debugLineNum = 4;BA.debugLine="Private todb As clsDB";
tretrx._todb = RemoteObject.createNew ("b4j.example.clsdb");__ref.setField("_todb",tretrx._todb);
 //BA.debugLineNum = 5;BA.debugLine="Private todbdms As clsDBDms";
tretrx._todbdms = RemoteObject.createNew ("b4j.example.clsdbdms");__ref.setField("_todbdms",tretrx._todbdms);
 //BA.debugLineNum = 6;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _detail_permohonan(RemoteObject __ref,RemoteObject _id,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("detail_permohonan (tretrx) ","tretrx",1,__ref.getField(false, "ba"),__ref,138);
if (RapidSub.canDelegate("detail_permohonan")) return __ref.runUserSub(false, "tretrx","detail_permohonan", __ref, _id, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("id", _id);
Debug.locals.put("method", _method);
 BA.debugLineNum = 138;BA.debugLine="Private Sub detail_permohonan(id As String, method";
Debug.ShouldStop(512);
 BA.debugLineNum = 139;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(1024);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 140;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(2048);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 141;BA.debugLine="mp.Initialize";
Debug.ShouldStop(4096);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 143;BA.debugLine="query = $\" select * from v_detail_trx_mohon_bayar";
Debug.ShouldStop(16384);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from v_detail_trx_mohon_bayar where f1 = '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_id))),RemoteObject.createImmutable("' or f37 = '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_id))),RemoteObject.createImmutable("' ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 144;BA.debugLine="data = exequery.multi_string(query, method)";
Debug.ShouldStop(32768);
_data = tretrx._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 146;BA.debugLine="Return data";
Debug.ShouldStop(131072);
if (true) return _data;
 BA.debugLineNum = 147;BA.debugLine="End Sub";
Debug.ShouldStop(262144);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _doc_vendor(RemoteObject __ref,RemoteObject _idmohon_bayar,RemoteObject _path_url,RemoteObject _name_doc,RemoteObject _kode_doc,RemoteObject _jml_doc,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("doc_vendor (tretrx) ","tretrx",1,__ref.getField(false, "ba"),__ref,111);
if (RapidSub.canDelegate("doc_vendor")) return __ref.runUserSub(false, "tretrx","doc_vendor", __ref, _idmohon_bayar, _path_url, _name_doc, _kode_doc, _jml_doc, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _temp = RemoteObject.createImmutable("");
RemoteObject _path = null;
RemoteObject _name = null;
RemoteObject _kode = null;
RemoteObject _en = RemoteObject.declareNull("anywheresoftware.b4a.objects.StringUtils");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
int _i = 0;
Debug.locals.put("idmohon_bayar", _idmohon_bayar);
Debug.locals.put("path_url", _path_url);
Debug.locals.put("name_doc", _name_doc);
Debug.locals.put("kode_doc", _kode_doc);
Debug.locals.put("jml_doc", _jml_doc);
Debug.locals.put("method", _method);
 BA.debugLineNum = 111;BA.debugLine="Private Sub doc_vendor(idmohon_bayar As String, pa";
Debug.ShouldStop(16384);
 BA.debugLineNum = 112;BA.debugLine="Dim query, data, temp As String";
Debug.ShouldStop(32768);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
_temp = RemoteObject.createImmutable("");Debug.locals.put("temp", _temp);
 BA.debugLineNum = 113;BA.debugLine="Dim path(), name(), kode() As String";
Debug.ShouldStop(65536);
_path = RemoteObject.createNewArray ("String", new int[] {0}, new Object[]{});Debug.locals.put("path", _path);
_name = RemoteObject.createNewArray ("String", new int[] {0}, new Object[]{});Debug.locals.put("name", _name);
_kode = RemoteObject.createNewArray ("String", new int[] {0}, new Object[]{});Debug.locals.put("kode", _kode);
 BA.debugLineNum = 114;BA.debugLine="Dim en As StringUtils";
Debug.ShouldStop(131072);
_en = RemoteObject.createNew ("anywheresoftware.b4a.objects.StringUtils");Debug.locals.put("en", _en);
 BA.debugLineNum = 115;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(262144);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 116;BA.debugLine="mp.Initialize";
Debug.ShouldStop(524288);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 118;BA.debugLine="File.WriteList(File.DirApp, \"path1.txt\", path)";
Debug.ShouldStop(2097152);
tretrx.__c.getField(false,"File").runVoidMethod ("WriteList",(Object)(tretrx.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(BA.ObjectToString("path1.txt")),(Object)(tretrx.__c.runMethod(false, "ArrayToList", (Object)(_path))));
 BA.debugLineNum = 119;BA.debugLine="File.WriteList(File.DirApp, \"name2.txt\", name)";
Debug.ShouldStop(4194304);
tretrx.__c.getField(false,"File").runVoidMethod ("WriteList",(Object)(tretrx.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(BA.ObjectToString("name2.txt")),(Object)(tretrx.__c.runMethod(false, "ArrayToList", (Object)(_name))));
 BA.debugLineNum = 120;BA.debugLine="File.WriteList(File.DirApp, \"kode3.txt\", kode)";
Debug.ShouldStop(8388608);
tretrx.__c.getField(false,"File").runVoidMethod ("WriteList",(Object)(tretrx.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(BA.ObjectToString("kode3.txt")),(Object)(tretrx.__c.runMethod(false, "ArrayToList", (Object)(_kode))));
 BA.debugLineNum = 122;BA.debugLine="path = Regex.Split(\",\", path_url)";
Debug.ShouldStop(33554432);
_path = tretrx.__c.getField(false,"Regex").runMethod(false,"Split",(Object)(BA.ObjectToString(",")),(Object)(_path_url));Debug.locals.put("path", _path);
 BA.debugLineNum = 123;BA.debugLine="name = Regex.Split(\",\", name_doc)";
Debug.ShouldStop(67108864);
_name = tretrx.__c.getField(false,"Regex").runMethod(false,"Split",(Object)(BA.ObjectToString(",")),(Object)(_name_doc));Debug.locals.put("name", _name);
 BA.debugLineNum = 124;BA.debugLine="kode = Regex.Split(\",\", kode_doc)";
Debug.ShouldStop(134217728);
_kode = tretrx.__c.getField(false,"Regex").runMethod(false,"Split",(Object)(BA.ObjectToString(",")),(Object)(_kode_doc));Debug.locals.put("kode", _kode);
 BA.debugLineNum = 125;BA.debugLine="File.WriteList(File.DirApp, \"path.txt\", path)";
Debug.ShouldStop(268435456);
tretrx.__c.getField(false,"File").runVoidMethod ("WriteList",(Object)(tretrx.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(BA.ObjectToString("path.txt")),(Object)(tretrx.__c.runMethod(false, "ArrayToList", (Object)(_path))));
 BA.debugLineNum = 126;BA.debugLine="File.WriteList(File.DirApp, \"name.txt\", name)";
Debug.ShouldStop(536870912);
tretrx.__c.getField(false,"File").runVoidMethod ("WriteList",(Object)(tretrx.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(BA.ObjectToString("name.txt")),(Object)(tretrx.__c.runMethod(false, "ArrayToList", (Object)(_name))));
 BA.debugLineNum = 127;BA.debugLine="File.WriteList(File.DirApp, \"kode.txt\", kode)";
Debug.ShouldStop(1073741824);
tretrx.__c.getField(false,"File").runVoidMethod ("WriteList",(Object)(tretrx.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(BA.ObjectToString("kode.txt")),(Object)(tretrx.__c.runMethod(false, "ArrayToList", (Object)(_kode))));
 BA.debugLineNum = 129;BA.debugLine="For i = 0 To jml_doc -1";
Debug.ShouldStop(1);
{
final int step15 = 1;
final int limit15 = RemoteObject.solve(new RemoteObject[] {_jml_doc,RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = 0 ;
for (;(step15 > 0 && _i <= limit15) || (step15 < 0 && _i >= limit15) ;_i = ((int)(0 + _i + step15))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 130;BA.debugLine="query = $\" select * from table(PKG_DMS_TRANSAKSI";
Debug.ShouldStop(2);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_TRANSAKSI.fn_document_vendor('"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_idmohon_bayar))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_path.getArrayElement(true,BA.numberCast(int.class, _i))))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_name.getArrayElement(true,BA.numberCast(int.class, _i))))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_kode.getArrayElement(true,BA.numberCast(int.class, _i))))),RemoteObject.createImmutable("',"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)(RemoteObject.createImmutable((1)))),RemoteObject.createImmutable(")) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 131;BA.debugLine="data  = exequery.multi_string(query, method)";
Debug.ShouldStop(4);
_data = tretrx._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 132;BA.debugLine="File.WriteString(File.DirApp, i &\".txt\", data)";
Debug.ShouldStop(8);
tretrx.__c.getField(false,"File").runVoidMethod ("WriteString",(Object)(tretrx.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(RemoteObject.concat(RemoteObject.createImmutable(_i),RemoteObject.createImmutable(".txt"))),(Object)(_data));
 BA.debugLineNum = 133;BA.debugLine="File.WriteString(File.DirApp, i &\"q.txt\", data)";
Debug.ShouldStop(16);
tretrx.__c.getField(false,"File").runVoidMethod ("WriteString",(Object)(tretrx.__c.getField(false,"File").runMethod(true,"getDirApp")),(Object)(RemoteObject.concat(RemoteObject.createImmutable(_i),RemoteObject.createImmutable("q.txt"))),(Object)(_data));
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 135;BA.debugLine="Return data";
Debug.ShouldStop(64);
if (true) return _data;
 BA.debugLineNum = 136;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _handle(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Handle (tretrx) ","tretrx",1,__ref.getField(false, "ba"),__ref,15);
if (RapidSub.canDelegate("handle")) return __ref.runUserSub(false, "tretrx","handle", __ref, _req, _resp);
RemoteObject _appid = RemoteObject.createImmutable("");
RemoteObject _response = RemoteObject.createImmutable("");
RemoteObject _method = RemoteObject.createImmutable("");
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _nilaiinvoice = RemoteObject.createImmutable(0);
RemoteObject _nilaivat = RemoteObject.createImmutable(0);
RemoteObject _nilaiwithold = RemoteObject.createImmutable(0);
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 15;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
Debug.ShouldStop(16384);
 BA.debugLineNum = 16;BA.debugLine="Dim appid, response, method, hasil As String";
Debug.ShouldStop(32768);
_appid = RemoteObject.createImmutable("");Debug.locals.put("appid", _appid);
_response = RemoteObject.createImmutable("");Debug.locals.put("response", _response);
_method = RemoteObject.createImmutable("");Debug.locals.put("method", _method);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 19;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
Debug.ShouldStop(262144);
_appid = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("clsakses")));Debug.locals.put("appid", _appid);
 BA.debugLineNum = 20;BA.debugLine="method = req.GetParameter(\"method\")";
Debug.ShouldStop(524288);
_method = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("method")));Debug.locals.put("method", _method);
 BA.debugLineNum = 21;BA.debugLine="Log(req.GetParameter(\"key\"))";
Debug.ShouldStop(1048576);
tretrx.__c.runVoidMethod ("Log",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("key")))));
 BA.debugLineNum = 23;BA.debugLine="If appid <> clsid Then";
Debug.ShouldStop(4194304);
if (RemoteObject.solveBoolean("!",_appid,__ref.getField(true,"_clsid"))) { 
 BA.debugLineNum = 24;BA.debugLine="response = modGenerateReturn.return_json(modGene";
Debug.ShouldStop(8388608);
_response = tretrx._modgeneratereturn.runMethod(true,"_return_json",(Object)(tretrx._modgeneratereturn._label_rejected),(Object)(tretrx._modgeneratereturn._kode_rejected_class),(Object)(_method));Debug.locals.put("response", _response);
 BA.debugLineNum = 25;BA.debugLine="resp.Write(response)";
Debug.ShouldStop(16777216);
_resp.runVoidMethod ("Write",(Object)(_response));
 BA.debugLineNum = 26;BA.debugLine="Return";
Debug.ShouldStop(33554432);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 28;BA.debugLine="Select method";
Debug.ShouldStop(134217728);
switch (BA.switchObjectToInt(_method,BA.ObjectToString("mohonbayar"),BA.ObjectToString("listbayar"),BA.ObjectToString("listverifikasi"),BA.ObjectToString("docvendor"),BA.ObjectToString("detail_permohonan"),BA.ObjectToString("list_doc"))) {
case 0: {
 BA.debugLineNum = 30;BA.debugLine="Dim nilaiinvoice As Int = req.GetParameter(\"nil";
Debug.ShouldStop(536870912);
_nilaiinvoice = BA.numberCast(int.class, _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nilaiinvoice"))));Debug.locals.put("nilaiinvoice", _nilaiinvoice);Debug.locals.put("nilaiinvoice", _nilaiinvoice);
 BA.debugLineNum = 31;BA.debugLine="Dim nilaivat As Int = req.GetParameter(\"nilaiva";
Debug.ShouldStop(1073741824);
_nilaivat = BA.numberCast(int.class, _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nilaivat"))));Debug.locals.put("nilaivat", _nilaivat);Debug.locals.put("nilaivat", _nilaivat);
 BA.debugLineNum = 32;BA.debugLine="Dim nilaiwithold As Int = req.GetParameter(\"nil";
Debug.ShouldStop(-2147483648);
_nilaiwithold = BA.numberCast(int.class, _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nilaiwhitehold"))));Debug.locals.put("nilaiwithold", _nilaiwithold);Debug.locals.put("nilaiwithold", _nilaiwithold);
 BA.debugLineNum = 33;BA.debugLine="resp.Write(mohonbayar(req.GetParameter(\"bank\"),";
Debug.ShouldStop(1);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.tretrx.class, "_mohonbayar",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("bank")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("termin")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("noinvoice")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("vendor")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("proyek")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("bankgaransi")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("perihalsurat")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nosuratkontrak")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nojanjikontrak")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nominalkontrak")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("tglsurat")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("norekening")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("currency")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nokwitansi")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("tglkwitansi")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nosertifikat")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("tglexp")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nofaktur")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("tglfaktur")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nobankgaransi")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("tglbankgaransi")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("swiftkode")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cabang")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("pemilik")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("terminke")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("worknumber")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("status")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("tglinvoice")))),(Object)(_nilaiinvoice),(Object)(_nilaivat),(Object)(_nilaiwithold),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("username")))),(Object)(_method))));
 break; }
case 1: {
 BA.debugLineNum = 35;BA.debugLine="resp.Write(list_pembayaran(req.GetParameter(\"no";
Debug.ShouldStop(4);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.tretrx.class, "_list_pembayaran",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nokontrak")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idvendor")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("username")))),(Object)(_method))));
 break; }
case 2: {
 BA.debugLineNum = 37;BA.debugLine="resp.Write(list_verifikasi(req.GetParameter(\"us";
Debug.ShouldStop(16);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.tretrx.class, "_list_verifikasi",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("username")))),(Object)(_method))));
 break; }
case 3: {
 BA.debugLineNum = 39;BA.debugLine="resp.Write(doc_vendor(req.GetParameter(\"id\"), r";
Debug.ShouldStop(64);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.tretrx.class, "_doc_vendor",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("path")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("name")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("kode")))),(Object)(BA.numberCast(int.class, _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("jml"))))),(Object)(_method))));
 break; }
case 4: {
 BA.debugLineNum = 41;BA.debugLine="resp.Write(detail_permohonan(req.GetParameter(\"";
Debug.ShouldStop(256);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.tretrx.class, "_detail_permohonan",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id_permohonan")))),(Object)(_method))));
 break; }
case 5: {
 BA.debugLineNum = 43;BA.debugLine="resp.Write(list_doc(req.GetParameter(\"id_permoh";
Debug.ShouldStop(1024);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.tretrx.class, "_list_doc",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id_permohonan")))),(Object)(_method))));
 break; }
default: {
 BA.debugLineNum = 45;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
Debug.ShouldStop(4096);
_hasil = tretrx._modgeneratereturn.runMethod(true,"_return_json",(Object)(BA.ObjectToString("TIDAK ADA METHOD")),(Object)(BA.ObjectToString("404")),(Object)(_method));Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 46;BA.debugLine="resp.Write(hasil)";
Debug.ShouldStop(8192);
_resp.runVoidMethod ("Write",(Object)(_hasil));
 break; }
}
;
 BA.debugLineNum = 50;BA.debugLine="End Sub";
Debug.ShouldStop(131072);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (tretrx) ","tretrx",1,__ref.getField(false, "ba"),__ref,8);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "tretrx","initialize", __ref, _ba);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
 BA.debugLineNum = 8;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(128);
 BA.debugLineNum = 9;BA.debugLine="clsid = \"tre_trx\"";
Debug.ShouldStop(256);
__ref.setField ("_clsid",BA.ObjectToString("tre_trx"));
 BA.debugLineNum = 10;BA.debugLine="todb.Initialize";
Debug.ShouldStop(512);
__ref.getField(false,"_todb").runClassMethod (b4j.example.clsdb.class, "_initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 11;BA.debugLine="todbdms.Initialize";
Debug.ShouldStop(1024);
__ref.getField(false,"_todbdms").runClassMethod (b4j.example.clsdbdms.class, "_initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 13;BA.debugLine="End Sub";
Debug.ShouldStop(4096);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _list_doc(RemoteObject __ref,RemoteObject _id,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("list_doc (tretrx) ","tretrx",1,__ref.getField(false, "ba"),__ref,81);
if (RapidSub.canDelegate("list_doc")) return __ref.runUserSub(false, "tretrx","list_doc", __ref, _id, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("id", _id);
Debug.locals.put("method", _method);
 BA.debugLineNum = 81;BA.debugLine="Private Sub list_doc(id As String, method As Strin";
Debug.ShouldStop(65536);
 BA.debugLineNum = 82;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(131072);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 83;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(262144);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 84;BA.debugLine="mp.Initialize";
Debug.ShouldStop(524288);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 86;BA.debugLine="query = $\" select * from v_load_path_file_vendor";
Debug.ShouldStop(2097152);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from v_load_path_file_vendor where f5 = '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_id))),RemoteObject.createImmutable("' or f6 = '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_id))),RemoteObject.createImmutable("' ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 87;BA.debugLine="data = exequery.multi_string(query, method)";
Debug.ShouldStop(4194304);
_data = tretrx._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 89;BA.debugLine="Return data";
Debug.ShouldStop(16777216);
if (true) return _data;
 BA.debugLineNum = 90;BA.debugLine="End Sub";
Debug.ShouldStop(33554432);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _list_pembayaran(RemoteObject __ref,RemoteObject _no_surat,RemoteObject _kode_vendor,RemoteObject _username,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("list_pembayaran (tretrx) ","tretrx",1,__ref.getField(false, "ba"),__ref,70);
if (RapidSub.canDelegate("list_pembayaran")) return __ref.runUserSub(false, "tretrx","list_pembayaran", __ref, _no_surat, _kode_vendor, _username, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("no_surat", _no_surat);
Debug.locals.put("kode_vendor", _kode_vendor);
Debug.locals.put("username", _username);
Debug.locals.put("method", _method);
 BA.debugLineNum = 70;BA.debugLine="Private Sub list_pembayaran(no_surat As String, ko";
Debug.ShouldStop(32);
 BA.debugLineNum = 71;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(64);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 72;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(128);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 73;BA.debugLine="mp.Initialize";
Debug.ShouldStop(256);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 74;BA.debugLine="If username.Length= 0 Then username = \"-\"";
Debug.ShouldStop(512);
if (RemoteObject.solveBoolean("=",_username.runMethod(true,"length"),BA.numberCast(double.class, 0))) { 
_username = BA.ObjectToString("-");Debug.locals.put("username", _username);};
 BA.debugLineNum = 76;BA.debugLine="query = $\" select * from table(PKG_DMS_TRANSAKSI.";
Debug.ShouldStop(2048);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_TRANSAKSI.fn_listmohonbayar('"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_no_surat))),RemoteObject.createImmutable("', '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_kode_vendor))),RemoteObject.createImmutable("', '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_username))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 77;BA.debugLine="data  = exequery.multi_string(query, method)";
Debug.ShouldStop(4096);
_data = tretrx._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 78;BA.debugLine="Return data";
Debug.ShouldStop(8192);
if (true) return _data;
 BA.debugLineNum = 79;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _list_verifikasi(RemoteObject __ref,RemoteObject _p_username,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("list_verifikasi (tretrx) ","tretrx",1,__ref.getField(false, "ba"),__ref,92);
if (RapidSub.canDelegate("list_verifikasi")) return __ref.runUserSub(false, "tretrx","list_verifikasi", __ref, _p_username, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("p_username", _p_username);
Debug.locals.put("method", _method);
 BA.debugLineNum = 92;BA.debugLine="Private Sub list_verifikasi(p_username As String,";
Debug.ShouldStop(134217728);
 BA.debugLineNum = 93;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(268435456);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 94;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(536870912);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 95;BA.debugLine="mp.Initialize";
Debug.ShouldStop(1073741824);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 97;BA.debugLine="query = $\" select * from table(PKG_DMS_TRANSAKSI.";
Debug.ShouldStop(1);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_TRANSAKSI.fn_load_verifikasi('"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_username))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 98;BA.debugLine="data = exequery.multi_string(query, method)";
Debug.ShouldStop(2);
_data = tretrx._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 100;BA.debugLine="Return data";
Debug.ShouldStop(8);
if (true) return _data;
 BA.debugLineNum = 101;BA.debugLine="End Sub";
Debug.ShouldStop(16);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _load_data_verifikasi_uip(RemoteObject __ref) throws Exception{
try {
		Debug.PushSubsStack("load_data_verifikasi_uip (tretrx) ","tretrx",1,__ref.getField(false, "ba"),__ref,103);
if (RapidSub.canDelegate("load_data_verifikasi_uip")) return __ref.runUserSub(false, "tretrx","load_data_verifikasi_uip", __ref);
 BA.debugLineNum = 103;BA.debugLine="Private Sub load_data_verifikasi_uip";
Debug.ShouldStop(64);
 BA.debugLineNum = 105;BA.debugLine="End Sub";
Debug.ShouldStop(256);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _mohonbayar(RemoteObject __ref,RemoteObject _p_bank,RemoteObject _p_termin,RemoteObject _p_invoice,RemoteObject _p_vendor,RemoteObject _p_proyek,RemoteObject _p_bankgaransi,RemoteObject _p_perihal_surat,RemoteObject _p_nosurat_kontrak,RemoteObject _p_noperjanjian_kontrak,RemoteObject _p_nominalkontrak,RemoteObject _p_tglsurat,RemoteObject _p_norekening,RemoteObject _p_currency,RemoteObject _p_nokwitansi,RemoteObject _p_tglkwitansi,RemoteObject _p_nosertifikat,RemoteObject _p_tglexp,RemoteObject _p_nofaktur,RemoteObject _p_tglfaktur,RemoteObject _p_nobank_garansi,RemoteObject _p_tglbank_garansi,RemoteObject _p_swiftkode_rekening,RemoteObject _p_cabang,RemoteObject _p_pemilik,RemoteObject _p_termink_ke,RemoteObject _p_worknumb,RemoteObject _p_status,RemoteObject _tglinvoice,RemoteObject _nilaiinvoice,RemoteObject _nilaivat,RemoteObject _nilaiwhitehold,RemoteObject _username,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("mohonbayar (tretrx) ","tretrx",1,__ref.getField(false, "ba"),__ref,52);
if (RapidSub.canDelegate("mohonbayar")) return __ref.runUserSub(false, "tretrx","mohonbayar", __ref, _p_bank, _p_termin, _p_invoice, _p_vendor, _p_proyek, _p_bankgaransi, _p_perihal_surat, _p_nosurat_kontrak, _p_noperjanjian_kontrak, _p_nominalkontrak, _p_tglsurat, _p_norekening, _p_currency, _p_nokwitansi, _p_tglkwitansi, _p_nosertifikat, _p_tglexp, _p_nofaktur, _p_tglfaktur, _p_nobank_garansi, _p_tglbank_garansi, _p_swiftkode_rekening, _p_cabang, _p_pemilik, _p_termink_ke, _p_worknumb, _p_status, _tglinvoice, _nilaiinvoice, _nilaivat, _nilaiwhitehold, _username, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("p_bank", _p_bank);
Debug.locals.put("p_termin", _p_termin);
Debug.locals.put("p_invoice", _p_invoice);
Debug.locals.put("p_vendor", _p_vendor);
Debug.locals.put("p_proyek", _p_proyek);
Debug.locals.put("p_bankgaransi", _p_bankgaransi);
Debug.locals.put("p_perihal_surat", _p_perihal_surat);
Debug.locals.put("p_nosurat_kontrak", _p_nosurat_kontrak);
Debug.locals.put("p_noperjanjian_kontrak", _p_noperjanjian_kontrak);
Debug.locals.put("p_nominalkontrak", _p_nominalkontrak);
Debug.locals.put("p_tglsurat", _p_tglsurat);
Debug.locals.put("p_norekening", _p_norekening);
Debug.locals.put("p_currency", _p_currency);
Debug.locals.put("p_nokwitansi", _p_nokwitansi);
Debug.locals.put("p_tglkwitansi", _p_tglkwitansi);
Debug.locals.put("p_nosertifikat", _p_nosertifikat);
Debug.locals.put("p_tglexp", _p_tglexp);
Debug.locals.put("p_nofaktur", _p_nofaktur);
Debug.locals.put("p_tglfaktur", _p_tglfaktur);
Debug.locals.put("p_nobank_garansi", _p_nobank_garansi);
Debug.locals.put("p_tglbank_garansi", _p_tglbank_garansi);
Debug.locals.put("p_swiftkode_rekening", _p_swiftkode_rekening);
Debug.locals.put("p_cabang", _p_cabang);
Debug.locals.put("p_pemilik", _p_pemilik);
Debug.locals.put("p_termink_ke", _p_termink_ke);
Debug.locals.put("p_worknumb", _p_worknumb);
Debug.locals.put("p_status", _p_status);
Debug.locals.put("tglinvoice", _tglinvoice);
Debug.locals.put("nilaiinvoice", _nilaiinvoice);
Debug.locals.put("nilaivat", _nilaivat);
Debug.locals.put("nilaiwhitehold", _nilaiwhitehold);
Debug.locals.put("username", _username);
Debug.locals.put("method", _method);
 BA.debugLineNum = 52;BA.debugLine="Private Sub mohonbayar(p_bank As String, p_termin";
Debug.ShouldStop(524288);
 BA.debugLineNum = 53;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(1048576);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 54;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(2097152);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 55;BA.debugLine="mp.Initialize";
Debug.ShouldStop(4194304);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 60;BA.debugLine="query = $\" select * from table(PKG_DMS_TRANSAKSI.";
Debug.ShouldStop(134217728);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_TRANSAKSI.fn_savemohonbayar('"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_bank))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_termin))),RemoteObject.createImmutable("', '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_invoice))),RemoteObject.createImmutable("', '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_vendor))),RemoteObject.createImmutable("',\n"),RemoteObject.createImmutable("	'"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_proyek))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_bankgaransi))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_perihal_surat))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_nosurat_kontrak))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_noperjanjian_kontrak))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_nominalkontrak))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_tglsurat))),RemoteObject.createImmutable("',\n"),RemoteObject.createImmutable("	'"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_norekening))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_currency))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_nokwitansi))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_tglkwitansi))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_nosertifikat))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_tglexp))),RemoteObject.createImmutable("',\n"),RemoteObject.createImmutable("	'"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_nofaktur))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_tglfaktur))),RemoteObject.createImmutable("',\n"),RemoteObject.createImmutable("	'"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_nobank_garansi))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_tglbank_garansi))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_swiftkode_rekening))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_cabang))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_pemilik))),RemoteObject.createImmutable("',\n"),RemoteObject.createImmutable("	'"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_termink_ke))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_worknumb))),RemoteObject.createImmutable("','"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_p_status))),RemoteObject.createImmutable("', '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_tglinvoice))),RemoteObject.createImmutable("', '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_nilaiinvoice))),RemoteObject.createImmutable("', '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_nilaivat))),RemoteObject.createImmutable("', '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_nilaiwhitehold))),RemoteObject.createImmutable("', '"),tretrx.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_username))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 66;BA.debugLine="data  = exequery.multi_string(query, method)";
Debug.ShouldStop(2);
_data = tretrx._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 67;BA.debugLine="Return data";
Debug.ShouldStop(4);
if (true) return _data;
 BA.debugLineNum = 68;BA.debugLine="End Sub";
Debug.ShouldStop(8);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}