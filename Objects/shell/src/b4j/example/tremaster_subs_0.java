package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class tremaster_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private clsid As String";
tremaster._clsid = RemoteObject.createImmutable("");__ref.setField("_clsid",tremaster._clsid);
 //BA.debugLineNum = 4;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _handle(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Handle (tremaster) ","tremaster",10,__ref.getField(false, "ba"),__ref,10);
if (RapidSub.canDelegate("handle")) return __ref.runUserSub(false, "tremaster","handle", __ref, _req, _resp);
RemoteObject _appid = RemoteObject.createImmutable("");
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _method = RemoteObject.createImmutable("");
RemoteObject _response = RemoteObject.createImmutable("");
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 10;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
Debug.ShouldStop(512);
 BA.debugLineNum = 11;BA.debugLine="Dim appid, hasil, method As String";
Debug.ShouldStop(1024);
_appid = RemoteObject.createImmutable("");Debug.locals.put("appid", _appid);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
_method = RemoteObject.createImmutable("");Debug.locals.put("method", _method);
 BA.debugLineNum = 12;BA.debugLine="Dim response As String";
Debug.ShouldStop(2048);
_response = RemoteObject.createImmutable("");Debug.locals.put("response", _response);
 BA.debugLineNum = 14;BA.debugLine="If req.Method <> \"POST\" Then";
Debug.ShouldStop(8192);
if (RemoteObject.solveBoolean("!",_req.runMethod(true,"getMethod"),BA.ObjectToString("POST"))) { 
 BA.debugLineNum = 15;BA.debugLine="resp.SendError(500, \"method not supported.\")";
Debug.ShouldStop(16384);
_resp.runVoidMethodAndSync ("SendError",(Object)(BA.numberCast(int.class, 500)),(Object)(RemoteObject.createImmutable("method not supported.")));
 BA.debugLineNum = 16;BA.debugLine="Return";
Debug.ShouldStop(32768);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 20;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
Debug.ShouldStop(524288);
_appid = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("clsakses")));Debug.locals.put("appid", _appid);
 BA.debugLineNum = 22;BA.debugLine="method = req.GetParameter(\"method\")";
Debug.ShouldStop(2097152);
_method = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("method")));Debug.locals.put("method", _method);
 BA.debugLineNum = 24;BA.debugLine="If appid <> clsid Then";
Debug.ShouldStop(8388608);
if (RemoteObject.solveBoolean("!",_appid,__ref.getField(true,"_clsid"))) { 
 BA.debugLineNum = 25;BA.debugLine="response = modGenerateReturn.return_json(modGene";
Debug.ShouldStop(16777216);
_response = tremaster._modgeneratereturn.runMethod(true,"_return_json",(Object)(tremaster._modgeneratereturn._label_rejected),(Object)(tremaster._modgeneratereturn._kode_rejected_class),(Object)(_method));Debug.locals.put("response", _response);
 BA.debugLineNum = 26;BA.debugLine="resp.Write(response)";
Debug.ShouldStop(33554432);
_resp.runVoidMethod ("Write",(Object)(_response));
 BA.debugLineNum = 27;BA.debugLine="Return";
Debug.ShouldStop(67108864);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 30;BA.debugLine="Select method";
Debug.ShouldStop(536870912);
switch (BA.switchObjectToInt(_method,BA.ObjectToString("createbank"),BA.ObjectToString("readbank"),BA.ObjectToString("updatebank"),BA.ObjectToString("deletebank"),BA.ObjectToString("createvendor"),BA.ObjectToString("readvendor"),BA.ObjectToString("editvendor"),BA.ObjectToString("deletevendor"),BA.ObjectToString("createtipebayar"),BA.ObjectToString("readtipebayar"),BA.ObjectToString("updatetipebayar"),BA.ObjectToString("deletetipebayar"),BA.ObjectToString("createtermin"),BA.ObjectToString("readtermin"),BA.ObjectToString("updatetermin"),BA.ObjectToString("deletetermin"),BA.ObjectToString("createsumberdana"),BA.ObjectToString("readsumberdana"),BA.ObjectToString("updatesumberdana"),BA.ObjectToString("deletesumberdana"),BA.ObjectToString("createjenisproyek"),BA.ObjectToString("readjenisproyek"),BA.ObjectToString("updatejenisproyek"),BA.ObjectToString("deletejenisproyek"),BA.ObjectToString("createproyek"),BA.ObjectToString("readproyek"),BA.ObjectToString("updateproyek"),BA.ObjectToString("deleteproyek"),BA.ObjectToString("createunit"),BA.ObjectToString("readunit"),BA.ObjectToString("updateunit"),BA.ObjectToString("deleteunit"),BA.ObjectToString("readjabatan"))) {
case 0: {
 BA.debugLineNum = 32;BA.debugLine="hasil = clsMaster.create_bank(req.GetParameter(";
Debug.ShouldStop(-2147483648);
_hasil = tremaster._clsmaster.runMethod(true,"_create_bank",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namabank")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 1: {
 BA.debugLineNum = 34;BA.debugLine="hasil = clsMaster.read_bank(req.GetParameter(\"c";
Debug.ShouldStop(2);
_hasil = tremaster._clsmaster.runMethod(true,"_read_bank",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cari")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 2: {
 BA.debugLineNum = 36;BA.debugLine="hasil = clsMaster.update_bank(req.GetParameter(";
Debug.ShouldStop(8);
_hasil = tremaster._clsmaster.runMethod(true,"_update_bank",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 3: {
 BA.debugLineNum = 38;BA.debugLine="hasil = clsMaster.delete_bank(req.GetParameter(";
Debug.ShouldStop(32);
_hasil = tremaster._clsmaster.runMethod(true,"_delete_bank",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 4: {
 BA.debugLineNum = 41;BA.debugLine="hasil = clsMaster.create_vendor(req.GetParamete";
Debug.ShouldStop(256);
_hasil = tremaster._clsmaster.runMethod(true,"_create_vendor",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namavendor")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namajabatan")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("sebutanpejabatvendor")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namavendor1")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namavendor2")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namavendor3")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namavendor4")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 5: {
 BA.debugLineNum = 43;BA.debugLine="hasil = clsMaster.read_vendor(req.GetParameter(";
Debug.ShouldStop(1024);
_hasil = tremaster._clsmaster.runMethod(true,"_read_vendor",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cari")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 6: {
 BA.debugLineNum = 45;BA.debugLine="hasil = clsMaster.update_vendor(req.GetParamete";
Debug.ShouldStop(4096);
_hasil = tremaster._clsmaster.runMethod(true,"_update_vendor",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("kodevendor")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namavendor")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namajabatan")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("sebutanpejabatvendor")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namavendor1")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namavendor2")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namavendor3")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namavendor4")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 7: {
 BA.debugLineNum = 47;BA.debugLine="hasil = clsMaster.delete_vendor(req.GetParamete";
Debug.ShouldStop(16384);
_hasil = tremaster._clsmaster.runMethod(true,"_delete_vendor",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("kodevendor")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 8: {
 BA.debugLineNum = 50;BA.debugLine="hasil = clsMaster.create_tipebayar(req.GetParam";
Debug.ShouldStop(131072);
_hasil = tremaster._clsmaster.runMethod(true,"_create_tipebayar",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namatipebayar")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 9: {
 BA.debugLineNum = 52;BA.debugLine="hasil = clsMaster.read_tipebayar(req.GetParamet";
Debug.ShouldStop(524288);
_hasil = tremaster._clsmaster.runMethod(true,"_read_tipebayar",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cari")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 10: {
 BA.debugLineNum = 54;BA.debugLine="hasil = clsMaster.update_tipebayar(req.GetParam";
Debug.ShouldStop(2097152);
_hasil = tremaster._clsmaster.runMethod(true,"_update_tipebayar",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 11: {
 BA.debugLineNum = 56;BA.debugLine="hasil = clsMaster.delete_tipebayar(req.GetParam";
Debug.ShouldStop(8388608);
_hasil = tremaster._clsmaster.runMethod(true,"_delete_tipebayar",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 12: {
 BA.debugLineNum = 59;BA.debugLine="hasil = clsMaster.create_termin(req.GetParamete";
Debug.ShouldStop(67108864);
_hasil = tremaster._clsmaster.runMethod(true,"_create_termin",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namatermin")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("kodetermin")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idbayar")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 13: {
 BA.debugLineNum = 61;BA.debugLine="hasil = clsMaster.read_termin(req.GetParameter(";
Debug.ShouldStop(268435456);
_hasil = tremaster._clsmaster.runMethod(true,"_read_termin",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cari")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 14: {
 BA.debugLineNum = 63;BA.debugLine="hasil = clsMaster.update_termin(req.GetParamete";
Debug.ShouldStop(1073741824);
_hasil = tremaster._clsmaster.runMethod(true,"_update_termin",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("kode")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 15: {
 BA.debugLineNum = 65;BA.debugLine="hasil = clsMaster.delete_termin(req.GetParamete";
Debug.ShouldStop(1);
_hasil = tremaster._clsmaster.runMethod(true,"_delete_termin",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 16: {
 BA.debugLineNum = 68;BA.debugLine="hasil = clsMaster.create_sumberdana(req.GetPara";
Debug.ShouldStop(8);
_hasil = tremaster._clsmaster.runMethod(true,"_create_sumberdana",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("namatipebayar")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 17: {
 BA.debugLineNum = 70;BA.debugLine="hasil = clsMaster.read_sumberdana(req.GetParame";
Debug.ShouldStop(32);
_hasil = tremaster._clsmaster.runMethod(true,"_read_sumberdana",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cari")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 18: {
 BA.debugLineNum = 72;BA.debugLine="hasil = clsMaster.update_sumberdana(req.GetPara";
Debug.ShouldStop(128);
_hasil = tremaster._clsmaster.runMethod(true,"_update_sumberdana",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 19: {
 BA.debugLineNum = 74;BA.debugLine="hasil = clsMaster.delete_sumberdana(req.GetPara";
Debug.ShouldStop(512);
_hasil = tremaster._clsmaster.runMethod(true,"_delete_sumberdana",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 20: {
 BA.debugLineNum = 77;BA.debugLine="hasil = clsMaster.create_jenisproyek(req.GetPar";
Debug.ShouldStop(4096);
_hasil = tremaster._clsmaster.runMethod(true,"_create_jenisproyek",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("kode")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 21: {
 BA.debugLineNum = 79;BA.debugLine="hasil = clsMaster.read_jenisproyek(req.GetParam";
Debug.ShouldStop(16384);
_hasil = tremaster._clsmaster.runMethod(true,"_read_jenisproyek",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cari")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 22: {
 BA.debugLineNum = 81;BA.debugLine="hasil = clsMaster.update_jenisproyek(req.GetPar";
Debug.ShouldStop(65536);
_hasil = tremaster._clsmaster.runMethod(true,"_update_jenisproyek",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("kode")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 23: {
 BA.debugLineNum = 83;BA.debugLine="hasil = clsMaster.delete_jenisproyek(req.GetPar";
Debug.ShouldStop(262144);
_hasil = tremaster._clsmaster.runMethod(true,"_delete_jenisproyek",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 24: {
 BA.debugLineNum = 86;BA.debugLine="hasil = clsMaster.create_proyek(req.GetParamete";
Debug.ShouldStop(2097152);
_hasil = tremaster._clsmaster.runMethod(true,"_create_proyek",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("kode")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idunit")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idjenisproyek")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 25: {
 BA.debugLineNum = 88;BA.debugLine="hasil = clsMaster.read_proyek(req.GetParameter(";
Debug.ShouldStop(8388608);
_hasil = tremaster._clsmaster.runMethod(true,"_read_proyek",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cari")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 26: {
 BA.debugLineNum = 90;BA.debugLine="hasil = clsMaster.update_proyek(req.GetParamete";
Debug.ShouldStop(33554432);
_hasil = tremaster._clsmaster.runMethod(true,"_update_proyek",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("kode")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idunit")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("idjenisproyek")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 27: {
 BA.debugLineNum = 92;BA.debugLine="hasil = clsMaster.delete_proyek(req.GetParamete";
Debug.ShouldStop(134217728);
_hasil = tremaster._clsmaster.runMethod(true,"_delete_proyek",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 28: {
 BA.debugLineNum = 95;BA.debugLine="hasil = clsMaster.create_unit(req.GetParameter(";
Debug.ShouldStop(1073741824);
_hasil = tremaster._clsmaster.runMethod(true,"_create_unit",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("singkatan")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("as")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("alamat")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("ket")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 29: {
 BA.debugLineNum = 97;BA.debugLine="hasil = clsMaster.read_unit(req.GetParameter(\"c";
Debug.ShouldStop(1);
_hasil = tremaster._clsmaster.runMethod(true,"_read_unit",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cari")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 30: {
 BA.debugLineNum = 99;BA.debugLine="hasil = clsMaster.update_unit(req.GetParameter(";
Debug.ShouldStop(4);
_hasil = tremaster._clsmaster.runMethod(true,"_update_unit",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("nama")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("singkatan")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("as")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("alamat")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("ket")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 31: {
 BA.debugLineNum = 101;BA.debugLine="hasil = clsMaster.delete_unit(req.GetParameter(";
Debug.ShouldStop(16);
_hasil = tremaster._clsmaster.runMethod(true,"_delete_unit",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("id")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
case 32: {
 BA.debugLineNum = 104;BA.debugLine="hasil = clsMaster.read_jabatan(req.GetParameter";
Debug.ShouldStop(128);
_hasil = tremaster._clsmaster.runMethod(true,"_read_jabatan",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("cari")))),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
default: {
 BA.debugLineNum = 107;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
Debug.ShouldStop(1024);
_hasil = tremaster._modgeneratereturn.runMethod(true,"_return_json",(Object)(BA.ObjectToString("TIDAK ADA METHOD")),(Object)(BA.ObjectToString("404")),(Object)(_method));Debug.locals.put("hasil", _hasil);
 break; }
}
;
 BA.debugLineNum = 110;BA.debugLine="resp.Write(hasil)";
Debug.ShouldStop(8192);
_resp.runVoidMethod ("Write",(Object)(_hasil));
 BA.debugLineNum = 111;BA.debugLine="End Sub";
Debug.ShouldStop(16384);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (tremaster) ","tremaster",10,__ref.getField(false, "ba"),__ref,6);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "tremaster","initialize", __ref, _ba);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
 BA.debugLineNum = 6;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(32);
 BA.debugLineNum = 7;BA.debugLine="clsid = \"tre_menumaster\"";
Debug.ShouldStop(64);
__ref.setField ("_clsid",BA.ObjectToString("tre_menumaster"));
 BA.debugLineNum = 8;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}