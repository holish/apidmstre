package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class clsdb_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private serverdb, pwd, user As String";
clsdb._serverdb = RemoteObject.createImmutable("");__ref.setField("_serverdb",clsdb._serverdb);
clsdb._pwd = RemoteObject.createImmutable("");__ref.setField("_pwd",clsdb._pwd);
clsdb._user = RemoteObject.createImmutable("");__ref.setField("_user",clsdb._user);
 //BA.debugLineNum = 4;BA.debugLine="Dim std As String=\"oracle.jdbc.driver.OracleDrive";
clsdb._std = BA.ObjectToString("oracle.jdbc.driver.OracleDriver");__ref.setField("_std",clsdb._std);
 //BA.debugLineNum = 5;BA.debugLine="Private en As StringUtils";
clsdb._en = RemoteObject.createNew ("anywheresoftware.b4a.objects.StringUtils");__ref.setField("_en",clsdb._en);
 //BA.debugLineNum = 6;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (clsdb) ","clsdb",3,__ref.getField(false, "ba"),__ref,8);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "clsdb","initialize", __ref, _ba);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
 BA.debugLineNum = 8;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(128);
 BA.debugLineNum = 9;BA.debugLine="serverdb = Main.ipdb & \"/\" & Main.tns";
Debug.ShouldStop(256);
__ref.setField ("_serverdb",RemoteObject.concat(clsdb._main._ipdb,RemoteObject.createImmutable("/"),clsdb._main._tns));
 BA.debugLineNum = 10;BA.debugLine="user = Main.userdb";
Debug.ShouldStop(512);
__ref.setField ("_user",clsdb._main._userdb);
 BA.debugLineNum = 11;BA.debugLine="pwd = Main.pwddb";
Debug.ShouldStop(1024);
__ref.setField ("_pwd",clsdb._main._pwddb);
 BA.debugLineNum = 12;BA.debugLine="End Sub";
Debug.ShouldStop(2048);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sqlselectmap(RemoteObject __ref,RemoteObject _sql,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("SqlSelectMap (clsdb) ","clsdb",3,__ref.getField(false, "ba"),__ref,14);
if (RapidSub.canDelegate("sqlselectmap")) return __ref.runUserSub(false, "clsdb","sqlselectmap", __ref, _sql, _method);
RemoteObject _koneksi = RemoteObject.declareNull("anywheresoftware.b4j.objects.SQL");
RemoteObject _rs = RemoteObject.declareNull("anywheresoftware.b4j.objects.SQL.ResultSetWrapper");
RemoteObject _i = RemoteObject.createImmutable(0);
RemoteObject _j = RemoteObject.createImmutable(0);
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _field = RemoteObject.createImmutable("");
RemoteObject _error = RemoteObject.createImmutable("");
RemoteObject _lret = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _val = RemoteObject.createImmutable("");
Debug.locals.put("Sql", _sql);
Debug.locals.put("method", _method);
 BA.debugLineNum = 14;BA.debugLine="Sub SqlSelectMap(Sql As String, method As String)";
Debug.ShouldStop(8192);
 BA.debugLineNum = 15;BA.debugLine="Dim koneksi As SQL";
Debug.ShouldStop(16384);
_koneksi = RemoteObject.createNew ("anywheresoftware.b4j.objects.SQL");Debug.locals.put("koneksi", _koneksi);
 BA.debugLineNum = 16;BA.debugLine="Dim rs As ResultSet";
Debug.ShouldStop(32768);
_rs = RemoteObject.createNew ("anywheresoftware.b4j.objects.SQL.ResultSetWrapper");Debug.locals.put("rs", _rs);
 BA.debugLineNum = 17;BA.debugLine="Dim i, j As Int";
Debug.ShouldStop(65536);
_i = RemoteObject.createImmutable(0);Debug.locals.put("i", _i);
_j = RemoteObject.createImmutable(0);Debug.locals.put("j", _j);
 BA.debugLineNum = 18;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(131072);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 19;BA.debugLine="Dim field, error As String";
Debug.ShouldStop(262144);
_field = RemoteObject.createImmutable("");Debug.locals.put("field", _field);
_error = RemoteObject.createImmutable("");Debug.locals.put("error", _error);
 BA.debugLineNum = 20;BA.debugLine="Dim lRet As List";
Debug.ShouldStop(524288);
_lret = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("lRet", _lret);
 BA.debugLineNum = 21;BA.debugLine="mp.Initialize";
Debug.ShouldStop(1048576);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 22;BA.debugLine="lRet.Initialize";
Debug.ShouldStop(2097152);
_lret.runVoidMethod ("Initialize");
 BA.debugLineNum = 25;BA.debugLine="Try";
Debug.ShouldStop(16777216);
try { BA.debugLineNum = 26;BA.debugLine="If koneksi.IsInitialized = True Then";
Debug.ShouldStop(33554432);
if (RemoteObject.solveBoolean("=",_koneksi.runMethod(true,"IsInitialized"),clsdb.__c.getField(true,"True"))) { 
 BA.debugLineNum = 27;BA.debugLine="koneksi.Close";
Debug.ShouldStop(67108864);
_koneksi.runVoidMethod ("Close");
 };
 BA.debugLineNum = 29;BA.debugLine="koneksi.Initialize2(std,\"jdbc:oracle:thin:@//\" &";
Debug.ShouldStop(268435456);
_koneksi.runVoidMethod ("Initialize2",(Object)(__ref.getField(true,"_std")),(Object)(RemoteObject.concat(RemoteObject.createImmutable("jdbc:oracle:thin:@//"),__ref.getField(true,"_serverdb"))),(Object)(__ref.getField(true,"_user")),(Object)(__ref.getField(true,"_pwd")));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e15) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e15.toString()); BA.debugLineNum = 31;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
Debug.ShouldStop(1073741824);
_mp = clsdb.__c.runMethod(false, "createMap", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("response")),(clsdb._modgeneratereturn._kode_gagal_koneksi_db),RemoteObject.createImmutable(("method")),(_method),RemoteObject.createImmutable(("pesan")),(__ref.getField(false,"_en").runMethod(true,"EncodeUrl",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")),(Object)(RemoteObject.createImmutable("UTF8")))),RemoteObject.createImmutable(("data")),(RemoteObject.createNewArray("Object",new int[] {0},new Object[] {}))}));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 32;BA.debugLine="Main.appendSqlLog(\" \")";
Debug.ShouldStop(-2147483648);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 33;BA.debugLine="Main.appendSqlLog(Sql)";
Debug.ShouldStop(1);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(_sql));
 BA.debugLineNum = 34;BA.debugLine="Main.appendSqlLog(LastException.Message)";
Debug.ShouldStop(2);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")));
 BA.debugLineNum = 35;BA.debugLine="koneksi.Close";
Debug.ShouldStop(4);
_koneksi.runVoidMethod ("Close");
 BA.debugLineNum = 36;BA.debugLine="Return mp";
Debug.ShouldStop(8);
if (true) return _mp;
 };
 BA.debugLineNum = 40;BA.debugLine="Try";
Debug.ShouldStop(128);
try { BA.debugLineNum = 41;BA.debugLine="rs = koneksi.ExecQuery(Sql.Replace(\"'null'\",Null";
Debug.ShouldStop(256);
_rs = _koneksi.runMethod(false,"ExecQuery",(Object)(_sql.runMethod(true,"replace",(Object)(BA.ObjectToString("'null'")),(Object)(BA.ObjectToString(clsdb.__c.getField(false,"Null"))))));Debug.locals.put("rs", _rs);
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e25) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e25.toString()); BA.debugLineNum = 43;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
Debug.ShouldStop(1024);
_mp = clsdb.__c.runMethod(false, "createMap", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("response")),(clsdb._modgeneratereturn._kode_gagal_execute_query),RemoteObject.createImmutable(("method")),(_method),RemoteObject.createImmutable(("pesan")),(__ref.getField(false,"_en").runMethod(true,"EncodeUrl",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")),(Object)(RemoteObject.createImmutable("UTF8")))),RemoteObject.createImmutable(("data")),(RemoteObject.createNewArray("Object",new int[] {0},new Object[] {}))}));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 44;BA.debugLine="Main.appendSqlLog(\" \")";
Debug.ShouldStop(2048);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 45;BA.debugLine="Main.appendSqlLog(Sql)";
Debug.ShouldStop(4096);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(_sql));
 BA.debugLineNum = 46;BA.debugLine="Main.appendSqlLog(LastException.Message)";
Debug.ShouldStop(8192);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")));
 BA.debugLineNum = 47;BA.debugLine="koneksi.Close";
Debug.ShouldStop(16384);
_koneksi.runVoidMethod ("Close");
 BA.debugLineNum = 48;BA.debugLine="Return mp";
Debug.ShouldStop(32768);
if (true) return _mp;
 };
 BA.debugLineNum = 52;BA.debugLine="Try";
Debug.ShouldStop(524288);
try { BA.debugLineNum = 53;BA.debugLine="i=0";
Debug.ShouldStop(1048576);
_i = BA.numberCast(int.class, 0);Debug.locals.put("i", _i);
 BA.debugLineNum = 54;BA.debugLine="j=0";
Debug.ShouldStop(2097152);
_j = BA.numberCast(int.class, 0);Debug.locals.put("j", _j);
 BA.debugLineNum = 55;BA.debugLine="Do While rs.NextRow";
Debug.ShouldStop(4194304);
while (_rs.runMethod(true,"NextRow").<Boolean>get().booleanValue()) {
 BA.debugLineNum = 56;BA.debugLine="mp.Initialize";
Debug.ShouldStop(8388608);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 57;BA.debugLine="j = j+1";
Debug.ShouldStop(16777216);
_j = RemoteObject.solve(new RemoteObject[] {_j,RemoteObject.createImmutable(1)}, "+",1, 1);Debug.locals.put("j", _j);
 BA.debugLineNum = 58;BA.debugLine="For i=0 To rs.ColumnCount-1";
Debug.ShouldStop(33554432);
{
final int step38 = 1;
final int limit38 = RemoteObject.solve(new RemoteObject[] {_rs.runMethod(true,"getColumnCount"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = BA.numberCast(int.class, 0) ;
for (;(step38 > 0 && _i.<Integer>get().intValue() <= limit38) || (step38 < 0 && _i.<Integer>get().intValue() >= limit38) ;_i = RemoteObject.createImmutable((int)(0 + _i.<Integer>get().intValue() + step38))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 59;BA.debugLine="Dim val As String";
Debug.ShouldStop(67108864);
_val = RemoteObject.createImmutable("");Debug.locals.put("val", _val);
 BA.debugLineNum = 60;BA.debugLine="val = rs.GetString2(i)";
Debug.ShouldStop(134217728);
_val = _rs.runMethod(true,"GetString2",(Object)(_i));Debug.locals.put("val", _val);
 BA.debugLineNum = 61;BA.debugLine="If val <> Null Then";
Debug.ShouldStop(268435456);
if (RemoteObject.solveBoolean("N",_val)) { 
 BA.debugLineNum = 62;BA.debugLine="val = en.EncodeUrl(val, \"UTF8\")";
Debug.ShouldStop(536870912);
_val = __ref.getField(false,"_en").runMethod(true,"EncodeUrl",(Object)(_val),(Object)(RemoteObject.createImmutable("UTF8")));Debug.locals.put("val", _val);
 }else {
 BA.debugLineNum = 64;BA.debugLine="val = \"null\"";
Debug.ShouldStop(-2147483648);
_val = BA.ObjectToString("null");Debug.locals.put("val", _val);
 };
 BA.debugLineNum = 67;BA.debugLine="field = rs.GetColumnName(i).ToLowerCase";
Debug.ShouldStop(4);
_field = _rs.runMethod(true,"GetColumnName",(Object)(_i)).runMethod(true,"toLowerCase");Debug.locals.put("field", _field);
 BA.debugLineNum = 68;BA.debugLine="If field = \"GETDATE(ENTRYTIME)\" Then";
Debug.ShouldStop(8);
if (RemoteObject.solveBoolean("=",_field,BA.ObjectToString("GETDATE(ENTRYTIME)"))) { 
 BA.debugLineNum = 69;BA.debugLine="field = \"DATE\"";
Debug.ShouldStop(16);
_field = BA.ObjectToString("DATE");Debug.locals.put("field", _field);
 };
 BA.debugLineNum = 71;BA.debugLine="mp.Put(field, val)";
Debug.ShouldStop(64);
_mp.runVoidMethod ("Put",(Object)((_field)),(Object)((_val)));
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 73;BA.debugLine="If method = \"GETMODUL\" Then mp.Put(\"nama\",modGe";
Debug.ShouldStop(256);
if (RemoteObject.solveBoolean("=",_method,BA.ObjectToString("GETMODUL"))) { 
_mp.runVoidMethod ("Put",(Object)(RemoteObject.createImmutable(("nama"))),(Object)((clsdb._modgeneratereturn._namauser)));};
 BA.debugLineNum = 74;BA.debugLine="lRet.Add(mp)";
Debug.ShouldStop(512);
_lret.runVoidMethod ("Add",(Object)((_mp.getObject())));
 BA.debugLineNum = 75;BA.debugLine="mp = Null";
Debug.ShouldStop(1024);
_mp.setObject(clsdb.__c.getField(false,"Null"));
 }
;
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e57) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e57.toString()); BA.debugLineNum = 81;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
Debug.ShouldStop(65536);
_mp = clsdb.__c.runMethod(false, "createMap", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("response")),(clsdb._modgeneratereturn._kode_gagal_parsing),RemoteObject.createImmutable(("method")),(_method),RemoteObject.createImmutable(("pesan")),(__ref.getField(false,"_en").runMethod(true,"EncodeUrl",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")),(Object)(RemoteObject.createImmutable("UTF8")))),RemoteObject.createImmutable(("data")),(RemoteObject.createNewArray("Object",new int[] {0},new Object[] {}))}));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 82;BA.debugLine="Main.appendSqlLog(\" \")";
Debug.ShouldStop(131072);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 83;BA.debugLine="Main.appendSqlLog(Sql)";
Debug.ShouldStop(262144);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(_sql));
 BA.debugLineNum = 84;BA.debugLine="Main.appendSqlLog(LastException.Message)";
Debug.ShouldStop(524288);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")));
 BA.debugLineNum = 85;BA.debugLine="koneksi.Close";
Debug.ShouldStop(1048576);
_koneksi.runVoidMethod ("Close");
 BA.debugLineNum = 86;BA.debugLine="Return mp";
Debug.ShouldStop(2097152);
if (true) return _mp;
 };
 BA.debugLineNum = 88;BA.debugLine="koneksi.Close";
Debug.ShouldStop(8388608);
_koneksi.runVoidMethod ("Close");
 BA.debugLineNum = 89;BA.debugLine="mp = CreateMap(\"response\":modGenerateReturn.kode_";
Debug.ShouldStop(16777216);
_mp = clsdb.__c.runMethod(false, "createMap", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("response")),(clsdb._modgeneratereturn._kode_succes),RemoteObject.createImmutable(("method")),(_method),RemoteObject.createImmutable(("pesan")),RemoteObject.createImmutable(("")),RemoteObject.createImmutable(("data")),(_lret.getObject())}));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 90;BA.debugLine="Return mp";
Debug.ShouldStop(33554432);
if (true) return _mp;
 BA.debugLineNum = 91;BA.debugLine="End Sub";
Debug.ShouldStop(67108864);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _sqlselectsinglemap(RemoteObject __ref,RemoteObject _sql,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("SqlSelectSingleMap (clsdb) ","clsdb",3,__ref.getField(false, "ba"),__ref,93);
if (RapidSub.canDelegate("sqlselectsinglemap")) return __ref.runUserSub(false, "clsdb","sqlselectsinglemap", __ref, _sql, _method);
RemoteObject _koneksi = RemoteObject.declareNull("anywheresoftware.b4j.objects.SQL");
RemoteObject _rs = RemoteObject.declareNull("anywheresoftware.b4j.objects.SQL.ResultSetWrapper");
RemoteObject _i = RemoteObject.createImmutable(0);
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _field = RemoteObject.createImmutable("");
RemoteObject _lret = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _val = RemoteObject.createImmutable("");
Debug.locals.put("Sql", _sql);
Debug.locals.put("method", _method);
 BA.debugLineNum = 93;BA.debugLine="Sub SqlSelectSingleMap(Sql As String, method As St";
Debug.ShouldStop(268435456);
 BA.debugLineNum = 94;BA.debugLine="Dim koneksi As SQL";
Debug.ShouldStop(536870912);
_koneksi = RemoteObject.createNew ("anywheresoftware.b4j.objects.SQL");Debug.locals.put("koneksi", _koneksi);
 BA.debugLineNum = 95;BA.debugLine="Dim rs As ResultSet";
Debug.ShouldStop(1073741824);
_rs = RemoteObject.createNew ("anywheresoftware.b4j.objects.SQL.ResultSetWrapper");Debug.locals.put("rs", _rs);
 BA.debugLineNum = 96;BA.debugLine="Dim i As Int";
Debug.ShouldStop(-2147483648);
_i = RemoteObject.createImmutable(0);Debug.locals.put("i", _i);
 BA.debugLineNum = 97;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(1);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 98;BA.debugLine="Dim field As String";
Debug.ShouldStop(2);
_field = RemoteObject.createImmutable("");Debug.locals.put("field", _field);
 BA.debugLineNum = 99;BA.debugLine="Dim lRet As List";
Debug.ShouldStop(4);
_lret = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("lRet", _lret);
 BA.debugLineNum = 100;BA.debugLine="mp.Initialize";
Debug.ShouldStop(8);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 101;BA.debugLine="lRet.Initialize";
Debug.ShouldStop(16);
_lret.runVoidMethod ("Initialize");
 BA.debugLineNum = 104;BA.debugLine="Try";
Debug.ShouldStop(128);
try { BA.debugLineNum = 105;BA.debugLine="If koneksi.IsInitialized = True Then";
Debug.ShouldStop(256);
if (RemoteObject.solveBoolean("=",_koneksi.runMethod(true,"IsInitialized"),clsdb.__c.getField(true,"True"))) { 
 BA.debugLineNum = 106;BA.debugLine="koneksi.Close";
Debug.ShouldStop(512);
_koneksi.runVoidMethod ("Close");
 };
 BA.debugLineNum = 108;BA.debugLine="koneksi.Initialize2(std,\"jdbc:oracle:thin:@//\" &";
Debug.ShouldStop(2048);
_koneksi.runVoidMethod ("Initialize2",(Object)(__ref.getField(true,"_std")),(Object)(RemoteObject.concat(RemoteObject.createImmutable("jdbc:oracle:thin:@//"),__ref.getField(true,"_serverdb"))),(Object)(__ref.getField(true,"_user")),(Object)(__ref.getField(true,"_pwd")));
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e15) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e15.toString()); BA.debugLineNum = 110;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
Debug.ShouldStop(8192);
_mp = clsdb.__c.runMethod(false, "createMap", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("response")),(clsdb._modgeneratereturn._kode_gagal_koneksi_db),RemoteObject.createImmutable(("method")),(_method),RemoteObject.createImmutable(("pesan")),(__ref.getField(false,"_en").runMethod(true,"EncodeUrl",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")),(Object)(RemoteObject.createImmutable("UTF8")))),RemoteObject.createImmutable(("data")),(RemoteObject.createNewArray("Object",new int[] {0},new Object[] {})),RemoteObject.createImmutable(("fn")),(RemoteObject.createImmutable("SqlSelectMapSinngle"))}));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 111;BA.debugLine="Main.appendSqlLog(\" \")";
Debug.ShouldStop(16384);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 112;BA.debugLine="Main.appendSqlLog(Sql)";
Debug.ShouldStop(32768);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(_sql));
 BA.debugLineNum = 113;BA.debugLine="Main.appendSqlLog(LastException.Message)";
Debug.ShouldStop(65536);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")));
 BA.debugLineNum = 114;BA.debugLine="koneksi.Close";
Debug.ShouldStop(131072);
_koneksi.runVoidMethod ("Close");
 BA.debugLineNum = 115;BA.debugLine="Return mp";
Debug.ShouldStop(262144);
if (true) return _mp;
 };
 BA.debugLineNum = 119;BA.debugLine="Try";
Debug.ShouldStop(4194304);
try { BA.debugLineNum = 120;BA.debugLine="rs = koneksi.ExecQuery(Sql.Replace(\"'null'\",Null";
Debug.ShouldStop(8388608);
_rs = _koneksi.runMethod(false,"ExecQuery",(Object)(_sql.runMethod(true,"replace",(Object)(BA.ObjectToString("'null'")),(Object)(BA.ObjectToString(clsdb.__c.getField(false,"Null"))))));Debug.locals.put("rs", _rs);
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e25) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e25.toString()); BA.debugLineNum = 122;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
Debug.ShouldStop(33554432);
_mp = clsdb.__c.runMethod(false, "createMap", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("response")),(clsdb._modgeneratereturn._kode_gagal_execute_query),RemoteObject.createImmutable(("method")),(_method),RemoteObject.createImmutable(("pesan")),(__ref.getField(false,"_en").runMethod(true,"EncodeUrl",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")),(Object)(RemoteObject.createImmutable("UTF8")))),RemoteObject.createImmutable(("data")),(RemoteObject.createNewArray("Object",new int[] {0},new Object[] {})),RemoteObject.createImmutable(("fn")),(RemoteObject.createImmutable("SqlSelectMapSinngle"))}));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 123;BA.debugLine="Main.appendSqlLog(\" \")";
Debug.ShouldStop(67108864);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 124;BA.debugLine="Main.appendSqlLog(Sql)";
Debug.ShouldStop(134217728);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(_sql));
 BA.debugLineNum = 125;BA.debugLine="Main.appendSqlLog(LastException.Message)";
Debug.ShouldStop(268435456);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")));
 BA.debugLineNum = 126;BA.debugLine="koneksi.Close";
Debug.ShouldStop(536870912);
_koneksi.runVoidMethod ("Close");
 BA.debugLineNum = 127;BA.debugLine="Return mp";
Debug.ShouldStop(1073741824);
if (true) return _mp;
 };
 BA.debugLineNum = 131;BA.debugLine="Try";
Debug.ShouldStop(4);
try { BA.debugLineNum = 132;BA.debugLine="i=0";
Debug.ShouldStop(8);
_i = BA.numberCast(int.class, 0);Debug.locals.put("i", _i);
 BA.debugLineNum = 133;BA.debugLine="Do While rs.NextRow";
Debug.ShouldStop(16);
while (_rs.runMethod(true,"NextRow").<Boolean>get().booleanValue()) {
 BA.debugLineNum = 134;BA.debugLine="mp.Initialize";
Debug.ShouldStop(32);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 135;BA.debugLine="For i=0 To rs.ColumnCount-1";
Debug.ShouldStop(64);
{
final int step36 = 1;
final int limit36 = RemoteObject.solve(new RemoteObject[] {_rs.runMethod(true,"getColumnCount"),RemoteObject.createImmutable(1)}, "-",1, 1).<Integer>get().intValue();
_i = BA.numberCast(int.class, 0) ;
for (;(step36 > 0 && _i.<Integer>get().intValue() <= limit36) || (step36 < 0 && _i.<Integer>get().intValue() >= limit36) ;_i = RemoteObject.createImmutable((int)(0 + _i.<Integer>get().intValue() + step36))  ) {
Debug.locals.put("i", _i);
 BA.debugLineNum = 136;BA.debugLine="Dim val As String";
Debug.ShouldStop(128);
_val = RemoteObject.createImmutable("");Debug.locals.put("val", _val);
 BA.debugLineNum = 137;BA.debugLine="val = rs.GetString2(i)";
Debug.ShouldStop(256);
_val = _rs.runMethod(true,"GetString2",(Object)(_i));Debug.locals.put("val", _val);
 BA.debugLineNum = 138;BA.debugLine="If val <> Null Then";
Debug.ShouldStop(512);
if (RemoteObject.solveBoolean("N",_val)) { 
 BA.debugLineNum = 139;BA.debugLine="val = en.EncodeUrl(val, \"UTF8\")";
Debug.ShouldStop(1024);
_val = __ref.getField(false,"_en").runMethod(true,"EncodeUrl",(Object)(_val),(Object)(RemoteObject.createImmutable("UTF8")));Debug.locals.put("val", _val);
 }else {
 BA.debugLineNum = 141;BA.debugLine="val = \"null\"";
Debug.ShouldStop(4096);
_val = BA.ObjectToString("null");Debug.locals.put("val", _val);
 };
 BA.debugLineNum = 144;BA.debugLine="field = rs.GetColumnName(i).ToLowerCase";
Debug.ShouldStop(32768);
_field = _rs.runMethod(true,"GetColumnName",(Object)(_i)).runMethod(true,"toLowerCase");Debug.locals.put("field", _field);
 BA.debugLineNum = 145;BA.debugLine="If field = \"GETDATE(ENTRYTIME)\" Then";
Debug.ShouldStop(65536);
if (RemoteObject.solveBoolean("=",_field,BA.ObjectToString("GETDATE(ENTRYTIME)"))) { 
 BA.debugLineNum = 146;BA.debugLine="field = \"DATE\"";
Debug.ShouldStop(131072);
_field = BA.ObjectToString("DATE");Debug.locals.put("field", _field);
 };
 BA.debugLineNum = 148;BA.debugLine="mp.Put(field, val)";
Debug.ShouldStop(524288);
_mp.runVoidMethod ("Put",(Object)((_field)),(Object)((_val)));
 }
}Debug.locals.put("i", _i);
;
 BA.debugLineNum = 150;BA.debugLine="mp.Put(\"response\": modGenerateReturn.kode_succe";
Debug.ShouldStop(2097152);
_mp.runVoidMethod ("Put",(Object)(RemoteObject.createImmutable(("response"))),(Object)((clsdb._modgeneratereturn._kode_succes)));
 }
;
 Debug.CheckDeviceExceptions();
} 
       catch (Exception e53) {
			BA.rdebugUtils.runVoidMethod("setLastException",__ref.getField(false, "ba"), e53.toString()); BA.debugLineNum = 153;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
Debug.ShouldStop(16777216);
_mp = clsdb.__c.runMethod(false, "createMap", (Object)(new RemoteObject[] {RemoteObject.createImmutable(("response")),(clsdb._modgeneratereturn._kode_gagal_parsing),RemoteObject.createImmutable(("method")),(_method),RemoteObject.createImmutable(("pesan")),(__ref.getField(false,"_en").runMethod(true,"EncodeUrl",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")),(Object)(RemoteObject.createImmutable("UTF8")))),RemoteObject.createImmutable(("data")),(RemoteObject.createNewArray("Object",new int[] {0},new Object[] {})),RemoteObject.createImmutable(("fn")),(RemoteObject.createImmutable("SqlSelectMapSinngle"))}));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 154;BA.debugLine="Main.appendSqlLog(\" \")";
Debug.ShouldStop(33554432);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(RemoteObject.createImmutable(" ")));
 BA.debugLineNum = 155;BA.debugLine="Main.appendSqlLog(Sql)";
Debug.ShouldStop(67108864);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(_sql));
 BA.debugLineNum = 156;BA.debugLine="Main.appendSqlLog(LastException.Message)";
Debug.ShouldStop(134217728);
clsdb._main.runVoidMethod ("_appendsqllog",(Object)(clsdb.__c.runMethod(false,"LastException",__ref.getField(false, "ba")).runMethod(true,"getMessage")));
 BA.debugLineNum = 157;BA.debugLine="koneksi.Close";
Debug.ShouldStop(268435456);
_koneksi.runVoidMethod ("Close");
 BA.debugLineNum = 158;BA.debugLine="Return mp";
Debug.ShouldStop(536870912);
if (true) return _mp;
 };
 BA.debugLineNum = 160;BA.debugLine="koneksi.Close";
Debug.ShouldStop(-2147483648);
_koneksi.runVoidMethod ("Close");
 BA.debugLineNum = 161;BA.debugLine="Return mp";
Debug.ShouldStop(1);
if (true) return _mp;
 BA.debugLineNum = 162;BA.debugLine="End Sub";
Debug.ShouldStop(2);
return RemoteObject.createImmutable(null);
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}