package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class tredatasetting_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private clsid As String";
tredatasetting._clsid = RemoteObject.createImmutable("");__ref.setField("_clsid",tredatasetting._clsid);
 //BA.debugLineNum = 4;BA.debugLine="Private todb As clsDB";
tredatasetting._todb = RemoteObject.createNew ("b4j.example.clsdb");__ref.setField("_todb",tredatasetting._todb);
 //BA.debugLineNum = 5;BA.debugLine="Private todbdms As clsDBDms";
tredatasetting._todbdms = RemoteObject.createNew ("b4j.example.clsdbdms");__ref.setField("_todbdms",tredatasetting._todbdms);
 //BA.debugLineNum = 6;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _exe_query(RemoteObject __ref,RemoteObject _query,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("exe_query (tredatasetting) ","tredatasetting",5,__ref.getField(false, "ba"),__ref,41);
if (RapidSub.canDelegate("exe_query")) return __ref.runUserSub(false, "tredatasetting","exe_query", __ref, _query, _method);
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
RemoteObject _lst = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.List");
RemoteObject _js = RemoteObject.declareNull("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");
Debug.locals.put("query", _query);
Debug.locals.put("method", _method);
 BA.debugLineNum = 41;BA.debugLine="Private Sub exe_query(query As String, method As S";
Debug.ShouldStop(256);
 BA.debugLineNum = 42;BA.debugLine="Dim hasil As String";
Debug.ShouldStop(512);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 43;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(1024);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 44;BA.debugLine="Dim lst As List";
Debug.ShouldStop(2048);
_lst = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.List");Debug.locals.put("lst", _lst);
 BA.debugLineNum = 45;BA.debugLine="Dim js As JSONGenerator";
Debug.ShouldStop(4096);
_js = RemoteObject.createNew ("anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator");Debug.locals.put("js", _js);
 BA.debugLineNum = 47;BA.debugLine="lst.Initialize";
Debug.ShouldStop(16384);
_lst.runVoidMethod ("Initialize");
 BA.debugLineNum = 48;BA.debugLine="mp.Initialize";
Debug.ShouldStop(32768);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 50;BA.debugLine="mp = todb.SqlSelectMap(query, method)";
Debug.ShouldStop(131072);
_mp = __ref.getField(false,"_todb").runClassMethod (b4j.example.clsdb.class, "_sqlselectmap",(Object)(_query),(Object)(_method));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 51;BA.debugLine="js.Initialize(mp)";
Debug.ShouldStop(262144);
_js.runVoidMethod ("Initialize",(Object)(_mp));
 BA.debugLineNum = 52;BA.debugLine="hasil = js.ToString";
Debug.ShouldStop(524288);
_hasil = _js.runMethod(true,"ToString");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 54;BA.debugLine="Return hasil";
Debug.ShouldStop(2097152);
if (true) return _hasil;
 BA.debugLineNum = 55;BA.debugLine="End Sub";
Debug.ShouldStop(4194304);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _get(RemoteObject __ref,RemoteObject _key,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("get (tredatasetting) ","tredatasetting",5,__ref.getField(false, "ba"),__ref,57);
if (RapidSub.canDelegate("get")) return __ref.runUserSub(false, "tredatasetting","get", __ref, _key, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("key", _key);
Debug.locals.put("method", _method);
 BA.debugLineNum = 57;BA.debugLine="Private Sub get(key As String, method As String) A";
Debug.ShouldStop(16777216);
 BA.debugLineNum = 58;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(33554432);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 59;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(67108864);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 60;BA.debugLine="mp.Initialize";
Debug.ShouldStop(134217728);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 62;BA.debugLine="query = $\" select * from table(PKG_DMS_SETTING.fn";
Debug.ShouldStop(536870912);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_SETTING.fn_getsetting('"),tredatasetting.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_key))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 63;BA.debugLine="data  = exe_query(query, method)";
Debug.ShouldStop(1073741824);
_data = __ref.runClassMethod (b4j.example.tredatasetting.class, "_exe_query",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 64;BA.debugLine="Return data";
Debug.ShouldStop(-2147483648);
if (true) return _data;
 BA.debugLineNum = 65;BA.debugLine="End Sub";
Debug.ShouldStop(1);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _getval(RemoteObject __ref,RemoteObject _key,RemoteObject _val,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("getval (tredatasetting) ","tredatasetting",5,__ref.getField(false, "ba"),__ref,77);
if (RapidSub.canDelegate("getval")) return __ref.runUserSub(false, "tredatasetting","getval", __ref, _key, _val, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("key", _key);
Debug.locals.put("val", _val);
Debug.locals.put("method", _method);
 BA.debugLineNum = 77;BA.debugLine="Private Sub getval (key As String, val As String,";
Debug.ShouldStop(4096);
 BA.debugLineNum = 78;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(8192);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 79;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(16384);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 80;BA.debugLine="mp.Initialize";
Debug.ShouldStop(32768);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 82;BA.debugLine="query = $\" select * from table(PKG_DMS_SETTING.fn";
Debug.ShouldStop(131072);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_SETTING.fn_getsettingval('"),tredatasetting.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_key))),RemoteObject.createImmutable("', '"),tredatasetting.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_val))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 83;BA.debugLine="data  = exe_query(query, method)";
Debug.ShouldStop(262144);
_data = __ref.runClassMethod (b4j.example.tredatasetting.class, "_exe_query",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 84;BA.debugLine="Return data";
Debug.ShouldStop(524288);
if (true) return _data;
 BA.debugLineNum = 85;BA.debugLine="End Sub";
Debug.ShouldStop(1048576);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _handle(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Handle (tredatasetting) ","tredatasetting",5,__ref.getField(false, "ba"),__ref,14);
if (RapidSub.canDelegate("handle")) return __ref.runUserSub(false, "tredatasetting","handle", __ref, _req, _resp);
RemoteObject _appid = RemoteObject.createImmutable("");
RemoteObject _response = RemoteObject.createImmutable("");
RemoteObject _method = RemoteObject.createImmutable("");
RemoteObject _hasil = RemoteObject.createImmutable("");
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 14;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
Debug.ShouldStop(8192);
 BA.debugLineNum = 15;BA.debugLine="Dim appid, response, method, hasil As String";
Debug.ShouldStop(16384);
_appid = RemoteObject.createImmutable("");Debug.locals.put("appid", _appid);
_response = RemoteObject.createImmutable("");Debug.locals.put("response", _response);
_method = RemoteObject.createImmutable("");Debug.locals.put("method", _method);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 18;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
Debug.ShouldStop(131072);
_appid = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("clsakses")));Debug.locals.put("appid", _appid);
 BA.debugLineNum = 19;BA.debugLine="method = req.GetParameter(\"method\")";
Debug.ShouldStop(262144);
_method = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("method")));Debug.locals.put("method", _method);
 BA.debugLineNum = 21;BA.debugLine="If appid <> clsid Then";
Debug.ShouldStop(1048576);
if (RemoteObject.solveBoolean("!",_appid,__ref.getField(true,"_clsid"))) { 
 BA.debugLineNum = 22;BA.debugLine="response = modGenerateReturn.return_json(modGene";
Debug.ShouldStop(2097152);
_response = tredatasetting._modgeneratereturn.runMethod(true,"_return_json",(Object)(tredatasetting._modgeneratereturn._label_rejected),(Object)(tredatasetting._modgeneratereturn._kode_rejected_class),(Object)(_method));Debug.locals.put("response", _response);
 BA.debugLineNum = 23;BA.debugLine="resp.Write(response)";
Debug.ShouldStop(4194304);
_resp.runVoidMethod ("Write",(Object)(_response));
 BA.debugLineNum = 24;BA.debugLine="Return";
Debug.ShouldStop(8388608);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 26;BA.debugLine="Select method";
Debug.ShouldStop(33554432);
switch (BA.switchObjectToInt(_method,BA.ObjectToString("get"),BA.ObjectToString("save"),BA.ObjectToString("getval"))) {
case 0: {
 BA.debugLineNum = 28;BA.debugLine="resp.Write(get(req.GetParameter(\"key\"), method)";
Debug.ShouldStop(134217728);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.tredatasetting.class, "_get",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("key")))),(Object)(_method))));
 break; }
case 1: {
 BA.debugLineNum = 30;BA.debugLine="resp.Write(save(req.GetParameter(\"key\"), req.Ge";
Debug.ShouldStop(536870912);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.tredatasetting.class, "_save",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("key")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("name")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("value")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("ket")))),(Object)(_method))));
 break; }
case 2: {
 BA.debugLineNum = 32;BA.debugLine="resp.Write(getval(req.GetParameter(\"key\"), req.";
Debug.ShouldStop(-2147483648);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.tredatasetting.class, "_getval",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("key")))),(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("name")))),(Object)(_method))));
 break; }
default: {
 BA.debugLineNum = 34;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
Debug.ShouldStop(2);
_hasil = tredatasetting._modgeneratereturn.runMethod(true,"_return_json",(Object)(BA.ObjectToString("TIDAK ADA METHOD")),(Object)(BA.ObjectToString("404")),(Object)(_method));Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 35;BA.debugLine="resp.Write(hasil)";
Debug.ShouldStop(4);
_resp.runVoidMethod ("Write",(Object)(_hasil));
 break; }
}
;
 BA.debugLineNum = 39;BA.debugLine="End Sub";
Debug.ShouldStop(64);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (tredatasetting) ","tredatasetting",5,__ref.getField(false, "ba"),__ref,8);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "tredatasetting","initialize", __ref, _ba);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
 BA.debugLineNum = 8;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(128);
 BA.debugLineNum = 9;BA.debugLine="clsid = \"tre_datasetting\"";
Debug.ShouldStop(256);
__ref.setField ("_clsid",BA.ObjectToString("tre_datasetting"));
 BA.debugLineNum = 10;BA.debugLine="todb.Initialize";
Debug.ShouldStop(512);
__ref.getField(false,"_todb").runClassMethod (b4j.example.clsdb.class, "_initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 11;BA.debugLine="todbdms.Initialize";
Debug.ShouldStop(1024);
__ref.getField(false,"_todbdms").runClassMethod (b4j.example.clsdbdms.class, "_initialize",__ref.getField(false, "ba"));
 BA.debugLineNum = 12;BA.debugLine="End Sub";
Debug.ShouldStop(2048);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _save(RemoteObject __ref,RemoteObject _key,RemoteObject _name,RemoteObject _value,RemoteObject _ket,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("save (tredatasetting) ","tredatasetting",5,__ref.getField(false, "ba"),__ref,67);
if (RapidSub.canDelegate("save")) return __ref.runUserSub(false, "tredatasetting","save", __ref, _key, _name, _value, _ket, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("key", _key);
Debug.locals.put("name", _name);
Debug.locals.put("value", _value);
Debug.locals.put("ket", _ket);
Debug.locals.put("method", _method);
 BA.debugLineNum = 67;BA.debugLine="Private Sub save(key As String,name As String,valu";
Debug.ShouldStop(4);
 BA.debugLineNum = 68;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(8);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 69;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(16);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 70;BA.debugLine="mp.Initialize";
Debug.ShouldStop(32);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 72;BA.debugLine="query = $\" select * from data_setting where key_s";
Debug.ShouldStop(128);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from data_setting where key_setting = '"),tredatasetting.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_key))),RemoteObject.createImmutable("' ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 73;BA.debugLine="data  = exe_query(query, method)";
Debug.ShouldStop(256);
_data = __ref.runClassMethod (b4j.example.tredatasetting.class, "_exe_query",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 74;BA.debugLine="Return data";
Debug.ShouldStop(512);
if (true) return _data;
 BA.debugLineNum = 75;BA.debugLine="End Sub";
Debug.ShouldStop(1024);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}