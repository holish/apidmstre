
package b4j.example;

import anywheresoftware.b4a.pc.PCBA;
import anywheresoftware.b4a.pc.RemoteObject;

public class clsdb {
    public static RemoteObject myClass;
	public clsdb() {
	}
    public static PCBA staticBA = new PCBA(null, clsdb.class);

public static RemoteObject __c = RemoteObject.declareNull("anywheresoftware.b4a.keywords.Common");
public static RemoteObject _serverdb = RemoteObject.createImmutable("");
public static RemoteObject _pwd = RemoteObject.createImmutable("");
public static RemoteObject _user = RemoteObject.createImmutable("");
public static RemoteObject _std = RemoteObject.createImmutable("");
public static RemoteObject _en = RemoteObject.declareNull("anywheresoftware.b4a.objects.StringUtils");
public static b4j.example.main _main = null;
public static b4j.example.exequery _exequery = null;
public static b4j.example.clsmanagement _clsmanagement = null;
public static b4j.example.clsmaster _clsmaster = null;
public static b4j.example.modgeneratereturn _modgeneratereturn = null;
public static Object[] GetGlobals(RemoteObject _ref) throws Exception {
		return new Object[] {"en",_ref.getField(false, "_en"),"pwd",_ref.getField(false, "_pwd"),"serverdb",_ref.getField(false, "_serverdb"),"std",_ref.getField(false, "_std"),"user",_ref.getField(false, "_user")};
}
}