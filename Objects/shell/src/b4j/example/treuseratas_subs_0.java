package b4j.example;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.pc.*;

public class treuseratas_subs_0 {


public static RemoteObject  _class_globals(RemoteObject __ref) throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Class_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Private clsid As String";
treuseratas._clsid = RemoteObject.createImmutable("");__ref.setField("_clsid",treuseratas._clsid);
 //BA.debugLineNum = 4;BA.debugLine="End Sub";
return RemoteObject.createImmutable("");
}
public static RemoteObject  _getuip(RemoteObject __ref,RemoteObject _username,RemoteObject _method) throws Exception{
try {
		Debug.PushSubsStack("getuip (treuseratas) ","treuseratas",15,__ref.getField(false, "ba"),__ref,41);
if (RapidSub.canDelegate("getuip")) return __ref.runUserSub(false, "treuseratas","getuip", __ref, _username, _method);
RemoteObject _query = RemoteObject.createImmutable("");
RemoteObject _data = RemoteObject.createImmutable("");
RemoteObject _mp = RemoteObject.declareNull("anywheresoftware.b4a.objects.collections.Map");
Debug.locals.put("username", _username);
Debug.locals.put("method", _method);
 BA.debugLineNum = 41;BA.debugLine="Private Sub getuip(username As String, method As S";
Debug.ShouldStop(256);
 BA.debugLineNum = 42;BA.debugLine="Dim query, data As String";
Debug.ShouldStop(512);
_query = RemoteObject.createImmutable("");Debug.locals.put("query", _query);
_data = RemoteObject.createImmutable("");Debug.locals.put("data", _data);
 BA.debugLineNum = 43;BA.debugLine="Dim mp As Map";
Debug.ShouldStop(1024);
_mp = RemoteObject.createNew ("anywheresoftware.b4a.objects.collections.Map");Debug.locals.put("mp", _mp);
 BA.debugLineNum = 44;BA.debugLine="mp.Initialize";
Debug.ShouldStop(2048);
_mp.runVoidMethod ("Initialize");
 BA.debugLineNum = 46;BA.debugLine="query = $\" select a.id_jabatan from m_pegawai a";
Debug.ShouldStop(8192);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select a.id_jabatan from m_pegawai a\n"),RemoteObject.createImmutable("	inner join m_jabatan b on a.id_jabatan = b.id_jabatan \n"),RemoteObject.createImmutable("	where upper(username_pegawai) = upper('"),treuseratas.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)((_username))),RemoteObject.createImmutable("') ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 49;BA.debugLine="mp = exequery.single_map(query, method)";
Debug.ShouldStop(65536);
_mp = treuseratas._exequery.runMethod(false,"_single_map",(Object)(_query),(Object)(_method));Debug.locals.put("mp", _mp);
 BA.debugLineNum = 51;BA.debugLine="query = $\" select * from table(PKG_DMS_USER_FLOW.";
Debug.ShouldStop(262144);
_query = (RemoteObject.concat(RemoteObject.createImmutable(" select * from table(PKG_DMS_USER_FLOW.fn_loaduseratas('"),treuseratas.__c.runMethod(true,"SmartStringFormatter",(Object)(BA.ObjectToString("")),(Object)(_mp.runMethod(false,"Get",(Object)((RemoteObject.createImmutable("id_jabatan")))))),RemoteObject.createImmutable("')) ")));Debug.locals.put("query", _query);
 BA.debugLineNum = 52;BA.debugLine="data  = exequery.multi_string(query, method)";
Debug.ShouldStop(524288);
_data = treuseratas._exequery.runMethod(true,"_multi_string",(Object)(_query),(Object)(_method));Debug.locals.put("data", _data);
 BA.debugLineNum = 53;BA.debugLine="Return data";
Debug.ShouldStop(1048576);
if (true) return _data;
 BA.debugLineNum = 54;BA.debugLine="End Sub";
Debug.ShouldStop(2097152);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _handle(RemoteObject __ref,RemoteObject _req,RemoteObject _resp) throws Exception{
try {
		Debug.PushSubsStack("Handle (treuseratas) ","treuseratas",15,__ref.getField(false, "ba"),__ref,10);
if (RapidSub.canDelegate("handle")) return __ref.runUserSub(false, "treuseratas","handle", __ref, _req, _resp);
RemoteObject _appid = RemoteObject.createImmutable("");
RemoteObject _hasil = RemoteObject.createImmutable("");
RemoteObject _method = RemoteObject.createImmutable("");
RemoteObject _response = RemoteObject.createImmutable("");
Debug.locals.put("req", _req);
Debug.locals.put("resp", _resp);
 BA.debugLineNum = 10;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
Debug.ShouldStop(512);
 BA.debugLineNum = 11;BA.debugLine="Dim appid, hasil, method As String";
Debug.ShouldStop(1024);
_appid = RemoteObject.createImmutable("");Debug.locals.put("appid", _appid);
_hasil = RemoteObject.createImmutable("");Debug.locals.put("hasil", _hasil);
_method = RemoteObject.createImmutable("");Debug.locals.put("method", _method);
 BA.debugLineNum = 12;BA.debugLine="Dim response As String";
Debug.ShouldStop(2048);
_response = RemoteObject.createImmutable("");Debug.locals.put("response", _response);
 BA.debugLineNum = 14;BA.debugLine="If req.Method <> \"POST\" Then";
Debug.ShouldStop(8192);
if (RemoteObject.solveBoolean("!",_req.runMethod(true,"getMethod"),BA.ObjectToString("POST"))) { 
 BA.debugLineNum = 15;BA.debugLine="resp.SendError(500, \"method not supported.\")";
Debug.ShouldStop(16384);
_resp.runVoidMethodAndSync ("SendError",(Object)(BA.numberCast(int.class, 500)),(Object)(RemoteObject.createImmutable("method not supported.")));
 BA.debugLineNum = 16;BA.debugLine="Return";
Debug.ShouldStop(32768);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 20;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
Debug.ShouldStop(524288);
_appid = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("clsakses")));Debug.locals.put("appid", _appid);
 BA.debugLineNum = 21;BA.debugLine="method = req.GetParameter(\"method\")";
Debug.ShouldStop(1048576);
_method = _req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("method")));Debug.locals.put("method", _method);
 BA.debugLineNum = 24;BA.debugLine="If appid <> clsid Then";
Debug.ShouldStop(8388608);
if (RemoteObject.solveBoolean("!",_appid,__ref.getField(true,"_clsid"))) { 
 BA.debugLineNum = 25;BA.debugLine="response = modGenerateReturn.return_json(modGene";
Debug.ShouldStop(16777216);
_response = treuseratas._modgeneratereturn.runMethod(true,"_return_json",(Object)(treuseratas._modgeneratereturn._label_rejected),(Object)(treuseratas._modgeneratereturn._kode_rejected_class),(Object)(_method));Debug.locals.put("response", _response);
 BA.debugLineNum = 26;BA.debugLine="resp.Write(response)";
Debug.ShouldStop(33554432);
_resp.runVoidMethod ("Write",(Object)(_response));
 BA.debugLineNum = 27;BA.debugLine="Return";
Debug.ShouldStop(67108864);
if (true) return RemoteObject.createImmutable("");
 };
 BA.debugLineNum = 30;BA.debugLine="Select method";
Debug.ShouldStop(536870912);
switch (BA.switchObjectToInt(_method,BA.ObjectToString("get"))) {
case 0: {
 BA.debugLineNum = 32;BA.debugLine="resp.Write(getuip(req.GetParameter(\"username\"),";
Debug.ShouldStop(-2147483648);
_resp.runVoidMethod ("Write",(Object)(__ref.runClassMethod (b4j.example.treuseratas.class, "_getuip",(Object)(_req.runMethod(true,"GetParameter",(Object)(RemoteObject.createImmutable("username")))),(Object)(_method))));
 break; }
default: {
 BA.debugLineNum = 34;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
Debug.ShouldStop(2);
_hasil = treuseratas._modgeneratereturn.runMethod(true,"_return_json",(Object)(BA.ObjectToString("TIDAK ADA METHOD")),(Object)(BA.ObjectToString("404")),(Object)(_method));Debug.locals.put("hasil", _hasil);
 BA.debugLineNum = 35;BA.debugLine="resp.Write(hasil)";
Debug.ShouldStop(4);
_resp.runVoidMethod ("Write",(Object)(_hasil));
 break; }
}
;
 BA.debugLineNum = 38;BA.debugLine="End Sub";
Debug.ShouldStop(32);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
public static RemoteObject  _initialize(RemoteObject __ref,RemoteObject _ba) throws Exception{
try {
		Debug.PushSubsStack("Initialize (treuseratas) ","treuseratas",15,__ref.getField(false, "ba"),__ref,6);
if (RapidSub.canDelegate("initialize")) return __ref.runUserSub(false, "treuseratas","initialize", __ref, _ba);
__ref.runVoidMethodAndSync("innerInitializeHelper", _ba);
Debug.locals.put("ba", _ba);
 BA.debugLineNum = 6;BA.debugLine="Public Sub Initialize";
Debug.ShouldStop(32);
 BA.debugLineNum = 7;BA.debugLine="clsid = \"tre_atasan\"";
Debug.ShouldStop(64);
__ref.setField ("_clsid",BA.ObjectToString("tre_atasan"));
 BA.debugLineNum = 8;BA.debugLine="End Sub";
Debug.ShouldStop(128);
return RemoteObject.createImmutable("");
}
catch (Exception e) {
			throw Debug.ErrorCaught(e);
		} 
finally {
			Debug.PopSubsStack();
		}}
}