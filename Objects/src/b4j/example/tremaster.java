package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.debug.*;

public class tremaster extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    public static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.tremaster", this);
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            ba.htSubs = htSubs;
             
        }
        if (BA.isShellModeRuntimeCheck(ba))
                this.getClass().getMethod("_class_globals", b4j.example.tremaster.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public String _clsid = "";
public b4j.example.main _main = null;
public b4j.example.exequery _exequery = null;
public b4j.example.clsmanagement _clsmanagement = null;
public b4j.example.clsmaster _clsmaster = null;
public b4j.example.modgeneratereturn _modgeneratereturn = null;
public String  _class_globals(b4j.example.tremaster __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="tremaster";
RDebugUtils.currentLine=6422528;
 //BA.debugLineNum = 6422528;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=6422529;
 //BA.debugLineNum = 6422529;BA.debugLine="Private clsid As String";
_clsid = "";
RDebugUtils.currentLine=6422530;
 //BA.debugLineNum = 6422530;BA.debugLine="End Sub";
return "";
}
public String  _handle(b4j.example.tremaster __ref,anywheresoftware.b4j.object.JServlet.ServletRequestWrapper _req,anywheresoftware.b4j.object.JServlet.ServletResponseWrapper _resp) throws Exception{
__ref = this;
RDebugUtils.currentModule="tremaster";
if (Debug.shouldDelegate(ba, "handle"))
	return (String) Debug.delegate(ba, "handle", new Object[] {_req,_resp});
String _appid = "";
String _hasil = "";
String _method = "";
String _response = "";
RDebugUtils.currentLine=6553600;
 //BA.debugLineNum = 6553600;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
RDebugUtils.currentLine=6553601;
 //BA.debugLineNum = 6553601;BA.debugLine="Dim appid, hasil, method As String";
_appid = "";
_hasil = "";
_method = "";
RDebugUtils.currentLine=6553602;
 //BA.debugLineNum = 6553602;BA.debugLine="Dim response As String";
_response = "";
RDebugUtils.currentLine=6553604;
 //BA.debugLineNum = 6553604;BA.debugLine="If req.Method <> \"POST\" Then";
if ((_req.getMethod()).equals("POST") == false) { 
RDebugUtils.currentLine=6553605;
 //BA.debugLineNum = 6553605;BA.debugLine="resp.SendError(500, \"method not supported.\")";
_resp.SendError((int) (500),"method not supported.");
RDebugUtils.currentLine=6553606;
 //BA.debugLineNum = 6553606;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=6553610;
 //BA.debugLineNum = 6553610;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
_appid = _req.GetParameter("clsakses");
RDebugUtils.currentLine=6553612;
 //BA.debugLineNum = 6553612;BA.debugLine="method = req.GetParameter(\"method\")";
_method = _req.GetParameter("method");
RDebugUtils.currentLine=6553614;
 //BA.debugLineNum = 6553614;BA.debugLine="If appid <> clsid Then";
if ((_appid).equals(__ref._clsid) == false) { 
RDebugUtils.currentLine=6553615;
 //BA.debugLineNum = 6553615;BA.debugLine="response = modGenerateReturn.return_json(modGene";
_response = _modgeneratereturn._return_json(_modgeneratereturn._label_rejected,_modgeneratereturn._kode_rejected_class,_method);
RDebugUtils.currentLine=6553616;
 //BA.debugLineNum = 6553616;BA.debugLine="resp.Write(response)";
_resp.Write(_response);
RDebugUtils.currentLine=6553617;
 //BA.debugLineNum = 6553617;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=6553620;
 //BA.debugLineNum = 6553620;BA.debugLine="Select method";
switch (BA.switchObjectToInt(_method,"createbank","readbank","updatebank","deletebank","createvendor","readvendor","editvendor","deletevendor","createtipebayar","readtipebayar","updatetipebayar","deletetipebayar","createtermin","readtermin","updatetermin","deletetermin","createsumberdana","readsumberdana","updatesumberdana","deletesumberdana","createjenisproyek","readjenisproyek","updatejenisproyek","deletejenisproyek","createproyek","readproyek","updateproyek","deleteproyek","createunit","readunit","updateunit","deleteunit","readjabatan")) {
case 0: {
RDebugUtils.currentLine=6553622;
 //BA.debugLineNum = 6553622;BA.debugLine="hasil = clsMaster.create_bank(req.GetParameter(";
_hasil = _clsmaster._create_bank(_req.GetParameter("namabank"),_method);
 break; }
case 1: {
RDebugUtils.currentLine=6553624;
 //BA.debugLineNum = 6553624;BA.debugLine="hasil = clsMaster.read_bank(req.GetParameter(\"c";
_hasil = _clsmaster._read_bank(_req.GetParameter("cari"),_method);
 break; }
case 2: {
RDebugUtils.currentLine=6553626;
 //BA.debugLineNum = 6553626;BA.debugLine="hasil = clsMaster.update_bank(req.GetParameter(";
_hasil = _clsmaster._update_bank(_req.GetParameter("id"),_req.GetParameter("nama"),_method);
 break; }
case 3: {
RDebugUtils.currentLine=6553628;
 //BA.debugLineNum = 6553628;BA.debugLine="hasil = clsMaster.delete_bank(req.GetParameter(";
_hasil = _clsmaster._delete_bank(_req.GetParameter("id"),_method);
 break; }
case 4: {
RDebugUtils.currentLine=6553631;
 //BA.debugLineNum = 6553631;BA.debugLine="hasil = clsMaster.create_vendor(req.GetParamete";
_hasil = _clsmaster._create_vendor(_req.GetParameter("namavendor"),_req.GetParameter("namajabatan"),_req.GetParameter("sebutanpejabatvendor"),_req.GetParameter("namavendor1"),_req.GetParameter("namavendor2"),_req.GetParameter("namavendor3"),_req.GetParameter("namavendor4"),_method);
 break; }
case 5: {
RDebugUtils.currentLine=6553633;
 //BA.debugLineNum = 6553633;BA.debugLine="hasil = clsMaster.read_vendor(req.GetParameter(";
_hasil = _clsmaster._read_vendor(_req.GetParameter("cari"),_method);
 break; }
case 6: {
RDebugUtils.currentLine=6553635;
 //BA.debugLineNum = 6553635;BA.debugLine="hasil = clsMaster.update_vendor(req.GetParamete";
_hasil = _clsmaster._update_vendor(_req.GetParameter("kodevendor"),_req.GetParameter("namavendor"),_req.GetParameter("namajabatan"),_req.GetParameter("sebutanpejabatvendor"),_req.GetParameter("namavendor1"),_req.GetParameter("namavendor2"),_req.GetParameter("namavendor3"),_req.GetParameter("namavendor4"),_method);
 break; }
case 7: {
RDebugUtils.currentLine=6553637;
 //BA.debugLineNum = 6553637;BA.debugLine="hasil = clsMaster.delete_vendor(req.GetParamete";
_hasil = _clsmaster._delete_vendor(_req.GetParameter("kodevendor"),_method);
 break; }
case 8: {
RDebugUtils.currentLine=6553640;
 //BA.debugLineNum = 6553640;BA.debugLine="hasil = clsMaster.create_tipebayar(req.GetParam";
_hasil = _clsmaster._create_tipebayar(_req.GetParameter("namatipebayar"),_method);
 break; }
case 9: {
RDebugUtils.currentLine=6553642;
 //BA.debugLineNum = 6553642;BA.debugLine="hasil = clsMaster.read_tipebayar(req.GetParamet";
_hasil = _clsmaster._read_tipebayar(_req.GetParameter("cari"),_method);
 break; }
case 10: {
RDebugUtils.currentLine=6553644;
 //BA.debugLineNum = 6553644;BA.debugLine="hasil = clsMaster.update_tipebayar(req.GetParam";
_hasil = _clsmaster._update_tipebayar(_req.GetParameter("id"),_req.GetParameter("nama"),_method);
 break; }
case 11: {
RDebugUtils.currentLine=6553646;
 //BA.debugLineNum = 6553646;BA.debugLine="hasil = clsMaster.delete_tipebayar(req.GetParam";
_hasil = _clsmaster._delete_tipebayar(_req.GetParameter("id"),_method);
 break; }
case 12: {
RDebugUtils.currentLine=6553649;
 //BA.debugLineNum = 6553649;BA.debugLine="hasil = clsMaster.create_termin(req.GetParamete";
_hasil = _clsmaster._create_termin(_req.GetParameter("namatermin"),_req.GetParameter("kodetermin"),_req.GetParameter("idbayar"),_method);
 break; }
case 13: {
RDebugUtils.currentLine=6553651;
 //BA.debugLineNum = 6553651;BA.debugLine="hasil = clsMaster.read_termin(req.GetParameter(";
_hasil = _clsmaster._read_termin(_req.GetParameter("cari"),_method);
 break; }
case 14: {
RDebugUtils.currentLine=6553653;
 //BA.debugLineNum = 6553653;BA.debugLine="hasil = clsMaster.update_termin(req.GetParamete";
_hasil = _clsmaster._update_termin(_req.GetParameter("id"),_req.GetParameter("nama"),_req.GetParameter("kode"),_method);
 break; }
case 15: {
RDebugUtils.currentLine=6553655;
 //BA.debugLineNum = 6553655;BA.debugLine="hasil = clsMaster.delete_termin(req.GetParamete";
_hasil = _clsmaster._delete_termin(_req.GetParameter("id"),_method);
 break; }
case 16: {
RDebugUtils.currentLine=6553658;
 //BA.debugLineNum = 6553658;BA.debugLine="hasil = clsMaster.create_sumberdana(req.GetPara";
_hasil = _clsmaster._create_sumberdana(_req.GetParameter("namatipebayar"),_method);
 break; }
case 17: {
RDebugUtils.currentLine=6553660;
 //BA.debugLineNum = 6553660;BA.debugLine="hasil = clsMaster.read_sumberdana(req.GetParame";
_hasil = _clsmaster._read_sumberdana(_req.GetParameter("cari"),_method);
 break; }
case 18: {
RDebugUtils.currentLine=6553662;
 //BA.debugLineNum = 6553662;BA.debugLine="hasil = clsMaster.update_sumberdana(req.GetPara";
_hasil = _clsmaster._update_sumberdana(_req.GetParameter("id"),_req.GetParameter("nama"),_method);
 break; }
case 19: {
RDebugUtils.currentLine=6553664;
 //BA.debugLineNum = 6553664;BA.debugLine="hasil = clsMaster.delete_sumberdana(req.GetPara";
_hasil = _clsmaster._delete_sumberdana(_req.GetParameter("id"),_method);
 break; }
case 20: {
RDebugUtils.currentLine=6553667;
 //BA.debugLineNum = 6553667;BA.debugLine="hasil = clsMaster.create_jenisproyek(req.GetPar";
_hasil = _clsmaster._create_jenisproyek(_req.GetParameter("nama"),_req.GetParameter("kode"),_method);
 break; }
case 21: {
RDebugUtils.currentLine=6553669;
 //BA.debugLineNum = 6553669;BA.debugLine="hasil = clsMaster.read_jenisproyek(req.GetParam";
_hasil = _clsmaster._read_jenisproyek(_req.GetParameter("cari"),_method);
 break; }
case 22: {
RDebugUtils.currentLine=6553671;
 //BA.debugLineNum = 6553671;BA.debugLine="hasil = clsMaster.update_jenisproyek(req.GetPar";
_hasil = _clsmaster._update_jenisproyek(_req.GetParameter("id"),_req.GetParameter("nama"),_req.GetParameter("kode"),_method);
 break; }
case 23: {
RDebugUtils.currentLine=6553673;
 //BA.debugLineNum = 6553673;BA.debugLine="hasil = clsMaster.delete_jenisproyek(req.GetPar";
_hasil = _clsmaster._delete_jenisproyek(_req.GetParameter("id"),_method);
 break; }
case 24: {
RDebugUtils.currentLine=6553676;
 //BA.debugLineNum = 6553676;BA.debugLine="hasil = clsMaster.create_proyek(req.GetParamete";
_hasil = _clsmaster._create_proyek(_req.GetParameter("kode"),_req.GetParameter("nama"),_req.GetParameter("idunit"),_req.GetParameter("idjenisproyek"),_method);
 break; }
case 25: {
RDebugUtils.currentLine=6553678;
 //BA.debugLineNum = 6553678;BA.debugLine="hasil = clsMaster.read_proyek(req.GetParameter(";
_hasil = _clsmaster._read_proyek(_req.GetParameter("cari"),_method);
 break; }
case 26: {
RDebugUtils.currentLine=6553680;
 //BA.debugLineNum = 6553680;BA.debugLine="hasil = clsMaster.update_proyek(req.GetParamete";
_hasil = _clsmaster._update_proyek(_req.GetParameter("id"),_req.GetParameter("kode"),_req.GetParameter("nama"),_req.GetParameter("idunit"),_req.GetParameter("idjenisproyek"),_method);
 break; }
case 27: {
RDebugUtils.currentLine=6553682;
 //BA.debugLineNum = 6553682;BA.debugLine="hasil = clsMaster.delete_proyek(req.GetParamete";
_hasil = _clsmaster._delete_proyek(_req.GetParameter("id"),_method);
 break; }
case 28: {
RDebugUtils.currentLine=6553685;
 //BA.debugLineNum = 6553685;BA.debugLine="hasil = clsMaster.create_unit(req.GetParameter(";
_hasil = _clsmaster._create_unit(_req.GetParameter("nama"),_req.GetParameter("singkatan"),_req.GetParameter("as"),_req.GetParameter("alamat"),_req.GetParameter("ket"),_method);
 break; }
case 29: {
RDebugUtils.currentLine=6553687;
 //BA.debugLineNum = 6553687;BA.debugLine="hasil = clsMaster.read_unit(req.GetParameter(\"c";
_hasil = _clsmaster._read_unit(_req.GetParameter("cari"),_method);
 break; }
case 30: {
RDebugUtils.currentLine=6553689;
 //BA.debugLineNum = 6553689;BA.debugLine="hasil = clsMaster.update_unit(req.GetParameter(";
_hasil = _clsmaster._update_unit(_req.GetParameter("id"),_req.GetParameter("nama"),_req.GetParameter("singkatan"),_req.GetParameter("as"),_req.GetParameter("alamat"),_req.GetParameter("ket"),_method);
 break; }
case 31: {
RDebugUtils.currentLine=6553691;
 //BA.debugLineNum = 6553691;BA.debugLine="hasil = clsMaster.delete_unit(req.GetParameter(";
_hasil = _clsmaster._delete_unit(_req.GetParameter("id"),_method);
 break; }
case 32: {
RDebugUtils.currentLine=6553694;
 //BA.debugLineNum = 6553694;BA.debugLine="hasil = clsMaster.read_jabatan(req.GetParameter";
_hasil = _clsmaster._read_jabatan(_req.GetParameter("cari"),_method);
 break; }
default: {
RDebugUtils.currentLine=6553697;
 //BA.debugLineNum = 6553697;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
_hasil = _modgeneratereturn._return_json("TIDAK ADA METHOD","404",_method);
 break; }
}
;
RDebugUtils.currentLine=6553700;
 //BA.debugLineNum = 6553700;BA.debugLine="resp.Write(hasil)";
_resp.Write(_hasil);
RDebugUtils.currentLine=6553701;
 //BA.debugLineNum = 6553701;BA.debugLine="End Sub";
return "";
}
public String  _initialize(b4j.example.tremaster __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="tremaster";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
RDebugUtils.currentLine=6488064;
 //BA.debugLineNum = 6488064;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=6488065;
 //BA.debugLineNum = 6488065;BA.debugLine="clsid = \"tre_menumaster\"";
__ref._clsid = "tre_menumaster";
RDebugUtils.currentLine=6488066;
 //BA.debugLineNum = 6488066;BA.debugLine="End Sub";
return "";
}
}