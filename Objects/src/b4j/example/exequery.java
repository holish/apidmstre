package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.debug.*;

public class exequery extends Object{
public static exequery mostCurrent = new exequery();

public static BA ba;
static {
		ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.exequery", null);
		ba.loadHtSubs(exequery.class);
        if (ba.getClass().getName().endsWith("ShellBA")) {
			
			ba.raiseEvent2(null, true, "SHELL", false);
			ba.raiseEvent2(null, true, "CREATE", true, "b4j.example.exequery", ba);
		}
	}
    public static Class<?> getObject() {
		return exequery.class;
	}

 
public static anywheresoftware.b4a.keywords.Common __c = null;
public static b4j.example.clsdb _todb = null;
public static b4j.example.clsdbdms _todbdms = null;
public static b4j.example.main _main = null;
public static b4j.example.clsmanagement _clsmanagement = null;
public static b4j.example.clsmaster _clsmaster = null;
public static b4j.example.modgeneratereturn _modgeneratereturn = null;
public static String  _multi_string(String _query,String _method) throws Exception{
RDebugUtils.currentModule="exequery";
if (Debug.shouldDelegate(ba, "multi_string"))
	return (String) Debug.delegate(ba, "multi_string", new Object[] {_query,_method});
String _hasil = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
anywheresoftware.b4a.objects.collections.List _lst = null;
anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator _js = null;
RDebugUtils.currentLine=1114112;
 //BA.debugLineNum = 1114112;BA.debugLine="Public Sub multi_string(query As String, method As";
RDebugUtils.currentLine=1114113;
 //BA.debugLineNum = 1114113;BA.debugLine="todb.Initialize";
_todb._initialize(null,ba);
RDebugUtils.currentLine=1114114;
 //BA.debugLineNum = 1114114;BA.debugLine="Dim hasil As String";
_hasil = "";
RDebugUtils.currentLine=1114115;
 //BA.debugLineNum = 1114115;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=1114116;
 //BA.debugLineNum = 1114116;BA.debugLine="Dim lst As List";
_lst = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=1114117;
 //BA.debugLineNum = 1114117;BA.debugLine="Dim js As JSONGenerator";
_js = new anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator();
RDebugUtils.currentLine=1114119;
 //BA.debugLineNum = 1114119;BA.debugLine="lst.Initialize";
_lst.Initialize();
RDebugUtils.currentLine=1114120;
 //BA.debugLineNum = 1114120;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=1114122;
 //BA.debugLineNum = 1114122;BA.debugLine="mp = todb.SqlSelectMap(query, method)";
_mp = _todb._sqlselectmap(null,_query,_method);
RDebugUtils.currentLine=1114123;
 //BA.debugLineNum = 1114123;BA.debugLine="js.Initialize(mp)";
_js.Initialize(_mp);
RDebugUtils.currentLine=1114124;
 //BA.debugLineNum = 1114124;BA.debugLine="hasil = js.ToString";
_hasil = _js.ToString();
RDebugUtils.currentLine=1114126;
 //BA.debugLineNum = 1114126;BA.debugLine="Return hasil";
if (true) return _hasil;
RDebugUtils.currentLine=1114127;
 //BA.debugLineNum = 1114127;BA.debugLine="End Sub";
return "";
}
public static anywheresoftware.b4a.objects.collections.Map  _multi_map(String _query,String _method) throws Exception{
RDebugUtils.currentModule="exequery";
if (Debug.shouldDelegate(ba, "multi_map"))
	return (anywheresoftware.b4a.objects.collections.Map) Debug.delegate(ba, "multi_map", new Object[] {_query,_method});
anywheresoftware.b4a.objects.collections.Map _mp = null;
anywheresoftware.b4a.objects.collections.List _lst = null;
RDebugUtils.currentLine=1179648;
 //BA.debugLineNum = 1179648;BA.debugLine="Public Sub multi_map(query As String, method As St";
RDebugUtils.currentLine=1179649;
 //BA.debugLineNum = 1179649;BA.debugLine="todb.Initialize";
_todb._initialize(null,ba);
RDebugUtils.currentLine=1179650;
 //BA.debugLineNum = 1179650;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=1179651;
 //BA.debugLineNum = 1179651;BA.debugLine="Dim lst As List";
_lst = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=1179653;
 //BA.debugLineNum = 1179653;BA.debugLine="lst.Initialize";
_lst.Initialize();
RDebugUtils.currentLine=1179654;
 //BA.debugLineNum = 1179654;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=1179656;
 //BA.debugLineNum = 1179656;BA.debugLine="mp = todb.SqlSelectMap(query, method)";
_mp = _todb._sqlselectmap(null,_query,_method);
RDebugUtils.currentLine=1179658;
 //BA.debugLineNum = 1179658;BA.debugLine="Return mp";
if (true) return _mp;
RDebugUtils.currentLine=1179659;
 //BA.debugLineNum = 1179659;BA.debugLine="End Sub";
return null;
}
public static anywheresoftware.b4a.objects.collections.Map  _single_map(String _query,String _method) throws Exception{
RDebugUtils.currentModule="exequery";
if (Debug.shouldDelegate(ba, "single_map"))
	return (anywheresoftware.b4a.objects.collections.Map) Debug.delegate(ba, "single_map", new Object[] {_query,_method});
anywheresoftware.b4a.objects.collections.Map _mp = null;
anywheresoftware.b4a.objects.collections.List _lst = null;
RDebugUtils.currentLine=1048576;
 //BA.debugLineNum = 1048576;BA.debugLine="Public Sub single_map(query As String, method As S";
RDebugUtils.currentLine=1048577;
 //BA.debugLineNum = 1048577;BA.debugLine="todb.Initialize";
_todb._initialize(null,ba);
RDebugUtils.currentLine=1048578;
 //BA.debugLineNum = 1048578;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=1048579;
 //BA.debugLineNum = 1048579;BA.debugLine="Dim lst As List";
_lst = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=1048581;
 //BA.debugLineNum = 1048581;BA.debugLine="lst.Initialize";
_lst.Initialize();
RDebugUtils.currentLine=1048582;
 //BA.debugLineNum = 1048582;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=1048584;
 //BA.debugLineNum = 1048584;BA.debugLine="mp = todb.SqlSelectSingleMap(query, method)";
_mp = _todb._sqlselectsinglemap(null,_query,_method);
RDebugUtils.currentLine=1048586;
 //BA.debugLineNum = 1048586;BA.debugLine="Return mp";
if (true) return _mp;
RDebugUtils.currentLine=1048587;
 //BA.debugLineNum = 1048587;BA.debugLine="End Sub";
return null;
}
public static String  _single_string(String _query,String _method) throws Exception{
RDebugUtils.currentModule="exequery";
if (Debug.shouldDelegate(ba, "single_string"))
	return (String) Debug.delegate(ba, "single_string", new Object[] {_query,_method});
String _hasil = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
anywheresoftware.b4a.objects.collections.List _lst = null;
anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator _js = null;
RDebugUtils.currentLine=983040;
 //BA.debugLineNum = 983040;BA.debugLine="Public Sub single_string(query As String, method A";
RDebugUtils.currentLine=983041;
 //BA.debugLineNum = 983041;BA.debugLine="todb.Initialize";
_todb._initialize(null,ba);
RDebugUtils.currentLine=983042;
 //BA.debugLineNum = 983042;BA.debugLine="Dim hasil As String";
_hasil = "";
RDebugUtils.currentLine=983043;
 //BA.debugLineNum = 983043;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=983044;
 //BA.debugLineNum = 983044;BA.debugLine="Dim lst As List";
_lst = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=983045;
 //BA.debugLineNum = 983045;BA.debugLine="Dim js As JSONGenerator";
_js = new anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator();
RDebugUtils.currentLine=983047;
 //BA.debugLineNum = 983047;BA.debugLine="lst.Initialize";
_lst.Initialize();
RDebugUtils.currentLine=983048;
 //BA.debugLineNum = 983048;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=983050;
 //BA.debugLineNum = 983050;BA.debugLine="mp = todb.SqlSelectSingleMap(query, method)";
_mp = _todb._sqlselectsinglemap(null,_query,_method);
RDebugUtils.currentLine=983051;
 //BA.debugLineNum = 983051;BA.debugLine="js.Initialize(mp)";
_js.Initialize(_mp);
RDebugUtils.currentLine=983052;
 //BA.debugLineNum = 983052;BA.debugLine="hasil = js.ToString";
_hasil = _js.ToString();
RDebugUtils.currentLine=983054;
 //BA.debugLineNum = 983054;BA.debugLine="Return hasil";
if (true) return _hasil;
RDebugUtils.currentLine=983055;
 //BA.debugLineNum = 983055;BA.debugLine="End Sub";
return "";
}
}