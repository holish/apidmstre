package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.debug.*;

public class clsmanagement extends Object{
public static clsmanagement mostCurrent = new clsmanagement();

public static BA ba;
static {
		ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.clsmanagement", null);
		ba.loadHtSubs(clsmanagement.class);
        if (ba.getClass().getName().endsWith("ShellBA")) {
			
			ba.raiseEvent2(null, true, "SHELL", false);
			ba.raiseEvent2(null, true, "CREATE", true, "b4j.example.clsmanagement", ba);
		}
	}
    public static Class<?> getObject() {
		return clsmanagement.class;
	}

 
public static anywheresoftware.b4a.keywords.Common __c = null;
public static b4j.example.clsdb _koneksidb = null;
public static b4j.example.main _main = null;
public static b4j.example.exequery _exequery = null;
public static b4j.example.clsmaster _clsmaster = null;
public static b4j.example.modgeneratereturn _modgeneratereturn = null;
public static String  _create_roles(String _p1,String _p2,String _p3,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "create_roles"))
	return (String) Debug.delegate(ba, "create_roles", new Object[] {_p1,_p2,_p3,_method});
String _data = "";
String _query = "";
String _p4 = "";
String _p5 = "";
String _templatemenu = "";
anywheresoftware.b4a.objects.StringUtils _jstut = null;
anywheresoftware.b4a.objects.collections.List _arr = null;
anywheresoftware.b4a.objects.collections.List _arrmenu = null;
anywheresoftware.b4a.objects.collections.List _arrotoritas = null;
int _i = 0;
int _x = 0;
int _y = 0;
RDebugUtils.currentLine=2424832;
 //BA.debugLineNum = 2424832;BA.debugLine="Public Sub create_roles(p1 As String,  p2 As Strin";
RDebugUtils.currentLine=2424833;
 //BA.debugLineNum = 2424833;BA.debugLine="Dim data, query, p4, p5, templatemenu As String";
_data = "";
_query = "";
_p4 = "";
_p5 = "";
_templatemenu = "";
RDebugUtils.currentLine=2424834;
 //BA.debugLineNum = 2424834;BA.debugLine="Dim jstut As StringUtils";
_jstut = new anywheresoftware.b4a.objects.StringUtils();
RDebugUtils.currentLine=2424835;
 //BA.debugLineNum = 2424835;BA.debugLine="Dim arr, arrmenu, arrotoritas As List";
_arr = new anywheresoftware.b4a.objects.collections.List();
_arrmenu = new anywheresoftware.b4a.objects.collections.List();
_arrotoritas = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=2424836;
 //BA.debugLineNum = 2424836;BA.debugLine="arr.Initialize";
_arr.Initialize();
RDebugUtils.currentLine=2424837;
 //BA.debugLineNum = 2424837;BA.debugLine="arrmenu.Initialize";
_arrmenu.Initialize();
RDebugUtils.currentLine=2424838;
 //BA.debugLineNum = 2424838;BA.debugLine="arrotoritas.Initialize";
_arrotoritas.Initialize();
RDebugUtils.currentLine=2424839;
 //BA.debugLineNum = 2424839;BA.debugLine="p3 = jstut.DecodeUrl(p3, \"UTF8\")";
_p3 = _jstut.DecodeUrl(_p3,"UTF8");
RDebugUtils.currentLine=2424840;
 //BA.debugLineNum = 2424840;BA.debugLine="arr = Regex.Split(\"#\", p3)";
_arr = anywheresoftware.b4a.keywords.Common.ArrayToList(anywheresoftware.b4a.keywords.Common.Regex.Split("#",_p3));
RDebugUtils.currentLine=2424841;
 //BA.debugLineNum = 2424841;BA.debugLine="templatemenu = \"1,2,3,4,5\"";
_templatemenu = "1,2,3,4,5";
RDebugUtils.currentLine=2424842;
 //BA.debugLineNum = 2424842;BA.debugLine="p3 = \"\"";
_p3 = "";
RDebugUtils.currentLine=2424843;
 //BA.debugLineNum = 2424843;BA.debugLine="p5 = arr.Size";
_p5 = BA.NumberToString(_arr.getSize());
RDebugUtils.currentLine=2424844;
 //BA.debugLineNum = 2424844;BA.debugLine="For i = 0 To arr.Size - 1";
{
final int step12 = 1;
final int limit12 = (int) (_arr.getSize()-1);
_i = (int) (0) ;
for (;(step12 > 0 && _i <= limit12) || (step12 < 0 && _i >= limit12) ;_i = ((int)(0 + _i + step12))  ) {
RDebugUtils.currentLine=2424845;
 //BA.debugLineNum = 2424845;BA.debugLine="arrmenu = Regex.Split(\"=\", arr.Get(i))";
_arrmenu = anywheresoftware.b4a.keywords.Common.ArrayToList(anywheresoftware.b4a.keywords.Common.Regex.Split("=",BA.ObjectToString(_arr.Get(_i))));
RDebugUtils.currentLine=2424846;
 //BA.debugLineNum = 2424846;BA.debugLine="If p3.Length = 0 Then";
if (_p3.length()==0) { 
RDebugUtils.currentLine=2424847;
 //BA.debugLineNum = 2424847;BA.debugLine="p3 = p3 & arrmenu.Get(0)";
_p3 = _p3+BA.ObjectToString(_arrmenu.Get((int) (0)));
 }else {
RDebugUtils.currentLine=2424849;
 //BA.debugLineNum = 2424849;BA.debugLine="p3 = p3 & \"#\" & arrmenu.Get(0)";
_p3 = _p3+"#"+BA.ObjectToString(_arrmenu.Get((int) (0)));
 };
RDebugUtils.currentLine=2424852;
 //BA.debugLineNum = 2424852;BA.debugLine="For x = 1 To arrmenu.Size - 1";
{
final int step19 = 1;
final int limit19 = (int) (_arrmenu.getSize()-1);
_x = (int) (1) ;
for (;(step19 > 0 && _x <= limit19) || (step19 < 0 && _x >= limit19) ;_x = ((int)(0 + _x + step19))  ) {
RDebugUtils.currentLine=2424853;
 //BA.debugLineNum = 2424853;BA.debugLine="templatemenu = \"1,2,3,4,5\"";
_templatemenu = "1,2,3,4,5";
RDebugUtils.currentLine=2424854;
 //BA.debugLineNum = 2424854;BA.debugLine="arrotoritas = Regex.Split(\",\", arrmenu.Get(x))";
_arrotoritas = anywheresoftware.b4a.keywords.Common.ArrayToList(anywheresoftware.b4a.keywords.Common.Regex.Split(",",BA.ObjectToString(_arrmenu.Get(_x))));
RDebugUtils.currentLine=2424855;
 //BA.debugLineNum = 2424855;BA.debugLine="If arrotoritas.Size = 1 Then";
if (_arrotoritas.getSize()==1) { 
RDebugUtils.currentLine=2424856;
 //BA.debugLineNum = 2424856;BA.debugLine="Select arrotoritas.Get(0)";
switch (BA.switchObjectToInt(_arrotoritas.Get((int) (0)),(Object)("1"),(Object)("2"),(Object)("3"),(Object)("4"),(Object)("5"))) {
case 0: {
RDebugUtils.currentLine=2424858;
 //BA.debugLineNum = 2424858;BA.debugLine="templatemenu = \"1,0,0,0,0\"";
_templatemenu = "1,0,0,0,0";
 break; }
case 1: {
RDebugUtils.currentLine=2424860;
 //BA.debugLineNum = 2424860;BA.debugLine="templatemenu = \"0,1,0,0,0\"";
_templatemenu = "0,1,0,0,0";
 break; }
case 2: {
RDebugUtils.currentLine=2424862;
 //BA.debugLineNum = 2424862;BA.debugLine="templatemenu = \"0,0,1,0,0\"";
_templatemenu = "0,0,1,0,0";
 break; }
case 3: {
RDebugUtils.currentLine=2424864;
 //BA.debugLineNum = 2424864;BA.debugLine="templatemenu = \"0,0,0,1,0\"";
_templatemenu = "0,0,0,1,0";
 break; }
case 4: {
RDebugUtils.currentLine=2424866;
 //BA.debugLineNum = 2424866;BA.debugLine="templatemenu = \"0,0,0,0,1\"";
_templatemenu = "0,0,0,0,1";
 break; }
}
;
 }else {
RDebugUtils.currentLine=2424869;
 //BA.debugLineNum = 2424869;BA.debugLine="For y = 0 To arrotoritas.Size - 1";
{
final int step36 = 1;
final int limit36 = (int) (_arrotoritas.getSize()-1);
_y = (int) (0) ;
for (;(step36 > 0 && _y <= limit36) || (step36 < 0 && _y >= limit36) ;_y = ((int)(0 + _y + step36))  ) {
RDebugUtils.currentLine=2424870;
 //BA.debugLineNum = 2424870;BA.debugLine="If arrotoritas.Get(0) <> \"1\" Then templatemen";
if ((_arrotoritas.Get((int) (0))).equals((Object)("1")) == false) { 
_templatemenu = _templatemenu.replace("1","0");};
RDebugUtils.currentLine=2424871;
 //BA.debugLineNum = 2424871;BA.debugLine="templatemenu = templatemenu.Replace(arrotorit";
_templatemenu = _templatemenu.replace(BA.ObjectToString(_arrotoritas.Get(_y)),BA.NumberToString(1));
 }
};
 };
RDebugUtils.currentLine=2424875;
 //BA.debugLineNum = 2424875;BA.debugLine="If p4.Length = 0 Then";
if (_p4.length()==0) { 
RDebugUtils.currentLine=2424876;
 //BA.debugLineNum = 2424876;BA.debugLine="p4 = templatemenu";
_p4 = _templatemenu;
 }else {
RDebugUtils.currentLine=2424878;
 //BA.debugLineNum = 2424878;BA.debugLine="p4 = p4 & \"#\" & templatemenu";
_p4 = _p4+"#"+_templatemenu;
 };
 }
};
 }
};
RDebugUtils.currentLine=2424882;
 //BA.debugLineNum = 2424882;BA.debugLine="query = \"select * from table(PKG_DMS_MANAGEMENT_T";
_query = "select * from table(PKG_DMS_MANAGEMENT_TRE.fn_createrole('"+_p1+"','"+_p2+"', '"+_p3+"', '"+_p4+"', "+_p5+"))";
RDebugUtils.currentLine=2424883;
 //BA.debugLineNum = 2424883;BA.debugLine="data = exe_query(query, method)";
_data = _exe_query(_query,_method);
RDebugUtils.currentLine=2424884;
 //BA.debugLineNum = 2424884;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2424885;
 //BA.debugLineNum = 2424885;BA.debugLine="End Sub";
return "";
}
public static String  _exe_query(String _query,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "exe_query"))
	return (String) Debug.delegate(ba, "exe_query", new Object[] {_query,_method});
String _hasil = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
anywheresoftware.b4a.objects.collections.List _lst = null;
anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator _js = null;
RDebugUtils.currentLine=2359296;
 //BA.debugLineNum = 2359296;BA.debugLine="Private Sub exe_query(query As String, method As S";
RDebugUtils.currentLine=2359297;
 //BA.debugLineNum = 2359297;BA.debugLine="Dim hasil As String";
_hasil = "";
RDebugUtils.currentLine=2359298;
 //BA.debugLineNum = 2359298;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=2359299;
 //BA.debugLineNum = 2359299;BA.debugLine="Dim lst As List";
_lst = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=2359300;
 //BA.debugLineNum = 2359300;BA.debugLine="Dim js As JSONGenerator";
_js = new anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator();
RDebugUtils.currentLine=2359302;
 //BA.debugLineNum = 2359302;BA.debugLine="lst.Initialize";
_lst.Initialize();
RDebugUtils.currentLine=2359303;
 //BA.debugLineNum = 2359303;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=2359304;
 //BA.debugLineNum = 2359304;BA.debugLine="koneksidb.Initialize";
_koneksidb._initialize(null,ba);
RDebugUtils.currentLine=2359306;
 //BA.debugLineNum = 2359306;BA.debugLine="mp = koneksidb.SqlSelectMap(query, method)";
_mp = _koneksidb._sqlselectmap(null,_query,_method);
RDebugUtils.currentLine=2359307;
 //BA.debugLineNum = 2359307;BA.debugLine="js.Initialize(mp)";
_js.Initialize(_mp);
RDebugUtils.currentLine=2359308;
 //BA.debugLineNum = 2359308;BA.debugLine="hasil = js.ToString";
_hasil = _js.ToString();
RDebugUtils.currentLine=2359309;
 //BA.debugLineNum = 2359309;BA.debugLine="Return hasil";
if (true) return _hasil;
RDebugUtils.currentLine=2359310;
 //BA.debugLineNum = 2359310;BA.debugLine="End Sub";
return "";
}
public static String  _deactivate_roles(String _p1,String _p2,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "deactivate_roles"))
	return (String) Debug.delegate(ba, "deactivate_roles", new Object[] {_p1,_p2,_method});
String _data = "";
String _query = "";
RDebugUtils.currentLine=2621440;
 //BA.debugLineNum = 2621440;BA.debugLine="Public Sub deactivate_roles(p1 As String, p2 As St";
RDebugUtils.currentLine=2621441;
 //BA.debugLineNum = 2621441;BA.debugLine="Dim data, query As String";
_data = "";
_query = "";
RDebugUtils.currentLine=2621442;
 //BA.debugLineNum = 2621442;BA.debugLine="query = \"select * from table(PKG_DMS_MANAGEMENT_T";
_query = "select * from table(PKG_DMS_MANAGEMENT_TRE.fn_deactivate('"+_p1+"','"+_p2+"'))";
RDebugUtils.currentLine=2621443;
 //BA.debugLineNum = 2621443;BA.debugLine="data = exe_query(query, method)";
_data = _exe_query(_query,_method);
RDebugUtils.currentLine=2621444;
 //BA.debugLineNum = 2621444;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2621445;
 //BA.debugLineNum = 2621445;BA.debugLine="End Sub";
return "";
}
public static String  _detail_roles(String _p1,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "detail_roles"))
	return (String) Debug.delegate(ba, "detail_roles", new Object[] {_p1,_method});
String _data = "";
String _query = "";
RDebugUtils.currentLine=2686976;
 //BA.debugLineNum = 2686976;BA.debugLine="Public Sub detail_roles(p1 As String, method As St";
RDebugUtils.currentLine=2686977;
 //BA.debugLineNum = 2686977;BA.debugLine="Dim data, query As String";
_data = "";
_query = "";
RDebugUtils.currentLine=2686978;
 //BA.debugLineNum = 2686978;BA.debugLine="query = \"select * from table(PKG_DMS_MANAGEMENT_T";
_query = "select * from table(PKG_DMS_MANAGEMENT_TRE.fn_detailrole('"+_p1+"'))";
RDebugUtils.currentLine=2686979;
 //BA.debugLineNum = 2686979;BA.debugLine="data = exe_query(query, method)";
_data = _exe_query(_query,_method);
RDebugUtils.currentLine=2686980;
 //BA.debugLineNum = 2686980;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2686981;
 //BA.debugLineNum = 2686981;BA.debugLine="End Sub";
return "";
}
public static String  _detail_user(String _p1,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "detail_user"))
	return (String) Debug.delegate(ba, "detail_user", new Object[] {_p1,_method});
String _data = "";
String _query = "";
RDebugUtils.currentLine=2818048;
 //BA.debugLineNum = 2818048;BA.debugLine="Public Sub detail_user(p1 As String, method As Str";
RDebugUtils.currentLine=2818049;
 //BA.debugLineNum = 2818049;BA.debugLine="Dim data, query As String";
_data = "";
_query = "";
RDebugUtils.currentLine=2818050;
 //BA.debugLineNum = 2818050;BA.debugLine="query = $\" select * from table(PKG_DMS_MANAGEMENT";
_query = (" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_detailuser('"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p1))+"')) ");
RDebugUtils.currentLine=2818051;
 //BA.debugLineNum = 2818051;BA.debugLine="data = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=2818052;
 //BA.debugLineNum = 2818052;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2818053;
 //BA.debugLineNum = 2818053;BA.debugLine="End Sub";
return "";
}
public static String  _edit_user(String _p1,String _p2,String _p3,String _p4,String _p5,String _p6,String _p7,String _p8,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "edit_user"))
	return (String) Debug.delegate(ba, "edit_user", new Object[] {_p1,_p2,_p3,_p4,_p5,_p6,_p7,_p8,_method});
String _data = "";
String _query = "";
RDebugUtils.currentLine=2949120;
 //BA.debugLineNum = 2949120;BA.debugLine="Public Sub edit_user(p1 As String, p2 As String, p";
RDebugUtils.currentLine=2949121;
 //BA.debugLineNum = 2949121;BA.debugLine="Dim data, query As String";
_data = "";
_query = "";
RDebugUtils.currentLine=2949122;
 //BA.debugLineNum = 2949122;BA.debugLine="query = $\" select * from table(PKG_DMS_MANAGEMENT";
_query = (" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_edituser('"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p1))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p2))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p3))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p4))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p5))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p6))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p7))+"', '"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p8))+"')) ");
RDebugUtils.currentLine=2949123;
 //BA.debugLineNum = 2949123;BA.debugLine="data = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=2949124;
 //BA.debugLineNum = 2949124;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2949125;
 //BA.debugLineNum = 2949125;BA.debugLine="End Sub";
return "";
}
public static String  _list_user(String _p1,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "list_user"))
	return (String) Debug.delegate(ba, "list_user", new Object[] {_p1,_method});
String _data = "";
String _query = "";
RDebugUtils.currentLine=2752512;
 //BA.debugLineNum = 2752512;BA.debugLine="Public Sub list_user(p1 As String, method As Strin";
RDebugUtils.currentLine=2752513;
 //BA.debugLineNum = 2752513;BA.debugLine="Dim data, query As String";
_data = "";
_query = "";
RDebugUtils.currentLine=2752514;
 //BA.debugLineNum = 2752514;BA.debugLine="query = $\" select id_pegawai f1, nama_pegawai f2,";
_query = (" select id_pegawai f1, nama_pegawai f2, nip_pegawai f3, roles_nama f4,  nama_unit f5, \n"+"	nama_jabatan f6 \n"+"	from v_user_pegawai ");
RDebugUtils.currentLine=2752517;
 //BA.debugLineNum = 2752517;BA.debugLine="data = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=2752518;
 //BA.debugLineNum = 2752518;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2752519;
 //BA.debugLineNum = 2752519;BA.debugLine="End Sub";
return "";
}
public static String  _read_menu(String _p1,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "read_menu"))
	return (String) Debug.delegate(ba, "read_menu", new Object[] {_p1,_method});
String _data = "";
String _query = "";
RDebugUtils.currentLine=3014656;
 //BA.debugLineNum = 3014656;BA.debugLine="Public Sub read_menu(p1 As String, method As Strin";
RDebugUtils.currentLine=3014658;
 //BA.debugLineNum = 3014658;BA.debugLine="Dim data, query As String";
_data = "";
_query = "";
RDebugUtils.currentLine=3014659;
 //BA.debugLineNum = 3014659;BA.debugLine="query = $\" select * from table(PKG_DMS_MANAGEMENT";
_query = (" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_menuapps('"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p1))+"')) ");
RDebugUtils.currentLine=3014660;
 //BA.debugLineNum = 3014660;BA.debugLine="data = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=3014661;
 //BA.debugLineNum = 3014661;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=3014663;
 //BA.debugLineNum = 3014663;BA.debugLine="End Sub";
return "";
}
public static String  _read_roles(String _p1,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "read_roles"))
	return (String) Debug.delegate(ba, "read_roles", new Object[] {_p1,_method});
String _data = "";
String _query = "";
RDebugUtils.currentLine=2490368;
 //BA.debugLineNum = 2490368;BA.debugLine="Public Sub read_roles(p1 As String, method As Stri";
RDebugUtils.currentLine=2490369;
 //BA.debugLineNum = 2490369;BA.debugLine="Dim data, query As String";
_data = "";
_query = "";
RDebugUtils.currentLine=2490370;
 //BA.debugLineNum = 2490370;BA.debugLine="query = \"select * from roles\"";
_query = "select * from roles";
RDebugUtils.currentLine=2490371;
 //BA.debugLineNum = 2490371;BA.debugLine="data = exe_query(query, method)";
_data = _exe_query(_query,_method);
RDebugUtils.currentLine=2490372;
 //BA.debugLineNum = 2490372;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2490373;
 //BA.debugLineNum = 2490373;BA.debugLine="End Sub";
return "";
}
public static String  _save_menu(String _p1,String _p2,String _p3,String _p4,String _p5,String _p6,String _p7,String _p8,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "save_menu"))
	return (String) Debug.delegate(ba, "save_menu", new Object[] {_p1,_p2,_p3,_p4,_p5,_p6,_p7,_p8,_method});
String _data = "";
String _query = "";
RDebugUtils.currentLine=3080192;
 //BA.debugLineNum = 3080192;BA.debugLine="Public Sub save_menu(p1 As String,p2 As String,p3";
RDebugUtils.currentLine=3080194;
 //BA.debugLineNum = 3080194;BA.debugLine="Dim data, query As String";
_data = "";
_query = "";
RDebugUtils.currentLine=3080195;
 //BA.debugLineNum = 3080195;BA.debugLine="query = $\" select * from table(PKG_DMS_MANAGEMENT";
_query = (" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_savemenu('"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p1))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p2))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p3))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p4))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p5))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p6))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p7))+"',"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p8))+")) ");
RDebugUtils.currentLine=3080196;
 //BA.debugLineNum = 3080196;BA.debugLine="data = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=3080197;
 //BA.debugLineNum = 3080197;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=3080199;
 //BA.debugLineNum = 3080199;BA.debugLine="End Sub";
return "";
}
public static String  _save_user(String _p1,String _p2,String _p3,String _p4,String _p5,String _p6,String _p7,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "save_user"))
	return (String) Debug.delegate(ba, "save_user", new Object[] {_p1,_p2,_p3,_p4,_p5,_p6,_p7,_method});
String _data = "";
String _query = "";
RDebugUtils.currentLine=2883584;
 //BA.debugLineNum = 2883584;BA.debugLine="Public Sub save_user(p1 As String, p2 As String, p";
RDebugUtils.currentLine=2883585;
 //BA.debugLineNum = 2883585;BA.debugLine="Dim data, query As String";
_data = "";
_query = "";
RDebugUtils.currentLine=2883586;
 //BA.debugLineNum = 2883586;BA.debugLine="query = $\" select * from table(PKG_DMS_MANAGEMENT";
_query = (" select * from table(PKG_DMS_MANAGEMENT_TRE.fn_saveuser('"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p1))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p2))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p3))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p4))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p5))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p6))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_p7))+"')) ");
RDebugUtils.currentLine=2883587;
 //BA.debugLineNum = 2883587;BA.debugLine="data = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=2883588;
 //BA.debugLineNum = 2883588;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2883589;
 //BA.debugLineNum = 2883589;BA.debugLine="End Sub";
return "";
}
public static String  _update_roles(String _p1,String _p2,String _p3,String _p4,String _p5,int _p6,String _method) throws Exception{
RDebugUtils.currentModule="clsmanagement";
if (Debug.shouldDelegate(ba, "update_roles"))
	return (String) Debug.delegate(ba, "update_roles", new Object[] {_p1,_p2,_p3,_p4,_p5,_p6,_method});
String _data = "";
String _query = "";
RDebugUtils.currentLine=2555904;
 //BA.debugLineNum = 2555904;BA.debugLine="Public Sub update_roles(p1 As String,  p2 As Strin";
RDebugUtils.currentLine=2555905;
 //BA.debugLineNum = 2555905;BA.debugLine="Dim data, query As String";
_data = "";
_query = "";
RDebugUtils.currentLine=2555906;
 //BA.debugLineNum = 2555906;BA.debugLine="query = \"select * from table(PKG_DMS_MANAGEMENT_T";
_query = "select * from table(PKG_DMS_MANAGEMENT_TRE.fn_editrole('"+_p1+"','"+_p2+"', '"+_p3+"', '"+_p4+"', '"+_p5+"', "+BA.NumberToString(_p6)+"))";
RDebugUtils.currentLine=2555907;
 //BA.debugLineNum = 2555907;BA.debugLine="data = exe_query(query, method)";
_data = _exe_query(_query,_method);
RDebugUtils.currentLine=2555908;
 //BA.debugLineNum = 2555908;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2555909;
 //BA.debugLineNum = 2555909;BA.debugLine="End Sub";
return "";
}
}