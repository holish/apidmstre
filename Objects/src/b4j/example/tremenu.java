package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.debug.*;

public class tremenu extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    public static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.tremenu", this);
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            ba.htSubs = htSubs;
             
        }
        if (BA.isShellModeRuntimeCheck(ba))
                this.getClass().getMethod("_class_globals", b4j.example.tremenu.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public String _clsid = "";
public b4j.example.main _main = null;
public b4j.example.exequery _exequery = null;
public b4j.example.clsmanagement _clsmanagement = null;
public b4j.example.clsmaster _clsmaster = null;
public b4j.example.modgeneratereturn _modgeneratereturn = null;
public String  _class_globals(b4j.example.tremenu __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="tremenu";
RDebugUtils.currentLine=7208960;
 //BA.debugLineNum = 7208960;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=7208961;
 //BA.debugLineNum = 7208961;BA.debugLine="Private clsid As String";
_clsid = "";
RDebugUtils.currentLine=7208962;
 //BA.debugLineNum = 7208962;BA.debugLine="End Sub";
return "";
}
public String  _handle(b4j.example.tremenu __ref,anywheresoftware.b4j.object.JServlet.ServletRequestWrapper _req,anywheresoftware.b4j.object.JServlet.ServletResponseWrapper _resp) throws Exception{
__ref = this;
RDebugUtils.currentModule="tremenu";
if (Debug.shouldDelegate(ba, "handle"))
	return (String) Debug.delegate(ba, "handle", new Object[] {_req,_resp});
String _appid = "";
String _query = "";
String _response = "";
RDebugUtils.currentLine=7340032;
 //BA.debugLineNum = 7340032;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
RDebugUtils.currentLine=7340033;
 //BA.debugLineNum = 7340033;BA.debugLine="Dim appid, query As String";
_appid = "";
_query = "";
RDebugUtils.currentLine=7340034;
 //BA.debugLineNum = 7340034;BA.debugLine="Dim response As String";
_response = "";
RDebugUtils.currentLine=7340037;
 //BA.debugLineNum = 7340037;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
_appid = _req.GetParameter("clsakses");
RDebugUtils.currentLine=7340040;
 //BA.debugLineNum = 7340040;BA.debugLine="If appid <> clsid Then";
if ((_appid).equals(__ref._clsid) == false) { 
RDebugUtils.currentLine=7340041;
 //BA.debugLineNum = 7340041;BA.debugLine="response = modGenerateReturn.return_json(modGene";
_response = _modgeneratereturn._return_json(_modgeneratereturn._label_rejected,_modgeneratereturn._kode_rejected_class,"");
RDebugUtils.currentLine=7340042;
 //BA.debugLineNum = 7340042;BA.debugLine="resp.Write(response)";
_resp.Write(_response);
RDebugUtils.currentLine=7340043;
 //BA.debugLineNum = 7340043;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=7340046;
 //BA.debugLineNum = 7340046;BA.debugLine="query = \"\"";
_query = "";
RDebugUtils.currentLine=7340048;
 //BA.debugLineNum = 7340048;BA.debugLine="End Sub";
return "";
}
public String  _initialize(b4j.example.tremenu __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="tremenu";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
RDebugUtils.currentLine=7274496;
 //BA.debugLineNum = 7274496;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=7274497;
 //BA.debugLineNum = 7274497;BA.debugLine="clsid = \"tre_menuakses\"";
__ref._clsid = "tre_menuakses";
RDebugUtils.currentLine=7274498;
 //BA.debugLineNum = 7274498;BA.debugLine="End Sub";
return "";
}
}