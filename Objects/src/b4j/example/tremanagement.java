package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.debug.*;

public class tremanagement extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    public static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.tremanagement", this);
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            ba.htSubs = htSubs;
             
        }
        if (BA.isShellModeRuntimeCheck(ba))
                this.getClass().getMethod("_class_globals", b4j.example.tremanagement.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public String _clsid = "";
public b4j.example.main _main = null;
public b4j.example.exequery _exequery = null;
public b4j.example.clsmanagement _clsmanagement = null;
public b4j.example.clsmaster _clsmaster = null;
public b4j.example.modgeneratereturn _modgeneratereturn = null;
public String  _class_globals(b4j.example.tremanagement __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="tremanagement";
RDebugUtils.currentLine=3145728;
 //BA.debugLineNum = 3145728;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=3145729;
 //BA.debugLineNum = 3145729;BA.debugLine="Private clsid As String";
_clsid = "";
RDebugUtils.currentLine=3145731;
 //BA.debugLineNum = 3145731;BA.debugLine="End Sub";
return "";
}
public String  _handle(b4j.example.tremanagement __ref,anywheresoftware.b4j.object.JServlet.ServletRequestWrapper _req,anywheresoftware.b4j.object.JServlet.ServletResponseWrapper _resp) throws Exception{
__ref = this;
RDebugUtils.currentModule="tremanagement";
if (Debug.shouldDelegate(ba, "handle"))
	return (String) Debug.delegate(ba, "handle", new Object[] {_req,_resp});
String _appid = "";
String _hasil = "";
String _method = "";
String _response = "";
RDebugUtils.currentLine=3276800;
 //BA.debugLineNum = 3276800;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
RDebugUtils.currentLine=3276801;
 //BA.debugLineNum = 3276801;BA.debugLine="Dim appid, hasil, method As String";
_appid = "";
_hasil = "";
_method = "";
RDebugUtils.currentLine=3276802;
 //BA.debugLineNum = 3276802;BA.debugLine="Dim response As String";
_response = "";
RDebugUtils.currentLine=3276804;
 //BA.debugLineNum = 3276804;BA.debugLine="If req.Method <> \"POST\" Then";
if ((_req.getMethod()).equals("POST") == false) { 
RDebugUtils.currentLine=3276805;
 //BA.debugLineNum = 3276805;BA.debugLine="resp.SendError(500, \"method not supported.\")";
_resp.SendError((int) (500),"method not supported.");
RDebugUtils.currentLine=3276806;
 //BA.debugLineNum = 3276806;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=3276810;
 //BA.debugLineNum = 3276810;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
_appid = _req.GetParameter("clsakses");
RDebugUtils.currentLine=3276811;
 //BA.debugLineNum = 3276811;BA.debugLine="method = req.GetParameter(\"method\")";
_method = _req.GetParameter("method");
RDebugUtils.currentLine=3276814;
 //BA.debugLineNum = 3276814;BA.debugLine="If appid <> clsid Then";
if ((_appid).equals(__ref._clsid) == false) { 
RDebugUtils.currentLine=3276815;
 //BA.debugLineNum = 3276815;BA.debugLine="response = modGenerateReturn.return_json(modGene";
_response = _modgeneratereturn._return_json(_modgeneratereturn._label_rejected,_modgeneratereturn._kode_rejected_class,_method);
RDebugUtils.currentLine=3276816;
 //BA.debugLineNum = 3276816;BA.debugLine="resp.Write(response)";
_resp.Write(_response);
RDebugUtils.currentLine=3276817;
 //BA.debugLineNum = 3276817;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=3276820;
 //BA.debugLineNum = 3276820;BA.debugLine="Select method";
switch (BA.switchObjectToInt(_method,"createrole","readrole","editrole","deactivate","detailrole","listuser","detailuser","saveuser","edituser","listmenu","savemenu")) {
case 0: {
RDebugUtils.currentLine=3276822;
 //BA.debugLineNum = 3276822;BA.debugLine="resp.Write( clsManagement.create_roles(req.GetP";
_resp.Write(_clsmanagement._create_roles(_req.GetParameter("nama"),_req.GetParameter("ket"),_req.GetParameter("menu"),_method));
 break; }
case 1: {
RDebugUtils.currentLine=3276824;
 //BA.debugLineNum = 3276824;BA.debugLine="resp.Write( clsManagement.read_roles(req.GetPar";
_resp.Write(_clsmanagement._read_roles(_req.GetParameter("cari"),_method));
 break; }
case 2: {
RDebugUtils.currentLine=3276826;
 //BA.debugLineNum = 3276826;BA.debugLine="resp.Write( clsManagement.update_roles(req.GetP";
_resp.Write(_clsmanagement._update_roles(_req.GetParameter("id"),_req.GetParameter("nama"),_req.GetParameter("ket"),_req.GetParameter("menu"),_req.GetParameter("roleotoritas"),(int)(Double.parseDouble(_req.GetParameter("totalmenu"))),_method));
 break; }
case 3: {
RDebugUtils.currentLine=3276828;
 //BA.debugLineNum = 3276828;BA.debugLine="resp.Write( clsManagement.deactivate_roles(req.";
_resp.Write(_clsmanagement._deactivate_roles(_req.GetParameter("id"),_req.GetParameter("status"),_method));
 break; }
case 4: {
RDebugUtils.currentLine=3276830;
 //BA.debugLineNum = 3276830;BA.debugLine="resp.Write( clsManagement.detail_roles(req.GetP";
_resp.Write(_clsmanagement._detail_roles(_req.GetParameter("id"),_method));
 break; }
case 5: {
RDebugUtils.currentLine=3276833;
 //BA.debugLineNum = 3276833;BA.debugLine="resp.Write(clsManagement.list_user(req.GetParam";
_resp.Write(_clsmanagement._list_user(_req.GetParameter("cari"),_method));
 break; }
case 6: {
RDebugUtils.currentLine=3276835;
 //BA.debugLineNum = 3276835;BA.debugLine="resp.Write(clsManagement.detail_user(req.GetPar";
_resp.Write(_clsmanagement._detail_user(_req.GetParameter("id"),_method));
 break; }
case 7: {
RDebugUtils.currentLine=3276837;
 //BA.debugLineNum = 3276837;BA.debugLine="resp.Write(clsManagement.save_user(req.GetParam";
_resp.Write(_clsmanagement._save_user(_req.GetParameter("username"),_req.GetParameter("rolesid"),_req.GetParameter("nip"),_req.GetParameter("nama"),_req.GetParameter("idunit"),_req.GetParameter("idjabatan"),_req.GetParameter("ispegawai"),_method));
 break; }
case 8: {
RDebugUtils.currentLine=3276839;
 //BA.debugLineNum = 3276839;BA.debugLine="resp.Write(clsManagement.edit_user(req.GetParam";
_resp.Write(_clsmanagement._edit_user(_req.GetParameter("iduser"),_req.GetParameter("username"),_req.GetParameter("rolesid"),_req.GetParameter("nip"),_req.GetParameter("nama"),_req.GetParameter("idunit"),_req.GetParameter("idjabatan"),_req.GetParameter("ispegawai"),_method));
 break; }
case 9: {
RDebugUtils.currentLine=3276842;
 //BA.debugLineNum = 3276842;BA.debugLine="resp.Write(clsManagement.read_menu(req.GetParam";
_resp.Write(_clsmanagement._read_menu(_req.GetParameter("id"),_method));
 break; }
case 10: {
RDebugUtils.currentLine=3276844;
 //BA.debugLineNum = 3276844;BA.debugLine="resp.Write(clsManagement.save_menu(req.GetParam";
_resp.Write(_clsmanagement._save_menu(_req.GetParameter("nama"),_req.GetParameter("url"),_req.GetParameter("ket"),_req.GetParameter("icon"),_req.GetParameter("class"),_req.GetParameter("parent"),_req.GetParameter("islabel"),_req.GetParameter("urutan"),_method));
 break; }
default: {
RDebugUtils.currentLine=3276847;
 //BA.debugLineNum = 3276847;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
_hasil = _modgeneratereturn._return_json("TIDAK ADA METHOD","404",_method);
RDebugUtils.currentLine=3276848;
 //BA.debugLineNum = 3276848;BA.debugLine="resp.Write(hasil)";
_resp.Write(_hasil);
 break; }
}
;
RDebugUtils.currentLine=3276851;
 //BA.debugLineNum = 3276851;BA.debugLine="End Sub";
return "";
}
public String  _initialize(b4j.example.tremanagement __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="tremanagement";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
RDebugUtils.currentLine=3211264;
 //BA.debugLineNum = 3211264;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=3211265;
 //BA.debugLineNum = 3211265;BA.debugLine="clsid = \"tre_management\"";
__ref._clsid = "tre_management";
RDebugUtils.currentLine=3211266;
 //BA.debugLineNum = 3211266;BA.debugLine="End Sub";
return "";
}
}