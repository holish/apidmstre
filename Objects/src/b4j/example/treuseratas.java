package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.debug.*;

public class treuseratas extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    public static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.treuseratas", this);
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            ba.htSubs = htSubs;
             
        }
        if (BA.isShellModeRuntimeCheck(ba))
                this.getClass().getMethod("_class_globals", b4j.example.treuseratas.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public String _clsid = "";
public b4j.example.main _main = null;
public b4j.example.exequery _exequery = null;
public b4j.example.clsmanagement _clsmanagement = null;
public b4j.example.clsmaster _clsmaster = null;
public b4j.example.modgeneratereturn _modgeneratereturn = null;
public String  _class_globals(b4j.example.treuseratas __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="treuseratas";
RDebugUtils.currentLine=7405568;
 //BA.debugLineNum = 7405568;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=7405569;
 //BA.debugLineNum = 7405569;BA.debugLine="Private clsid As String";
_clsid = "";
RDebugUtils.currentLine=7405570;
 //BA.debugLineNum = 7405570;BA.debugLine="End Sub";
return "";
}
public String  _getuip(b4j.example.treuseratas __ref,String _username,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="treuseratas";
if (Debug.shouldDelegate(ba, "getuip"))
	return (String) Debug.delegate(ba, "getuip", new Object[] {_username,_method});
String _query = "";
String _data = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
RDebugUtils.currentLine=7602176;
 //BA.debugLineNum = 7602176;BA.debugLine="Private Sub getuip(username As String, method As S";
RDebugUtils.currentLine=7602177;
 //BA.debugLineNum = 7602177;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=7602178;
 //BA.debugLineNum = 7602178;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=7602179;
 //BA.debugLineNum = 7602179;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=7602181;
 //BA.debugLineNum = 7602181;BA.debugLine="query = $\" select a.id_jabatan from m_pegawai a";
_query = (" select a.id_jabatan from m_pegawai a\n"+"	inner join m_jabatan b on a.id_jabatan = b.id_jabatan \n"+"	where upper(username_pegawai) = upper('"+__c.SmartStringFormatter("",(Object)(_username))+"') ");
RDebugUtils.currentLine=7602184;
 //BA.debugLineNum = 7602184;BA.debugLine="mp = exequery.single_map(query, method)";
_mp = _exequery._single_map(_query,_method);
RDebugUtils.currentLine=7602186;
 //BA.debugLineNum = 7602186;BA.debugLine="query = $\" select * from table(PKG_DMS_USER_FLOW.";
_query = (" select * from table(PKG_DMS_USER_FLOW.fn_loaduseratas('"+__c.SmartStringFormatter("",_mp.Get((Object)("id_jabatan")))+"')) ");
RDebugUtils.currentLine=7602187;
 //BA.debugLineNum = 7602187;BA.debugLine="data  = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=7602188;
 //BA.debugLineNum = 7602188;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=7602189;
 //BA.debugLineNum = 7602189;BA.debugLine="End Sub";
return "";
}
public String  _handle(b4j.example.treuseratas __ref,anywheresoftware.b4j.object.JServlet.ServletRequestWrapper _req,anywheresoftware.b4j.object.JServlet.ServletResponseWrapper _resp) throws Exception{
__ref = this;
RDebugUtils.currentModule="treuseratas";
if (Debug.shouldDelegate(ba, "handle"))
	return (String) Debug.delegate(ba, "handle", new Object[] {_req,_resp});
String _appid = "";
String _hasil = "";
String _method = "";
String _response = "";
RDebugUtils.currentLine=7536640;
 //BA.debugLineNum = 7536640;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
RDebugUtils.currentLine=7536641;
 //BA.debugLineNum = 7536641;BA.debugLine="Dim appid, hasil, method As String";
_appid = "";
_hasil = "";
_method = "";
RDebugUtils.currentLine=7536642;
 //BA.debugLineNum = 7536642;BA.debugLine="Dim response As String";
_response = "";
RDebugUtils.currentLine=7536644;
 //BA.debugLineNum = 7536644;BA.debugLine="If req.Method <> \"POST\" Then";
if ((_req.getMethod()).equals("POST") == false) { 
RDebugUtils.currentLine=7536645;
 //BA.debugLineNum = 7536645;BA.debugLine="resp.SendError(500, \"method not supported.\")";
_resp.SendError((int) (500),"method not supported.");
RDebugUtils.currentLine=7536646;
 //BA.debugLineNum = 7536646;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=7536650;
 //BA.debugLineNum = 7536650;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
_appid = _req.GetParameter("clsakses");
RDebugUtils.currentLine=7536651;
 //BA.debugLineNum = 7536651;BA.debugLine="method = req.GetParameter(\"method\")";
_method = _req.GetParameter("method");
RDebugUtils.currentLine=7536654;
 //BA.debugLineNum = 7536654;BA.debugLine="If appid <> clsid Then";
if ((_appid).equals(__ref._clsid) == false) { 
RDebugUtils.currentLine=7536655;
 //BA.debugLineNum = 7536655;BA.debugLine="response = modGenerateReturn.return_json(modGene";
_response = _modgeneratereturn._return_json(_modgeneratereturn._label_rejected,_modgeneratereturn._kode_rejected_class,_method);
RDebugUtils.currentLine=7536656;
 //BA.debugLineNum = 7536656;BA.debugLine="resp.Write(response)";
_resp.Write(_response);
RDebugUtils.currentLine=7536657;
 //BA.debugLineNum = 7536657;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=7536660;
 //BA.debugLineNum = 7536660;BA.debugLine="Select method";
switch (BA.switchObjectToInt(_method,"get")) {
case 0: {
RDebugUtils.currentLine=7536662;
 //BA.debugLineNum = 7536662;BA.debugLine="resp.Write(getuip(req.GetParameter(\"username\"),";
_resp.Write(__ref._getuip(null,_req.GetParameter("username"),_method));
 break; }
default: {
RDebugUtils.currentLine=7536664;
 //BA.debugLineNum = 7536664;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
_hasil = _modgeneratereturn._return_json("TIDAK ADA METHOD","404",_method);
RDebugUtils.currentLine=7536665;
 //BA.debugLineNum = 7536665;BA.debugLine="resp.Write(hasil)";
_resp.Write(_hasil);
 break; }
}
;
RDebugUtils.currentLine=7536668;
 //BA.debugLineNum = 7536668;BA.debugLine="End Sub";
return "";
}
public String  _initialize(b4j.example.treuseratas __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="treuseratas";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
RDebugUtils.currentLine=7471104;
 //BA.debugLineNum = 7471104;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=7471105;
 //BA.debugLineNum = 7471105;BA.debugLine="clsid = \"tre_atasan\"";
__ref._clsid = "tre_atasan";
RDebugUtils.currentLine=7471106;
 //BA.debugLineNum = 7471106;BA.debugLine="End Sub";
return "";
}
}