package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.debug.*;

public class tredatasetting extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    public static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.tredatasetting", this);
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            ba.htSubs = htSubs;
             
        }
        if (BA.isShellModeRuntimeCheck(ba))
                this.getClass().getMethod("_class_globals", b4j.example.tredatasetting.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public String _clsid = "";
public b4j.example.clsdb _todb = null;
public b4j.example.clsdbdms _todbdms = null;
public b4j.example.main _main = null;
public b4j.example.exequery _exequery = null;
public b4j.example.clsmanagement _clsmanagement = null;
public b4j.example.clsmaster _clsmaster = null;
public b4j.example.modgeneratereturn _modgeneratereturn = null;
public String  _class_globals(b4j.example.tredatasetting __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="tredatasetting";
RDebugUtils.currentLine=1835008;
 //BA.debugLineNum = 1835008;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=1835009;
 //BA.debugLineNum = 1835009;BA.debugLine="Private clsid As String";
_clsid = "";
RDebugUtils.currentLine=1835010;
 //BA.debugLineNum = 1835010;BA.debugLine="Private todb As clsDB";
_todb = new b4j.example.clsdb();
RDebugUtils.currentLine=1835011;
 //BA.debugLineNum = 1835011;BA.debugLine="Private todbdms As clsDBDms";
_todbdms = new b4j.example.clsdbdms();
RDebugUtils.currentLine=1835012;
 //BA.debugLineNum = 1835012;BA.debugLine="End Sub";
return "";
}
public String  _exe_query(b4j.example.tredatasetting __ref,String _query,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="tredatasetting";
if (Debug.shouldDelegate(ba, "exe_query"))
	return (String) Debug.delegate(ba, "exe_query", new Object[] {_query,_method});
String _hasil = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
anywheresoftware.b4a.objects.collections.List _lst = null;
anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator _js = null;
RDebugUtils.currentLine=2031616;
 //BA.debugLineNum = 2031616;BA.debugLine="Private Sub exe_query(query As String, method As S";
RDebugUtils.currentLine=2031617;
 //BA.debugLineNum = 2031617;BA.debugLine="Dim hasil As String";
_hasil = "";
RDebugUtils.currentLine=2031618;
 //BA.debugLineNum = 2031618;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=2031619;
 //BA.debugLineNum = 2031619;BA.debugLine="Dim lst As List";
_lst = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=2031620;
 //BA.debugLineNum = 2031620;BA.debugLine="Dim js As JSONGenerator";
_js = new anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator();
RDebugUtils.currentLine=2031622;
 //BA.debugLineNum = 2031622;BA.debugLine="lst.Initialize";
_lst.Initialize();
RDebugUtils.currentLine=2031623;
 //BA.debugLineNum = 2031623;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=2031625;
 //BA.debugLineNum = 2031625;BA.debugLine="mp = todb.SqlSelectMap(query, method)";
_mp = __ref._todb._sqlselectmap(null,_query,_method);
RDebugUtils.currentLine=2031626;
 //BA.debugLineNum = 2031626;BA.debugLine="js.Initialize(mp)";
_js.Initialize(_mp);
RDebugUtils.currentLine=2031627;
 //BA.debugLineNum = 2031627;BA.debugLine="hasil = js.ToString";
_hasil = _js.ToString();
RDebugUtils.currentLine=2031629;
 //BA.debugLineNum = 2031629;BA.debugLine="Return hasil";
if (true) return _hasil;
RDebugUtils.currentLine=2031630;
 //BA.debugLineNum = 2031630;BA.debugLine="End Sub";
return "";
}
public String  _get(b4j.example.tredatasetting __ref,String _key,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="tredatasetting";
if (Debug.shouldDelegate(ba, "get"))
	return (String) Debug.delegate(ba, "get", new Object[] {_key,_method});
String _query = "";
String _data = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
RDebugUtils.currentLine=2097152;
 //BA.debugLineNum = 2097152;BA.debugLine="Private Sub get(key As String, method As String) A";
RDebugUtils.currentLine=2097153;
 //BA.debugLineNum = 2097153;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=2097154;
 //BA.debugLineNum = 2097154;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=2097155;
 //BA.debugLineNum = 2097155;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=2097157;
 //BA.debugLineNum = 2097157;BA.debugLine="query = $\" select * from table(PKG_DMS_SETTING.fn";
_query = (" select * from table(PKG_DMS_SETTING.fn_getsetting('"+__c.SmartStringFormatter("",(Object)(_key))+"')) ");
RDebugUtils.currentLine=2097158;
 //BA.debugLineNum = 2097158;BA.debugLine="data  = exe_query(query, method)";
_data = __ref._exe_query(null,_query,_method);
RDebugUtils.currentLine=2097159;
 //BA.debugLineNum = 2097159;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2097160;
 //BA.debugLineNum = 2097160;BA.debugLine="End Sub";
return "";
}
public String  _getval(b4j.example.tredatasetting __ref,String _key,String _val,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="tredatasetting";
if (Debug.shouldDelegate(ba, "getval"))
	return (String) Debug.delegate(ba, "getval", new Object[] {_key,_val,_method});
String _query = "";
String _data = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
RDebugUtils.currentLine=2228224;
 //BA.debugLineNum = 2228224;BA.debugLine="Private Sub getval (key As String, val As String,";
RDebugUtils.currentLine=2228225;
 //BA.debugLineNum = 2228225;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=2228226;
 //BA.debugLineNum = 2228226;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=2228227;
 //BA.debugLineNum = 2228227;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=2228229;
 //BA.debugLineNum = 2228229;BA.debugLine="query = $\" select * from table(PKG_DMS_SETTING.fn";
_query = (" select * from table(PKG_DMS_SETTING.fn_getsettingval('"+__c.SmartStringFormatter("",(Object)(_key))+"', '"+__c.SmartStringFormatter("",(Object)(_val))+"')) ");
RDebugUtils.currentLine=2228230;
 //BA.debugLineNum = 2228230;BA.debugLine="data  = exe_query(query, method)";
_data = __ref._exe_query(null,_query,_method);
RDebugUtils.currentLine=2228231;
 //BA.debugLineNum = 2228231;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2228232;
 //BA.debugLineNum = 2228232;BA.debugLine="End Sub";
return "";
}
public String  _handle(b4j.example.tredatasetting __ref,anywheresoftware.b4j.object.JServlet.ServletRequestWrapper _req,anywheresoftware.b4j.object.JServlet.ServletResponseWrapper _resp) throws Exception{
__ref = this;
RDebugUtils.currentModule="tredatasetting";
if (Debug.shouldDelegate(ba, "handle"))
	return (String) Debug.delegate(ba, "handle", new Object[] {_req,_resp});
String _appid = "";
String _response = "";
String _method = "";
String _hasil = "";
RDebugUtils.currentLine=1966080;
 //BA.debugLineNum = 1966080;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
RDebugUtils.currentLine=1966081;
 //BA.debugLineNum = 1966081;BA.debugLine="Dim appid, response, method, hasil As String";
_appid = "";
_response = "";
_method = "";
_hasil = "";
RDebugUtils.currentLine=1966084;
 //BA.debugLineNum = 1966084;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
_appid = _req.GetParameter("clsakses");
RDebugUtils.currentLine=1966085;
 //BA.debugLineNum = 1966085;BA.debugLine="method = req.GetParameter(\"method\")";
_method = _req.GetParameter("method");
RDebugUtils.currentLine=1966087;
 //BA.debugLineNum = 1966087;BA.debugLine="If appid <> clsid Then";
if ((_appid).equals(__ref._clsid) == false) { 
RDebugUtils.currentLine=1966088;
 //BA.debugLineNum = 1966088;BA.debugLine="response = modGenerateReturn.return_json(modGene";
_response = _modgeneratereturn._return_json(_modgeneratereturn._label_rejected,_modgeneratereturn._kode_rejected_class,_method);
RDebugUtils.currentLine=1966089;
 //BA.debugLineNum = 1966089;BA.debugLine="resp.Write(response)";
_resp.Write(_response);
RDebugUtils.currentLine=1966090;
 //BA.debugLineNum = 1966090;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=1966092;
 //BA.debugLineNum = 1966092;BA.debugLine="Select method";
switch (BA.switchObjectToInt(_method,"get","save","getval")) {
case 0: {
RDebugUtils.currentLine=1966094;
 //BA.debugLineNum = 1966094;BA.debugLine="resp.Write(get(req.GetParameter(\"key\"), method)";
_resp.Write(__ref._get(null,_req.GetParameter("key"),_method));
 break; }
case 1: {
RDebugUtils.currentLine=1966096;
 //BA.debugLineNum = 1966096;BA.debugLine="resp.Write(save(req.GetParameter(\"key\"), req.Ge";
_resp.Write(__ref._save(null,_req.GetParameter("key"),_req.GetParameter("name"),_req.GetParameter("value"),_req.GetParameter("ket"),_method));
 break; }
case 2: {
RDebugUtils.currentLine=1966098;
 //BA.debugLineNum = 1966098;BA.debugLine="resp.Write(getval(req.GetParameter(\"key\"), req.";
_resp.Write(__ref._getval(null,_req.GetParameter("key"),_req.GetParameter("name"),_method));
 break; }
default: {
RDebugUtils.currentLine=1966100;
 //BA.debugLineNum = 1966100;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
_hasil = _modgeneratereturn._return_json("TIDAK ADA METHOD","404",_method);
RDebugUtils.currentLine=1966101;
 //BA.debugLineNum = 1966101;BA.debugLine="resp.Write(hasil)";
_resp.Write(_hasil);
 break; }
}
;
RDebugUtils.currentLine=1966105;
 //BA.debugLineNum = 1966105;BA.debugLine="End Sub";
return "";
}
public String  _save(b4j.example.tredatasetting __ref,String _key,String _name,String _value,String _ket,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="tredatasetting";
if (Debug.shouldDelegate(ba, "save"))
	return (String) Debug.delegate(ba, "save", new Object[] {_key,_name,_value,_ket,_method});
String _query = "";
String _data = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
RDebugUtils.currentLine=2162688;
 //BA.debugLineNum = 2162688;BA.debugLine="Private Sub save(key As String,name As String,valu";
RDebugUtils.currentLine=2162689;
 //BA.debugLineNum = 2162689;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=2162690;
 //BA.debugLineNum = 2162690;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=2162691;
 //BA.debugLineNum = 2162691;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=2162693;
 //BA.debugLineNum = 2162693;BA.debugLine="query = $\" select * from data_setting where key_s";
_query = (" select * from data_setting where key_setting = '"+__c.SmartStringFormatter("",(Object)(_key))+"' ");
RDebugUtils.currentLine=2162694;
 //BA.debugLineNum = 2162694;BA.debugLine="data  = exe_query(query, method)";
_data = __ref._exe_query(null,_query,_method);
RDebugUtils.currentLine=2162695;
 //BA.debugLineNum = 2162695;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=2162696;
 //BA.debugLineNum = 2162696;BA.debugLine="End Sub";
return "";
}
public String  _initialize(b4j.example.tredatasetting __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="tredatasetting";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
RDebugUtils.currentLine=1900544;
 //BA.debugLineNum = 1900544;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=1900545;
 //BA.debugLineNum = 1900545;BA.debugLine="clsid = \"tre_datasetting\"";
__ref._clsid = "tre_datasetting";
RDebugUtils.currentLine=1900546;
 //BA.debugLineNum = 1900546;BA.debugLine="todb.Initialize";
__ref._todb._initialize(null,ba);
RDebugUtils.currentLine=1900547;
 //BA.debugLineNum = 1900547;BA.debugLine="todbdms.Initialize";
__ref._todbdms._initialize(null,ba);
RDebugUtils.currentLine=1900548;
 //BA.debugLineNum = 1900548;BA.debugLine="End Sub";
return "";
}
}