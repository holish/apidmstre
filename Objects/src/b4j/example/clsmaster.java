package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.debug.*;

public class clsmaster extends Object{
public static clsmaster mostCurrent = new clsmaster();

public static BA ba;
static {
		ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.clsmaster", null);
		ba.loadHtSubs(clsmaster.class);
        if (ba.getClass().getName().endsWith("ShellBA")) {
			
			ba.raiseEvent2(null, true, "SHELL", false);
			ba.raiseEvent2(null, true, "CREATE", true, "b4j.example.clsmaster", ba);
		}
	}
    public static Class<?> getObject() {
		return clsmaster.class;
	}

 
public static anywheresoftware.b4a.keywords.Common __c = null;
public static b4j.example.clsdb _koneksidb = null;
public static b4j.example.main _main = null;
public static b4j.example.exequery _exequery = null;
public static b4j.example.clsmanagement _clsmanagement = null;
public static b4j.example.modgeneratereturn _modgeneratereturn = null;
public static String  _create_bank(String _namabank,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "create_bank"))
	return (String) Debug.delegate(ba, "create_bank", new Object[] {_namabank,_method});
RDebugUtils.currentLine=4063232;
 //BA.debugLineNum = 4063232;BA.debugLine="public Sub create_bank(namabank As String, method";
RDebugUtils.currentLine=4063233;
 //BA.debugLineNum = 4063233;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createbank('"+_namabank+"'))",_method);
RDebugUtils.currentLine=4063234;
 //BA.debugLineNum = 4063234;BA.debugLine="End Sub";
return "";
}
public static String  _exe_query(String _query,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "exe_query"))
	return (String) Debug.delegate(ba, "exe_query", new Object[] {_query,_method});
String _hasil = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
anywheresoftware.b4a.objects.collections.List _lst = null;
anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator _js = null;
RDebugUtils.currentLine=3997696;
 //BA.debugLineNum = 3997696;BA.debugLine="Private Sub exe_query(query As String, method As S";
RDebugUtils.currentLine=3997697;
 //BA.debugLineNum = 3997697;BA.debugLine="Dim hasil As String";
_hasil = "";
RDebugUtils.currentLine=3997698;
 //BA.debugLineNum = 3997698;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=3997699;
 //BA.debugLineNum = 3997699;BA.debugLine="Dim lst As List";
_lst = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=3997700;
 //BA.debugLineNum = 3997700;BA.debugLine="Dim js As JSONGenerator";
_js = new anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator();
RDebugUtils.currentLine=3997702;
 //BA.debugLineNum = 3997702;BA.debugLine="lst.Initialize";
_lst.Initialize();
RDebugUtils.currentLine=3997703;
 //BA.debugLineNum = 3997703;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=3997704;
 //BA.debugLineNum = 3997704;BA.debugLine="koneksidb.Initialize";
_koneksidb._initialize(null,ba);
RDebugUtils.currentLine=3997706;
 //BA.debugLineNum = 3997706;BA.debugLine="mp = koneksidb.SqlSelectMap(query, method)";
_mp = _koneksidb._sqlselectmap(null,_query,_method);
RDebugUtils.currentLine=3997707;
 //BA.debugLineNum = 3997707;BA.debugLine="js.Initialize(mp)";
_js.Initialize(_mp);
RDebugUtils.currentLine=3997708;
 //BA.debugLineNum = 3997708;BA.debugLine="hasil = js.ToString";
_hasil = _js.ToString();
RDebugUtils.currentLine=3997709;
 //BA.debugLineNum = 3997709;BA.debugLine="Return hasil";
if (true) return _hasil;
RDebugUtils.currentLine=3997710;
 //BA.debugLineNum = 3997710;BA.debugLine="End Sub";
return "";
}
public static String  _create_jabatan(String _p1,String _p2,String _p3,String _p4,String _p5,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "create_jabatan"))
	return (String) Debug.delegate(ba, "create_jabatan", new Object[] {_p1,_p2,_p3,_p4,_p5,_method});
RDebugUtils.currentLine=6160384;
 //BA.debugLineNum = 6160384;BA.debugLine="public Sub create_jabatan(p1 As String, p2 As Stri";
RDebugUtils.currentLine=6160385;
 //BA.debugLineNum = 6160385;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createjabatan('"+_p1+"','"+_p2+"','"+_p3+"','"+_p4+"','"+_p5+"'))",_method);
RDebugUtils.currentLine=6160386;
 //BA.debugLineNum = 6160386;BA.debugLine="End Sub";
return "";
}
public static String  _create_jenisproyek(String _p1,String _p2,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "create_jenisproyek"))
	return (String) Debug.delegate(ba, "create_jenisproyek", new Object[] {_p1,_p2,_method});
RDebugUtils.currentLine=5373952;
 //BA.debugLineNum = 5373952;BA.debugLine="public Sub create_jenisproyek(p1 As String, p2 As";
RDebugUtils.currentLine=5373953;
 //BA.debugLineNum = 5373953;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createjenisproyek('"+_p1+"','"+_p2+"'))",_method);
RDebugUtils.currentLine=5373954;
 //BA.debugLineNum = 5373954;BA.debugLine="End Sub";
return "";
}
public static String  _create_proyek(String _p1,String _p2,String _p3,String _p4,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "create_proyek"))
	return (String) Debug.delegate(ba, "create_proyek", new Object[] {_p1,_p2,_p3,_p4,_method});
String _query = "";
RDebugUtils.currentLine=5636096;
 //BA.debugLineNum = 5636096;BA.debugLine="public Sub create_proyek(p1 As String, p2 As Strin";
RDebugUtils.currentLine=5636097;
 //BA.debugLineNum = 5636097;BA.debugLine="Dim query	As String = \"select * from table(PKG_DM";
_query = "select * from table(PKG_DMS_MASTER_TRE.fn_createproyek('"+_p1+"','"+_p2+"', '"+_p3+"', '"+_p4+"'))";
RDebugUtils.currentLine=5636098;
 //BA.debugLineNum = 5636098;BA.debugLine="Return exe_query(query, method)";
if (true) return _exe_query(_query,_method);
RDebugUtils.currentLine=5636099;
 //BA.debugLineNum = 5636099;BA.debugLine="End Sub";
return "";
}
public static String  _create_sumberdana(String _p1,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "create_sumberdana"))
	return (String) Debug.delegate(ba, "create_sumberdana", new Object[] {_p1,_method});
RDebugUtils.currentLine=5111808;
 //BA.debugLineNum = 5111808;BA.debugLine="public Sub create_sumberdana(p1 As String,  method";
RDebugUtils.currentLine=5111809;
 //BA.debugLineNum = 5111809;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createsumberdana('"+_p1+"'))",_method);
RDebugUtils.currentLine=5111810;
 //BA.debugLineNum = 5111810;BA.debugLine="End Sub";
return "";
}
public static String  _create_termin(String _p1,String _p2,String _p3,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "create_termin"))
	return (String) Debug.delegate(ba, "create_termin", new Object[] {_p1,_p2,_p3,_method});
RDebugUtils.currentLine=4849664;
 //BA.debugLineNum = 4849664;BA.debugLine="public Sub create_termin(p1 As String, p2 As Strin";
RDebugUtils.currentLine=4849665;
 //BA.debugLineNum = 4849665;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createtermin('"+_p1+"','"+_p2+"','"+_p3+"'))",_method);
RDebugUtils.currentLine=4849666;
 //BA.debugLineNum = 4849666;BA.debugLine="End Sub";
return "";
}
public static String  _create_tipebayar(String _p1,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "create_tipebayar"))
	return (String) Debug.delegate(ba, "create_tipebayar", new Object[] {_p1,_method});
RDebugUtils.currentLine=4587520;
 //BA.debugLineNum = 4587520;BA.debugLine="public Sub create_tipebayar(p1 As String,  method";
RDebugUtils.currentLine=4587521;
 //BA.debugLineNum = 4587521;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createtipebayar('"+_p1+"'))",_method);
RDebugUtils.currentLine=4587522;
 //BA.debugLineNum = 4587522;BA.debugLine="End Sub";
return "";
}
public static String  _create_unit(String _p1,String _p2,String _p3,String _p4,String _p5,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "create_unit"))
	return (String) Debug.delegate(ba, "create_unit", new Object[] {_p1,_p2,_p3,_p4,_p5,_method});
RDebugUtils.currentLine=5898240;
 //BA.debugLineNum = 5898240;BA.debugLine="public Sub create_unit(p1 As String, p2 As String,";
RDebugUtils.currentLine=5898241;
 //BA.debugLineNum = 5898241;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createunit('"+_p1+"','"+_p2+"','"+_p3+"','"+_p4+"','"+_p5+"'))",_method);
RDebugUtils.currentLine=5898242;
 //BA.debugLineNum = 5898242;BA.debugLine="End Sub";
return "";
}
public static String  _create_vendor(String _p1,String _p2,String _p3,String _p4,String _p5,String _p6,String _p7,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "create_vendor"))
	return (String) Debug.delegate(ba, "create_vendor", new Object[] {_p1,_p2,_p3,_p4,_p5,_p6,_p7,_method});
RDebugUtils.currentLine=4325376;
 //BA.debugLineNum = 4325376;BA.debugLine="public Sub create_vendor(p1 As String,p2 As String";
RDebugUtils.currentLine=4325377;
 //BA.debugLineNum = 4325377;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createvendor('"+_p1+"','"+_p2+"','"+_p3+"','"+_p4+"','"+_p5+"','"+_p6+"','"+_p7+"'))",_method);
RDebugUtils.currentLine=4325378;
 //BA.debugLineNum = 4325378;BA.debugLine="End Sub";
return "";
}
public static String  _delete_bank(String _id,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "delete_bank"))
	return (String) Debug.delegate(ba, "delete_bank", new Object[] {_id,_method});
RDebugUtils.currentLine=4259840;
 //BA.debugLineNum = 4259840;BA.debugLine="Public Sub delete_bank(id As String, method As Str";
RDebugUtils.currentLine=4259841;
 //BA.debugLineNum = 4259841;BA.debugLine="Return exe_query($\" select * from table(PKG_DMS_M";
if (true) return _exe_query((" select * from table(PKG_DMS_MASTER_TRE.fn_deletebank('"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_id))+"')) "),_method);
RDebugUtils.currentLine=4259842;
 //BA.debugLineNum = 4259842;BA.debugLine="End Sub";
return "";
}
public static String  _delete_jabatan(String _kode,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "delete_jabatan"))
	return (String) Debug.delegate(ba, "delete_jabatan", new Object[] {_kode,_method});
RDebugUtils.currentLine=6356992;
 //BA.debugLineNum = 6356992;BA.debugLine="Public Sub delete_jabatan(kode As String, method A";
RDebugUtils.currentLine=6356993;
 //BA.debugLineNum = 6356993;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletejabatan('"+_kode+"'))",_method);
RDebugUtils.currentLine=6356994;
 //BA.debugLineNum = 6356994;BA.debugLine="End Sub";
return "";
}
public static String  _delete_jenisproyek(String _kode,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "delete_jenisproyek"))
	return (String) Debug.delegate(ba, "delete_jenisproyek", new Object[] {_kode,_method});
RDebugUtils.currentLine=5570560;
 //BA.debugLineNum = 5570560;BA.debugLine="Public Sub delete_jenisproyek(kode As String, meth";
RDebugUtils.currentLine=5570561;
 //BA.debugLineNum = 5570561;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletejenisproyek('"+_kode+"'))",_method);
RDebugUtils.currentLine=5570562;
 //BA.debugLineNum = 5570562;BA.debugLine="End Sub";
return "";
}
public static String  _delete_proyek(String _kode,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "delete_proyek"))
	return (String) Debug.delegate(ba, "delete_proyek", new Object[] {_kode,_method});
RDebugUtils.currentLine=5832704;
 //BA.debugLineNum = 5832704;BA.debugLine="Public Sub delete_proyek(kode As String, method As";
RDebugUtils.currentLine=5832705;
 //BA.debugLineNum = 5832705;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deleteproyek('"+_kode+"'))",_method);
RDebugUtils.currentLine=5832706;
 //BA.debugLineNum = 5832706;BA.debugLine="End Sub";
return "";
}
public static String  _delete_sumberdana(String _kode,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "delete_sumberdana"))
	return (String) Debug.delegate(ba, "delete_sumberdana", new Object[] {_kode,_method});
RDebugUtils.currentLine=5308416;
 //BA.debugLineNum = 5308416;BA.debugLine="Public Sub delete_sumberdana(kode As String, metho";
RDebugUtils.currentLine=5308417;
 //BA.debugLineNum = 5308417;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletesumberdana('"+_kode+"'))",_method);
RDebugUtils.currentLine=5308418;
 //BA.debugLineNum = 5308418;BA.debugLine="End Sub";
return "";
}
public static String  _delete_termin(String _kode,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "delete_termin"))
	return (String) Debug.delegate(ba, "delete_termin", new Object[] {_kode,_method});
RDebugUtils.currentLine=5046272;
 //BA.debugLineNum = 5046272;BA.debugLine="Public Sub delete_termin(kode As String, method As";
RDebugUtils.currentLine=5046273;
 //BA.debugLineNum = 5046273;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletetipebayar('"+_kode+"'))",_method);
RDebugUtils.currentLine=5046274;
 //BA.debugLineNum = 5046274;BA.debugLine="End Sub";
return "";
}
public static String  _delete_tipebayar(String _kode,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "delete_tipebayar"))
	return (String) Debug.delegate(ba, "delete_tipebayar", new Object[] {_kode,_method});
RDebugUtils.currentLine=4784128;
 //BA.debugLineNum = 4784128;BA.debugLine="Public Sub delete_tipebayar(kode As String, method";
RDebugUtils.currentLine=4784129;
 //BA.debugLineNum = 4784129;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletetipebayar('"+_kode+"'))",_method);
RDebugUtils.currentLine=4784130;
 //BA.debugLineNum = 4784130;BA.debugLine="End Sub";
return "";
}
public static String  _delete_unit(String _kode,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "delete_unit"))
	return (String) Debug.delegate(ba, "delete_unit", new Object[] {_kode,_method});
RDebugUtils.currentLine=6094848;
 //BA.debugLineNum = 6094848;BA.debugLine="Public Sub delete_unit(kode As String, method As S";
RDebugUtils.currentLine=6094849;
 //BA.debugLineNum = 6094849;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deleteunit('"+_kode+"'))",_method);
RDebugUtils.currentLine=6094850;
 //BA.debugLineNum = 6094850;BA.debugLine="End Sub";
return "";
}
public static String  _delete_vendor(String _kode,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "delete_vendor"))
	return (String) Debug.delegate(ba, "delete_vendor", new Object[] {_kode,_method});
RDebugUtils.currentLine=4521984;
 //BA.debugLineNum = 4521984;BA.debugLine="Public Sub delete_vendor(kode As String, method As";
RDebugUtils.currentLine=4521985;
 //BA.debugLineNum = 4521985;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletevendor('"+_kode+"'))",_method);
RDebugUtils.currentLine=4521986;
 //BA.debugLineNum = 4521986;BA.debugLine="End Sub";
return "";
}
public static String  _read_bank(String _kriteria,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "read_bank"))
	return (String) Debug.delegate(ba, "read_bank", new Object[] {_kriteria,_method});
RDebugUtils.currentLine=4128768;
 //BA.debugLineNum = 4128768;BA.debugLine="Public Sub read_bank(kriteria As String, method As";
RDebugUtils.currentLine=4128769;
 //BA.debugLineNum = 4128769;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
if ((_kriteria).equals("")) { 
_kriteria = "-";};
RDebugUtils.currentLine=4128770;
 //BA.debugLineNum = 4128770;BA.debugLine="Return exe_query(\"select f1 id_bank, f2 nama_bank";
if (true) return _exe_query("select f1 id_bank, f2 nama_bank from table(PKG_DMS_MASTER_TRE.fn_readbank('"+_kriteria+"'))",_method);
RDebugUtils.currentLine=4128771;
 //BA.debugLineNum = 4128771;BA.debugLine="End Sub";
return "";
}
public static String  _read_jabatan(String _kriteria,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "read_jabatan"))
	return (String) Debug.delegate(ba, "read_jabatan", new Object[] {_kriteria,_method});
RDebugUtils.currentLine=6225920;
 //BA.debugLineNum = 6225920;BA.debugLine="Public Sub read_jabatan(kriteria As String, method";
RDebugUtils.currentLine=6225921;
 //BA.debugLineNum = 6225921;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
if ((_kriteria).equals("")) { 
_kriteria = "-";};
RDebugUtils.currentLine=6225922;
 //BA.debugLineNum = 6225922;BA.debugLine="Return exe_query(\"select f1 id_jabatan,f2 nama_ja";
if (true) return _exe_query("select f1 id_jabatan,f2 nama_jabatan,f3 urut_flow from table(PKG_DMS_MASTER_TRE.fn_readjabatan('"+_kriteria+"'))",_method);
RDebugUtils.currentLine=6225923;
 //BA.debugLineNum = 6225923;BA.debugLine="End Sub";
return "";
}
public static String  _read_jenisproyek(String _kriteria,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "read_jenisproyek"))
	return (String) Debug.delegate(ba, "read_jenisproyek", new Object[] {_kriteria,_method});
RDebugUtils.currentLine=5439488;
 //BA.debugLineNum = 5439488;BA.debugLine="Public Sub read_jenisproyek(kriteria As String, me";
RDebugUtils.currentLine=5439489;
 //BA.debugLineNum = 5439489;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
if ((_kriteria).equals("")) { 
_kriteria = "-";};
RDebugUtils.currentLine=5439490;
 //BA.debugLineNum = 5439490;BA.debugLine="Return exe_query(\"select f1 id_jenis_proyek, f2 n";
if (true) return _exe_query("select f1 id_jenis_proyek, f2 nama_jenis_proyek, f3 kode_jenis_proyek from table(PKG_DMS_MASTER_TRE.fn_readjenisproyek('"+_kriteria+"'))",_method);
RDebugUtils.currentLine=5439491;
 //BA.debugLineNum = 5439491;BA.debugLine="End Sub";
return "";
}
public static String  _read_proyek(String _kriteria,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "read_proyek"))
	return (String) Debug.delegate(ba, "read_proyek", new Object[] {_kriteria,_method});
RDebugUtils.currentLine=5701632;
 //BA.debugLineNum = 5701632;BA.debugLine="Public Sub read_proyek(kriteria As String, method";
RDebugUtils.currentLine=5701633;
 //BA.debugLineNum = 5701633;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
if ((_kriteria).equals("")) { 
_kriteria = "-";};
RDebugUtils.currentLine=5701634;
 //BA.debugLineNum = 5701634;BA.debugLine="Return exe_query(\"select f1 id_jenis_proyek,f2 na";
if (true) return _exe_query("select f1 id_jenis_proyek,f2 nama_proyek,f3 kode_proyek,f4 id_unit,f5 id_proyek from table(PKG_DMS_MASTER_TRE.fn_readproyek('"+_kriteria+"'))",_method);
RDebugUtils.currentLine=5701635;
 //BA.debugLineNum = 5701635;BA.debugLine="End Sub";
return "";
}
public static String  _read_sumberdana(String _kriteria,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "read_sumberdana"))
	return (String) Debug.delegate(ba, "read_sumberdana", new Object[] {_kriteria,_method});
RDebugUtils.currentLine=5177344;
 //BA.debugLineNum = 5177344;BA.debugLine="Public Sub read_sumberdana(kriteria As String, met";
RDebugUtils.currentLine=5177345;
 //BA.debugLineNum = 5177345;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
if ((_kriteria).equals("")) { 
_kriteria = "-";};
RDebugUtils.currentLine=5177346;
 //BA.debugLineNum = 5177346;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_readsumberdana('"+_kriteria+"'))",_method);
RDebugUtils.currentLine=5177347;
 //BA.debugLineNum = 5177347;BA.debugLine="End Sub";
return "";
}
public static String  _read_termin(String _kriteria,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "read_termin"))
	return (String) Debug.delegate(ba, "read_termin", new Object[] {_kriteria,_method});
RDebugUtils.currentLine=4915200;
 //BA.debugLineNum = 4915200;BA.debugLine="Public Sub read_termin(kriteria As String, method";
RDebugUtils.currentLine=4915201;
 //BA.debugLineNum = 4915201;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
if ((_kriteria).equals("")) { 
_kriteria = "-";};
RDebugUtils.currentLine=4915202;
 //BA.debugLineNum = 4915202;BA.debugLine="Return exe_query(\"select f1 id_termin,f2 nama_ter";
if (true) return _exe_query("select f1 id_termin,f2 nama_termin,f3 kode_termin,f4 id_tipe_bayar from table(PKG_DMS_MASTER_TRE.fn_readtermin('"+_kriteria+"'))",_method);
RDebugUtils.currentLine=4915203;
 //BA.debugLineNum = 4915203;BA.debugLine="End Sub";
return "";
}
public static String  _read_tipebayar(String _kriteria,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "read_tipebayar"))
	return (String) Debug.delegate(ba, "read_tipebayar", new Object[] {_kriteria,_method});
RDebugUtils.currentLine=4653056;
 //BA.debugLineNum = 4653056;BA.debugLine="Public Sub read_tipebayar(kriteria As String, meth";
RDebugUtils.currentLine=4653057;
 //BA.debugLineNum = 4653057;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
if ((_kriteria).equals("")) { 
_kriteria = "-";};
RDebugUtils.currentLine=4653058;
 //BA.debugLineNum = 4653058;BA.debugLine="Return exe_query(\"select f1 id_tipe_bayar, f2 nam";
if (true) return _exe_query("select f1 id_tipe_bayar, f2 nama_tipe_bayar from table(PKG_DMS_MASTER_TRE.fn_readtipebayar('"+_kriteria+"'))",_method);
RDebugUtils.currentLine=4653059;
 //BA.debugLineNum = 4653059;BA.debugLine="End Sub";
return "";
}
public static String  _read_unit(String _kriteria,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "read_unit"))
	return (String) Debug.delegate(ba, "read_unit", new Object[] {_kriteria,_method});
RDebugUtils.currentLine=5963776;
 //BA.debugLineNum = 5963776;BA.debugLine="Public Sub read_unit(kriteria As String, method As";
RDebugUtils.currentLine=5963777;
 //BA.debugLineNum = 5963777;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
if ((_kriteria).equals("")) { 
_kriteria = "-";};
RDebugUtils.currentLine=5963778;
 //BA.debugLineNum = 5963778;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_readunit('"+_kriteria+"'))",_method);
RDebugUtils.currentLine=5963779;
 //BA.debugLineNum = 5963779;BA.debugLine="End Sub";
return "";
}
public static String  _read_vendor(String _kriteria,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "read_vendor"))
	return (String) Debug.delegate(ba, "read_vendor", new Object[] {_kriteria,_method});
RDebugUtils.currentLine=4390912;
 //BA.debugLineNum = 4390912;BA.debugLine="Public Sub read_vendor(kriteria As String, method";
RDebugUtils.currentLine=4390913;
 //BA.debugLineNum = 4390913;BA.debugLine="If kriteria = \"\" Then kriteria = \"-\"";
if ((_kriteria).equals("")) { 
_kriteria = "-";};
RDebugUtils.currentLine=4390914;
 //BA.debugLineNum = 4390914;BA.debugLine="Return exe_query($\"select f1 kode_vendor, f2 nama";
if (true) return _exe_query(("select f1 kode_vendor, f2 nama_vendor, f3 sebutan_pejabat_vendor, f4 nama_vendor1, \n"+"	f5 nama_vendor2, f6 nama_vendor3, f7 nama_vendor4, f8 nama_pejabat_vendor, f9 username, f10 password \n"+"	from table(PKG_DMS_MASTER_TRE.fn_readvendor('"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_kriteria))+"')) "),_method);
RDebugUtils.currentLine=4390917;
 //BA.debugLineNum = 4390917;BA.debugLine="End Sub";
return "";
}
public static String  _update_bank(String _id,String _nama,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "update_bank"))
	return (String) Debug.delegate(ba, "update_bank", new Object[] {_id,_nama,_method});
RDebugUtils.currentLine=4194304;
 //BA.debugLineNum = 4194304;BA.debugLine="Public Sub update_bank(id As String, nama As Strin";
RDebugUtils.currentLine=4194305;
 //BA.debugLineNum = 4194305;BA.debugLine="Return exe_query($\" select * from table(PKG_DMS_M";
if (true) return _exe_query((" select * from table(PKG_DMS_MASTER_TRE.fn_editbank('"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_id))+"','"+anywheresoftware.b4a.keywords.Common.SmartStringFormatter("",(Object)(_nama))+"')) "),_method);
RDebugUtils.currentLine=4194306;
 //BA.debugLineNum = 4194306;BA.debugLine="End Sub";
return "";
}
public static String  _update_jabatan(String _id,String _p1,String _p2,String _p3,String _p4,String _p5,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "update_jabatan"))
	return (String) Debug.delegate(ba, "update_jabatan", new Object[] {_id,_p1,_p2,_p3,_p4,_p5,_method});
RDebugUtils.currentLine=6291456;
 //BA.debugLineNum = 6291456;BA.debugLine="Public Sub update_jabatan(id As String, p1 As Stri";
RDebugUtils.currentLine=6291457;
 //BA.debugLineNum = 6291457;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editjabatan('"+_id+"','"+_p1+"','"+_p2+"','"+_p3+"','"+_p4+"','"+_p5+"'))",_method);
RDebugUtils.currentLine=6291458;
 //BA.debugLineNum = 6291458;BA.debugLine="End Sub";
return "";
}
public static String  _update_jenisproyek(String _kode,String _p1,String _p2,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "update_jenisproyek"))
	return (String) Debug.delegate(ba, "update_jenisproyek", new Object[] {_kode,_p1,_p2,_method});
RDebugUtils.currentLine=5505024;
 //BA.debugLineNum = 5505024;BA.debugLine="Public Sub update_jenisproyek(kode As String, p1 A";
RDebugUtils.currentLine=5505025;
 //BA.debugLineNum = 5505025;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editjenisproyek('"+_kode+"','"+_p1+"','"+_p2+"'))",_method);
RDebugUtils.currentLine=5505026;
 //BA.debugLineNum = 5505026;BA.debugLine="End Sub";
return "";
}
public static String  _update_proyek(String _id,String _p1,String _p2,String _p3,String _p4,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "update_proyek"))
	return (String) Debug.delegate(ba, "update_proyek", new Object[] {_id,_p1,_p2,_p3,_p4,_method});
RDebugUtils.currentLine=5767168;
 //BA.debugLineNum = 5767168;BA.debugLine="Public Sub update_proyek(id As String, p1 As Strin";
RDebugUtils.currentLine=5767169;
 //BA.debugLineNum = 5767169;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editproyek('"+_id+"','"+_p1+"','"+_p2+"','"+_p3+"','"+_p4+"'))",_method);
RDebugUtils.currentLine=5767170;
 //BA.debugLineNum = 5767170;BA.debugLine="End Sub";
return "";
}
public static String  _update_sumberdana(String _kode,String _p1,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "update_sumberdana"))
	return (String) Debug.delegate(ba, "update_sumberdana", new Object[] {_kode,_p1,_method});
RDebugUtils.currentLine=5242880;
 //BA.debugLineNum = 5242880;BA.debugLine="Public Sub update_sumberdana(kode As String, p1 As";
RDebugUtils.currentLine=5242881;
 //BA.debugLineNum = 5242881;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editsumberdana('"+_kode+"','"+_p1+"'))",_method);
RDebugUtils.currentLine=5242882;
 //BA.debugLineNum = 5242882;BA.debugLine="End Sub";
return "";
}
public static String  _update_termin(String _kode,String _p1,String _p2,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "update_termin"))
	return (String) Debug.delegate(ba, "update_termin", new Object[] {_kode,_p1,_p2,_method});
RDebugUtils.currentLine=4980736;
 //BA.debugLineNum = 4980736;BA.debugLine="Public Sub update_termin(kode As String, p1 As Str";
RDebugUtils.currentLine=4980737;
 //BA.debugLineNum = 4980737;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_edittipebayar('"+_kode+"','"+_p1+"','"+_p2+"'))",_method);
RDebugUtils.currentLine=4980738;
 //BA.debugLineNum = 4980738;BA.debugLine="End Sub";
return "";
}
public static String  _update_tipebayar(String _kode,String _p1,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "update_tipebayar"))
	return (String) Debug.delegate(ba, "update_tipebayar", new Object[] {_kode,_p1,_method});
RDebugUtils.currentLine=4718592;
 //BA.debugLineNum = 4718592;BA.debugLine="Public Sub update_tipebayar(kode As String, p1 As";
RDebugUtils.currentLine=4718593;
 //BA.debugLineNum = 4718593;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_edittipebayar('"+_kode+"','"+_p1+"'))",_method);
RDebugUtils.currentLine=4718594;
 //BA.debugLineNum = 4718594;BA.debugLine="End Sub";
return "";
}
public static String  _update_unit(String _id,String _p1,String _p2,String _p3,String _p4,String _p5,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "update_unit"))
	return (String) Debug.delegate(ba, "update_unit", new Object[] {_id,_p1,_p2,_p3,_p4,_p5,_method});
RDebugUtils.currentLine=6029312;
 //BA.debugLineNum = 6029312;BA.debugLine="Public Sub update_unit(id As String, p1 As String,";
RDebugUtils.currentLine=6029313;
 //BA.debugLineNum = 6029313;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editunit('"+_id+"','"+_p1+"','"+_p2+"','"+_p3+"','"+_p4+"','"+_p5+"'))",_method);
RDebugUtils.currentLine=6029314;
 //BA.debugLineNum = 6029314;BA.debugLine="End Sub";
return "";
}
public static String  _update_vendor(String _kode,String _p1,String _p2,String _p3,String _p4,String _p5,String _p6,String _p7,String _method) throws Exception{
RDebugUtils.currentModule="clsmaster";
if (Debug.shouldDelegate(ba, "update_vendor"))
	return (String) Debug.delegate(ba, "update_vendor", new Object[] {_kode,_p1,_p2,_p3,_p4,_p5,_p6,_p7,_method});
RDebugUtils.currentLine=4456448;
 //BA.debugLineNum = 4456448;BA.debugLine="Public Sub update_vendor(kode As String, p1 As Str";
RDebugUtils.currentLine=4456449;
 //BA.debugLineNum = 4456449;BA.debugLine="Return exe_query(\"select * from table(PKG_DMS_MAS";
if (true) return _exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editvendor('"+_kode+"','"+_p1+"','"+_p2+"','"+_p3+"','"+_p4+"','"+_p5+"','"+_p6+"','"+_p7+"'))",_method);
RDebugUtils.currentLine=4456450;
 //BA.debugLineNum = 4456450;BA.debugLine="End Sub";
return "";
}
}