package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.debug.*;

public class modgeneratereturn extends Object{
public static modgeneratereturn mostCurrent = new modgeneratereturn();

public static BA ba;
static {
		ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.modgeneratereturn", null);
		ba.loadHtSubs(modgeneratereturn.class);
        if (ba.getClass().getName().endsWith("ShellBA")) {
			
			ba.raiseEvent2(null, true, "SHELL", false);
			ba.raiseEvent2(null, true, "CREATE", true, "b4j.example.modgeneratereturn", ba);
		}
	}
    public static Class<?> getObject() {
		return modgeneratereturn.class;
	}

 
public static anywheresoftware.b4a.keywords.Common __c = null;
public static String _label_rejected = "";
public static String _kode_rejected_class = "";
public static String _kode_succes = "";
public static String _kode_gagal_koneksi_db = "";
public static String _kode_gagal_execute_query = "";
public static String _kode_gagal_parsing = "";
public static String _namauser = "";
public static b4j.example.main _main = null;
public static b4j.example.exequery _exequery = null;
public static b4j.example.clsmanagement _clsmanagement = null;
public static b4j.example.clsmaster _clsmaster = null;
public static String  _return_json(String _pesan,String _kode,String _method) throws Exception{
RDebugUtils.currentModule="modgeneratereturn";
if (Debug.shouldDelegate(ba, "return_json"))
	return (String) Debug.delegate(ba, "return_json", new Object[] {_pesan,_kode,_method});
anywheresoftware.b4a.objects.collections.Map _ret = null;
String _strret = "";
anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator _js = null;
RDebugUtils.currentLine=7143424;
 //BA.debugLineNum = 7143424;BA.debugLine="Public Sub return_json(pesan As String, kode As St";
RDebugUtils.currentLine=7143425;
 //BA.debugLineNum = 7143425;BA.debugLine="Dim ret As Map";
_ret = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=7143426;
 //BA.debugLineNum = 7143426;BA.debugLine="Dim strRet As String";
_strret = "";
RDebugUtils.currentLine=7143427;
 //BA.debugLineNum = 7143427;BA.debugLine="Dim js As JSONGenerator";
_js = new anywheresoftware.b4j.objects.collections.JSONParser.JSONGenerator();
RDebugUtils.currentLine=7143429;
 //BA.debugLineNum = 7143429;BA.debugLine="ret = CreateMap(\"response\": kode, \"method\" :metho";
_ret = anywheresoftware.b4a.keywords.Common.createMap(new Object[] {(Object)("response"),(Object)(_kode),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(_pesan),(Object)("data"),(Object)(new Object[]{})});
RDebugUtils.currentLine=7143430;
 //BA.debugLineNum = 7143430;BA.debugLine="js.Initialize(ret)";
_js.Initialize(_ret);
RDebugUtils.currentLine=7143431;
 //BA.debugLineNum = 7143431;BA.debugLine="strRet = js.ToString";
_strret = _js.ToString();
RDebugUtils.currentLine=7143432;
 //BA.debugLineNum = 7143432;BA.debugLine="Return strRet";
if (true) return _strret;
RDebugUtils.currentLine=7143433;
 //BA.debugLineNum = 7143433;BA.debugLine="End Sub";
return "";
}
}