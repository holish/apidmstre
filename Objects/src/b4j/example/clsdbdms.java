package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.debug.*;

public class clsdbdms extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    public static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.clsdbdms", this);
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            ba.htSubs = htSubs;
             
        }
        if (BA.isShellModeRuntimeCheck(ba))
                this.getClass().getMethod("_class_globals", b4j.example.clsdbdms.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public String _serverdb = "";
public String _pwd = "";
public String _user = "";
public String _std = "";
public anywheresoftware.b4a.objects.StringUtils _en = null;
public b4j.example.main _main = null;
public b4j.example.exequery _exequery = null;
public b4j.example.clsmanagement _clsmanagement = null;
public b4j.example.clsmaster _clsmaster = null;
public b4j.example.modgeneratereturn _modgeneratereturn = null;
public String  _initialize(b4j.example.clsdbdms __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="clsdbdms";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
RDebugUtils.currentLine=6684672;
 //BA.debugLineNum = 6684672;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=6684673;
 //BA.debugLineNum = 6684673;BA.debugLine="serverdb = Main.ipdbdms & \"/\" & Main.tnsdms";
__ref._serverdb = _main._ipdbdms+"/"+_main._tnsdms;
RDebugUtils.currentLine=6684674;
 //BA.debugLineNum = 6684674;BA.debugLine="user = Main.userdbdms";
__ref._user = _main._userdbdms;
RDebugUtils.currentLine=6684675;
 //BA.debugLineNum = 6684675;BA.debugLine="pwd = Main.pwddbdms";
__ref._pwd = _main._pwddbdms;
RDebugUtils.currentLine=6684676;
 //BA.debugLineNum = 6684676;BA.debugLine="End Sub";
return "";
}
public anywheresoftware.b4a.objects.collections.Map  _sqlselectmap(b4j.example.clsdbdms __ref,String _sql,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="clsdbdms";
if (Debug.shouldDelegate(ba, "sqlselectmap"))
	return (anywheresoftware.b4a.objects.collections.Map) Debug.delegate(ba, "sqlselectmap", new Object[] {_sql,_method});
anywheresoftware.b4j.objects.SQL _koneksi = null;
anywheresoftware.b4j.objects.SQL.ResultSetWrapper _rs = null;
int _i = 0;
int _j = 0;
anywheresoftware.b4a.objects.collections.Map _mp = null;
String _field = "";
String _error = "";
anywheresoftware.b4a.objects.collections.List _lret = null;
String _val = "";
RDebugUtils.currentLine=6750208;
 //BA.debugLineNum = 6750208;BA.debugLine="Sub SqlSelectMap(Sql As String, method As String)";
RDebugUtils.currentLine=6750209;
 //BA.debugLineNum = 6750209;BA.debugLine="Dim koneksi As SQL";
_koneksi = new anywheresoftware.b4j.objects.SQL();
RDebugUtils.currentLine=6750210;
 //BA.debugLineNum = 6750210;BA.debugLine="Dim rs As ResultSet";
_rs = new anywheresoftware.b4j.objects.SQL.ResultSetWrapper();
RDebugUtils.currentLine=6750211;
 //BA.debugLineNum = 6750211;BA.debugLine="Dim i, j As Int";
_i = 0;
_j = 0;
RDebugUtils.currentLine=6750212;
 //BA.debugLineNum = 6750212;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=6750213;
 //BA.debugLineNum = 6750213;BA.debugLine="Dim field, error As String";
_field = "";
_error = "";
RDebugUtils.currentLine=6750214;
 //BA.debugLineNum = 6750214;BA.debugLine="Dim lRet As List";
_lret = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=6750215;
 //BA.debugLineNum = 6750215;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=6750216;
 //BA.debugLineNum = 6750216;BA.debugLine="lRet.Initialize";
_lret.Initialize();
RDebugUtils.currentLine=6750219;
 //BA.debugLineNum = 6750219;BA.debugLine="Try";
try {RDebugUtils.currentLine=6750220;
 //BA.debugLineNum = 6750220;BA.debugLine="If koneksi.IsInitialized = True Then";
if (_koneksi.IsInitialized()==__c.True) { 
RDebugUtils.currentLine=6750221;
 //BA.debugLineNum = 6750221;BA.debugLine="koneksi.Close";
_koneksi.Close();
 };
RDebugUtils.currentLine=6750223;
 //BA.debugLineNum = 6750223;BA.debugLine="koneksi.Initialize2(std,\"jdbc:oracle:thin:@//\" &";
_koneksi.Initialize2(__ref._std,"jdbc:oracle:thin:@//"+__ref._serverdb,__ref._user,__ref._pwd);
 } 
       catch (Exception e15) {
			ba.setLastException(e15);RDebugUtils.currentLine=6750225;
 //BA.debugLineNum = 6750225;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
_mp = __c.createMap(new Object[] {(Object)("response"),(Object)(_modgeneratereturn._kode_gagal_koneksi_db),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(__ref._en.EncodeUrl(__c.LastException(ba).getMessage(),"UTF8")),(Object)("data"),(Object)(new Object[]{})});
RDebugUtils.currentLine=6750226;
 //BA.debugLineNum = 6750226;BA.debugLine="Main.appendSqlLog(\" \")";
_main._appendsqllog(" ");
RDebugUtils.currentLine=6750227;
 //BA.debugLineNum = 6750227;BA.debugLine="Main.appendSqlLog(Sql)";
_main._appendsqllog(_sql);
RDebugUtils.currentLine=6750228;
 //BA.debugLineNum = 6750228;BA.debugLine="Main.appendSqlLog(LastException.Message)";
_main._appendsqllog(__c.LastException(ba).getMessage());
RDebugUtils.currentLine=6750229;
 //BA.debugLineNum = 6750229;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=6750230;
 //BA.debugLineNum = 6750230;BA.debugLine="Return mp";
if (true) return _mp;
 };
RDebugUtils.currentLine=6750234;
 //BA.debugLineNum = 6750234;BA.debugLine="Try";
try {RDebugUtils.currentLine=6750235;
 //BA.debugLineNum = 6750235;BA.debugLine="rs = koneksi.ExecQuery(Sql.Replace(\"'null'\",Null";
_rs = _koneksi.ExecQuery(_sql.replace("'null'",BA.ObjectToString(__c.Null)));
 } 
       catch (Exception e25) {
			ba.setLastException(e25);RDebugUtils.currentLine=6750237;
 //BA.debugLineNum = 6750237;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
_mp = __c.createMap(new Object[] {(Object)("response"),(Object)(_modgeneratereturn._kode_gagal_execute_query),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(__ref._en.EncodeUrl(__c.LastException(ba).getMessage(),"UTF8")),(Object)("data"),(Object)(new Object[]{})});
RDebugUtils.currentLine=6750238;
 //BA.debugLineNum = 6750238;BA.debugLine="Main.appendSqlLog(\" \")";
_main._appendsqllog(" ");
RDebugUtils.currentLine=6750239;
 //BA.debugLineNum = 6750239;BA.debugLine="Main.appendSqlLog(Sql)";
_main._appendsqllog(_sql);
RDebugUtils.currentLine=6750240;
 //BA.debugLineNum = 6750240;BA.debugLine="Main.appendSqlLog(LastException.Message)";
_main._appendsqllog(__c.LastException(ba).getMessage());
RDebugUtils.currentLine=6750241;
 //BA.debugLineNum = 6750241;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=6750242;
 //BA.debugLineNum = 6750242;BA.debugLine="Return mp";
if (true) return _mp;
 };
RDebugUtils.currentLine=6750246;
 //BA.debugLineNum = 6750246;BA.debugLine="Try";
try {RDebugUtils.currentLine=6750247;
 //BA.debugLineNum = 6750247;BA.debugLine="i=0";
_i = (int) (0);
RDebugUtils.currentLine=6750248;
 //BA.debugLineNum = 6750248;BA.debugLine="j=0";
_j = (int) (0);
RDebugUtils.currentLine=6750249;
 //BA.debugLineNum = 6750249;BA.debugLine="Do While rs.NextRow";
while (_rs.NextRow()) {
RDebugUtils.currentLine=6750250;
 //BA.debugLineNum = 6750250;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=6750251;
 //BA.debugLineNum = 6750251;BA.debugLine="j = j+1";
_j = (int) (_j+1);
RDebugUtils.currentLine=6750252;
 //BA.debugLineNum = 6750252;BA.debugLine="For i=0 To rs.ColumnCount-1";
{
final int step38 = 1;
final int limit38 = (int) (_rs.getColumnCount()-1);
_i = (int) (0) ;
for (;(step38 > 0 && _i <= limit38) || (step38 < 0 && _i >= limit38) ;_i = ((int)(0 + _i + step38))  ) {
RDebugUtils.currentLine=6750253;
 //BA.debugLineNum = 6750253;BA.debugLine="Dim val As String";
_val = "";
RDebugUtils.currentLine=6750254;
 //BA.debugLineNum = 6750254;BA.debugLine="val = rs.GetString2(i)";
_val = _rs.GetString2(_i);
RDebugUtils.currentLine=6750255;
 //BA.debugLineNum = 6750255;BA.debugLine="If val <> Null Then";
if (_val!= null) { 
RDebugUtils.currentLine=6750256;
 //BA.debugLineNum = 6750256;BA.debugLine="val = en.EncodeUrl(val, \"UTF8\")";
_val = __ref._en.EncodeUrl(_val,"UTF8");
 }else {
RDebugUtils.currentLine=6750258;
 //BA.debugLineNum = 6750258;BA.debugLine="val = \"null\"";
_val = "null";
 };
RDebugUtils.currentLine=6750261;
 //BA.debugLineNum = 6750261;BA.debugLine="field = rs.GetColumnName(i).ToLowerCase";
_field = _rs.GetColumnName(_i).toLowerCase();
RDebugUtils.currentLine=6750262;
 //BA.debugLineNum = 6750262;BA.debugLine="If field = \"GETDATE(ENTRYTIME)\" Then";
if ((_field).equals("GETDATE(ENTRYTIME)")) { 
RDebugUtils.currentLine=6750263;
 //BA.debugLineNum = 6750263;BA.debugLine="field = \"DATE\"";
_field = "DATE";
 };
RDebugUtils.currentLine=6750265;
 //BA.debugLineNum = 6750265;BA.debugLine="mp.Put(field, val)";
_mp.Put((Object)(_field),(Object)(_val));
 }
};
RDebugUtils.currentLine=6750267;
 //BA.debugLineNum = 6750267;BA.debugLine="lRet.Add(mp)";
_lret.Add((Object)(_mp.getObject()));
RDebugUtils.currentLine=6750268;
 //BA.debugLineNum = 6750268;BA.debugLine="mp = Null";
_mp.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(__c.Null));
 }
;
 } 
       catch (Exception e56) {
			ba.setLastException(e56);RDebugUtils.currentLine=6750271;
 //BA.debugLineNum = 6750271;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
_mp = __c.createMap(new Object[] {(Object)("response"),(Object)(_modgeneratereturn._kode_gagal_parsing),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(__ref._en.EncodeUrl(__c.LastException(ba).getMessage(),"UTF8")),(Object)("data"),(Object)(new Object[]{})});
RDebugUtils.currentLine=6750272;
 //BA.debugLineNum = 6750272;BA.debugLine="Main.appendSqlLog(\" \")";
_main._appendsqllog(" ");
RDebugUtils.currentLine=6750273;
 //BA.debugLineNum = 6750273;BA.debugLine="Main.appendSqlLog(Sql)";
_main._appendsqllog(_sql);
RDebugUtils.currentLine=6750274;
 //BA.debugLineNum = 6750274;BA.debugLine="Main.appendSqlLog(LastException.Message)";
_main._appendsqllog(__c.LastException(ba).getMessage());
RDebugUtils.currentLine=6750275;
 //BA.debugLineNum = 6750275;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=6750276;
 //BA.debugLineNum = 6750276;BA.debugLine="Return mp";
if (true) return _mp;
 };
RDebugUtils.currentLine=6750278;
 //BA.debugLineNum = 6750278;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=6750279;
 //BA.debugLineNum = 6750279;BA.debugLine="mp = CreateMap(\"response\":modGenerateReturn.kode_";
_mp = __c.createMap(new Object[] {(Object)("response"),(Object)(_modgeneratereturn._kode_succes),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(""),(Object)("data"),(Object)(_lret.getObject())});
RDebugUtils.currentLine=6750280;
 //BA.debugLineNum = 6750280;BA.debugLine="Return mp";
if (true) return _mp;
RDebugUtils.currentLine=6750281;
 //BA.debugLineNum = 6750281;BA.debugLine="End Sub";
return null;
}
public String  _class_globals(b4j.example.clsdbdms __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="clsdbdms";
RDebugUtils.currentLine=6619136;
 //BA.debugLineNum = 6619136;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=6619137;
 //BA.debugLineNum = 6619137;BA.debugLine="Private serverdb, pwd, user As String";
_serverdb = "";
_pwd = "";
_user = "";
RDebugUtils.currentLine=6619138;
 //BA.debugLineNum = 6619138;BA.debugLine="Dim std As String=\"oracle.jdbc.driver.OracleDrive";
_std = "oracle.jdbc.driver.OracleDriver";
RDebugUtils.currentLine=6619139;
 //BA.debugLineNum = 6619139;BA.debugLine="Private en As StringUtils";
_en = new anywheresoftware.b4a.objects.StringUtils();
RDebugUtils.currentLine=6619140;
 //BA.debugLineNum = 6619140;BA.debugLine="End Sub";
return "";
}
}