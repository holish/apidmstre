package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.debug.*;

public class trenotiftas extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    public static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.trenotiftas", this);
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            ba.htSubs = htSubs;
             
        }
        if (BA.isShellModeRuntimeCheck(ba))
                this.getClass().getMethod("_class_globals", b4j.example.trenotiftas.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public String _clsid = "";
public b4j.example.clsdb _todb = null;
public b4j.example.clsdbdms _todbdms = null;
public b4j.example.main _main = null;
public b4j.example.exequery _exequery = null;
public b4j.example.clsmanagement _clsmanagement = null;
public b4j.example.clsmaster _clsmaster = null;
public b4j.example.modgeneratereturn _modgeneratereturn = null;
public String  _class_globals(b4j.example.trenotiftas __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="trenotiftas";
RDebugUtils.currentLine=6815744;
 //BA.debugLineNum = 6815744;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=6815745;
 //BA.debugLineNum = 6815745;BA.debugLine="Private clsid As String";
_clsid = "";
RDebugUtils.currentLine=6815746;
 //BA.debugLineNum = 6815746;BA.debugLine="Private todb As clsDB";
_todb = new b4j.example.clsdb();
RDebugUtils.currentLine=6815747;
 //BA.debugLineNum = 6815747;BA.debugLine="Private todbdms As clsDBDms";
_todbdms = new b4j.example.clsdbdms();
RDebugUtils.currentLine=6815748;
 //BA.debugLineNum = 6815748;BA.debugLine="End Sub";
return "";
}
public String  _handle(b4j.example.trenotiftas __ref,anywheresoftware.b4j.object.JServlet.ServletRequestWrapper _req,anywheresoftware.b4j.object.JServlet.ServletResponseWrapper _resp) throws Exception{
__ref = this;
RDebugUtils.currentModule="trenotiftas";
if (Debug.shouldDelegate(ba, "handle"))
	return (String) Debug.delegate(ba, "handle", new Object[] {_req,_resp});
String _appid = "";
String _response = "";
String _worknumb = "";
String _hasil = "";
String _username = "";
anywheresoftware.b4a.objects.StringUtils _strutl = null;
RDebugUtils.currentLine=6946816;
 //BA.debugLineNum = 6946816;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
RDebugUtils.currentLine=6946817;
 //BA.debugLineNum = 6946817;BA.debugLine="Dim appid, response, worknumb, hasil, username As";
_appid = "";
_response = "";
_worknumb = "";
_hasil = "";
_username = "";
RDebugUtils.currentLine=6946818;
 //BA.debugLineNum = 6946818;BA.debugLine="Dim strutl As StringUtils";
_strutl = new anywheresoftware.b4a.objects.StringUtils();
RDebugUtils.currentLine=6946821;
 //BA.debugLineNum = 6946821;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
_appid = _req.GetParameter("clsakses");
RDebugUtils.currentLine=6946825;
 //BA.debugLineNum = 6946825;BA.debugLine="If appid <> clsid Then";
if ((_appid).equals(__ref._clsid) == false) { 
RDebugUtils.currentLine=6946826;
 //BA.debugLineNum = 6946826;BA.debugLine="response = modGenerateReturn.return_json(modGene";
_response = _modgeneratereturn._return_json(_modgeneratereturn._label_rejected,_modgeneratereturn._kode_rejected_class,"Notif");
RDebugUtils.currentLine=6946827;
 //BA.debugLineNum = 6946827;BA.debugLine="resp.Write(response)";
_resp.Write(_response);
RDebugUtils.currentLine=6946828;
 //BA.debugLineNum = 6946828;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=6946831;
 //BA.debugLineNum = 6946831;BA.debugLine="username = req.GetParameter(\"username\")";
_username = _req.GetParameter("username");
RDebugUtils.currentLine=6946832;
 //BA.debugLineNum = 6946832;BA.debugLine="worknumb = strutl.DecodeUrl(req.GetParameter(\"wor";
_worknumb = _strutl.DecodeUrl(_req.GetParameter("worknumb"),"UTF8");
RDebugUtils.currentLine=6946834;
 //BA.debugLineNum = 6946834;BA.debugLine="hasil = load_notif(username, worknumb)";
_hasil = __ref._load_notif(null,_username,_worknumb);
RDebugUtils.currentLine=6946836;
 //BA.debugLineNum = 6946836;BA.debugLine="resp.Write(hasil)";
_resp.Write(_hasil);
RDebugUtils.currentLine=6946838;
 //BA.debugLineNum = 6946838;BA.debugLine="End Sub";
return "";
}
public String  _load_notif(b4j.example.trenotiftas __ref,String _username,String _worknumb) throws Exception{
__ref = this;
RDebugUtils.currentModule="trenotiftas";
if (Debug.shouldDelegate(ba, "load_notif"))
	return (String) Debug.delegate(ba, "load_notif", new Object[] {_username,_worknumb});
String _query = "";
String _data = "";
anywheresoftware.b4a.objects.collections.List _worknumblst = null;
int _totaldata = 0;
int _totalcurr = 0;
float _bagidec = 0f;
int _bagiint = 0;
int _i = 0;
int _x = 0;
RDebugUtils.currentLine=7012352;
 //BA.debugLineNum = 7012352;BA.debugLine="Private Sub load_notif(username As String, worknum";
RDebugUtils.currentLine=7012353;
 //BA.debugLineNum = 7012353;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=7012354;
 //BA.debugLineNum = 7012354;BA.debugLine="Dim worknumbLst As List";
_worknumblst = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=7012355;
 //BA.debugLineNum = 7012355;BA.debugLine="Dim totalData, totalcurr As Int";
_totaldata = 0;
_totalcurr = 0;
RDebugUtils.currentLine=7012356;
 //BA.debugLineNum = 7012356;BA.debugLine="Dim bagidec As Float";
_bagidec = 0f;
RDebugUtils.currentLine=7012357;
 //BA.debugLineNum = 7012357;BA.debugLine="Dim bagiint As Int";
_bagiint = 0;
RDebugUtils.currentLine=7012358;
 //BA.debugLineNum = 7012358;BA.debugLine="worknumbLst.Initialize";
_worknumblst.Initialize();
RDebugUtils.currentLine=7012359;
 //BA.debugLineNum = 7012359;BA.debugLine="totalData = Regex.Split(\",\", worknumb).Length";
_totaldata = __c.Regex.Split(",",_worknumb).length;
RDebugUtils.currentLine=7012360;
 //BA.debugLineNum = 7012360;BA.debugLine="worknumbLst = Regex.Split(\",\", worknumb)";
_worknumblst = anywheresoftware.b4a.keywords.Common.ArrayToList(__c.Regex.Split(",",_worknumb));
RDebugUtils.currentLine=7012361;
 //BA.debugLineNum = 7012361;BA.debugLine="totalcurr = 0";
_totalcurr = (int) (0);
RDebugUtils.currentLine=7012362;
 //BA.debugLineNum = 7012362;BA.debugLine="If totalData > 100 Then";
if (_totaldata>100) { 
RDebugUtils.currentLine=7012363;
 //BA.debugLineNum = 7012363;BA.debugLine="bagidec = totalData / 50";
_bagidec = (float) (_totaldata/(double)50);
RDebugUtils.currentLine=7012364;
 //BA.debugLineNum = 7012364;BA.debugLine="bagiint = bagidec";
_bagiint = (int) (_bagidec);
RDebugUtils.currentLine=7012366;
 //BA.debugLineNum = 7012366;BA.debugLine="If bagidec > bagiint Then bagiint = bagiint + 1";
if (_bagidec>_bagiint) { 
_bagiint = (int) (_bagiint+1);};
RDebugUtils.currentLine=7012367;
 //BA.debugLineNum = 7012367;BA.debugLine="For i = 0 To bagiint - 1";
{
final int step14 = 1;
final int limit14 = (int) (_bagiint-1);
_i = (int) (0) ;
for (;(step14 > 0 && _i <= limit14) || (step14 < 0 && _i >= limit14) ;_i = ((int)(0 + _i + step14))  ) {
RDebugUtils.currentLine=7012368;
 //BA.debugLineNum = 7012368;BA.debugLine="worknumb = \"\"";
_worknumb = "";
RDebugUtils.currentLine=7012369;
 //BA.debugLineNum = 7012369;BA.debugLine="If totalcurr > (totalData - totalcurr) Then";
if (_totalcurr>(_totaldata-_totalcurr)) { 
RDebugUtils.currentLine=7012370;
 //BA.debugLineNum = 7012370;BA.debugLine="For x = totalcurr To totalData - 1";
{
final int step17 = 1;
final int limit17 = (int) (_totaldata-1);
_x = _totalcurr ;
for (;(step17 > 0 && _x <= limit17) || (step17 < 0 && _x >= limit17) ;_x = ((int)(0 + _x + step17))  ) {
RDebugUtils.currentLine=7012371;
 //BA.debugLineNum = 7012371;BA.debugLine="If x = totalcurr Then";
if (_x==_totalcurr) { 
RDebugUtils.currentLine=7012372;
 //BA.debugLineNum = 7012372;BA.debugLine="worknumb = worknumbLst.Get(x)";
_worknumb = BA.ObjectToString(_worknumblst.Get(_x));
 }else {
RDebugUtils.currentLine=7012374;
 //BA.debugLineNum = 7012374;BA.debugLine="worknumb = worknumb & \",\" & worknumbLst.Get(";
_worknumb = _worknumb+","+BA.ObjectToString(_worknumblst.Get(_x));
 };
 }
};
RDebugUtils.currentLine=7012377;
 //BA.debugLineNum = 7012377;BA.debugLine="query = $\" select * from table(PKG_DMS_WORKFLO";
_query = (" select * from table(PKG_DMS_WORKFLOW.fn_save_notif_worknumber('"+__c.SmartStringFormatter("",(Object)(_username))+"', '"+__c.SmartStringFormatter("",(Object)(_worknumb))+"', "+__c.SmartStringFormatter("",(Object)(__c.Regex.Split(",",_worknumb).length))+")) ");
RDebugUtils.currentLine=7012378;
 //BA.debugLineNum = 7012378;BA.debugLine="data  = exequery.multi_string(query, \"notif\")";
_data = _exequery._multi_string(_query,"notif");
 }else {
RDebugUtils.currentLine=7012380;
 //BA.debugLineNum = 7012380;BA.debugLine="For x = totalcurr To (totalcurr + 49) - 1";
{
final int step27 = 1;
final int limit27 = (int) ((_totalcurr+49)-1);
_x = _totalcurr ;
for (;(step27 > 0 && _x <= limit27) || (step27 < 0 && _x >= limit27) ;_x = ((int)(0 + _x + step27))  ) {
RDebugUtils.currentLine=7012381;
 //BA.debugLineNum = 7012381;BA.debugLine="If x = totalcurr Then";
if (_x==_totalcurr) { 
RDebugUtils.currentLine=7012382;
 //BA.debugLineNum = 7012382;BA.debugLine="worknumb = worknumbLst.Get(x)";
_worknumb = BA.ObjectToString(_worknumblst.Get(_x));
 }else {
RDebugUtils.currentLine=7012384;
 //BA.debugLineNum = 7012384;BA.debugLine="worknumb = worknumb & \",\" & worknumbLst.Get(";
_worknumb = _worknumb+","+BA.ObjectToString(_worknumblst.Get(_x));
 };
 }
};
RDebugUtils.currentLine=7012387;
 //BA.debugLineNum = 7012387;BA.debugLine="query = $\" select * from table(PKG_DMS_WORKFLO";
_query = (" select * from table(PKG_DMS_WORKFLOW.fn_save_notif_worknumber('"+__c.SmartStringFormatter("",(Object)(_username))+"', '"+__c.SmartStringFormatter("",(Object)(_worknumb))+"', "+__c.SmartStringFormatter("",(Object)(__c.Regex.Split(",",_worknumb).length))+")) ");
RDebugUtils.currentLine=7012388;
 //BA.debugLineNum = 7012388;BA.debugLine="data  = exequery.multi_string(query, \"notif\")";
_data = _exequery._multi_string(_query,"notif");
 };
RDebugUtils.currentLine=7012390;
 //BA.debugLineNum = 7012390;BA.debugLine="totalcurr = totalcurr + 49";
_totalcurr = (int) (_totalcurr+49);
 }
};
 };
RDebugUtils.currentLine=7012396;
 //BA.debugLineNum = 7012396;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=7012397;
 //BA.debugLineNum = 7012397;BA.debugLine="End Sub";
return "";
}
public String  _initialize(b4j.example.trenotiftas __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="trenotiftas";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
RDebugUtils.currentLine=6881280;
 //BA.debugLineNum = 6881280;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=6881281;
 //BA.debugLineNum = 6881281;BA.debugLine="clsid = \"tre_notif\"";
__ref._clsid = "tre_notif";
RDebugUtils.currentLine=6881282;
 //BA.debugLineNum = 6881282;BA.debugLine="todb.Initialize";
__ref._todb._initialize(null,ba);
RDebugUtils.currentLine=6881283;
 //BA.debugLineNum = 6881283;BA.debugLine="todbdms.Initialize";
__ref._todbdms._initialize(null,ba);
RDebugUtils.currentLine=6881285;
 //BA.debugLineNum = 6881285;BA.debugLine="End Sub";
return "";
}
}