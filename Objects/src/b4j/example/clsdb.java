package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.debug.*;

public class clsdb extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    public static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.clsdb", this);
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            ba.htSubs = htSubs;
             
        }
        if (BA.isShellModeRuntimeCheck(ba))
                this.getClass().getMethod("_class_globals", b4j.example.clsdb.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public String _serverdb = "";
public String _pwd = "";
public String _user = "";
public String _std = "";
public anywheresoftware.b4a.objects.StringUtils _en = null;
public b4j.example.main _main = null;
public b4j.example.exequery _exequery = null;
public b4j.example.clsmanagement _clsmanagement = null;
public b4j.example.clsmaster _clsmaster = null;
public b4j.example.modgeneratereturn _modgeneratereturn = null;
public String  _initialize(b4j.example.clsdb __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="clsdb";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
RDebugUtils.currentLine=1310720;
 //BA.debugLineNum = 1310720;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=1310721;
 //BA.debugLineNum = 1310721;BA.debugLine="serverdb = Main.ipdb & \"/\" & Main.tns";
__ref._serverdb = _main._ipdb+"/"+_main._tns;
RDebugUtils.currentLine=1310722;
 //BA.debugLineNum = 1310722;BA.debugLine="user = Main.userdb";
__ref._user = _main._userdb;
RDebugUtils.currentLine=1310723;
 //BA.debugLineNum = 1310723;BA.debugLine="pwd = Main.pwddb";
__ref._pwd = _main._pwddb;
RDebugUtils.currentLine=1310724;
 //BA.debugLineNum = 1310724;BA.debugLine="End Sub";
return "";
}
public anywheresoftware.b4a.objects.collections.Map  _sqlselectmap(b4j.example.clsdb __ref,String _sql,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="clsdb";
if (Debug.shouldDelegate(ba, "sqlselectmap"))
	return (anywheresoftware.b4a.objects.collections.Map) Debug.delegate(ba, "sqlselectmap", new Object[] {_sql,_method});
anywheresoftware.b4j.objects.SQL _koneksi = null;
anywheresoftware.b4j.objects.SQL.ResultSetWrapper _rs = null;
int _i = 0;
int _j = 0;
anywheresoftware.b4a.objects.collections.Map _mp = null;
String _field = "";
String _error = "";
anywheresoftware.b4a.objects.collections.List _lret = null;
String _val = "";
RDebugUtils.currentLine=1376256;
 //BA.debugLineNum = 1376256;BA.debugLine="Sub SqlSelectMap(Sql As String, method As String)";
RDebugUtils.currentLine=1376257;
 //BA.debugLineNum = 1376257;BA.debugLine="Dim koneksi As SQL";
_koneksi = new anywheresoftware.b4j.objects.SQL();
RDebugUtils.currentLine=1376258;
 //BA.debugLineNum = 1376258;BA.debugLine="Dim rs As ResultSet";
_rs = new anywheresoftware.b4j.objects.SQL.ResultSetWrapper();
RDebugUtils.currentLine=1376259;
 //BA.debugLineNum = 1376259;BA.debugLine="Dim i, j As Int";
_i = 0;
_j = 0;
RDebugUtils.currentLine=1376260;
 //BA.debugLineNum = 1376260;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=1376261;
 //BA.debugLineNum = 1376261;BA.debugLine="Dim field, error As String";
_field = "";
_error = "";
RDebugUtils.currentLine=1376262;
 //BA.debugLineNum = 1376262;BA.debugLine="Dim lRet As List";
_lret = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=1376263;
 //BA.debugLineNum = 1376263;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=1376264;
 //BA.debugLineNum = 1376264;BA.debugLine="lRet.Initialize";
_lret.Initialize();
RDebugUtils.currentLine=1376267;
 //BA.debugLineNum = 1376267;BA.debugLine="Try";
try {RDebugUtils.currentLine=1376268;
 //BA.debugLineNum = 1376268;BA.debugLine="If koneksi.IsInitialized = True Then";
if (_koneksi.IsInitialized()==__c.True) { 
RDebugUtils.currentLine=1376269;
 //BA.debugLineNum = 1376269;BA.debugLine="koneksi.Close";
_koneksi.Close();
 };
RDebugUtils.currentLine=1376271;
 //BA.debugLineNum = 1376271;BA.debugLine="koneksi.Initialize2(std,\"jdbc:oracle:thin:@//\" &";
_koneksi.Initialize2(__ref._std,"jdbc:oracle:thin:@//"+__ref._serverdb,__ref._user,__ref._pwd);
 } 
       catch (Exception e15) {
			ba.setLastException(e15);RDebugUtils.currentLine=1376273;
 //BA.debugLineNum = 1376273;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
_mp = __c.createMap(new Object[] {(Object)("response"),(Object)(_modgeneratereturn._kode_gagal_koneksi_db),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(__ref._en.EncodeUrl(__c.LastException(ba).getMessage(),"UTF8")),(Object)("data"),(Object)(new Object[]{})});
RDebugUtils.currentLine=1376274;
 //BA.debugLineNum = 1376274;BA.debugLine="Main.appendSqlLog(\" \")";
_main._appendsqllog(" ");
RDebugUtils.currentLine=1376275;
 //BA.debugLineNum = 1376275;BA.debugLine="Main.appendSqlLog(Sql)";
_main._appendsqllog(_sql);
RDebugUtils.currentLine=1376276;
 //BA.debugLineNum = 1376276;BA.debugLine="Main.appendSqlLog(LastException.Message)";
_main._appendsqllog(__c.LastException(ba).getMessage());
RDebugUtils.currentLine=1376277;
 //BA.debugLineNum = 1376277;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=1376278;
 //BA.debugLineNum = 1376278;BA.debugLine="Return mp";
if (true) return _mp;
 };
RDebugUtils.currentLine=1376282;
 //BA.debugLineNum = 1376282;BA.debugLine="Try";
try {RDebugUtils.currentLine=1376283;
 //BA.debugLineNum = 1376283;BA.debugLine="rs = koneksi.ExecQuery(Sql.Replace(\"'null'\",Null";
_rs = _koneksi.ExecQuery(_sql.replace("'null'",BA.ObjectToString(__c.Null)));
 } 
       catch (Exception e25) {
			ba.setLastException(e25);RDebugUtils.currentLine=1376285;
 //BA.debugLineNum = 1376285;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
_mp = __c.createMap(new Object[] {(Object)("response"),(Object)(_modgeneratereturn._kode_gagal_execute_query),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(__ref._en.EncodeUrl(__c.LastException(ba).getMessage(),"UTF8")),(Object)("data"),(Object)(new Object[]{})});
RDebugUtils.currentLine=1376286;
 //BA.debugLineNum = 1376286;BA.debugLine="Main.appendSqlLog(\" \")";
_main._appendsqllog(" ");
RDebugUtils.currentLine=1376287;
 //BA.debugLineNum = 1376287;BA.debugLine="Main.appendSqlLog(Sql)";
_main._appendsqllog(_sql);
RDebugUtils.currentLine=1376288;
 //BA.debugLineNum = 1376288;BA.debugLine="Main.appendSqlLog(LastException.Message)";
_main._appendsqllog(__c.LastException(ba).getMessage());
RDebugUtils.currentLine=1376289;
 //BA.debugLineNum = 1376289;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=1376290;
 //BA.debugLineNum = 1376290;BA.debugLine="Return mp";
if (true) return _mp;
 };
RDebugUtils.currentLine=1376294;
 //BA.debugLineNum = 1376294;BA.debugLine="Try";
try {RDebugUtils.currentLine=1376295;
 //BA.debugLineNum = 1376295;BA.debugLine="i=0";
_i = (int) (0);
RDebugUtils.currentLine=1376296;
 //BA.debugLineNum = 1376296;BA.debugLine="j=0";
_j = (int) (0);
RDebugUtils.currentLine=1376297;
 //BA.debugLineNum = 1376297;BA.debugLine="Do While rs.NextRow";
while (_rs.NextRow()) {
RDebugUtils.currentLine=1376298;
 //BA.debugLineNum = 1376298;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=1376299;
 //BA.debugLineNum = 1376299;BA.debugLine="j = j+1";
_j = (int) (_j+1);
RDebugUtils.currentLine=1376300;
 //BA.debugLineNum = 1376300;BA.debugLine="For i=0 To rs.ColumnCount-1";
{
final int step38 = 1;
final int limit38 = (int) (_rs.getColumnCount()-1);
_i = (int) (0) ;
for (;(step38 > 0 && _i <= limit38) || (step38 < 0 && _i >= limit38) ;_i = ((int)(0 + _i + step38))  ) {
RDebugUtils.currentLine=1376301;
 //BA.debugLineNum = 1376301;BA.debugLine="Dim val As String";
_val = "";
RDebugUtils.currentLine=1376302;
 //BA.debugLineNum = 1376302;BA.debugLine="val = rs.GetString2(i)";
_val = _rs.GetString2(_i);
RDebugUtils.currentLine=1376303;
 //BA.debugLineNum = 1376303;BA.debugLine="If val <> Null Then";
if (_val!= null) { 
RDebugUtils.currentLine=1376304;
 //BA.debugLineNum = 1376304;BA.debugLine="val = en.EncodeUrl(val, \"UTF8\")";
_val = __ref._en.EncodeUrl(_val,"UTF8");
 }else {
RDebugUtils.currentLine=1376306;
 //BA.debugLineNum = 1376306;BA.debugLine="val = \"null\"";
_val = "null";
 };
RDebugUtils.currentLine=1376309;
 //BA.debugLineNum = 1376309;BA.debugLine="field = rs.GetColumnName(i).ToLowerCase";
_field = _rs.GetColumnName(_i).toLowerCase();
RDebugUtils.currentLine=1376310;
 //BA.debugLineNum = 1376310;BA.debugLine="If field = \"GETDATE(ENTRYTIME)\" Then";
if ((_field).equals("GETDATE(ENTRYTIME)")) { 
RDebugUtils.currentLine=1376311;
 //BA.debugLineNum = 1376311;BA.debugLine="field = \"DATE\"";
_field = "DATE";
 };
RDebugUtils.currentLine=1376313;
 //BA.debugLineNum = 1376313;BA.debugLine="mp.Put(field, val)";
_mp.Put((Object)(_field),(Object)(_val));
 }
};
RDebugUtils.currentLine=1376315;
 //BA.debugLineNum = 1376315;BA.debugLine="If method = \"GETMODUL\" Then mp.Put(\"nama\",modGe";
if ((_method).equals("GETMODUL")) { 
_mp.Put((Object)("nama"),(Object)(_modgeneratereturn._namauser));};
RDebugUtils.currentLine=1376316;
 //BA.debugLineNum = 1376316;BA.debugLine="lRet.Add(mp)";
_lret.Add((Object)(_mp.getObject()));
RDebugUtils.currentLine=1376317;
 //BA.debugLineNum = 1376317;BA.debugLine="mp = Null";
_mp.setObject((anywheresoftware.b4a.objects.collections.Map.MyMap)(__c.Null));
 }
;
 } 
       catch (Exception e57) {
			ba.setLastException(e57);RDebugUtils.currentLine=1376323;
 //BA.debugLineNum = 1376323;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
_mp = __c.createMap(new Object[] {(Object)("response"),(Object)(_modgeneratereturn._kode_gagal_parsing),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(__ref._en.EncodeUrl(__c.LastException(ba).getMessage(),"UTF8")),(Object)("data"),(Object)(new Object[]{})});
RDebugUtils.currentLine=1376324;
 //BA.debugLineNum = 1376324;BA.debugLine="Main.appendSqlLog(\" \")";
_main._appendsqllog(" ");
RDebugUtils.currentLine=1376325;
 //BA.debugLineNum = 1376325;BA.debugLine="Main.appendSqlLog(Sql)";
_main._appendsqllog(_sql);
RDebugUtils.currentLine=1376326;
 //BA.debugLineNum = 1376326;BA.debugLine="Main.appendSqlLog(LastException.Message)";
_main._appendsqllog(__c.LastException(ba).getMessage());
RDebugUtils.currentLine=1376327;
 //BA.debugLineNum = 1376327;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=1376328;
 //BA.debugLineNum = 1376328;BA.debugLine="Return mp";
if (true) return _mp;
 };
RDebugUtils.currentLine=1376330;
 //BA.debugLineNum = 1376330;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=1376331;
 //BA.debugLineNum = 1376331;BA.debugLine="mp = CreateMap(\"response\":modGenerateReturn.kode_";
_mp = __c.createMap(new Object[] {(Object)("response"),(Object)(_modgeneratereturn._kode_succes),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(""),(Object)("data"),(Object)(_lret.getObject())});
RDebugUtils.currentLine=1376332;
 //BA.debugLineNum = 1376332;BA.debugLine="Return mp";
if (true) return _mp;
RDebugUtils.currentLine=1376333;
 //BA.debugLineNum = 1376333;BA.debugLine="End Sub";
return null;
}
public anywheresoftware.b4a.objects.collections.Map  _sqlselectsinglemap(b4j.example.clsdb __ref,String _sql,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="clsdb";
if (Debug.shouldDelegate(ba, "sqlselectsinglemap"))
	return (anywheresoftware.b4a.objects.collections.Map) Debug.delegate(ba, "sqlselectsinglemap", new Object[] {_sql,_method});
anywheresoftware.b4j.objects.SQL _koneksi = null;
anywheresoftware.b4j.objects.SQL.ResultSetWrapper _rs = null;
int _i = 0;
anywheresoftware.b4a.objects.collections.Map _mp = null;
String _field = "";
anywheresoftware.b4a.objects.collections.List _lret = null;
String _val = "";
RDebugUtils.currentLine=1441792;
 //BA.debugLineNum = 1441792;BA.debugLine="Sub SqlSelectSingleMap(Sql As String, method As St";
RDebugUtils.currentLine=1441793;
 //BA.debugLineNum = 1441793;BA.debugLine="Dim koneksi As SQL";
_koneksi = new anywheresoftware.b4j.objects.SQL();
RDebugUtils.currentLine=1441794;
 //BA.debugLineNum = 1441794;BA.debugLine="Dim rs As ResultSet";
_rs = new anywheresoftware.b4j.objects.SQL.ResultSetWrapper();
RDebugUtils.currentLine=1441795;
 //BA.debugLineNum = 1441795;BA.debugLine="Dim i As Int";
_i = 0;
RDebugUtils.currentLine=1441796;
 //BA.debugLineNum = 1441796;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=1441797;
 //BA.debugLineNum = 1441797;BA.debugLine="Dim field As String";
_field = "";
RDebugUtils.currentLine=1441798;
 //BA.debugLineNum = 1441798;BA.debugLine="Dim lRet As List";
_lret = new anywheresoftware.b4a.objects.collections.List();
RDebugUtils.currentLine=1441799;
 //BA.debugLineNum = 1441799;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=1441800;
 //BA.debugLineNum = 1441800;BA.debugLine="lRet.Initialize";
_lret.Initialize();
RDebugUtils.currentLine=1441803;
 //BA.debugLineNum = 1441803;BA.debugLine="Try";
try {RDebugUtils.currentLine=1441804;
 //BA.debugLineNum = 1441804;BA.debugLine="If koneksi.IsInitialized = True Then";
if (_koneksi.IsInitialized()==__c.True) { 
RDebugUtils.currentLine=1441805;
 //BA.debugLineNum = 1441805;BA.debugLine="koneksi.Close";
_koneksi.Close();
 };
RDebugUtils.currentLine=1441807;
 //BA.debugLineNum = 1441807;BA.debugLine="koneksi.Initialize2(std,\"jdbc:oracle:thin:@//\" &";
_koneksi.Initialize2(__ref._std,"jdbc:oracle:thin:@//"+__ref._serverdb,__ref._user,__ref._pwd);
 } 
       catch (Exception e15) {
			ba.setLastException(e15);RDebugUtils.currentLine=1441809;
 //BA.debugLineNum = 1441809;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
_mp = __c.createMap(new Object[] {(Object)("response"),(Object)(_modgeneratereturn._kode_gagal_koneksi_db),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(__ref._en.EncodeUrl(__c.LastException(ba).getMessage(),"UTF8")),(Object)("data"),(Object)(new Object[]{}),(Object)("fn"),(Object)("SqlSelectMapSinngle")});
RDebugUtils.currentLine=1441810;
 //BA.debugLineNum = 1441810;BA.debugLine="Main.appendSqlLog(\" \")";
_main._appendsqllog(" ");
RDebugUtils.currentLine=1441811;
 //BA.debugLineNum = 1441811;BA.debugLine="Main.appendSqlLog(Sql)";
_main._appendsqllog(_sql);
RDebugUtils.currentLine=1441812;
 //BA.debugLineNum = 1441812;BA.debugLine="Main.appendSqlLog(LastException.Message)";
_main._appendsqllog(__c.LastException(ba).getMessage());
RDebugUtils.currentLine=1441813;
 //BA.debugLineNum = 1441813;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=1441814;
 //BA.debugLineNum = 1441814;BA.debugLine="Return mp";
if (true) return _mp;
 };
RDebugUtils.currentLine=1441818;
 //BA.debugLineNum = 1441818;BA.debugLine="Try";
try {RDebugUtils.currentLine=1441819;
 //BA.debugLineNum = 1441819;BA.debugLine="rs = koneksi.ExecQuery(Sql.Replace(\"'null'\",Null";
_rs = _koneksi.ExecQuery(_sql.replace("'null'",BA.ObjectToString(__c.Null)));
 } 
       catch (Exception e25) {
			ba.setLastException(e25);RDebugUtils.currentLine=1441821;
 //BA.debugLineNum = 1441821;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
_mp = __c.createMap(new Object[] {(Object)("response"),(Object)(_modgeneratereturn._kode_gagal_execute_query),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(__ref._en.EncodeUrl(__c.LastException(ba).getMessage(),"UTF8")),(Object)("data"),(Object)(new Object[]{}),(Object)("fn"),(Object)("SqlSelectMapSinngle")});
RDebugUtils.currentLine=1441822;
 //BA.debugLineNum = 1441822;BA.debugLine="Main.appendSqlLog(\" \")";
_main._appendsqllog(" ");
RDebugUtils.currentLine=1441823;
 //BA.debugLineNum = 1441823;BA.debugLine="Main.appendSqlLog(Sql)";
_main._appendsqllog(_sql);
RDebugUtils.currentLine=1441824;
 //BA.debugLineNum = 1441824;BA.debugLine="Main.appendSqlLog(LastException.Message)";
_main._appendsqllog(__c.LastException(ba).getMessage());
RDebugUtils.currentLine=1441825;
 //BA.debugLineNum = 1441825;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=1441826;
 //BA.debugLineNum = 1441826;BA.debugLine="Return mp";
if (true) return _mp;
 };
RDebugUtils.currentLine=1441830;
 //BA.debugLineNum = 1441830;BA.debugLine="Try";
try {RDebugUtils.currentLine=1441831;
 //BA.debugLineNum = 1441831;BA.debugLine="i=0";
_i = (int) (0);
RDebugUtils.currentLine=1441832;
 //BA.debugLineNum = 1441832;BA.debugLine="Do While rs.NextRow";
while (_rs.NextRow()) {
RDebugUtils.currentLine=1441833;
 //BA.debugLineNum = 1441833;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=1441834;
 //BA.debugLineNum = 1441834;BA.debugLine="For i=0 To rs.ColumnCount-1";
{
final int step36 = 1;
final int limit36 = (int) (_rs.getColumnCount()-1);
_i = (int) (0) ;
for (;(step36 > 0 && _i <= limit36) || (step36 < 0 && _i >= limit36) ;_i = ((int)(0 + _i + step36))  ) {
RDebugUtils.currentLine=1441835;
 //BA.debugLineNum = 1441835;BA.debugLine="Dim val As String";
_val = "";
RDebugUtils.currentLine=1441836;
 //BA.debugLineNum = 1441836;BA.debugLine="val = rs.GetString2(i)";
_val = _rs.GetString2(_i);
RDebugUtils.currentLine=1441837;
 //BA.debugLineNum = 1441837;BA.debugLine="If val <> Null Then";
if (_val!= null) { 
RDebugUtils.currentLine=1441838;
 //BA.debugLineNum = 1441838;BA.debugLine="val = en.EncodeUrl(val, \"UTF8\")";
_val = __ref._en.EncodeUrl(_val,"UTF8");
 }else {
RDebugUtils.currentLine=1441840;
 //BA.debugLineNum = 1441840;BA.debugLine="val = \"null\"";
_val = "null";
 };
RDebugUtils.currentLine=1441843;
 //BA.debugLineNum = 1441843;BA.debugLine="field = rs.GetColumnName(i).ToLowerCase";
_field = _rs.GetColumnName(_i).toLowerCase();
RDebugUtils.currentLine=1441844;
 //BA.debugLineNum = 1441844;BA.debugLine="If field = \"GETDATE(ENTRYTIME)\" Then";
if ((_field).equals("GETDATE(ENTRYTIME)")) { 
RDebugUtils.currentLine=1441845;
 //BA.debugLineNum = 1441845;BA.debugLine="field = \"DATE\"";
_field = "DATE";
 };
RDebugUtils.currentLine=1441847;
 //BA.debugLineNum = 1441847;BA.debugLine="mp.Put(field, val)";
_mp.Put((Object)(_field),(Object)(_val));
 }
};
RDebugUtils.currentLine=1441849;
 //BA.debugLineNum = 1441849;BA.debugLine="mp.Put(\"response\": modGenerateReturn.kode_succe";
_mp.Put((Object)("response"),(Object)(_modgeneratereturn._kode_succes));
 }
;
 } 
       catch (Exception e53) {
			ba.setLastException(e53);RDebugUtils.currentLine=1441852;
 //BA.debugLineNum = 1441852;BA.debugLine="mp = CreateMap(\"response\": modGenerateReturn.kod";
_mp = __c.createMap(new Object[] {(Object)("response"),(Object)(_modgeneratereturn._kode_gagal_parsing),(Object)("method"),(Object)(_method),(Object)("pesan"),(Object)(__ref._en.EncodeUrl(__c.LastException(ba).getMessage(),"UTF8")),(Object)("data"),(Object)(new Object[]{}),(Object)("fn"),(Object)("SqlSelectMapSinngle")});
RDebugUtils.currentLine=1441853;
 //BA.debugLineNum = 1441853;BA.debugLine="Main.appendSqlLog(\" \")";
_main._appendsqllog(" ");
RDebugUtils.currentLine=1441854;
 //BA.debugLineNum = 1441854;BA.debugLine="Main.appendSqlLog(Sql)";
_main._appendsqllog(_sql);
RDebugUtils.currentLine=1441855;
 //BA.debugLineNum = 1441855;BA.debugLine="Main.appendSqlLog(LastException.Message)";
_main._appendsqllog(__c.LastException(ba).getMessage());
RDebugUtils.currentLine=1441856;
 //BA.debugLineNum = 1441856;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=1441857;
 //BA.debugLineNum = 1441857;BA.debugLine="Return mp";
if (true) return _mp;
 };
RDebugUtils.currentLine=1441859;
 //BA.debugLineNum = 1441859;BA.debugLine="koneksi.Close";
_koneksi.Close();
RDebugUtils.currentLine=1441860;
 //BA.debugLineNum = 1441860;BA.debugLine="Return mp";
if (true) return _mp;
RDebugUtils.currentLine=1441861;
 //BA.debugLineNum = 1441861;BA.debugLine="End Sub";
return null;
}
public String  _class_globals(b4j.example.clsdb __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="clsdb";
RDebugUtils.currentLine=1245184;
 //BA.debugLineNum = 1245184;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=1245185;
 //BA.debugLineNum = 1245185;BA.debugLine="Private serverdb, pwd, user As String";
_serverdb = "";
_pwd = "";
_user = "";
RDebugUtils.currentLine=1245186;
 //BA.debugLineNum = 1245186;BA.debugLine="Dim std As String=\"oracle.jdbc.driver.OracleDrive";
_std = "oracle.jdbc.driver.OracleDriver";
RDebugUtils.currentLine=1245187;
 //BA.debugLineNum = 1245187;BA.debugLine="Private en As StringUtils";
_en = new anywheresoftware.b4a.objects.StringUtils();
RDebugUtils.currentLine=1245188;
 //BA.debugLineNum = 1245188;BA.debugLine="End Sub";
return "";
}
}