package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.debug.*;

public class treverif extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    public static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.treverif", this);
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            ba.htSubs = htSubs;
             
        }
        if (BA.isShellModeRuntimeCheck(ba))
                this.getClass().getMethod("_class_globals", b4j.example.treverif.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public String _clsid = "";
public b4j.example.main _main = null;
public b4j.example.exequery _exequery = null;
public b4j.example.clsmanagement _clsmanagement = null;
public b4j.example.clsmaster _clsmaster = null;
public b4j.example.modgeneratereturn _modgeneratereturn = null;
public String  _class_globals(b4j.example.treverif __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="treverif";
RDebugUtils.currentLine=1507328;
 //BA.debugLineNum = 1507328;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=1507329;
 //BA.debugLineNum = 1507329;BA.debugLine="Private clsid As String";
_clsid = "";
RDebugUtils.currentLine=1507330;
 //BA.debugLineNum = 1507330;BA.debugLine="End Sub";
return "";
}
public String  _handle(b4j.example.treverif __ref,anywheresoftware.b4j.object.JServlet.ServletRequestWrapper _req,anywheresoftware.b4j.object.JServlet.ServletResponseWrapper _resp) throws Exception{
__ref = this;
RDebugUtils.currentModule="treverif";
if (Debug.shouldDelegate(ba, "handle"))
	return (String) Debug.delegate(ba, "handle", new Object[] {_req,_resp});
String _appid = "";
String _hasil = "";
String _method = "";
String _response = "";
RDebugUtils.currentLine=1638400;
 //BA.debugLineNum = 1638400;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
RDebugUtils.currentLine=1638401;
 //BA.debugLineNum = 1638401;BA.debugLine="Dim appid, hasil, method As String";
_appid = "";
_hasil = "";
_method = "";
RDebugUtils.currentLine=1638402;
 //BA.debugLineNum = 1638402;BA.debugLine="Dim response As String";
_response = "";
RDebugUtils.currentLine=1638404;
 //BA.debugLineNum = 1638404;BA.debugLine="If req.Method <> \"POST\" Then";
if ((_req.getMethod()).equals("POST") == false) { 
RDebugUtils.currentLine=1638405;
 //BA.debugLineNum = 1638405;BA.debugLine="resp.SendError(500, \"method not supported.\")";
_resp.SendError((int) (500),"method not supported.");
RDebugUtils.currentLine=1638406;
 //BA.debugLineNum = 1638406;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=1638410;
 //BA.debugLineNum = 1638410;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
_appid = _req.GetParameter("clsakses");
RDebugUtils.currentLine=1638412;
 //BA.debugLineNum = 1638412;BA.debugLine="method = req.GetParameter(\"method\")";
_method = _req.GetParameter("method");
RDebugUtils.currentLine=1638414;
 //BA.debugLineNum = 1638414;BA.debugLine="If appid <> clsid Then";
if ((_appid).equals(__ref._clsid) == false) { 
RDebugUtils.currentLine=1638415;
 //BA.debugLineNum = 1638415;BA.debugLine="response = modGenerateReturn.return_json(modGene";
_response = _modgeneratereturn._return_json(_modgeneratereturn._label_rejected,_modgeneratereturn._kode_rejected_class,_method);
RDebugUtils.currentLine=1638416;
 //BA.debugLineNum = 1638416;BA.debugLine="resp.Write(response)";
_resp.Write(_response);
RDebugUtils.currentLine=1638417;
 //BA.debugLineNum = 1638417;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=1638420;
 //BA.debugLineNum = 1638420;BA.debugLine="Select method";
switch (BA.switchObjectToInt(_method,"verif_staffuip","verif_uip")) {
case 0: {
RDebugUtils.currentLine=1638422;
 //BA.debugLineNum = 1638422;BA.debugLine="resp.Write(send_staffuip(req.GetParameter(\"idsu";
_resp.Write(__ref._send_staffuip(null,_req.GetParameter("idsurat_mohonbayar"),_req.GetParameter("status"),_req.GetParameter("tipebayar"),_req.GetParameter("no_surat"),_req.GetParameter("tgl_surat"),_req.GetParameter("ket"),_req.GetParameter("username"),_req.GetParameter("path"),_req.GetParameter("filename"),_req.GetParameter("total"),_req.GetParameter("kode_doc"),_method));
 break; }
case 1: {
RDebugUtils.currentLine=1638424;
 //BA.debugLineNum = 1638424;BA.debugLine="resp.Write(send_uip(req.GetParameter(\"idsurat_m";
_resp.Write(__ref._send_uip(null,_req.GetParameter("idsurat_mohonbayar"),_req.GetParameter("status"),_req.GetParameter("tipebayar"),_req.GetParameter("no_surat"),_req.GetParameter("tgl_surat"),_req.GetParameter("username"),_req.GetParameter("ket"),_method));
 break; }
default: {
RDebugUtils.currentLine=1638426;
 //BA.debugLineNum = 1638426;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
_hasil = _modgeneratereturn._return_json("TIDAK ADA METHOD","404",_method);
RDebugUtils.currentLine=1638427;
 //BA.debugLineNum = 1638427;BA.debugLine="resp.Write(hasil)";
_resp.Write(_hasil);
 break; }
}
;
RDebugUtils.currentLine=1638430;
 //BA.debugLineNum = 1638430;BA.debugLine="End Sub";
return "";
}
public String  _send_staffuip(b4j.example.treverif __ref,String _p_idsurat,String _p_status,String _p_tipe_bayar,String _p_no_surat,String _p_tglsurat,String _p_ket,String _p_username,String _p_path_file,String _p_filename,String _p_totalfile,String _p_kode_doc,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="treverif";
if (Debug.shouldDelegate(ba, "send_staffuip"))
	return (String) Debug.delegate(ba, "send_staffuip", new Object[] {_p_idsurat,_p_status,_p_tipe_bayar,_p_no_surat,_p_tglsurat,_p_ket,_p_username,_p_path_file,_p_filename,_p_totalfile,_p_kode_doc,_method});
String _query = "";
String _data = "";
RDebugUtils.currentLine=1703936;
 //BA.debugLineNum = 1703936;BA.debugLine="Private Sub send_staffuip(p_idsurat As String, p_s";
RDebugUtils.currentLine=1703937;
 //BA.debugLineNum = 1703937;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=1703938;
 //BA.debugLineNum = 1703938;BA.debugLine="p_totalfile = Regex.Split(\",\", p_tglsurat).Length";
_p_totalfile = BA.NumberToString(__c.Regex.Split(",",_p_tglsurat).length);
RDebugUtils.currentLine=1703939;
 //BA.debugLineNum = 1703939;BA.debugLine="query = $\" select * from table(PKG_DMS_VERIFIKASI";
_query = (" select * from table(PKG_DMS_VERIFIKASI.fn_disposisi_uip_staff('"+__c.SmartStringFormatter("",(Object)(_p_idsurat))+"','"+__c.SmartStringFormatter("",(Object)(_p_status))+"', '"+__c.SmartStringFormatter("",(Object)(_p_tipe_bayar))+"'\n"+"	'"+__c.SmartStringFormatter("",(Object)(_p_no_surat))+"','"+__c.SmartStringFormatter("",(Object)(_p_tglsurat))+"','"+__c.SmartStringFormatter("",(Object)(_p_ket))+"','"+__c.SmartStringFormatter("",(Object)(_p_username))+"', '"+__c.SmartStringFormatter("",(Object)(_p_path_file))+"', '"+__c.SmartStringFormatter("",(Object)(_p_filename))+"', "+__c.SmartStringFormatter("",(Object)(_p_totalfile))+", \n"+"	'"+__c.SmartStringFormatter("",(Object)(_p_kode_doc))+"')) ");
RDebugUtils.currentLine=1703942;
 //BA.debugLineNum = 1703942;BA.debugLine="data  = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=1703943;
 //BA.debugLineNum = 1703943;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=1703944;
 //BA.debugLineNum = 1703944;BA.debugLine="End Sub";
return "";
}
public String  _send_uip(b4j.example.treverif __ref,String _p_idsurat,String _p_status,String _p_tipe_bayar,String _p_no_surat,String _p_tglsurat,String _p_username,String _p_ket,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="treverif";
if (Debug.shouldDelegate(ba, "send_uip"))
	return (String) Debug.delegate(ba, "send_uip", new Object[] {_p_idsurat,_p_status,_p_tipe_bayar,_p_no_surat,_p_tglsurat,_p_username,_p_ket,_method});
String _query = "";
String _data = "";
RDebugUtils.currentLine=1769472;
 //BA.debugLineNum = 1769472;BA.debugLine="Private Sub send_uip(p_idsurat As String, p_status";
RDebugUtils.currentLine=1769473;
 //BA.debugLineNum = 1769473;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=1769475;
 //BA.debugLineNum = 1769475;BA.debugLine="query = $\" select * from table(PKG_DMS_VERIFIKASI";
_query = (" select * from table(PKG_DMS_VERIFIKASI.fn_disposisi_uip('"+__c.SmartStringFormatter("",(Object)(_p_idsurat))+"','"+__c.SmartStringFormatter("",(Object)(_p_status))+"',\n"+"	'"+__c.SmartStringFormatter("",(Object)(_p_no_surat))+"','"+__c.SmartStringFormatter("",(Object)(_p_tglsurat))+"','"+__c.SmartStringFormatter("",(Object)(_p_username))+"','"+__c.SmartStringFormatter("",(Object)(_p_ket))+"')) ");
RDebugUtils.currentLine=1769477;
 //BA.debugLineNum = 1769477;BA.debugLine="data  = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=1769478;
 //BA.debugLineNum = 1769478;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=1769479;
 //BA.debugLineNum = 1769479;BA.debugLine="End Sub";
return "";
}
public String  _initialize(b4j.example.treverif __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="treverif";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
RDebugUtils.currentLine=1572864;
 //BA.debugLineNum = 1572864;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=1572865;
 //BA.debugLineNum = 1572865;BA.debugLine="clsid = \"tre_verifikasi\"";
__ref._clsid = "tre_verifikasi";
RDebugUtils.currentLine=1572866;
 //BA.debugLineNum = 1572866;BA.debugLine="End Sub";
return "";
}
}