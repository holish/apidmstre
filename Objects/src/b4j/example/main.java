package b4j.example;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.debug.*;

public class main extends Object{
public static main mostCurrent = new main();

public static BA ba;
static {
		ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.main", null);
		ba.loadHtSubs(main.class);
        if (ba.getClass().getName().endsWith("ShellBA")) {
			anywheresoftware.b4a.ShellBA.delegateBA = new anywheresoftware.b4a.StandardBA("b4j.example", null, null);
			ba.raiseEvent2(null, true, "SHELL", false);
			ba.raiseEvent2(null, true, "CREATE", true, "b4j.example.main", ba);
		}
	}
    public static Class<?> getObject() {
		return main.class;
	}

 
    public static void main(String[] args) throws Exception{
        try {
            anywheresoftware.b4a.keywords.Common.LogDebug("Program started.");
            initializeProcessGlobals();
            ba.raiseEvent(null, "appstart", (Object)args);
        } catch (Throwable t) {
			BA.printException(t, true);
		
        } finally {
            anywheresoftware.b4a.keywords.Common.LogDebug("Program terminated (StartMessageLoop was not called).");
        }
    }


private static boolean processGlobalsRun;
public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4j.object.ServerWrapper _srv = null;
public static anywheresoftware.b4a.objects.collections.Map _parammap = null;
public static String _urlsocketacmt = "";
public static String _dirparentpath = "";
public static String _ipftpdev = "";
public static String _ipftpprod = "";
public static String _ftpfolderdev = "";
public static String _ftpfolderprod = "";
public static String _tns = "";
public static String _ipdb = "";
public static String _userdb = "";
public static String _pwddb = "";
public static String _tnsdms = "";
public static String _ipdbdms = "";
public static String _userdbdms = "";
public static String _pwddbdms = "";
public static anywheresoftware.b4a.objects.collections.List _param = null;
public static anywheresoftware.b4a.objects.collections.List _paramdbdms = null;
public static anywheresoftware.b4j.objects.collections.JSONParser _json = null;
public static int _count = 0;
public static anywheresoftware.b4j.object.JavaObject _nativeme = null;
public static String _pwddms = "";
public static String _userdms = "";
public static b4j.example.exequery _exequery = null;
public static b4j.example.clsmanagement _clsmanagement = null;
public static b4j.example.clsmaster _clsmaster = null;
public static b4j.example.modgeneratereturn _modgeneratereturn = null;
public static String  _appendsqllog(String _logdata) throws Exception{
RDebugUtils.currentModule="main";
if (Debug.shouldDelegate(ba, "appendsqllog"))
	return (String) Debug.delegate(ba, "appendsqllog", new Object[] {_logdata});
b4j.example.jstringfunctions _bin = null;
String _tahun = "";
String _bulan = "";
String _tgl = "";
String _kode = "";
String _servername = "";
RDebugUtils.currentLine=851968;
 //BA.debugLineNum = 851968;BA.debugLine="Sub appendSqlLog(logdata As String)";
RDebugUtils.currentLine=851969;
 //BA.debugLineNum = 851969;BA.debugLine="Dim bin As JStringFunctions";
_bin = new b4j.example.jstringfunctions();
RDebugUtils.currentLine=851971;
 //BA.debugLineNum = 851971;BA.debugLine="bin.Initialize";
_bin._initialize(ba);
RDebugUtils.currentLine=851973;
 //BA.debugLineNum = 851973;BA.debugLine="Dim tahun As String = DateTime.GetYear(DateTime.N";
_tahun = BA.NumberToString(anywheresoftware.b4a.keywords.Common.DateTime.GetYear(anywheresoftware.b4a.keywords.Common.DateTime.getNow()));
RDebugUtils.currentLine=851974;
 //BA.debugLineNum = 851974;BA.debugLine="Dim bulan As String =   bin.Right(\"00\" & DateTime";
_bulan = _bin._right("00"+BA.NumberToString(anywheresoftware.b4a.keywords.Common.DateTime.GetMonth(anywheresoftware.b4a.keywords.Common.DateTime.getNow())),(long) (2));
RDebugUtils.currentLine=851975;
 //BA.debugLineNum = 851975;BA.debugLine="Dim tgl As String =  bin.Right(DateTime.GetDayOfM";
_tgl = _bin._right(BA.NumberToString(anywheresoftware.b4a.keywords.Common.DateTime.GetDayOfMonth(anywheresoftware.b4a.keywords.Common.DateTime.getNow())),(long) (2));
RDebugUtils.currentLine=851977;
 //BA.debugLineNum = 851977;BA.debugLine="Dim kode As String =  \"[\" & tahun & \"/\" & bulan &";
_kode = "["+_tahun+"/"+_bulan+"/"+_tgl+" "+anywheresoftware.b4a.keywords.Common.DateTime.Time(anywheresoftware.b4a.keywords.Common.DateTime.getNow())+"] ";
RDebugUtils.currentLine=851978;
 //BA.debugLineNum = 851978;BA.debugLine="Dim servername As String";
_servername = "";
RDebugUtils.currentLine=851980;
 //BA.debugLineNum = 851980;BA.debugLine="servername  = \"SERVER_\"  & \"_\" & srv.Port & \"_\" &";
_servername = "SERVER_"+"_"+BA.NumberToString(_srv.getPort())+"_"+_tahun+_bulan+_tgl+".txt";
RDebugUtils.currentLine=851983;
 //BA.debugLineNum = 851983;BA.debugLine="NativeMe.RunMethod(\"writeFile\",  Array As String(";
_nativeme.RunMethod("writeFile",(Object[])(new String[]{anywheresoftware.b4a.keywords.Common.File.getDirApp()+"/logsql/"+_servername,_kode+_logdata}));
RDebugUtils.currentLine=851984;
 //BA.debugLineNum = 851984;BA.debugLine="End Sub";
return "";
}
public static boolean  _application_error(anywheresoftware.b4a.objects.B4AException _error,String _stacktrace) throws Exception{
RDebugUtils.currentModule="main";
if (Debug.shouldDelegate(ba, "application_error"))
	return (Boolean) Debug.delegate(ba, "application_error", new Object[] {_error,_stacktrace});
RDebugUtils.currentLine=131072;
 //BA.debugLineNum = 131072;BA.debugLine="Sub Application_Error (Error As Exception, StackTr";
RDebugUtils.currentLine=131073;
 //BA.debugLineNum = 131073;BA.debugLine="Return True";
if (true) return anywheresoftware.b4a.keywords.Common.True;
RDebugUtils.currentLine=131074;
 //BA.debugLineNum = 131074;BA.debugLine="End Sub";
return false;
}
public static String  _appstart(String[] _args) throws Exception{
RDebugUtils.currentModule="main";
if (Debug.shouldDelegate(ba, "appstart"))
	return (String) Debug.delegate(ba, "appstart", new Object[] {_args});
anywheresoftware.b4a.objects.SocketWrapper.ServerSocketWrapper _ss = null;
String _str = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
RDebugUtils.currentLine=65536;
 //BA.debugLineNum = 65536;BA.debugLine="Sub AppStart (Args() As String)";
RDebugUtils.currentLine=65537;
 //BA.debugLineNum = 65537;BA.debugLine="Dim ss As ServerSocket";
_ss = new anywheresoftware.b4a.objects.SocketWrapper.ServerSocketWrapper();
RDebugUtils.currentLine=65538;
 //BA.debugLineNum = 65538;BA.debugLine="NativeMe = Me";
_nativeme.setObject((java.lang.Object)(main.getObject()));
RDebugUtils.currentLine=65539;
 //BA.debugLineNum = 65539;BA.debugLine="param.Initialize";
_param.Initialize();
RDebugUtils.currentLine=65540;
 //BA.debugLineNum = 65540;BA.debugLine="paramdbDMS.Initialize";
_paramdbdms.Initialize();
RDebugUtils.currentLine=65541;
 //BA.debugLineNum = 65541;BA.debugLine="Log(\" \")";
anywheresoftware.b4a.keywords.Common.Log(" ");
RDebugUtils.currentLine=65542;
 //BA.debugLineNum = 65542;BA.debugLine="Log(\"********************************************";
anywheresoftware.b4a.keywords.Common.Log("****************************************************");
RDebugUtils.currentLine=65543;
 //BA.debugLineNum = 65543;BA.debugLine="Log(\"     Copyright (c) 2017\"  )";
anywheresoftware.b4a.keywords.Common.Log("     Copyright (c) 2017");
RDebugUtils.currentLine=65544;
 //BA.debugLineNum = 65544;BA.debugLine="Log(\"     SBU TI - ICON+ \")";
anywheresoftware.b4a.keywords.Common.Log("     SBU TI - ICON+ ");
RDebugUtils.currentLine=65545;
 //BA.debugLineNum = 65545;BA.debugLine="Log(\"     Webservice API DMS Treasury, Jetty Serv";
anywheresoftware.b4a.keywords.Common.Log("     Webservice API DMS Treasury, Jetty Server 9.3");
RDebugUtils.currentLine=65546;
 //BA.debugLineNum = 65546;BA.debugLine="Log(\"     java 1.8 or higher \"  )";
anywheresoftware.b4a.keywords.Common.Log("     java 1.8 or higher ");
RDebugUtils.currentLine=65547;
 //BA.debugLineNum = 65547;BA.debugLine="Log(\" \"  )";
anywheresoftware.b4a.keywords.Common.Log(" ");
RDebugUtils.currentLine=65548;
 //BA.debugLineNum = 65548;BA.debugLine="Log(\"********************************************";
anywheresoftware.b4a.keywords.Common.Log("****************************************************");
RDebugUtils.currentLine=65549;
 //BA.debugLineNum = 65549;BA.debugLine="Args = Array As String(\"8821\")";
_args = new String[]{"8821"};
RDebugUtils.currentLine=65550;
 //BA.debugLineNum = 65550;BA.debugLine="If Args.Length < 1 Then";
if (_args.length<1) { 
RDebugUtils.currentLine=65551;
 //BA.debugLineNum = 65551;BA.debugLine="Log(\" \")";
anywheresoftware.b4a.keywords.Common.Log(" ");
RDebugUtils.currentLine=65552;
 //BA.debugLineNum = 65552;BA.debugLine="Log(\"     Parameter Port Tidak ada     \")";
anywheresoftware.b4a.keywords.Common.Log("     Parameter Port Tidak ada     ");
RDebugUtils.currentLine=65553;
 //BA.debugLineNum = 65553;BA.debugLine="Log(\" \")";
anywheresoftware.b4a.keywords.Common.Log(" ");
RDebugUtils.currentLine=65554;
 //BA.debugLineNum = 65554;BA.debugLine="ExitApplication";
anywheresoftware.b4a.keywords.Common.ExitApplication();
 };
RDebugUtils.currentLine=65557;
 //BA.debugLineNum = 65557;BA.debugLine="Log(\" \")";
anywheresoftware.b4a.keywords.Common.Log(" ");
RDebugUtils.currentLine=65558;
 //BA.debugLineNum = 65558;BA.debugLine="Log(\"     Checking file Config.json     \")";
anywheresoftware.b4a.keywords.Common.Log("     Checking file Config.json     ");
RDebugUtils.currentLine=65559;
 //BA.debugLineNum = 65559;BA.debugLine="Log(\" \")";
anywheresoftware.b4a.keywords.Common.Log(" ");
RDebugUtils.currentLine=65561;
 //BA.debugLineNum = 65561;BA.debugLine="If File.Exists(File.DirApp, \"config.json\") Then";
if (anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirApp(),"config.json")) { 
RDebugUtils.currentLine=65562;
 //BA.debugLineNum = 65562;BA.debugLine="Dim str As String";
_str = "";
RDebugUtils.currentLine=65563;
 //BA.debugLineNum = 65563;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=65564;
 //BA.debugLineNum = 65564;BA.debugLine="str = File.ReadString(File.DirApp, \"config.json\"";
_str = anywheresoftware.b4a.keywords.Common.File.ReadString(anywheresoftware.b4a.keywords.Common.File.getDirApp(),"config.json");
RDebugUtils.currentLine=65565;
 //BA.debugLineNum = 65565;BA.debugLine="json.Initialize(str)";
_json.Initialize(_str);
RDebugUtils.currentLine=65566;
 //BA.debugLineNum = 65566;BA.debugLine="mp = json.NextObject";
_mp = _json.NextObject();
RDebugUtils.currentLine=65567;
 //BA.debugLineNum = 65567;BA.debugLine="param.AddAll(Array As String(mp.Get(\"ipdb\"), mp.";
_param.AddAll(anywheresoftware.b4a.keywords.Common.ArrayToList(new String[]{BA.ObjectToString(_mp.Get((Object)("ipdb"))),BA.ObjectToString(_mp.Get((Object)("tnsdb"))),BA.ObjectToString(_mp.Get((Object)("userdb"))),BA.ObjectToString(_mp.Get((Object)("pwddb")))}));
RDebugUtils.currentLine=65568;
 //BA.debugLineNum = 65568;BA.debugLine="paramdbDMS.AddAll(Array As String(mp.Get(\"ipdbdm";
_paramdbdms.AddAll(anywheresoftware.b4a.keywords.Common.ArrayToList(new String[]{BA.ObjectToString(_mp.Get((Object)("ipdbdms"))),BA.ObjectToString(_mp.Get((Object)("tnsdbdms"))),BA.ObjectToString(_mp.Get((Object)("userdbdms"))),BA.ObjectToString(_mp.Get((Object)("pwddbdms")))}));
 }else {
RDebugUtils.currentLine=65570;
 //BA.debugLineNum = 65570;BA.debugLine="Log(\"     Tidak ada File Config.json, Silahkan h";
anywheresoftware.b4a.keywords.Common.Log("     Tidak ada File Config.json, Silahkan hubungi Team Terkait     ");
RDebugUtils.currentLine=65571;
 //BA.debugLineNum = 65571;BA.debugLine="Log(\"     \")";
anywheresoftware.b4a.keywords.Common.Log("     ");
RDebugUtils.currentLine=65572;
 //BA.debugLineNum = 65572;BA.debugLine="ExitApplication";
anywheresoftware.b4a.keywords.Common.ExitApplication();
 };
RDebugUtils.currentLine=65575;
 //BA.debugLineNum = 65575;BA.debugLine="File.MakeDir(File.DirApp,\"temp\")";
anywheresoftware.b4a.keywords.Common.File.MakeDir(anywheresoftware.b4a.keywords.Common.File.getDirApp(),"temp");
RDebugUtils.currentLine=65576;
 //BA.debugLineNum = 65576;BA.debugLine="File.MakeDir(File.DirApp,\"log\")";
anywheresoftware.b4a.keywords.Common.File.MakeDir(anywheresoftware.b4a.keywords.Common.File.getDirApp(),"log");
RDebugUtils.currentLine=65577;
 //BA.debugLineNum = 65577;BA.debugLine="dirparentpath = File.Combine(File.DirApp,\"temp\")";
_dirparentpath = anywheresoftware.b4a.keywords.Common.File.Combine(anywheresoftware.b4a.keywords.Common.File.getDirApp(),"temp");
RDebugUtils.currentLine=65579;
 //BA.debugLineNum = 65579;BA.debugLine="Try";
try {RDebugUtils.currentLine=65580;
 //BA.debugLineNum = 65580;BA.debugLine="ipdb = param.Get(0)";
_ipdb = BA.ObjectToString(_param.Get((int) (0)));
RDebugUtils.currentLine=65581;
 //BA.debugLineNum = 65581;BA.debugLine="tns = param.Get(1)";
_tns = BA.ObjectToString(_param.Get((int) (1)));
RDebugUtils.currentLine=65582;
 //BA.debugLineNum = 65582;BA.debugLine="userdb = param.Get(2)";
_userdb = BA.ObjectToString(_param.Get((int) (2)));
RDebugUtils.currentLine=65583;
 //BA.debugLineNum = 65583;BA.debugLine="pwddb = param.Get(3)";
_pwddb = BA.ObjectToString(_param.Get((int) (3)));
RDebugUtils.currentLine=65585;
 //BA.debugLineNum = 65585;BA.debugLine="ipdbdms = paramdbDMS.Get(0)";
_ipdbdms = BA.ObjectToString(_paramdbdms.Get((int) (0)));
RDebugUtils.currentLine=65586;
 //BA.debugLineNum = 65586;BA.debugLine="tnsdms = paramdbDMS.Get(1)";
_tnsdms = BA.ObjectToString(_paramdbdms.Get((int) (1)));
RDebugUtils.currentLine=65587;
 //BA.debugLineNum = 65587;BA.debugLine="userdbdms = paramdbDMS.Get(2)";
_userdbdms = BA.ObjectToString(_paramdbdms.Get((int) (2)));
RDebugUtils.currentLine=65588;
 //BA.debugLineNum = 65588;BA.debugLine="pwddbdms = paramdbDMS.Get(3)";
_pwddbdms = BA.ObjectToString(_paramdbdms.Get((int) (3)));
RDebugUtils.currentLine=65590;
 //BA.debugLineNum = 65590;BA.debugLine="srv.Port = Args(0)";
_srv.setPort((int)(Double.parseDouble(_args[(int) (0)])));
RDebugUtils.currentLine=65591;
 //BA.debugLineNum = 65591;BA.debugLine="srv.Initialize(\"serverInitialize\")";
_srv.Initialize(ba,"serverInitialize");
RDebugUtils.currentLine=65592;
 //BA.debugLineNum = 65592;BA.debugLine="srv.AddHandler(\"/login\", \"treLogin\", False)";
_srv.AddHandler("/login","treLogin",anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=65593;
 //BA.debugLineNum = 65593;BA.debugLine="srv.AddHandler(\"/notif\", \"trenotiftas\", False)";
_srv.AddHandler("/notif","trenotiftas",anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=65594;
 //BA.debugLineNum = 65594;BA.debugLine="srv.AddHandler(\"/getmenu\", \"tremenu\", False)";
_srv.AddHandler("/getmenu","tremenu",anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=65596;
 //BA.debugLineNum = 65596;BA.debugLine="srv.AddHandler(\"/master\", \"tremaster\", False)";
_srv.AddHandler("/master","tremaster",anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=65597;
 //BA.debugLineNum = 65597;BA.debugLine="srv.AddHandler(\"/management\", \"tremanagement\", F";
_srv.AddHandler("/management","tremanagement",anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=65598;
 //BA.debugLineNum = 65598;BA.debugLine="srv.AddHandler(\"/setting\", \"tredatasetting\", Fal";
_srv.AddHandler("/setting","tredatasetting",anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=65599;
 //BA.debugLineNum = 65599;BA.debugLine="srv.AddHandler(\"/trx\", \"tretrx\", False)";
_srv.AddHandler("/trx","tretrx",anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=65600;
 //BA.debugLineNum = 65600;BA.debugLine="srv.AddHandler(\"/verif\", \"treverif\", False)";
_srv.AddHandler("/verif","treverif",anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=65601;
 //BA.debugLineNum = 65601;BA.debugLine="srv.AddHandler(\"/up\", \"treuseratas\", False)";
_srv.AddHandler("/up","treuseratas",anywheresoftware.b4a.keywords.Common.False);
RDebugUtils.currentLine=65603;
 //BA.debugLineNum = 65603;BA.debugLine="srv.Start";
_srv.Start();
RDebugUtils.currentLine=65605;
 //BA.debugLineNum = 65605;BA.debugLine="Log(\"Server Started --> \" & ss.GetMyIP & \":\" & s";
anywheresoftware.b4a.keywords.Common.Log("Server Started --> "+_ss.GetMyIP()+":"+BA.NumberToString(_srv.getPort()));
RDebugUtils.currentLine=65606;
 //BA.debugLineNum = 65606;BA.debugLine="Log(\" \")";
anywheresoftware.b4a.keywords.Common.Log(" ");
RDebugUtils.currentLine=65607;
 //BA.debugLineNum = 65607;BA.debugLine="StartMessageLoop";
anywheresoftware.b4a.keywords.Common.StartMessageLoop(ba);
 } 
       catch (Exception e64) {
			ba.setLastException(e64);RDebugUtils.currentLine=65609;
 //BA.debugLineNum = 65609;BA.debugLine="appendSqlLog(LastException.Message)";
_appendsqllog(anywheresoftware.b4a.keywords.Common.LastException(ba).getMessage());
RDebugUtils.currentLine=65610;
 //BA.debugLineNum = 65610;BA.debugLine="Log(\"-------======= Gagal Menjalankan API DMS Tr";
anywheresoftware.b4a.keywords.Common.Log("-------======= Gagal Menjalankan API DMS Treasury =======-------");
RDebugUtils.currentLine=65611;
 //BA.debugLineNum = 65611;BA.debugLine="Log(\"-------======= Periksa Kembali Paramter ===";
anywheresoftware.b4a.keywords.Common.Log("-------======= Periksa Kembali Paramter =======-------");
 };
RDebugUtils.currentLine=65614;
 //BA.debugLineNum = 65614;BA.debugLine="End Sub";
return "";
}

public static void writeFile(String canonicalFilename, String text) 
throws IOException
{
  FileWriter fstream = new FileWriter(canonicalFilename, true);
		BufferedWriter out = new BufferedWriter(fstream);
  out.write(text); 
  out.newLine();
  out.close();
}

}