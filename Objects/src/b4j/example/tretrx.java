package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.B4AClass;
import anywheresoftware.b4a.debug.*;

public class tretrx extends B4AClass.ImplB4AClass implements BA.SubDelegator{
    public static java.util.HashMap<String, java.lang.reflect.Method> htSubs;
    private void innerInitialize(BA _ba) throws Exception {
        if (ba == null) {
            ba = new  anywheresoftware.b4a.ShellBA("b4j.example", "b4j.example.tretrx", this);
            if (htSubs == null) {
                ba.loadHtSubs(this.getClass());
                htSubs = ba.htSubs;
            }
            ba.htSubs = htSubs;
             
        }
        if (BA.isShellModeRuntimeCheck(ba))
                this.getClass().getMethod("_class_globals", b4j.example.tretrx.class).invoke(this, new Object[] {null});
        else
            ba.raiseEvent2(null, true, "class_globals", false);
    }

 
    public void  innerInitializeHelper(anywheresoftware.b4a.BA _ba) throws Exception{
        innerInitialize(_ba);
    }
    public Object callSub(String sub, Object sender, Object[] args) throws Exception {
        return BA.SubDelegator.SubNotFound;
    }
public anywheresoftware.b4a.keywords.Common __c = null;
public String _clsid = "";
public b4j.example.clsdb _todb = null;
public b4j.example.clsdbdms _todbdms = null;
public b4j.example.main _main = null;
public b4j.example.exequery _exequery = null;
public b4j.example.clsmanagement _clsmanagement = null;
public b4j.example.clsmaster _clsmaster = null;
public b4j.example.modgeneratereturn _modgeneratereturn = null;
public String  _class_globals(b4j.example.tretrx __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="tretrx";
RDebugUtils.currentLine=196608;
 //BA.debugLineNum = 196608;BA.debugLine="Sub Class_Globals";
RDebugUtils.currentLine=196609;
 //BA.debugLineNum = 196609;BA.debugLine="Private clsid As String";
_clsid = "";
RDebugUtils.currentLine=196610;
 //BA.debugLineNum = 196610;BA.debugLine="Private todb As clsDB";
_todb = new b4j.example.clsdb();
RDebugUtils.currentLine=196611;
 //BA.debugLineNum = 196611;BA.debugLine="Private todbdms As clsDBDms";
_todbdms = new b4j.example.clsdbdms();
RDebugUtils.currentLine=196612;
 //BA.debugLineNum = 196612;BA.debugLine="End Sub";
return "";
}
public String  _detail_permohonan(b4j.example.tretrx __ref,String _id,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="tretrx";
if (Debug.shouldDelegate(ba, "detail_permohonan"))
	return (String) Debug.delegate(ba, "detail_permohonan", new Object[] {_id,_method});
String _query = "";
String _data = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
RDebugUtils.currentLine=786432;
 //BA.debugLineNum = 786432;BA.debugLine="Private Sub detail_permohonan(id As String, method";
RDebugUtils.currentLine=786433;
 //BA.debugLineNum = 786433;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=786434;
 //BA.debugLineNum = 786434;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=786435;
 //BA.debugLineNum = 786435;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=786437;
 //BA.debugLineNum = 786437;BA.debugLine="query = $\" select * from v_detail_trx_mohon_bayar";
_query = (" select * from v_detail_trx_mohon_bayar where f1 = '"+__c.SmartStringFormatter("",(Object)(_id))+"' or f37 = '"+__c.SmartStringFormatter("",(Object)(_id))+"' ");
RDebugUtils.currentLine=786438;
 //BA.debugLineNum = 786438;BA.debugLine="data = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=786440;
 //BA.debugLineNum = 786440;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=786441;
 //BA.debugLineNum = 786441;BA.debugLine="End Sub";
return "";
}
public String  _doc_vendor(b4j.example.tretrx __ref,String _idmohon_bayar,String _path_url,String _name_doc,String _kode_doc,int _jml_doc,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="tretrx";
if (Debug.shouldDelegate(ba, "doc_vendor"))
	return (String) Debug.delegate(ba, "doc_vendor", new Object[] {_idmohon_bayar,_path_url,_name_doc,_kode_doc,_jml_doc,_method});
String _query = "";
String _data = "";
String _temp = "";
String[] _path = null;
String[] _name = null;
String[] _kode = null;
anywheresoftware.b4a.objects.StringUtils _en = null;
anywheresoftware.b4a.objects.collections.Map _mp = null;
int _i = 0;
RDebugUtils.currentLine=720896;
 //BA.debugLineNum = 720896;BA.debugLine="Private Sub doc_vendor(idmohon_bayar As String, pa";
RDebugUtils.currentLine=720897;
 //BA.debugLineNum = 720897;BA.debugLine="Dim query, data, temp As String";
_query = "";
_data = "";
_temp = "";
RDebugUtils.currentLine=720898;
 //BA.debugLineNum = 720898;BA.debugLine="Dim path(), name(), kode() As String";
_path = new String[(int) (0)];
java.util.Arrays.fill(_path,"");
_name = new String[(int) (0)];
java.util.Arrays.fill(_name,"");
_kode = new String[(int) (0)];
java.util.Arrays.fill(_kode,"");
RDebugUtils.currentLine=720899;
 //BA.debugLineNum = 720899;BA.debugLine="Dim en As StringUtils";
_en = new anywheresoftware.b4a.objects.StringUtils();
RDebugUtils.currentLine=720900;
 //BA.debugLineNum = 720900;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=720901;
 //BA.debugLineNum = 720901;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=720903;
 //BA.debugLineNum = 720903;BA.debugLine="File.WriteList(File.DirApp, \"path1.txt\", path)";
__c.File.WriteList(__c.File.getDirApp(),"path1.txt",anywheresoftware.b4a.keywords.Common.ArrayToList(_path));
RDebugUtils.currentLine=720904;
 //BA.debugLineNum = 720904;BA.debugLine="File.WriteList(File.DirApp, \"name2.txt\", name)";
__c.File.WriteList(__c.File.getDirApp(),"name2.txt",anywheresoftware.b4a.keywords.Common.ArrayToList(_name));
RDebugUtils.currentLine=720905;
 //BA.debugLineNum = 720905;BA.debugLine="File.WriteList(File.DirApp, \"kode3.txt\", kode)";
__c.File.WriteList(__c.File.getDirApp(),"kode3.txt",anywheresoftware.b4a.keywords.Common.ArrayToList(_kode));
RDebugUtils.currentLine=720907;
 //BA.debugLineNum = 720907;BA.debugLine="path = Regex.Split(\",\", path_url)";
_path = __c.Regex.Split(",",_path_url);
RDebugUtils.currentLine=720908;
 //BA.debugLineNum = 720908;BA.debugLine="name = Regex.Split(\",\", name_doc)";
_name = __c.Regex.Split(",",_name_doc);
RDebugUtils.currentLine=720909;
 //BA.debugLineNum = 720909;BA.debugLine="kode = Regex.Split(\",\", kode_doc)";
_kode = __c.Regex.Split(",",_kode_doc);
RDebugUtils.currentLine=720910;
 //BA.debugLineNum = 720910;BA.debugLine="File.WriteList(File.DirApp, \"path.txt\", path)";
__c.File.WriteList(__c.File.getDirApp(),"path.txt",anywheresoftware.b4a.keywords.Common.ArrayToList(_path));
RDebugUtils.currentLine=720911;
 //BA.debugLineNum = 720911;BA.debugLine="File.WriteList(File.DirApp, \"name.txt\", name)";
__c.File.WriteList(__c.File.getDirApp(),"name.txt",anywheresoftware.b4a.keywords.Common.ArrayToList(_name));
RDebugUtils.currentLine=720912;
 //BA.debugLineNum = 720912;BA.debugLine="File.WriteList(File.DirApp, \"kode.txt\", kode)";
__c.File.WriteList(__c.File.getDirApp(),"kode.txt",anywheresoftware.b4a.keywords.Common.ArrayToList(_kode));
RDebugUtils.currentLine=720914;
 //BA.debugLineNum = 720914;BA.debugLine="For i = 0 To jml_doc -1";
{
final int step15 = 1;
final int limit15 = (int) (_jml_doc-1);
_i = (int) (0) ;
for (;(step15 > 0 && _i <= limit15) || (step15 < 0 && _i >= limit15) ;_i = ((int)(0 + _i + step15))  ) {
RDebugUtils.currentLine=720915;
 //BA.debugLineNum = 720915;BA.debugLine="query = $\" select * from table(PKG_DMS_TRANSAKSI";
_query = (" select * from table(PKG_DMS_TRANSAKSI.fn_document_vendor('"+__c.SmartStringFormatter("",(Object)(_idmohon_bayar))+"','"+__c.SmartStringFormatter("",(Object)(_path[_i]))+"','"+__c.SmartStringFormatter("",(Object)(_name[_i]))+"','"+__c.SmartStringFormatter("",(Object)(_kode[_i]))+"',"+__c.SmartStringFormatter("",(Object)(1))+")) ");
RDebugUtils.currentLine=720916;
 //BA.debugLineNum = 720916;BA.debugLine="data  = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=720917;
 //BA.debugLineNum = 720917;BA.debugLine="File.WriteString(File.DirApp, i &\".txt\", data)";
__c.File.WriteString(__c.File.getDirApp(),BA.NumberToString(_i)+".txt",_data);
RDebugUtils.currentLine=720918;
 //BA.debugLineNum = 720918;BA.debugLine="File.WriteString(File.DirApp, i &\"q.txt\", data)";
__c.File.WriteString(__c.File.getDirApp(),BA.NumberToString(_i)+"q.txt",_data);
 }
};
RDebugUtils.currentLine=720920;
 //BA.debugLineNum = 720920;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=720921;
 //BA.debugLineNum = 720921;BA.debugLine="End Sub";
return "";
}
public String  _handle(b4j.example.tretrx __ref,anywheresoftware.b4j.object.JServlet.ServletRequestWrapper _req,anywheresoftware.b4j.object.JServlet.ServletResponseWrapper _resp) throws Exception{
__ref = this;
RDebugUtils.currentModule="tretrx";
if (Debug.shouldDelegate(ba, "handle"))
	return (String) Debug.delegate(ba, "handle", new Object[] {_req,_resp});
String _appid = "";
String _response = "";
String _method = "";
String _hasil = "";
int _nilaiinvoice = 0;
int _nilaivat = 0;
int _nilaiwithold = 0;
RDebugUtils.currentLine=327680;
 //BA.debugLineNum = 327680;BA.debugLine="Sub Handle(req As ServletRequest, resp As ServletR";
RDebugUtils.currentLine=327681;
 //BA.debugLineNum = 327681;BA.debugLine="Dim appid, response, method, hasil As String";
_appid = "";
_response = "";
_method = "";
_hasil = "";
RDebugUtils.currentLine=327684;
 //BA.debugLineNum = 327684;BA.debugLine="appid = req.GetParameter(\"clsakses\")";
_appid = _req.GetParameter("clsakses");
RDebugUtils.currentLine=327685;
 //BA.debugLineNum = 327685;BA.debugLine="method = req.GetParameter(\"method\")";
_method = _req.GetParameter("method");
RDebugUtils.currentLine=327686;
 //BA.debugLineNum = 327686;BA.debugLine="Log(req.GetParameter(\"key\"))";
__c.Log(_req.GetParameter("key"));
RDebugUtils.currentLine=327688;
 //BA.debugLineNum = 327688;BA.debugLine="If appid <> clsid Then";
if ((_appid).equals(__ref._clsid) == false) { 
RDebugUtils.currentLine=327689;
 //BA.debugLineNum = 327689;BA.debugLine="response = modGenerateReturn.return_json(modGene";
_response = _modgeneratereturn._return_json(_modgeneratereturn._label_rejected,_modgeneratereturn._kode_rejected_class,_method);
RDebugUtils.currentLine=327690;
 //BA.debugLineNum = 327690;BA.debugLine="resp.Write(response)";
_resp.Write(_response);
RDebugUtils.currentLine=327691;
 //BA.debugLineNum = 327691;BA.debugLine="Return";
if (true) return "";
 };
RDebugUtils.currentLine=327693;
 //BA.debugLineNum = 327693;BA.debugLine="Select method";
switch (BA.switchObjectToInt(_method,"mohonbayar","listbayar","listverifikasi","docvendor","detail_permohonan","list_doc")) {
case 0: {
RDebugUtils.currentLine=327695;
 //BA.debugLineNum = 327695;BA.debugLine="Dim nilaiinvoice As Int = req.GetParameter(\"nil";
_nilaiinvoice = (int)(Double.parseDouble(_req.GetParameter("nilaiinvoice")));
RDebugUtils.currentLine=327696;
 //BA.debugLineNum = 327696;BA.debugLine="Dim nilaivat As Int = req.GetParameter(\"nilaiva";
_nilaivat = (int)(Double.parseDouble(_req.GetParameter("nilaivat")));
RDebugUtils.currentLine=327697;
 //BA.debugLineNum = 327697;BA.debugLine="Dim nilaiwithold As Int = req.GetParameter(\"nil";
_nilaiwithold = (int)(Double.parseDouble(_req.GetParameter("nilaiwhitehold")));
RDebugUtils.currentLine=327698;
 //BA.debugLineNum = 327698;BA.debugLine="resp.Write(mohonbayar(req.GetParameter(\"bank\"),";
_resp.Write(__ref._mohonbayar(null,_req.GetParameter("bank"),_req.GetParameter("termin"),_req.GetParameter("noinvoice"),_req.GetParameter("vendor"),_req.GetParameter("proyek"),_req.GetParameter("bankgaransi"),_req.GetParameter("perihalsurat"),_req.GetParameter("nosuratkontrak"),_req.GetParameter("nojanjikontrak"),_req.GetParameter("nominalkontrak"),_req.GetParameter("tglsurat"),_req.GetParameter("norekening"),_req.GetParameter("currency"),_req.GetParameter("nokwitansi"),_req.GetParameter("tglkwitansi"),_req.GetParameter("nosertifikat"),_req.GetParameter("tglexp"),_req.GetParameter("nofaktur"),_req.GetParameter("tglfaktur"),_req.GetParameter("nobankgaransi"),_req.GetParameter("tglbankgaransi"),_req.GetParameter("swiftkode"),_req.GetParameter("cabang"),_req.GetParameter("pemilik"),_req.GetParameter("terminke"),_req.GetParameter("worknumber"),_req.GetParameter("status"),_req.GetParameter("tglinvoice"),_nilaiinvoice,_nilaivat,_nilaiwithold,_req.GetParameter("username"),_method));
 break; }
case 1: {
RDebugUtils.currentLine=327700;
 //BA.debugLineNum = 327700;BA.debugLine="resp.Write(list_pembayaran(req.GetParameter(\"no";
_resp.Write(__ref._list_pembayaran(null,_req.GetParameter("nokontrak"),_req.GetParameter("idvendor"),_req.GetParameter("username"),_method));
 break; }
case 2: {
RDebugUtils.currentLine=327702;
 //BA.debugLineNum = 327702;BA.debugLine="resp.Write(list_verifikasi(req.GetParameter(\"us";
_resp.Write(__ref._list_verifikasi(null,_req.GetParameter("username"),_method));
 break; }
case 3: {
RDebugUtils.currentLine=327704;
 //BA.debugLineNum = 327704;BA.debugLine="resp.Write(doc_vendor(req.GetParameter(\"id\"), r";
_resp.Write(__ref._doc_vendor(null,_req.GetParameter("id"),_req.GetParameter("path"),_req.GetParameter("name"),_req.GetParameter("kode"),(int)(Double.parseDouble(_req.GetParameter("jml"))),_method));
 break; }
case 4: {
RDebugUtils.currentLine=327706;
 //BA.debugLineNum = 327706;BA.debugLine="resp.Write(detail_permohonan(req.GetParameter(\"";
_resp.Write(__ref._detail_permohonan(null,_req.GetParameter("id_permohonan"),_method));
 break; }
case 5: {
RDebugUtils.currentLine=327708;
 //BA.debugLineNum = 327708;BA.debugLine="resp.Write(list_doc(req.GetParameter(\"id_permoh";
_resp.Write(__ref._list_doc(null,_req.GetParameter("id_permohonan"),_method));
 break; }
default: {
RDebugUtils.currentLine=327710;
 //BA.debugLineNum = 327710;BA.debugLine="hasil =modGenerateReturn.return_json(\"TIDAK ADA";
_hasil = _modgeneratereturn._return_json("TIDAK ADA METHOD","404",_method);
RDebugUtils.currentLine=327711;
 //BA.debugLineNum = 327711;BA.debugLine="resp.Write(hasil)";
_resp.Write(_hasil);
 break; }
}
;
RDebugUtils.currentLine=327715;
 //BA.debugLineNum = 327715;BA.debugLine="End Sub";
return "";
}
public String  _mohonbayar(b4j.example.tretrx __ref,String _p_bank,String _p_termin,String _p_invoice,String _p_vendor,String _p_proyek,String _p_bankgaransi,String _p_perihal_surat,String _p_nosurat_kontrak,String _p_noperjanjian_kontrak,String _p_nominalkontrak,String _p_tglsurat,String _p_norekening,String _p_currency,String _p_nokwitansi,String _p_tglkwitansi,String _p_nosertifikat,String _p_tglexp,String _p_nofaktur,String _p_tglfaktur,String _p_nobank_garansi,String _p_tglbank_garansi,String _p_swiftkode_rekening,String _p_cabang,String _p_pemilik,String _p_termink_ke,String _p_worknumb,String _p_status,String _tglinvoice,int _nilaiinvoice,int _nilaivat,int _nilaiwhitehold,String _username,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="tretrx";
if (Debug.shouldDelegate(ba, "mohonbayar"))
	return (String) Debug.delegate(ba, "mohonbayar", new Object[] {_p_bank,_p_termin,_p_invoice,_p_vendor,_p_proyek,_p_bankgaransi,_p_perihal_surat,_p_nosurat_kontrak,_p_noperjanjian_kontrak,_p_nominalkontrak,_p_tglsurat,_p_norekening,_p_currency,_p_nokwitansi,_p_tglkwitansi,_p_nosertifikat,_p_tglexp,_p_nofaktur,_p_tglfaktur,_p_nobank_garansi,_p_tglbank_garansi,_p_swiftkode_rekening,_p_cabang,_p_pemilik,_p_termink_ke,_p_worknumb,_p_status,_tglinvoice,_nilaiinvoice,_nilaivat,_nilaiwhitehold,_username,_method});
String _query = "";
String _data = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
RDebugUtils.currentLine=393216;
 //BA.debugLineNum = 393216;BA.debugLine="Private Sub mohonbayar(p_bank As String, p_termin";
RDebugUtils.currentLine=393217;
 //BA.debugLineNum = 393217;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=393218;
 //BA.debugLineNum = 393218;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=393219;
 //BA.debugLineNum = 393219;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=393224;
 //BA.debugLineNum = 393224;BA.debugLine="query = $\" select * from table(PKG_DMS_TRANSAKSI.";
_query = (" select * from table(PKG_DMS_TRANSAKSI.fn_savemohonbayar('"+__c.SmartStringFormatter("",(Object)(_p_bank))+"','"+__c.SmartStringFormatter("",(Object)(_p_termin))+"', '"+__c.SmartStringFormatter("",(Object)(_p_invoice))+"', '"+__c.SmartStringFormatter("",(Object)(_p_vendor))+"',\n"+"	'"+__c.SmartStringFormatter("",(Object)(_p_proyek))+"','"+__c.SmartStringFormatter("",(Object)(_p_bankgaransi))+"','"+__c.SmartStringFormatter("",(Object)(_p_perihal_surat))+"','"+__c.SmartStringFormatter("",(Object)(_p_nosurat_kontrak))+"','"+__c.SmartStringFormatter("",(Object)(_p_noperjanjian_kontrak))+"','"+__c.SmartStringFormatter("",(Object)(_p_nominalkontrak))+"','"+__c.SmartStringFormatter("",(Object)(_p_tglsurat))+"',\n"+"	'"+__c.SmartStringFormatter("",(Object)(_p_norekening))+"','"+__c.SmartStringFormatter("",(Object)(_p_currency))+"','"+__c.SmartStringFormatter("",(Object)(_p_nokwitansi))+"','"+__c.SmartStringFormatter("",(Object)(_p_tglkwitansi))+"','"+__c.SmartStringFormatter("",(Object)(_p_nosertifikat))+"','"+__c.SmartStringFormatter("",(Object)(_p_tglexp))+"',\n"+"	'"+__c.SmartStringFormatter("",(Object)(_p_nofaktur))+"','"+__c.SmartStringFormatter("",(Object)(_p_tglfaktur))+"',\n"+"	'"+__c.SmartStringFormatter("",(Object)(_p_nobank_garansi))+"','"+__c.SmartStringFormatter("",(Object)(_p_tglbank_garansi))+"','"+__c.SmartStringFormatter("",(Object)(_p_swiftkode_rekening))+"','"+__c.SmartStringFormatter("",(Object)(_p_cabang))+"','"+__c.SmartStringFormatter("",(Object)(_p_pemilik))+"',\n"+"	'"+__c.SmartStringFormatter("",(Object)(_p_termink_ke))+"','"+__c.SmartStringFormatter("",(Object)(_p_worknumb))+"','"+__c.SmartStringFormatter("",(Object)(_p_status))+"', '"+__c.SmartStringFormatter("",(Object)(_tglinvoice))+"', '"+__c.SmartStringFormatter("",(Object)(_nilaiinvoice))+"', '"+__c.SmartStringFormatter("",(Object)(_nilaivat))+"', '"+__c.SmartStringFormatter("",(Object)(_nilaiwhitehold))+"', '"+__c.SmartStringFormatter("",(Object)(_username))+"')) ");
RDebugUtils.currentLine=393230;
 //BA.debugLineNum = 393230;BA.debugLine="data  = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=393231;
 //BA.debugLineNum = 393231;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=393232;
 //BA.debugLineNum = 393232;BA.debugLine="End Sub";
return "";
}
public String  _list_pembayaran(b4j.example.tretrx __ref,String _no_surat,String _kode_vendor,String _username,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="tretrx";
if (Debug.shouldDelegate(ba, "list_pembayaran"))
	return (String) Debug.delegate(ba, "list_pembayaran", new Object[] {_no_surat,_kode_vendor,_username,_method});
String _query = "";
String _data = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
RDebugUtils.currentLine=458752;
 //BA.debugLineNum = 458752;BA.debugLine="Private Sub list_pembayaran(no_surat As String, ko";
RDebugUtils.currentLine=458753;
 //BA.debugLineNum = 458753;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=458754;
 //BA.debugLineNum = 458754;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=458755;
 //BA.debugLineNum = 458755;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=458756;
 //BA.debugLineNum = 458756;BA.debugLine="If username.Length= 0 Then username = \"-\"";
if (_username.length()==0) { 
_username = "-";};
RDebugUtils.currentLine=458758;
 //BA.debugLineNum = 458758;BA.debugLine="query = $\" select * from table(PKG_DMS_TRANSAKSI.";
_query = (" select * from table(PKG_DMS_TRANSAKSI.fn_listmohonbayar('"+__c.SmartStringFormatter("",(Object)(_no_surat))+"', '"+__c.SmartStringFormatter("",(Object)(_kode_vendor))+"', '"+__c.SmartStringFormatter("",(Object)(_username))+"')) ");
RDebugUtils.currentLine=458759;
 //BA.debugLineNum = 458759;BA.debugLine="data  = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=458760;
 //BA.debugLineNum = 458760;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=458761;
 //BA.debugLineNum = 458761;BA.debugLine="End Sub";
return "";
}
public String  _list_verifikasi(b4j.example.tretrx __ref,String _p_username,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="tretrx";
if (Debug.shouldDelegate(ba, "list_verifikasi"))
	return (String) Debug.delegate(ba, "list_verifikasi", new Object[] {_p_username,_method});
String _query = "";
String _data = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
RDebugUtils.currentLine=589824;
 //BA.debugLineNum = 589824;BA.debugLine="Private Sub list_verifikasi(p_username As String,";
RDebugUtils.currentLine=589825;
 //BA.debugLineNum = 589825;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=589826;
 //BA.debugLineNum = 589826;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=589827;
 //BA.debugLineNum = 589827;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=589829;
 //BA.debugLineNum = 589829;BA.debugLine="query = $\" select * from table(PKG_DMS_TRANSAKSI.";
_query = (" select * from table(PKG_DMS_TRANSAKSI.fn_load_verifikasi('"+__c.SmartStringFormatter("",(Object)(_p_username))+"')) ");
RDebugUtils.currentLine=589830;
 //BA.debugLineNum = 589830;BA.debugLine="data = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=589832;
 //BA.debugLineNum = 589832;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=589833;
 //BA.debugLineNum = 589833;BA.debugLine="End Sub";
return "";
}
public String  _list_doc(b4j.example.tretrx __ref,String _id,String _method) throws Exception{
__ref = this;
RDebugUtils.currentModule="tretrx";
if (Debug.shouldDelegate(ba, "list_doc"))
	return (String) Debug.delegate(ba, "list_doc", new Object[] {_id,_method});
String _query = "";
String _data = "";
anywheresoftware.b4a.objects.collections.Map _mp = null;
RDebugUtils.currentLine=524288;
 //BA.debugLineNum = 524288;BA.debugLine="Private Sub list_doc(id As String, method As Strin";
RDebugUtils.currentLine=524289;
 //BA.debugLineNum = 524289;BA.debugLine="Dim query, data As String";
_query = "";
_data = "";
RDebugUtils.currentLine=524290;
 //BA.debugLineNum = 524290;BA.debugLine="Dim mp As Map";
_mp = new anywheresoftware.b4a.objects.collections.Map();
RDebugUtils.currentLine=524291;
 //BA.debugLineNum = 524291;BA.debugLine="mp.Initialize";
_mp.Initialize();
RDebugUtils.currentLine=524293;
 //BA.debugLineNum = 524293;BA.debugLine="query = $\" select * from v_load_path_file_vendor";
_query = (" select * from v_load_path_file_vendor where f5 = '"+__c.SmartStringFormatter("",(Object)(_id))+"' or f6 = '"+__c.SmartStringFormatter("",(Object)(_id))+"' ");
RDebugUtils.currentLine=524294;
 //BA.debugLineNum = 524294;BA.debugLine="data = exequery.multi_string(query, method)";
_data = _exequery._multi_string(_query,_method);
RDebugUtils.currentLine=524296;
 //BA.debugLineNum = 524296;BA.debugLine="Return data";
if (true) return _data;
RDebugUtils.currentLine=524297;
 //BA.debugLineNum = 524297;BA.debugLine="End Sub";
return "";
}
public String  _initialize(b4j.example.tretrx __ref,anywheresoftware.b4a.BA _ba) throws Exception{
__ref = this;
innerInitialize(_ba);
RDebugUtils.currentModule="tretrx";
if (Debug.shouldDelegate(ba, "initialize"))
	return (String) Debug.delegate(ba, "initialize", new Object[] {_ba});
RDebugUtils.currentLine=262144;
 //BA.debugLineNum = 262144;BA.debugLine="Public Sub Initialize";
RDebugUtils.currentLine=262145;
 //BA.debugLineNum = 262145;BA.debugLine="clsid = \"tre_trx\"";
__ref._clsid = "tre_trx";
RDebugUtils.currentLine=262146;
 //BA.debugLineNum = 262146;BA.debugLine="todb.Initialize";
__ref._todb._initialize(null,ba);
RDebugUtils.currentLine=262147;
 //BA.debugLineNum = 262147;BA.debugLine="todbdms.Initialize";
__ref._todbdms._initialize(null,ba);
RDebugUtils.currentLine=262149;
 //BA.debugLineNum = 262149;BA.debugLine="End Sub";
return "";
}
public String  _load_data_verifikasi_uip(b4j.example.tretrx __ref) throws Exception{
__ref = this;
RDebugUtils.currentModule="tretrx";
if (Debug.shouldDelegate(ba, "load_data_verifikasi_uip"))
	return (String) Debug.delegate(ba, "load_data_verifikasi_uip", null);
RDebugUtils.currentLine=655360;
 //BA.debugLineNum = 655360;BA.debugLine="Private Sub load_data_verifikasi_uip";
RDebugUtils.currentLine=655362;
 //BA.debugLineNum = 655362;BA.debugLine="End Sub";
return "";
}
}