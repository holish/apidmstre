﻿Type=StaticCode
Version=5.82
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@

Sub Process_Globals
	Private koneksidb As clsDB
End Sub

Private Sub exe_query(query As String, method As String) As String
	Dim hasil As String
	Dim mp As Map
	Dim lst As List
	Dim js As JSONGenerator
	
	lst.Initialize
	mp.Initialize
	koneksidb.Initialize
	
	mp = koneksidb.SqlSelectMap(query, method)
	js.Initialize(mp)
	hasil = js.ToString
	Return hasil
End Sub


#Region Master Bank

public Sub create_bank(namabank As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createbank('" & namabank & "'))", method)
End Sub

Public Sub read_bank(kriteria As String, method As String) As String
'	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_readbank('" & kriteria & "'))", method)
	Return exe_query("select * from m_bank", method)
End Sub

Public Sub update_bank(id As String, nama As String, method As String) As String
	Return exe_query($" select * from table(PKG_DMS_MASTER_TRE.fn_editbank('${id}','${nama}')) "$, method)
End Sub

Public Sub delete_bank(id As String, method As String) As String
	Return exe_query($" select * from table(PKG_DMS_MASTER_TRE.fn_deletebank('${id}')) "$, method)
End Sub

#End Region


#Region Master Vendor

public Sub create_vendor(p1 As String,p2 As String,p3 As String,p4 As String,p5 As String,p6 As String, p7 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createvendor('" & p1 & "','" & p2 & "','" & p3 & "','" & p4 & "','" & p5 & "','" & p6 & "','" & p7 & "'))", method)
End Sub

Public Sub read_vendor(kriteria As String, method As String) As String
	Return exe_query("select * from m_vendor", method)
End Sub

Public Sub update_vendor(kode As String, p1 As String,p2 As String,p3 As String,p4 As String,p5 As String,p6 As String, p7 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editvendor('" & kode & "','" & p1 & "','" & p2 & "','" & p3 & "','" & p4 & "','" & p5 & "','" & p6 & "','" & p7 & "'))", method)
End Sub

Public Sub delete_vendor(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletevendor('" & kode & "'))", method)
End Sub

#End Region


#Region Master Tipe Pembayaran

public Sub create_tipebayar(p1 As String,  method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createtipebayar('" & p1 & "'))", method)
End Sub

Public Sub read_tipebayar(kriteria As String, method As String) As String
	Return exe_query("select * from m_tipe_pembayaran", method)
End Sub

Public Sub update_tipebayar(kode As String, p1 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_edittipebayar('" & kode & "','" & p1 & "'))", method)
End Sub

Public Sub delete_tipebayar(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletetipebayar('" & kode & "'))", method)
End Sub

#End Region


#Region Master TERMIN

public Sub create_termin(p1 As String, p2 As String, p3 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createtermin('" & p1 & "','" & p2 & "','" & p3 & "'))", method)
End Sub

Public Sub read_termin(kriteria As String, method As String) As String
	Return exe_query("select * from m_termin", method)
End Sub

Public Sub update_termin(kode As String, p1 As String,  p2 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_edittipebayar('" & kode & "','" & p1 & "','" & p2 & "'))", method)
End Sub

Public Sub delete_termin(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletetipebayar('" & kode & "'))", method)
End Sub

#End Region


#Region Master Sumber Dana

public Sub create_sumberdana(p1 As String,  method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createsumberdana('" & p1 & "'))", method)
End Sub

Public Sub read_sumberdana(kriteria As String, method As String) As String
	Return exe_query("select * from m_sumber_dana", method)
End Sub

Public Sub update_sumberdana(kode As String, p1 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editsumberdana('" & kode & "','" & p1 & "'))", method)
End Sub

Public Sub delete_sumberdana(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletesumberdana('" & kode & "'))", method)
End Sub

#End Region


#Region Master Jenis Proyek

public Sub create_jenisproyek(p1 As String, p2 As String,  method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createjenisproyek('" & p1 & "','" & p2 & "'))", method)
End Sub

Public Sub read_jenisproyek(kriteria As String, method As String) As String
	Return exe_query("select * from m_jenis_proyek", method)
End Sub

Public Sub update_jenisproyek(kode As String, p1 As String, p2 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editjenisproyek('" & kode & "','" & p1 & "','" & p2 & "'))", method)
End Sub

Public Sub delete_jenisproyek(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deletejenisproyek('" & kode & "'))", method)
End Sub

#End Region

#Region Master  Proyek

public Sub create_proyek(p1 As String, p2 As String, p3 As String,p4 As String,  method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_createproyek('" & p1 & "','" & p2 & "'))", method)
End Sub

Public Sub read_proyek(kriteria As String, method As String) As String
	Return exe_query("select * from m_jenis_proyek", method)
End Sub

Public Sub update_proyek(id As String, p1 As String, p2 As String,p3 As String,p4 As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_editproyek('" & id & "','" & p1 & "','" & p2 & "','" & p3 & "','" & p4 & "'))", method)
End Sub

Public Sub delete_proyek(kode As String, method As String) As String
	Return exe_query("select * from table(PKG_DMS_MASTER_TRE.fn_deleteproyek('" & kode & "'))", method)
End Sub

#End Region

