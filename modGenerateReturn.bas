﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=StaticCode
Version=5.82
@EndOfDesignText@

Sub Process_Globals
	Public label_rejected As String = "Rejected Access"
	Public kode_rejected_class As String = "501"
	Public kode_succes As String = "301"
	Public kode_gagal_koneksi_db As String = "102"
	Public kode_gagal_execute_query As String = "202"
	Public kode_gagal_parsing As String = "402"
	Public namaUser As String
End Sub



Public Sub return_json(pesan As String, kode As String, method As String) As String
	Dim ret As Map
	Dim strRet As String
	Dim js As JSONGenerator
	
	ret = CreateMap("response": kode, "method" :method, "pesan" : pesan, "data": Array())
	js.Initialize(ret)
	strRet = js.ToString
	Return strRet
End Sub
