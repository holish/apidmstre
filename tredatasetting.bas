﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=6
@EndOfDesignText@
'Handler class
Sub Class_Globals
	Private clsid As String
	Private todb As clsDB
	Private todbdms As clsDBDms
End Sub

Public Sub Initialize
	clsid = "tre_datasetting"
	todb.Initialize
	todbdms.Initialize
End Sub

Sub Handle(req As ServletRequest, resp As ServletResponse)
	Dim appid, response, method, hasil As String
	
	'------------ CEK CLASS AKSES --------'
	appid = req.GetParameter("clsakses")
	method = req.GetParameter("method")
	'------------ JIKA AKSES TIDAK SAMA -----------'
	If appid <> clsid Then
		response = modGenerateReturn.return_json(modGenerateReturn.label_rejected, modGenerateReturn.kode_rejected_class, method)
		resp.Write(response)
		Return
	End If
	Select method
		Case "get"
			resp.Write(get(req.GetParameter("key"), method))
		Case "save"
			resp.Write(save(req.GetParameter("key"), req.GetParameter("name"), req.GetParameter("value"),req.GetParameter("ket"), method))
		Case "getval"
			resp.Write(getval(req.GetParameter("key"), req.GetParameter("name"), method))
		Case Else
			hasil =modGenerateReturn.return_json("TIDAK ADA METHOD", "404", method)
			resp.Write(hasil)
	End Select
	
	
End Sub

Private Sub exe_query(query As String, method As String) As String
	Dim hasil As String
	Dim mp As Map
	Dim lst As List
	Dim js As JSONGenerator
	
	lst.Initialize
	mp.Initialize
	
	mp = todb.SqlSelectMap(query, method)
	js.Initialize(mp)
	hasil = js.ToString
	
	Return hasil
End Sub

Private Sub get(key As String, method As String) As String
	Dim query, data As String
	Dim mp As Map
	mp.Initialize
	
	query = $" select * from table(PKG_DMS_SETTING.fn_getsetting('${key}')) "$
	data  = exe_query(query, method)
	Return data
End Sub

Private Sub save(key As String,name As String,value As String, ket As String, method As String) As String
	Dim query, data As String
	Dim mp As Map
	mp.Initialize
	
	query = $" select * from data_setting where key_setting = '${key}' "$
	data  = exe_query(query, method)
	Return data
End Sub

Private Sub getval (key As String, val As String, method As String) As String
	Dim query, data As String
	Dim mp As Map
	mp.Initialize
	
	query = $" select * from table(PKG_DMS_SETTING.fn_getsettingval('${key}', '${val}')) "$
	data  = exe_query(query, method)
	Return data
End Sub

